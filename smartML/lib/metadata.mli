(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics

val for_contract :
  config:Config.t -> value_tcontract -> (string * Utils.Misc.json) list
