(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics

type loaded_scenario =
  { scenario : tscenario
  ; scenario_state : Basics.scenario_state
  ; typing_env : Typing.env }

val load_from_string :
     primitives:(module Primitives.Primitives)
  -> Config.t
  -> Yojson.Basic.t
  -> loaded_scenario
