(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Writer
open Utils
open Control
open Basics
open Scenario
open Printf

let action_of_operation
    ~id
    ~line_no
    ~title
    ~messageClass
    ~source
    ~sender
    ~chain_id
    ~time
    ~level
    ~voting_powers = function
  | Transfer {arg; destination; amount} ->
    begin
      match destination with
      | {v = Literal (Literal.Contract (Local id, entry_point, _)); _} ->
          let message = Base.Option.value entry_point ~default:"default" in
          let msg =
            let show_arg = Printer.value_to_string arg in
            let arg_shown =
              let the_max = 20 in
              if String.length show_arg > the_max
              then Base.String.prefix show_arg the_max ^ "..."
              else show_arg
            in
            Message
              { id
              ; valid = Expr.cst ~line_no (Literal.bool true)
              ; params = Expr.of_value arg
              ; line_no
              ; title =
                  sprintf
                    "Follow-up-transfer: %s (%s)%s"
                    (Printer.address_of_contract_id
                       ~html:false
                       id
                       (Some message))
                    arg_shown
                    (if title = "" then "" else sprintf " (%s)" title)
              ; messageClass
              ; sender
              ; source
              ; chain_id
              ; time
              ; amount = Expr.of_value amount
              ; message
              ; show = true
              ; level
              ; voting_powers
              ; export = false }
          in
          [msg]
      | _ -> []
    end
  | SetDelegate None -> [Set_delegate {id; line_no; baker = Expr.none ~line_no}]
  | SetDelegate (Some baker) ->
      let baker = Expr.of_value (Value.some baker) in
      [Set_delegate {id; line_no; baker}]
  | CreateContract {id; baker; contract} ->
      let baker = Expr.of_value baker in
      let op =
        New_contract
          { id
          ; line_no
          ; contract =
              map_contract_f Expr.of_value (fun x -> x) (fun x -> x) contract
          ; baker
          ; accept_unknown_types = false
          ; show = true }
      in
      [op]

let make_toc ~in_browser actions =
  let prep (step, action) =
    match action with
    | Html {tag; inner} ->
        let toc i = Some (i, step, inner) in
        ( match tag with
        | "h1" -> toc 1
        | "h2" -> toc 2
        | "h3" -> toc 3
        | "h4" -> toc 4
        | _ -> None )
    | _ -> None
  in
  let table_of_contents = List.map_some prep actions in
  let goto d d' l =
    let x = ref d in
    while !x <> d' do
      if !x < d'
      then (
        if in_browser then l := "<ul>" :: !l;
        incr x )
      else (
        if in_browser then l := "</ul>" :: !l;
        decr x )
    done
  in
  let d, table_of_contents =
    List.fold_left
      (fun (d, l) (d', id, s') ->
        let l = ref l in
        goto d d' l;
        let link =
          if in_browser
          then sprintf "<li><a href='#label%i'>%s</a>" id s'
          else sprintf "%s %s" (String.sub "\n####" 0 d') s'
        in
        (d', link :: !l))
      (1, [])
      table_of_contents
  in
  let table_of_contents = ref table_of_contents in
  goto d 1 table_of_contents;
  String.concat "" (List.rev !table_of_contents)

let run ~config ~primitives ~html ~install ~scenario output_dir =
  let {scenario_state = global_state; typing_env; scenario = {actions; flags}} =
    scenario
  in
  let config = Config.apply_flags flags config in
  let config = ref config in
  let buffer = Buffer.create 111024 in
  let noop _ = () in
  let incremental = false in
  let appendIn =
    if html
    then
      if incremental
      then SmartDom.addOutput
      else fun s -> bprintf buffer "%s\n" s
    else noop
  in
  let appendOut =
    match output_dir with
    | None -> noop
    | Some od ->
        let h = open_out (Filename.concat od "log.txt") in
        fprintf h "%s\n"
  in
  let closeOut =
    if html
    then
      if incremental
      then noop
      else
        fun () ->
        match output_dir with
        | None -> SmartDom.setOutput (Buffer.contents buffer)
        | Some od ->
            let name = "log" in
            let name, l =
              let html = Buffer.contents buffer in
              let html = wrap_html_document ~install html in
              write_html (sprintf "%s/%s" od name) html
            in
            appendOut (sprintf " => %s %d" name l)
    else (*fun () -> close_out h*) noop
  in
  Hashtbl.clear Html.simulatedContracts;
  let errors : ([ `Warning | `Error ] * smart_except list) list ref = ref [] in
  let actions = List.mapi pair actions in
  let table_of_contents = make_toc ~in_browser:false actions in
  let table_of_contents_html = make_toc ~in_browser:true actions in
  let appendError severity msg_out msg_full =
    errors := (severity, msg_full) :: !errors;
    appendOut msg_out
  in
  let handle_action ~queue ~reverted ~output_in scenario_state (step, action) =
    let with_file ~id name w x =
      match output_dir with
      | None -> ()
      | Some od ->
          let name =
            sprintf
              "%s/step_%03d_cont_%s_%s"
              od
              step
              (Printer.string_of_contract_id id)
              name
          in
          let name, l = w name x in
          appendOut (sprintf " => %s %d" name l)
    in
    match action with
    | New_contract {id; contract; accept_unknown_types; line_no; show} ->
        let with_file name = with_file ~id name in
        let contract =
          match Hashtbl.find_opt scenario_state.contracts id with
          | Some contract -> contract
          | None ->
              Interpreter.interpret_contract
                ~config:!config
                ~primitives
                ~scenario_state
                {tcontract = contract}
        in
        let substContractData = Hashtbl.create 10 in
        let mangle_env =
          Mangler.init_env
            ~substContractData
            ~reducer:
              (Interpreter.reducer ~config:!config ~primitives ~scenario_state)
            ()
        in
        let contract =
          Mangler.mangle_value_contract mangle_env typing_env contract
        in
        Solver.apply typing_env;
        let contract = Closer.close_value_contract contract in
        Hashtbl.replace scenario_state.contracts id contract;
        appendOut "Creating contract";
        let compiled_contract =
          Compiler.compile_value_tcontract ~config:!config contract
        in
        appendOut
          ( " -> "
          ^ Base.Option.value_map
              compiled_contract.storage
              ~default:"missing storage"
              ~f:Michelson.string_of_tliteral );
        if html && show
        then
          Html.full_html
            ~config:!config
            ~contract
            ~compiled_contract
            ~def:"SmartPy"
            ~onlyDefault:false
            ~id:(sprintf "%s_%d" (Printer.string_of_contract_id id) step)
            ~line_no
            ~accept_missings:accept_unknown_types
          |> Html.render
          |> appendIn;
        let contract_micheline =
          Michelson.to_micheline_tcontract compiled_contract
        in
        let storage_with_micheline =
          Option.map
            (fun x ->
              ( x
              , Michelson.(
                  To_micheline.literal (Michelson.erase_types_literal x)) ))
            compiled_contract.storage
        in
        if Option.is_some output_dir
        then begin
          begin
            match storage_with_micheline with
            | Some (storage, storage_micheline) ->
                let name = "storage" in
                with_file
                  name
                  write_mliteral
                  (Michelson.erase_types_literal storage);
                with_file name write_micheline storage_micheline
            | None -> ()
          end;
          let sizes =
            [ ("storage", Option.map snd storage_with_micheline)
            ; ("contract", Some contract_micheline) ]
          in
          let get_size (name, x) =
            try
              [ name
              ; Option.fold
                  ~none:"missing"
                  ~some:(fun x ->
                    Printf.sprintf "%i" (Micheline_encoding.micheline_size x))
                  x ]
            with
            | Failure s -> [name; s]
          in
          let name = "sizes" in
          with_file name write_csv (List.map get_size sizes);
          ( match contract.value_tcontract.storage with
          | None -> ()
          | Some storage ->
              let name = "storage" in
              with_file name write_tvalue storage );
          let name = "types" in
          with_file name write_contract_types contract;
          let write_metadata_file (name, metadata) =
            with_file ("metadata." ^ name) write_metadata metadata
          in
          List.iter
            write_metadata_file
            (Metadata.for_contract ~config:!config contract);
          begin
            match
              Michelson.has_error_tcontract
                ~accept_missings:accept_unknown_types
                compiled_contract
            with
            | [] -> ()
            | _ ->
                appendError
                  `Warning
                  "Error in generated contract"
                  [`Text "Error in generated Michelson contract"]
          end;
          ( match contract.value_tcontract.unknown_parts with
          | Some msg when not accept_unknown_types ->
              ksprintf
                (appendError `Warning)
                "Warning: unknown types or type errors: %s"
                msg
                [`Text "Error (unknown) in generated Michelson contract"]
          | _ -> () );
          let name = "contract" in
          with_file name write_contract_michelson compiled_contract;
          with_file name write_micheline contract_micheline;
          with_file name write_pretty contract;
          if html then with_file name (write_pretty_html ~install) contract;
          if !config.decompile
          then (
            let contract_michel =
              let st = Michel.Transformer.{var_counter = ref 0} in
              let c =
                Michel_decompiler.decompile_contract st compiled_contract
              in
              Michel.Transformer.smartMLify st c
            in
            with_file "pre_smartml" write_contract_michel contract_michel;
            let decompiled =
              match Michel.Typing.typecheck_precontract contract_michel with
              | Error msg -> raise (SmartExcept [`Text msg])
              | Ok c -> Decompiler.smartML_of_michel !config c
            in
            let suffix =
              {|

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
|}
            in
            with_file "decompiled" write_contract_py (decompiled, suffix) )
        end
    | Compute {id; expression; line_no} ->
        let value =
          Interpreter.interpret_expr_external
            ~config:!config
            ~primitives
            ~no_env:
              [`Text "Computing expression"; `Expr expression; `Line line_no]
            ~scenario_state
            expression
        in
        Hashtbl.replace scenario_state.variables id value
    | Simulation {id; line_no} ->
      ( match Hashtbl.find_opt scenario_state.contracts id with
      | None -> assert false
      | Some contract ->
          Html.simulation contract step ~line_no |> Html.render |> appendIn )
    | Set_delegate {id; line_no; baker} ->
        let contract =
          match Hashtbl.find_opt scenario_state.contracts id with
          | None ->
              raise
                (SmartExcept
                   [ `Text
                       (sprintf
                          "Missing contract in scenario %s"
                          (Printer.string_of_contract_id id))
                   ; `Line line_no ])
          | Some contract -> contract
        in
        let baker =
          Interpreter.interpret_expr_external
            ~config:!config
            ~primitives
            ~no_env:[`Text "Computing baker"; `Expr baker; `Line baker.line_no]
            ~scenario_state
            baker
        in
        let value_tcontract = {contract.value_tcontract with baker} in
        Hashtbl.replace scenario_state.contracts id {value_tcontract}
    | Message
        { id
        ; params
        ; line_no
        ; title
        ; messageClass
        ; sender
        ; source
        ; chain_id
        ; time
        ; amount
        ; level
        ; voting_powers
        ; message
        ; show
        ; export } ->
        let voting_powers_expr = voting_powers in
        let contract =
          match Hashtbl.find_opt scenario_state.contracts id with
          | None ->
              raise
                (SmartExcept
                   [ `Text
                       (sprintf
                          "Missing contract in scenario %s"
                          (Printer.string_of_contract_id id))
                   ; `Line line_no ])
          | Some contract -> contract
        in
        let amount =
          let pp () =
            [`Text "Computing amount"; `Expr amount; `Line amount.line_no]
          in
          Value.unMutez
            ~pp
            (Interpreter.interpret_expr_external
               ~config:!config
               ~primitives
               ~no_env:(pp ())
               ~scenario_state
               amount)
        in
        let level =
          let pp () =
            [`Text "Computing level"; `Expr level; `Line level.line_no]
          in
          Value.unInt
            ~pp
            (Interpreter.interpret_expr_external
               ~config:!config
               ~primitives
               ~no_env:(pp ())
               ~scenario_state
               level)
        in
        let voting_powers =
          let pp () =
            [ `Text "Computing voting powers"
            ; `Expr voting_powers
            ; `Line voting_powers.line_no ]
          in
          List.map
            (fun (k, v) -> (Value.unKey_hash ~pp k, Value.unInt ~pp v))
            (Value.unMap
               ~pp
               (Interpreter.interpret_expr_external
                  ~config:!config
                  ~primitives
                  ~no_env:(pp ())
                  ~scenario_state
                  voting_powers))
        in
        let time =
          let pp () = [`Text "Computing now"; `Expr time; `Line time.line_no] in
          Value.unTimestamp
            ~pp
            (Interpreter.interpret_expr_external
               ~config:!config
               ~primitives
               ~no_env:(pp ())
               ~scenario_state
               time)
        in
        let parse_chain_id (chain_id : Basics.Typed.texpr) =
          let pp () =
            [`Text "Computing chain_id"; `Expr chain_id; `Line chain_id.line_no]
          in
          Value.unChain_id
            ~pp
            (Interpreter.interpret_expr_external
               ~config:!config
               ~primitives
               ~no_env:(pp ())
               ~scenario_state
               chain_id)
        in
        let parse_address = function
          | Account x -> Literal.Real x.pkh
          | Address (address : Basics.Typed.texpr) ->
              let pp () =
                [`Text "Computing address"; `Expr address; `Line address.line_no]
              in
              Value.unAddress
                ~pp
                (Interpreter.interpret_expr_external
                   ~config:!config
                   ~primitives
                   ~no_env:(pp ())
                   ~scenario_state
                   address)
        in
        let params_result =
          Interpreter.interpret_expr_external
            ~config:!config
            ~primitives
            ~no_env:
              [`Text "Computing params"; `Expr params; `Line params.line_no]
            ~scenario_state
            params
        in
        let result =
          Contract.execMessageInner
            ~config:!config
            ~primitives
            ~scenario_state
            ~env:typing_env
            ~title
            ~execMessageClass:
              (if messageClass <> "" then " " ^ messageClass else messageClass)
            ~context:
              (Interpreter.context
                 ~contract_id:id
                 ?sender:(Base.Option.map sender ~f:parse_address)
                 ?source:(Base.Option.map source ~f:parse_address)
                 ?chain_id:(Base.Option.map chain_id ~f:parse_chain_id)
                 ~time
                 ~amount
                 ~level
                 ~voting_powers
                 ~line_no
                 ~debug:false
                 ())
            ~initContract:contract
            ~channel:message
            ~params:params_result
        in
        if export
        then begin
          let name = "params" in
          let params = Compiler.compile_value ~config:!config params_result in
          let params_micheline = Michelson.To_micheline.literal params in
          with_file ~id name write_tvalue params_result;
          with_file ~id name write_mliteral params;
          with_file ~id name write_micheline params_micheline
        end;
        Solver.apply typing_env;
        appendOut
          (sprintf
             "Executing %s(%s)..."
             message
             (Printer.texpr_to_string params));
        if show then output_in := result.html :: !output_in;
        ( match result.error with
        | None ->
            let contract =
              Base.Option.value_exn ~message:"No contract" result.contract
            in
            Hashtbl.replace scenario_state.contracts id contract;
            let storage =
              Option.cata
                "missing storage"
                (fun x ->
                  Michelson.string_of_literal
                    (Compiler.compile_value ~config:!config x))
                contract.value_tcontract.storage
            in
            appendOut (sprintf " -> %s" storage);
            let sender =
              Some (Address (Expr.cst ~line_no:None (Literal.local_address id)))
            in
            let follow_up operations =
              let update_balance = function
                | Transfer {amount} ->
                    let balance =
                      Value.sub
                        (Hashtbl.find scenario_state.contracts id)
                          .value_tcontract
                          .balance
                        amount
                    in
                    if Big_int.compare_big_int
                         (Value.unMutez
                            ~pp:(fun () -> [`Text "Computing balance"])
                            balance)
                         Big_int.zero_big_int
                       < 0
                    then
                      raise (SmartExcept [`Text "Balance < 0"; `Line line_no]);
                    Hashtbl.replace
                      scenario_state.contracts
                      id
                      {value_tcontract = {contract.value_tcontract with balance}}
                | _ -> ()
              in
              List.iter update_balance operations;
              let f op =
                action_of_operation
                  ~id
                  ~line_no
                  ~title
                  ~messageClass
                  ~source
                  ~sender
                  ~chain_id
                  ~time:(Expr.cst ~line_no (Literal.timestamp time))
                  ~level:(Expr.cst ~line_no (Literal.nat level))
                  ~voting_powers:voting_powers_expr
                  op
              in
              let l = List.map f operations |> List.concat in
              l
            in
            let todo = follow_up result.operations in
            let depth_first =
              match !config.protocol with
              | Delphi | Edo -> false
              | Florence | Proto10 -> true
            in
            queue := if depth_first then todo @ !queue else !queue @ todo
        | Some error -> reverted := Some error )
    | Exception exn ->
        appendError `Error (Printer.pp_smart_except false exn) exn
    | ScenarioError {message} ->
        appendError
          `Error
          (sprintf " !!! Python Error: %s" message)
          [`Text "Python Error"; `Text message]
    | Html {tag; inner} ->
        if inner = "[[TABLEOFCONTENTS]]"
        then begin
          appendOut "Table Of Contents";
          appendOut table_of_contents;
          appendIn (sprintf "<%s>%s</%s>" tag table_of_contents_html tag)
        end
        else begin
          ( match tag with
          | "h1" | "h2" | "h3" | "h4" ->
              appendIn (sprintf "<span id='label%i'></span>" step)
          | _ -> () );
          appendOut "Comment...";
          appendOut (sprintf " %s: %s" tag inner);
          appendIn (sprintf "<%s>%s</%s>" tag inner tag)
        end
    | Verify {condition; line_no} ->
        appendOut
          (sprintf "Verifying %s..." (Printer.texpr_to_string condition));
        let value =
          Interpreter.interpret_expr_external
            ~config:!config
            ~primitives
            ~no_env:
              [ `Text "Computing condition"
              ; `Expr condition
              ; `Line condition.line_no ]
            ~scenario_state
            condition
        in
        let result = Value.bool_of_value value in
        if result
        then appendOut " OK"
        else begin
          appendIn
            (sprintf
               "Verification Error: <br>%s<br> is false."
               (Printer.texpr_to_string condition));
          appendError
            `Error
            " KO"
            [ `Text "Verification Error"
            ; `Br
            ; `Expr condition
            ; `Br
            ; `Text "is false"
            ; `Line line_no ]
        end
    | Show {expression; html; stripStrings; compile} ->
        appendOut
          (sprintf "Computing %s..." (Printer.texpr_to_string expression));
        let value =
          Interpreter.interpret_expr_external
            ~config:!config
            ~primitives
            ~no_env:
              [ `Text "Computing expression"
              ; `Expr expression
              ; `Line expression.line_no ]
            ~scenario_state
            expression
        in
        if compile
        then begin
          let name = "expression" in
          let expression = Compiler.compile_value ~config:!config value in
          let expression_micheline =
            Michelson.To_micheline.literal expression
          in
          let with_file name w x =
            match output_dir with
            | None -> ()
            | Some od ->
                let name = sprintf "%s/step_%03d_%s" od step name in
                let name, l = w name x in
                appendOut (sprintf " => %s %d" name l)
          in
          with_file name write_tvalue value;
          with_file name write_mliteral expression;
          with_file name write_micheline expression_micheline
        end;
        let result =
          Printer.value_to_string ~options:Printer.Options.string value
        in
        let options =
          if html
          then
            if stripStrings
            then Printer.Options.htmlStripStrings
            else Printer.Options.html
          else Printer.Options.string
        in
        appendIn
          (sprintf
             "<div class='execMessage'>%s</div>"
             (Printer.value_to_string ~options value));
        appendOut (sprintf " => %s" result)
    | DynamicContract _ ->
        (* TODO: lookup contract, fail if tparameter or tstorage mismatch. *)
        ()
    | Add_flag {flag} -> config := Config.apply_flag flag !config
  in
  let options =
    match output_dir with
    | None -> Printer.Options.html
    | Some _ -> Printer.Options.string
  in
  ( try
      let check_valid reverted = function
        | Message {message; valid; line_no} ->
            let valid =
              let pp () =
                [`Text "Computing valid"; `Expr valid; `Line line_no]
              in
              Value.unBool
                ~pp
                (Interpreter.interpret_expr_external
                   ~config:!config
                   ~primitives
                   ~no_env:(pp ())
                   ~scenario_state:global_state
                   valid)
            in
            ( match (reverted, valid) with
            | None, true -> `OK
            | None, false ->
                appendError
                  `Error
                  (sprintf " -> !!! Valid but expected ERROR !!!")
                  [ `Text "Expected error in transaction but valid."
                  ; `Br
                  ; `Text message (*; `Expr params*)
                  ; `Br
                  ; `Line line_no ];
                `OK_but "Valid but expected ERROR"
            | Some error, true ->
                appendError
                  `Error
                  (sprintf
                     " -> !!! Unexpected ERROR !!! %s"
                     (Printer.error_to_string error))
                  [ `Text
                      "Unexpected error in transaction, please use \
                       .run(valid=False, ..)"
                  ; `Br
                  ; `Text (Printer.error_to_string ~options error)
                  ; `Br
                  ; `Text message (* ; `Expr params *)
                  ; `Br
                  ; `Line line_no ];
                `Error "Reverted transaction - unexpected error"
            | Some error, false ->
                appendOut
                  (sprintf
                     " -> --- Expected failure in transaction --- %s"
                     (Printer.error_to_string error));
                `Error "Reverted transaction" )
        | _ -> `OK
      in
      let global_state = ref global_state in
      List.iter
        (fun (step, x) ->
          let reverted = ref None in
          let queue = ref [] in
          let new_state = Basics.copy_scenario_state !global_state in
          let output_in = ref [] in
          handle_action ~queue ~reverted ~output_in new_state (step, x);
          while
            match !queue with
            | [] -> false
            | x :: rest ->
                queue := rest;
                handle_action ~reverted ~queue ~output_in new_state (step, x);
                true
          do
            ()
          done;
          let multiple = List.length !output_in > 1 in
          let output_in = String.concat "\n" (List.rev !output_in) in
          let pp msg =
            appendIn
              (Printf.sprintf
                 "<div style='border: double; padding: 5px; margin-bottom: \
                  10px'>%s</br>%s</div>"
                 msg
                 output_in)
          in
          match check_valid !reverted x with
          | `OK ->
              global_state := new_state;
              if multiple then pp "Multiple operations" else appendIn output_in
          | `OK_but msg ->
              global_state := new_state;
              pp msg
          | `Error msg -> pp msg)
        actions
    with
  | SmartExcept l as exn ->
      let s = Printer.exception_to_string false exn in
      appendError `Error (" (Exception) " ^ s) l
  | exn ->
      let s = Printer.exception_to_string false exn in
      appendError `Error (" (Exception) " ^ s) [`Text s] );
  closeOut ();
  List.rev !errors

let run_scenario_browser ~primitives ~scenario config =
  let scenario =
    try
      load_from_string ~primitives config (Yojson.Basic.from_string scenario)
    with
    | exn -> failwith (Printer.exception_to_string true exn)
  in
  let config = Config.default in
  let errors =
    run ~config ~primitives ~html:true ~install:"static" ~scenario None
  in
  match errors with
  | [] -> ()
  | l ->
      raise
        (SmartExcept
           [ `Text "Error in Scenario"
           ; `Br
           ; `Rec (List.concat (List.map (fun (_, l) -> [`Rec l; `Br]) l)) ])
