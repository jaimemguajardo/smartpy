(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics

val run : (line_no * typing_constraint) list -> unit

val apply : Typing.env -> unit
