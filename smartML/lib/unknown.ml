(* Copyright 2019-2021 Smart Chain Arena LLC. *)

type 'a t =
  | UnUnknown of string
  | UnValue   of 'a
  | UnRef     of 'a t ref [@equal ( == )]
[@@deriving eq, show {with_path = false}, ord]

type 'a t_ref = 'a t ref [@@deriving show {with_path = false}]

let equal_t_ref _ = ( == )

let compare_t_ref _ = Stdlib.compare

let rec getRef r =
  match !r with
  | UnRef r -> getRef r
  | _ -> r

let rec getRefOption r =
  match !r with
  | UnRef r -> getRefOption r
  | UnUnknown _ -> None
  | UnValue x -> Some x

let unknown () = ref (UnUnknown "")

let of_option = function
  | None -> unknown ()
  | Some x -> ref (UnValue x)
