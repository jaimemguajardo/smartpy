(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Utils.Control
module Literal = Literal

type vClass =
  | Storage
  | Local
  | Param
  | Iter
  | ListMap
  | MatchCons

type 'v record_f = (string * 'v) list [@@deriving show {with_path = false}]

let sort_record r =
  Base.List.sort r ~compare:(fun (lbl1, _) (lbl2, _) -> compare lbl1 lbl2)

let equal_record_f eq_val x y =
  let eq_entry (lbl1, val1) (lbl2, val2) = lbl1 = lbl2 && eq_val val1 val2 in
  Base.List.equal eq_entry (sort_record x) (sort_record y)

let compare_record_f cmp_val x y =
  let cmp_entry (lbl1, val1) (lbl2, val2) =
    match compare lbl1 lbl2 with
    | 0 -> cmp_val val1 val2
    | c -> c
  in
  Base.List.compare cmp_entry (sort_record x) (sort_record y)

type binOpInfix =
  | BNeq
  | BEq
  | BAnd
  | BOr
  | BAdd
  | BSub
  | BDiv
  | BEDiv
  | BMul  of {overloaded : bool}
  | BMod
  | BLt
  | BLe
  | BGt
  | BGe
  | BLsl
  | BLsr
  | BXor
[@@deriving show {with_path = false}, eq, ord]

type binOpPrefix =
  | BMax
  | BMin
[@@deriving show {with_path = false}, eq, ord]

type micheline =
  | Int       of string
  | String    of string
  | Bytes     of string
  | Primitive of
      { name : string
      ; annotations : string list
      ; arguments : micheline list }
  | Sequence  of micheline list
[@@deriving eq, show {with_path = false}, map, ord]

type 't inline_michelson =
  { name : string
  ; parsed : micheline
  ; typesIn : 't list
  ; typesOut : 't list }
[@@deriving eq, show {with_path = false}, map, fold, ord]

type hash_algo =
  | BLAKE2B
  | SHA256
  | SHA512
  | KECCAK
  | SHA3
[@@deriving show {with_path = false}, eq, ord]

let string_of_hash_algo = function
  | BLAKE2B -> "BLAKE2B"
  | SHA256 -> "SHA256"
  | SHA512 -> "SHA512"
  | KECCAK -> "KECCAK"
  | SHA3 -> "SHA3"

type 'command entry_point =
  { channel : string
  ; paramsType : Type.t
  ; originate : bool
  ; body : 'command }
[@@deriving show {with_path = false}, map, fold, eq, ord]

type 'command offchain_view =
  { name : string
  ; tparameter : Type.t option
  ; pure : bool
  ; body : 'command
  ; doc : string }
[@@deriving show {with_path = false}, map, fold, eq, ord]

type ('expr, 'glob, 'command) contract_f =
  { balance : 'expr
  ; storage : 'expr option
  ; baker : 'expr
  ; tstorage : Type.t
  ; tparameter : Type.t
  ; entry_points : 'command entry_point list
  ; entry_points_layout : Layout.t option
  ; unknown_parts : string option
  ; flags : Config.flag list
  ; global_variables : (string * 'glob) list
  ; metadata : (string * 'expr Misc.meta) list
  ; views : 'command offchain_view list }
[@@deriving show {with_path = false}, map, fold, eq, ord]

type ('command, 'type_) lambda_f =
  { id : int
  ; name : string
  ; tParams : 'type_
  ; tResult : 'type_
  ; body : 'command
  ; clean_stack : bool }
[@@deriving show {with_path = false}, map, fold]

let equal_lambda_f _ _ x y = x.id = y.id

let compare_lambda_f _ _ x y = compare x.id y.id

type 'type_ prim0 =
  | ECst                of 'type_ Literal.f
  | EBalance
  | ESender
  | ESource
  | EChain_id
  | ENow
  | EAmount
  | ELevel
  | ESelf
  | ESelf_address
  | ETotal_voting_power
  | ESelf_entry_point   of string
  | ELocal              of string
  | EMetaLocal          of string
  | EGlobal             of string * Literal.static_id
  | EVariant_arg        of string
  | EIter               of string
  | EMatchCons          of string
  | ESaplingEmptyState  of {memo : int}
  | EAccount_of_seed    of {seed : string}
  | EContract_balance   of Literal.contract_id
  | EContract_baker     of Literal.contract_id
  | EContract_data      of Literal.contract_id
  | EContract_typed     of Literal.contract_id
  | EScenario_var       of int * 'type_
[@@deriving eq, ord, show {with_path = false}, map, fold]

type 'type_ prim1 =
  | EAbs
  | EConcat_list
  | EContract_address
  | EProject          of int
  | EHash_key
  | EImplicit_account
  | EIsNat
  | EListElements     of bool
  | EListItems        of bool
  | EListKeys         of bool
  | EListRev
  | EListValues       of bool
  | ENeg
  | ENot
  | EPack
  | EReduce
  | ESetDelegate
  | ESign
  | ESize
  | ESum
  | EToInt
  | EUnpack           of 'type_
  | EHash             of hash_algo
  | EType_annotation  of 'type_
  | EAttr             of string
  | EVariant          of string
  | EIsVariant        of string
  | EReadTicket
  | EJoinTickets
  | EPairingCheck
  | EVotingPower
[@@deriving eq, ord, show {with_path = false}, map, fold]

type 'type_ prim2 =
  | EGetOpt
  | EBinOpInf    of binOpInfix
  | EBinOpPre    of binOpPrefix
  | EContains
  | ECallLambda
  | EApplyLambda
  | ECons
  | EAdd_seconds
  | ETicket
  | ESplitTicket
  | ETest_ticket of Literal.contract_id
[@@deriving eq, ord, show {with_path = false}, map, fold]

type 'type_ prim3 =
  | ESplit_tokens
  | ERange
  | ECheck_signature
  | EUpdate_map
  | EGet_and_update
  | EIf
[@@deriving eq, ord, show {with_path = false}, map, fold]

type ('expr, 'command, 'type_) expr_f =
  | EPrim0               of 'type_ prim0
  | EPrim1               of 'type_ prim1 * 'expr
  | EPrim2               of 'type_ prim2 * 'expr * 'expr
  | EPrim3               of 'type_ prim3 * 'expr * 'expr * 'expr
  | EOpenVariant         of string * 'expr * 'expr option
  | EItem                of
      { items : 'expr
      ; key : 'expr
      ; default_value : 'expr option
      ; missing_message : 'expr option }
  | ETuple               of 'expr list
  | ERecord              of (string * 'expr) list
  | EList                of 'expr list
  | EMap                 of bool * ('expr * 'expr) list
  | ESet                 of 'expr list
  | ESaplingVerifyUpdate of
      { state : 'expr
      ; transaction : 'expr }
  | EMichelson           of 'type_ inline_michelson * 'expr list
  | EMapFunction         of
      { f : 'expr
      ; l : 'expr }
  | ELambda              of ('command, 'type_) lambda_f
  | ELambdaParams        of
      { id : int
      ; name : string }
  | ECreate_contract     of
      { baker : 'expr
      ; contract_template : ('expr, 'expr, 'command) contract_f }
  | EContract            of
      { entry_point : string option
      ; arg_type : 'type_
      ; address : 'expr }
  | ESlice               of
      { offset : 'expr (* nat *)
      ; length : 'expr (* nat *)
      ; buffer : 'expr }
  | EMake_signature      of
      { secret_key : 'expr
      ; message : 'expr
      ; message_format : [ `Raw | `Hex ] }
  | ETransfer            of
      { arg : 'expr
      ; amount : 'expr
      ; destination : 'expr }
  | EMatch               of 'expr * (string * 'expr) list
[@@deriving eq, ord, show {with_path = false}, map, fold]

type record_field_binding =
  { var : string
  ; field : string }
[@@deriving eq, ord, show {with_path = false}]

type pattern =
  | Pattern_single of string
  | Pattern_tuple  of string list
  | Pattern_record of string * record_field_binding list
[@@deriving eq, ord, show {with_path = false}]

type ('expr, 'command, 'type_) command_f =
  | CNever         of 'expr
  | CFailwith      of 'expr
  | CVerify        of 'expr * 'expr option (* message *)
  | CIf            of 'expr * 'command * 'command
  | CMatch         of 'expr * (string * string * 'command) list
  | CMatchProduct  of 'expr * pattern * 'command
  | CModifyProduct of 'expr * pattern * 'command
  | CMatchCons     of
      { expr : 'expr
      ; id : string
      ; ok_match : 'command
      ; ko_match : 'command }
  | CDefineLocal   of string * 'expr
  | CSetVar        of 'expr * 'expr
  | CDelItem       of 'expr * 'expr
  | CUpdateSet     of 'expr * 'expr * bool
  | CBind          of string option * 'command * 'command
  | CFor           of string * 'expr * 'command
  | CWhile         of 'expr * 'command
  | CResult        of 'expr
  | CComment       of string
  | CSetType       of 'expr * 'type_
  | CSetResultType of 'command * 'type_
  | CTrace         of 'expr
[@@deriving show {with_path = false}, map, fold, eq, ord]

type line_no = int option [@@deriving eq, ord, show {with_path = false}]

let string_of_line_no = Option.cata "(no location info)" string_of_int

module Typed = struct
  type texpr =
    { e : (texpr, tcommand, Type.t) expr_f
    ; et : Type.t
    ; line_no : line_no }
  [@@deriving show {with_path = false}, eq, ord]

  and tcommand =
    { c : (texpr, tcommand, Type.t) command_f
    ; ct : Type.t
    ; line_no : line_no }
  [@@deriving show {with_path = false}, eq, ord]
end

open Typed

module Untyped = struct
  type expr =
    { e : (expr, command, Type.t) expr_f
    ; line_no : line_no }

  and command =
    { c : (expr, command, Type.t) command_f
    ; line_no : line_no }
  [@@deriving eq, ord, show {with_path = false}]
end

open Untyped

let rec equal_expr_modulo_line_nos e1 e2 =
  equal_expr_f
    equal_expr_modulo_line_nos
    equal_command_modulo_line_nos
    Type.equal
    e1.e
    e2.e

and equal_command_modulo_line_nos c1 c2 =
  equal_command_f
    equal_expr_modulo_line_nos
    equal_command_modulo_line_nos
    Type.equal
    c1.c
    c2.c

type ('e, 'c, 't) tsyntax_alg =
  { f_texpr : int option -> Type.t -> ('e, 'c, 't) expr_f -> 'e
  ; f_tcommand : int option -> Type.t -> ('e, 'c, 't) command_f -> 'c
  ; f_ttype : Type.t -> 't }

let cata_t {f_texpr; f_tcommand; f_ttype} =
  let rec ce {e; et; line_no} = f_texpr line_no et (map_expr_f ce cc f_ttype e)
  and cc {c; ct; line_no} =
    match c with
    | CBind (None, c1, c2) ->
        (* Optimized case for tail recursion. *)
        let c1 = cc c1 in
        f_tcommand line_no ct (CBind (None, c1, cc c2))
    | _ -> f_tcommand line_no ct (map_command_f ce cc f_ttype c)
  in
  (ce, cc)

let cata_texpr alg = fst (cata_t alg)

let cata_tcommand alg = snd (cata_t alg)

type ('e, 'c, 't) texpr_p = (texpr * 'e, tcommand * 'c, Type.t * 't) expr_f

type ('e, 'c, 't) tcommand_p =
  (texpr * 'e, tcommand * 'c, Type.t * 't) command_f

let para_talg ~p_texpr ~p_tcommand ~p_ttype =
  let f_texpr line_no et e =
    ({e = map_expr_f fst fst fst e; et; line_no}, p_texpr line_no et e)
  in
  let f_tcommand line_no ct c =
    ({c = map_command_f fst fst fst c; ct; line_no}, p_tcommand line_no ct c)
  in
  let f_ttype t = (t, p_ttype t) in
  {f_texpr; f_tcommand; f_ttype}

let para_texpr alg e = snd (cata_texpr alg e)

let para_tcommand alg c = snd (cata_tcommand alg c)

type ('e, 'c, 't) syntax_alg =
  { f_expr : int option -> ('e, 'c, 't) expr_f -> 'e
  ; f_command : int option -> ('e, 'c, 't) command_f -> 'c
  ; f_type : Type.t -> 't }

let cata {f_expr; f_command; f_type} =
  let rec ce {e; line_no} = f_expr line_no (map_expr_f ce cc f_type e)
  and cc {c; line_no} =
    match c with
    | CBind (None, c1, c2) ->
        (* Optimized case for tail recursion. *)
        let c1 = cc c1 in
        f_command line_no (CBind (None, c1, cc c2))
    | _ -> f_command line_no (map_command_f ce cc f_type c)
  in
  (ce, cc)

let cata_expr alg = fst (cata alg)

let cata_command alg = snd (cata alg)

(* An algebra on a monoid. *)
let monoid_alg append empty =
  { f_expr = (fun _ -> fold_expr_f append append append empty)
  ; f_command = (fun _ -> fold_command_f append append append empty)
  ; f_type = (fun _ -> empty) }

type ('e, 'c, 't) expr_p = (expr * 'e, command * 'c, Type.t * 't) expr_f

type ('e, 'c, 't) command_p = (expr * 'e, command * 'c, Type.t * 't) command_f

let para_alg ~p_expr ~p_command ~p_type =
  let f_expr line_no e =
    ({e = map_expr_f fst fst fst e; line_no}, p_expr line_no e)
  in
  let f_command line_no c =
    ({c = map_command_f fst fst fst c; line_no}, p_command line_no c)
  in
  let f_type t = (t, p_type t) in
  {f_expr; f_command; f_type}

let para_expr alg e = snd (cata_expr alg e)

let para_command alg c = snd (cata_command alg c)

(* An algebra on a monoid. *)
let monoid_talg append empty =
  { f_texpr = (fun _ _ -> fold_expr_f append append append empty)
  ; f_tcommand = (fun _ _ -> fold_command_f append append append empty)
  ; f_ttype = (fun _ -> empty) }

let size_talg = monoid_talg ( + ) 1

let size_tcommand = cata_tcommand size_talg

let size_texpr = cata_texpr size_talg

type lambda = (tcommand, Type.t) lambda_f [@@deriving show]

let equal_lambda l1 l2 = l1.id = l2.id

let compare_lambda l1 l2 = compare l1.id l2.id

type 'v operation =
  | Transfer       of
      { arg : 'v
      ; destination : 'v
      ; amount : 'v }
  | SetDelegate    of 'v option
  | CreateContract of
      { id : Literal.contract_id
      ; baker : 'v
      ; contract : ('v, texpr, tcommand) contract_f }
[@@deriving eq, ord, show, map, fold]

type 'v value_f =
  | Literal   of Literal.t
  | Record    of (string * 'v) list
  | Variant   of string * 'v
  | List      of 'v list
  | Set       of 'v list
  | Map       of ('v * 'v) list
  | Tuple     of 'v list
  | Closure   of lambda * 'v list
  | Operation of 'v operation
  | Ticket    of Literal.address * 'v * Bigint.t
[@@deriving eq, ord, show, map, fold]

type tvalue =
  { v : tvalue value_f
  ; vt : Type.t }
[@@deriving show {with_path = false}]

(** Equality (igoring [tvalue.t]). *)
let rec equal_tvalue {v = v1} {v = v2} = equal_value_f equal_tvalue v1 v2

type tmessage =
  { channel : string
  ; params : tvalue }
[@@deriving show {with_path = false}]

type contract = {contract : (expr, expr, command) contract_f}
[@@deriving show {with_path = false}]

type tcontract = {tcontract : (texpr, texpr, tcommand) contract_f}
[@@deriving show {with_path = false}]

type value_contract = {value_contract : (tvalue, expr, command) contract_f}
[@@deriving show {with_path = false}]

type value_tcontract = {value_tcontract : (tvalue, texpr, tcommand) contract_f}
[@@deriving show {with_path = false}]

let size_tcontract {entry_points} =
  List.fold_left
    (fun s (ep : _ entry_point) -> s + size_tcommand ep.body)
    0
    entry_points

type smart_except =
  [ `Expr of texpr
  | `Exprs of texpr list
  | `Expr_untyped of expr
  | `Value of tvalue
  | `Literal of Literal.t
  | `Line of line_no
  | `Text of string
  | `Type of Type.t
  | `Br
  | `Rec of smart_except list
  ]
[@@deriving show]

type typing_constraint =
  | HasAdd           of texpr * texpr * texpr
  | HasMul           of texpr * texpr * texpr * bool
  | HasSub           of texpr * texpr * texpr
  | HasDiv           of texpr * texpr * texpr
  | HasBitArithmetic of texpr * texpr * texpr
  | HasMap           of texpr * texpr * texpr
  | IsComparable     of texpr
  | HasGetItem       of texpr * texpr * Type.t
  | HasContains      of texpr * texpr * line_no
  | HasSize          of texpr
  | HasSlice         of texpr
  | AssertEqual      of Type.t * Type.t * (unit -> smart_except list)
  | IsInt            of Type.t * (unit -> smart_except list)
  | RowHasEntry      of
      { kind : [ `Record | `Variant ]
      ; t : Type.t
      ; label : string
      ; entry : Type.t }
  | SaplingVerify    of texpr * texpr
  | HasNeg           of texpr * Type.t
  | HasInt           of texpr
  | IsNotHot         of string * Type.t
[@@deriving show {with_path = false}]

module Execution = struct
  type error =
    | Exec_error             of smart_except list
    | Exec_channel_not_found of string
    | Exec_wrong_condition   of texpr * line_no * tvalue option
  [@@deriving show {with_path = false}]

  type step =
    { command : tcommand
    ; iters : (string * (tvalue * string option)) list
    ; locals : (string * tvalue) list
    ; storage : tvalue
    ; balance : Bigint.t
    ; operations : string list
    ; substeps : step list ref
    ; elements : (string * tvalue) list }
  [@@deriving show {with_path = false}]

  type 'html exec_message =
    { ok : bool
    ; contract : value_tcontract option
    ; operations : tvalue operation list
    ; error : error option
    ; html : 'html
    ; storage : tvalue
    ; steps : step list }
  [@@deriving show {with_path = false}]
end

exception SmartExcept of smart_except list

let setEqualUnknownOption ~pp r1 r2 =
  let r1 = Unknown.getRef r1 in
  let r2 = Unknown.getRef r2 in
  if r1 != r2
  then
    match (!r1, !r2) with
    | UnUnknown _, _ -> r1 := UnRef r2
    | _, UnUnknown _ -> r2 := UnRef r1
    | UnValue a, UnValue b when a = b -> if !r1 != !r2 then r1 := UnRef r2
    | _ -> raise (SmartExcept (pp ()))

type scenario_state =
  { contracts : (Literal.contract_id, value_tcontract) Hashtbl.t
  ; variables : (int, tvalue) Hashtbl.t
  ; addresses : (Literal.contract_id, string) Hashtbl.t
  ; next_dynamic_address_id : int ref }

let scenario_state () =
  { contracts = Hashtbl.create 5
  ; variables = Hashtbl.create 5
  ; addresses = Hashtbl.create 5
  ; next_dynamic_address_id = ref 0 }

let copy_scenario_state
    {contracts; variables; addresses; next_dynamic_address_id} =
  let contracts = Hashtbl.copy contracts in
  let variables = Hashtbl.copy variables in
  let addresses = Hashtbl.copy addresses in
  let next_dynamic_address_id = ref !next_dynamic_address_id in
  {contracts; variables; addresses; next_dynamic_address_id}

let get_parameter_type {tcontract} name =
  let filtered_entry_points =
    List.filter
      (fun ({channel = x} : _ entry_point) -> name = x)
      tcontract.entry_points
  in
  match filtered_entry_points with
  | [{paramsType}] -> Some paramsType
  | _ -> None

type 'a exists_in =
     exclude_create_contract:bool
  -> (texpr -> bool)
  -> (tcommand -> bool)
  -> 'a
  -> bool

let exists_talg ~exclude_create_contract f_expr f_command =
  let sub b1 (_, b2) = b1 || b2 in
  let p_texpr line_no et e =
    let holds_below =
      match e with
      | ECreate_contract _ when exclude_create_contract -> false
      | e -> fold_expr_f sub sub sub false e
    in
    f_expr {e = map_expr_f fst fst fst e; et; line_no} || holds_below
  in
  let p_tcommand line_no ct c =
    f_command {c = map_command_f fst fst fst c; ct; line_no}
    || fold_command_f sub sub sub false c
  in
  let p_ttype _ = false in
  para_talg ~p_texpr ~p_tcommand ~p_ttype

let exists_expr ~exclude_create_contract f_expr f_command =
  para_texpr (exists_talg ~exclude_create_contract f_expr f_command)

let exists_command ~exclude_create_contract f_expr f_command =
  para_tcommand (exists_talg ~exclude_create_contract f_expr f_command)

let exists_entry_point
    ~exclude_create_contract f_expr f_command ({body} : _ entry_point) =
  exists_command ~exclude_create_contract f_expr f_command body

let exists_contract
    ~exclude_create_contract f_expr f_command {entry_points; global_variables} =
  List.exists
    (exists_entry_point ~exclude_create_contract f_expr f_command)
    entry_points
  || List.exists
       (fun (_, e) -> exists_expr ~exclude_create_contract f_expr f_command e)
       global_variables

type 'address account_or_address =
  | Account of Primitives.account
  | Address of 'address
[@@deriving show {with_path = false}, map]

type ('expr, 'command) action_f =
  | New_contract    of
      { id : Literal.contract_id
      ; contract : ('expr, 'expr, 'command) contract_f
      ; line_no : line_no
      ; accept_unknown_types : bool
      ; baker : 'expr
      ; show : bool }
  | Compute         of
      { id : int
      ; expression : 'expr
      ; line_no : line_no }
  | Simulation      of
      { id : Literal.contract_id
      ; line_no : line_no }
  | Message         of
      { id : Literal.contract_id
      ; valid : 'expr
      ; params : 'expr
      ; line_no : line_no
      ; title : string
      ; messageClass : string
      ; source : 'expr account_or_address option
      ; sender : 'expr account_or_address option
      ; chain_id : 'expr option
      ; time : 'expr
      ; amount : 'expr
      ; level : 'expr
      ; voting_powers : 'expr
      ; message : string
      ; show : bool
      ; export : bool }
  | ScenarioError   of {message : string}
  | Html            of
      { tag : string
      ; inner : string
      ; line_no : line_no }
  | Verify          of
      { condition : 'expr
      ; line_no : line_no }
  | Show            of
      { expression : 'expr
      ; html : bool
      ; stripStrings : bool
      ; compile : bool
      ; line_no : line_no }
  | Exception       of smart_except list
  | Set_delegate    of
      { id : Literal.contract_id
      ; line_no : line_no
      ; baker : 'expr }
  | DynamicContract of
      { id : Literal.dynamic_id
      ; line_no : line_no
      ; tparameter : Type.t
      ; tstorage : Type.t }
  | Add_flag        of
      { flag : Config.flag
      ; line_no : line_no }
[@@deriving show {with_path = false}]

type action = (expr, command) action_f

type taction = (texpr, tcommand) action_f

type scenario_kind =
  | Test
  | Compilation

type ('expr, 'command) scenario_f =
  { shortname : string
  ; actions : ('expr, 'command) action_f list
  ; flags : Config.flag list
  ; kind : scenario_kind }

type scenario = (expr, command) scenario_f

type tscenario = (texpr, tcommand) scenario_f

let map_action_contract f = function
  | New_contract ({contract} as c) ->
      New_contract {c with contract = (f {tcontract = contract}).tcontract}
  | ( Compute _ | Simulation _ | Message _ | ScenarioError _ | Html _ | Verify _
    | Show _ | Exception _ | Set_delegate _ | DynamicContract _ | Add_flag _ )
    as c ->
      c

let erase_types_alg =
  { f_texpr = (fun line_no _ e -> {e; line_no})
  ; f_tcommand = (fun line_no _ c -> {c; line_no})
  ; f_ttype = (fun t -> t) }

let erase_types_command = cata_tcommand erase_types_alg

let erase_types_expr = cata_texpr erase_types_alg

let erase_types_contract {tcontract} =
  { contract =
      map_contract_f
        erase_types_expr
        erase_types_expr
        erase_types_command
        tcontract }

let erase_types_value_contract {value_tcontract} =
  { value_contract =
      map_contract_f id erase_types_expr erase_types_command value_tcontract }

let unerase_types_alg =
  { f_expr = (fun line_no e -> {e; et = Type.full_unknown (); line_no})
  ; f_command = (fun line_no c -> {c; ct = Type.full_unknown (); line_no})
  ; f_type = (fun t -> t) }

let unerase_types_command = cata_command unerase_types_alg

let unerase_types_expr = cata_expr unerase_types_alg

let unerase_types_contract {contract} =
  { tcontract =
      map_contract_f
        unerase_types_expr
        unerase_types_expr
        unerase_types_command
        contract }

let unerase_types_value_contract {value_contract} =
  { value_tcontract =
      map_contract_f id unerase_types_expr unerase_types_command value_contract
  }

let layout_records_f line_no et = function
  | ERecord l ->
      let layout =
        match Type.getRepr et with
        | TRecord {layout} -> Unknown.getRefOption layout
        | _ -> None
      in
      let fields =
        match layout with
        | None -> List.map (fun (x, _) -> (x, x)) l
        | Some layout ->
            let f Layout.{source; target} = (source, target) in
            List.map f (Binary_tree.to_list layout)
      in
      let e =
        ERecord
          (List.map
             (fun (n, _) ->
               let x = List.assoc n l in
               (n, x))
             fields)
      in
      {e; et; line_no}
  | e -> {e; et; line_no}

let layout_records_alg =
  { f_texpr = layout_records_f
  ; f_tcommand = (fun line_no ct c -> {c; ct; line_no})
  ; f_ttype = id }

let layout_records_expr = cata_texpr layout_records_alg

let layout_records_command = cata_tcommand layout_records_alg

let layout_records_contract {value_tcontract} =
  { value_tcontract =
      map_contract_f
        id
        layout_records_expr
        layout_records_command
        value_tcontract }

module Syntax (M : MONAD) = struct
  open M

  let rec sequence_meta =
    let open Utils.Misc in
    function
    | List xs ->
        let+ xs = map_list sequence_meta xs in
        Misc.List xs
    | Map xs ->
        let f (k, v) =
          let+ k = k
          and+ v = sequence_meta v in
          (k, v)
        in
        let+ xs = map_list f xs in
        Misc.Map xs
    | Other x ->
        let+ x = x in
        Other x
    | Offchain_view _ as m -> return m

  let sequence_entry_point (x : _ entry_point) =
    let+ body = x.body in
    {x with body}

  let sequence_offchain_view (x : _ offchain_view) =
    let+ body = x.body in
    {x with body}

  let sequence_contract_f c =
    let* balance = c.balance in
    let* storage = sequence_option c.storage in
    let* baker = c.baker in
    let* entry_points = map_list sequence_entry_point c.entry_points in
    let* global_variables = map_list sequence_snd c.global_variables in
    let* metadata =
      map_list
        (fun (x, y) ->
          let+ y = sequence_meta y in
          (x, y))
        c.metadata
    in
    let* views = map_list sequence_offchain_view c.views in
    return
      { c with
        balance
      ; storage
      ; baker
      ; entry_points
      ; global_variables
      ; metadata
      ; views }

  let sequence_command_f = function
    | CNever e -> (fun e -> CNever e) <$> e
    | CFailwith e -> (fun e -> CFailwith e) <$> e
    | CVerify (e, m) -> (fun e m -> CVerify (e, m)) <$> e <*> sequence_option m
    | CIf (e, c1, c2) -> (fun e c1 c2 -> CIf (e, c1, c2)) <$> e <*> c1 <*> c2
    | CMatch (scrutinee, cases) ->
        let* scrutinee = scrutinee in
        let* cases =
          flip map_list cases (fun (c, a, b) ->
              let* b = b in
              return (c, a, b))
        in
        return (CMatch (scrutinee, cases))
    | CMatchCons {expr; id; ok_match; ko_match} ->
        (fun expr ok_match ko_match ->
          CMatchCons {expr; id; ok_match; ko_match})
        <$> expr
        <*> ok_match
        <*> ko_match
    | CMatchProduct (e, p, c) ->
        let+ e = e
        and+ c = c in
        CMatchProduct (e, p, c)
    | CModifyProduct (e, p, c) ->
        let+ e = e
        and+ c = c in
        CModifyProduct (e, p, c)
    | CDefineLocal (x, e) -> (fun e -> CDefineLocal (x, e)) <$> e
    | CSetVar (e1, e2) -> (fun e1 e2 -> CSetVar (e1, e2)) <$> e1 <*> e2
    | CDelItem (e1, e2) -> (fun e1 e2 -> CDelItem (e1, e2)) <$> e1 <*> e2
    | CUpdateSet (e1, e2, b) ->
        (fun e1 e2 -> CUpdateSet (e1, e2, b)) <$> e1 <*> e2
    | CBind (x, c1, c2) -> (fun c1 c2 -> CBind (x, c1, c2)) <$> c1 <*> c2
    | CFor (x, e, c) -> (fun e c -> CFor (x, e, c)) <$> e <*> c
    | CWhile (e, c) -> (fun e c -> CWhile (e, c)) <$> e <*> c
    | CResult e -> (fun e -> CResult e) <$> e
    | CComment _ as e -> return e
    | CTrace e -> (fun e -> CTrace e) <$> e
    | CSetType (e, t) -> (fun e -> CSetType (e, t)) <$> e
    | CSetResultType (c, t) -> (fun c -> CSetResultType (c, t)) <$> c

  let sequence_expr_f = function
    | EPrim0 _ as e -> return e
    | EPrim1 (p, e) -> (fun e -> EPrim1 (p, e)) <$> e
    | EPrim2 (p, e1, e2) -> (fun e1 e2 -> EPrim2 (p, e1, e2)) <$> e1 <*> e2
    | EPrim3 (p, e1, e2, e3) ->
        (fun e1 e2 e3 -> EPrim3 (p, e1, e2, e3)) <$> e1 <*> e2 <*> e3
    | EOpenVariant (v, e1, e2) ->
        (fun e1 e2 -> EOpenVariant (v, e1, e2)) <$> e1 <*> sequence_option e2
    | EItem {items; key; default_value; missing_message} ->
        (fun items key default_value missing_message ->
          EItem {items; key; default_value; missing_message})
        <$> items
        <*> key
        <*> sequence_option default_value
        <*> sequence_option missing_message
    | ETuple es -> (fun es -> ETuple es) <$> sequence_list es
    | ERecord es -> (fun es -> ERecord es) <$> map_list sequence_snd es
    | EList es -> (fun es -> EList es) <$> sequence_list es
    | EMap (big, es) -> (fun es -> EMap (big, es)) <$> map_list sequence_pair es
    | ESet es -> (fun es -> ESet es) <$> sequence_list es
    | ESaplingVerifyUpdate {state; transaction} ->
        (fun state transaction -> ESaplingVerifyUpdate {state; transaction})
        <$> state
        <*> transaction
    | EMichelson (instrs, es) ->
        (fun es -> EMichelson (instrs, es)) <$> sequence_list es
    | EMapFunction {f; l} -> (fun f l -> EMapFunction {f; l}) <$> f <*> l
    | ELambda {id; name; tParams; tResult; body; clean_stack} ->
        (fun body -> ELambda {id; name; tParams; tResult; body; clean_stack})
        <$> body
    | ELambdaParams _ as e -> return e
    | ECreate_contract {baker; contract_template} ->
        (fun baker contract_template ->
          ECreate_contract {baker; contract_template})
        <$> baker
        <*> sequence_contract_f contract_template
    | EContract {entry_point; arg_type; address} ->
        (fun address -> EContract {entry_point; arg_type; address}) <$> address
    | ESlice {offset; length; buffer} ->
        (fun offset length buffer -> ESlice {offset; length; buffer})
        <$> offset
        <*> length
        <*> buffer
    | EMake_signature {secret_key; message; message_format} ->
        (fun secret_key message ->
          EMake_signature {secret_key; message; message_format})
        <$> secret_key
        <*> message
    | ETransfer {arg; amount; destination} ->
        (fun arg amount destination -> ETransfer {arg; amount; destination})
        <$> arg
        <*> amount
        <*> destination
    | EMatch (scrutinee, cases) ->
        (fun scrutinee cases -> EMatch (scrutinee, cases))
        <$> scrutinee
        <*> map_list sequence_snd cases

  let mapM_action_contract f = function
    | New_contract ({contract} as c) ->
        let* y = f {tcontract = contract} in
        return (New_contract {c with contract = y.tcontract})
    | ( Compute _ | Simulation _ | Message _ | ScenarioError _ | Html _
      | Verify _ | Show _ | Exception _ | Set_delegate _ | DynamicContract _
      | Add_flag _ ) as c ->
        return c

  type ('e, 'c, 't) malg =
    { fm_expr : int option -> ('e, 'c, 't) expr_f -> 'e t
    ; fm_command : int option -> ('e, 'c, 't) command_f -> 'c t
    ; fm_type : Type.t -> 't }

  let cata {fm_expr; fm_command; fm_type} =
    let rec ce {e; line_no} =
      let* e = sequence_expr_f (map_expr_f ce cc fm_type e) in
      fm_expr line_no e
    and cc {c; line_no} =
      let* c = sequence_command_f (map_command_f ce cc fm_type c) in
      fm_command line_no c
    in
    (ce, cc)

  let cataM_expr alg = fst (cata alg)

  let cataM_command alg = snd (cata alg)
end
