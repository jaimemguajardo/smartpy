(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Value

type inputGuiResult =
  { gui : string
  ; get : bool -> t }

let textInputGui path ?default nextId initialType cont =
  let id = nextId () in
  let gui = Printf.sprintf "<input type='text' id='%s'>" id in
  let get _withZeros =
    let s = SmartDom.getText id in
    try
      match (s, default) with
      | "", Some x -> cont x
      | x, _ -> cont x
    with
    | exn ->
        let error =
          match exn with
          | Failure error -> Printf.sprintf " (%s)" error
          | _ -> ""
        in
        failwith
          (Printf.sprintf
             "Cannot convert '%s' into %s for path %s%s"
             s
             initialType
             (String.concat "." (List.rev path))
             error)
  in
  {gui; get}

let error s = string s

let parseIntOrNat isNat s =
  let i = Big_int.big_int_of_string s in
  match Typing.intType isNat with
  | `Nat ->
      if Big_int.compare_big_int i Big_int.zero_big_int < 0
      then failwith (Printf.sprintf "%s is not a nat" s);
      nat i
  | `Unknown -> intOrNat (Type.intOrNat ()) i
  | `Int -> int i
  | `Error -> assert false

let enum_list t =
  Type.variant_default_layout
    [("Zero", Type.unit); ("One", t); ("Two", Type.pair t t)]

let fields_of_layout layout row =
  match Unknown.getRefOption layout with
  | None -> List.map (fun (x, _) -> (x, x)) row
  | Some layout ->
      let leaf Layout.{source; target} = [(source, target)] in
      Binary_tree.cata leaf ( @ ) layout

let rec inputGuiR path nextId initialType =
  match Type.getRepr initialType with
  | TBool ->
      let id = nextId () in
      let gui = Printf.sprintf "<input type='checkbox' id='%s'>" id in
      let get _ = bool (SmartDom.isChecked id) in
      {gui; get}
  | TInt {isNat} -> textInputGui path nextId "int" (parseIntOrNat isNat)
  | TString -> textInputGui path nextId "string" string
  | TBytes ->
      textInputGui path nextId "bytes" (fun s -> bytes (Misc.Hex.unhex s))
  | TToken ->
      let {gui; get} =
        inputGuiR
          path
          nextId
          (Type.variant_default_layout
             [("Tez", Type.nat ()); ("Mutez", Type.nat ())])
      in
      let get context =
        match (get context).v with
        | Variant ("Tez", i) ->
            Value.mutez
              (Big_int.mult_big_int
                 (Bigint.of_int 1000000)
                 (Value.unInt ~pp:(fun () -> []) i))
        | Variant ("Mutez", i) -> Value.mutez (Value.unInt ~pp:(fun () -> []) i)
        | _ -> assert false
      in
      {gui; get}
      (* textInputGui path nextId "token" (fun i ->
       *     mutez (Big_int.big_int_of_string i)) *)
  | TKeyHash -> textInputGui path nextId "key_hash" Value.key_hash
  | TBakerHash -> textInputGui path nextId "baker_hash" Value.baker_hash
  | TTimestamp ->
      textInputGui path nextId "timestamp" (fun i ->
          timestamp (Big_int.big_int_of_string i))
  | TChainId ->
      textInputGui path nextId "chainid" (fun s -> chain_id (Misc.Hex.unhex s))
  | TSecretKey -> textInputGui path nextId "secret_key" secret_key
  | TAddress ->
      textInputGui path nextId "address" (fun s ->
          if not
               ( Base.String.is_prefix ~prefix:"tz" s
               || Base.String.is_prefix ~prefix:"KT" s )
          then failwith (Printf.sprintf "Badly formed address '%s'" s);
          address s)
  | TContract t ->
      textInputGui path nextId "contract" (fun s ->
          if not (Base.String.is_prefix ~prefix:"KT" s)
          then failwith (Printf.sprintf "Badly formed contract '%s'" s);
          contract s t)
  | TKey -> textInputGui path nextId "key" key
  | TSignature -> textInputGui path nextId "signature" signature
  | TVariant {layout; row} ->
      let monoSelectionId = nextId () in
      let putRadio i (name, target) =
        let t = List.assoc name row in
        let input = inputGuiR (name :: path) nextId t in
        let id = nextId () in
        let gui =
          Printf.sprintf
            "<div id='%s' class='%s'>%s</div>"
            id
            (if i = 0 then "" else "hidden")
            input.gui
        in
        let get x = variant name (input.get x) initialType in
        (target, id, {gui; get})
      in
      let fields = fields_of_layout layout row in
      let radios = List.mapi putRadio fields in
      let gui =
        Printf.sprintf
          "<div class='subtype'><select class='selection' id='%s' \
           onchange=\"setSelectVisibility('%s',%s)\">%s</select>%s</div>"
          monoSelectionId
          monoSelectionId
          (String.concat
             ","
             (List.map
                (fun (name, id, _) -> Printf.sprintf "'%s','%s'" name id)
                radios))
          (String.concat
             ""
             (List.map
                (fun (name, _, _) ->
                  Printf.sprintf
                    "<option value='%s'>%s</option>"
                    name
                    (String.capitalize_ascii name))
                radios))
          (String.concat "" (List.map (fun (_, _, input) -> input.gui) radios))
      in
      let get context =
        let selected = SmartDom.getText monoSelectionId in
        match
          List.find_opt (fun (name, _id, _input) -> name = selected) radios
        with
        | Some (_, _, input) -> input.get context
        | None ->
            error
              ( "Missing constructor "
              ^ selected
              ^ " in "
              ^ Printer.type_to_string initialType )
      in
      {gui; get}
  | TRecord {layout; row} ->
      let subs =
        List.map
          (fun (name, t) -> (name, inputGuiR (name :: path) nextId t))
          row
      in
      let gui =
        let fields = fields_of_layout layout row in
        Printer.html_of_record_list
          ( List.map snd fields
          , [List.map (fun (source, _) -> (List.assoc source subs).gui) fields]
          )
          (fun x -> x)
      in
      let get context =
        record
          ~layout
          (List.map (fun (name, input) -> (name, input.get context)) subs)
      in
      {gui; get}
  | TUnit -> {gui = ""; get = (fun _ -> unit)}
  | TTuple ts ->
      let subs =
        List.mapi
          (fun i t ->
            let name = string_of_int i in
            (name, inputGuiR (name :: path) nextId t))
          ts
      in
      let gui =
        Printer.html_of_record_list
          ([], [List.map (fun (_, input) -> input.gui) subs])
          (fun x -> x)
      in
      let get context =
        Value.tuple (List.map (fun (_name, input) -> input.get context) subs)
      in
      {gui; get}
  | TUnknown _ ->
      let notImplemented =
        Printf.sprintf
          "InputGui of partial types (%s) not implemented"
          (Printer.type_to_string initialType)
      in
      {gui = notImplemented; get = (fun _ -> error notImplemented)}
  | TLambda _ -> textInputGui path nextId "lambda" string
  | TList t -> input_list Value.list t path nextId
  | TSet {telement} ->
      input_list (fun l telement -> Value.set l ~telement) telement path nextId
  | TMap {big; tkey; tvalue} ->
      input_list
        (fun l _ ->
          Value.map
            (List.map
               (function
                 | ({v = Tuple [x; y]} : t) -> (x, y)
                 | _ -> assert false)
               l)
            ~big
            ~tkey
            ~tvalue)
        (Type.pair tkey tvalue)
        path
        nextId
  | TOperation ->
      let gui = "Type operation not handled" in
      {gui; get = (fun _ -> error gui)}
  | TSaplingState _ ->
      let gui = "Type sapling_state not handled" in
      {gui; get = (fun _ -> error gui)}
  | TSaplingTransaction _ ->
      let gui = "Type sapling_transaction not handled" in
      {gui; get = (fun _ -> error gui)}
  | TNever ->
      let gui = "Type never has no value" in
      {gui; get = (fun _ -> error gui)}
  | TTicket _ ->
      let gui = "Type ticket cannot be forged" in
      {gui; get = (fun _ -> error gui)}
  | TBls12_381_g1 ->
      textInputGui path nextId "bls12_381_g1" (fun s ->
          bytes (Misc.Hex.unhex s))
  | TBls12_381_g2 ->
      textInputGui path nextId "bls12_381_g2" (fun s ->
          bytes (Misc.Hex.unhex s))
  | TBls12_381_fr ->
      textInputGui path nextId "bls12_381_fr" (fun s ->
          bytes (Misc.Hex.unhex s))

and input_list f t path nextId =
  let input = inputGuiR ("list" :: path) nextId (enum_list t) in
  let get context =
    match (input.get context).v with
    | Variant ("Zero", _) -> f [] t
    | Variant ("One", x) -> f [x] t
    | Variant ("Two", {v = Tuple [x; y]}) -> f [x; y] t
    | _ -> assert false
  in
  {input with get}

let inputGuiR ?(path = []) ~nextId initialType =
  inputGuiR path nextId initialType
