(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Control
open Basics
open Typed
open Untyped
open Printf

module Options = struct
  type t =
    { html : bool
    ; stripStrings : bool
    ; stripped : bool
    ; types : bool
    ; analysis : bool }

  let string =
    { html = false
    ; stripStrings = false
    ; stripped = false
    ; types = false
    ; analysis = false }

  let html = {string with html = true}

  let htmlStripStrings = {html with stripStrings = true}

  let types = {html with types = true}

  let analysis = {types with analysis = true}
end

let rec layout_to_string = function
  | Binary_tree.Leaf Layout.{source; target} ->
      if source = target
      then sprintf "%S" source
      else sprintf "\"%s as %s\"" source target
  | Binary_tree.Node (l1, l2) ->
      sprintf "(%s, %s)" (layout_to_string l1) (layout_to_string l2)

let layout_to_string layout =
  match Unknown.getRefOption layout with
  | None -> ""
  | Some l -> sprintf ".layout(%s)" (layout_to_string l)

let unknown_memo memo =
  Option.value
    (Option.map string_of_int (Unknown.getRefOption memo))
    ~default:""

let rec type_to_string ?toplevel ?(options = Options.string) ?(indent = "") t =
  let open Type in
  let shiftIdent = if options.html then "&nbsp;&nbsp;" else "  " in
  match getRepr t with
  | TBool -> "sp.TBool"
  | TString -> "sp.TString"
  | TTimestamp -> "sp.TTimestamp"
  | TBytes -> "sp.TBytes"
  | TInt {isNat} ->
    ( match Typing.intType isNat with
    | `Unknown -> "sp.TIntOrNat"
    | `Nat -> "sp.TNat"
    | `Int -> "sp.TInt" )
  | TRecord {row; layout} ->
      if options.html
      then
        sprintf
          "%s%s<span class='record'>{</span>%s<br>%s<span \
           class='record'>}%s</span>"
          (if toplevel = Some () then "" else "<br>")
          indent
          (String.concat
             ""
             (List.map
                (fun (s, t) ->
                  sprintf
                    "<br>%s<span class='record'>%s</span>: %s;"
                    (indent ^ shiftIdent)
                    s
                    (type_to_string
                       ~options
                       ~indent:(indent ^ shiftIdent ^ shiftIdent)
                       t))
                row))
          indent
          (layout_to_string layout)
      else
        sprintf
          "sp.TRecord(%s)%s"
          (pp_row ~options row)
          (layout_to_string layout)
  | TVariant {row; layout} ->
    ( match row with
    | [("None", _unit); ("Some", t)] ->
        sprintf
          "sp.TOption(%s)"
          (type_to_string ~options ~indent:(indent ^ shiftIdent ^ shiftIdent) t)
    | _ ->
        if options.html
        then
          let ppVariant (s, t) =
            let t =
              match getRepr t with
              | TUnit -> ""
              | _ ->
                  sprintf
                    " %s"
                    (type_to_string ~options ~indent:(indent ^ shiftIdent) t)
            in
            sprintf "%s<span class='variant'>| %s</span>%s" indent s t
          in
          (if toplevel = Some () then "" else "<br>")
          ^ String.concat "<br>" (List.map ppVariant row)
        else
          sprintf
            "sp.TVariant(%s)%s"
            (pp_row ~options row)
            (layout_to_string layout) )
  | TSet {telement} ->
      sprintf
        "sp.TSet(%s)"
        (type_to_string ~options ~indent:(indent ^ shiftIdent) telement)
  | TMap {big; tkey; tvalue} ->
      let name =
        match Unknown.getRefOption big with
        | None -> "sp.TMap??"
        | Some true -> "sp.TBigMap"
        | Some false -> "sp.TMap"
      in
      sprintf
        "%s(%s, %s)"
        name
        (type_to_string ~options ~indent:(indent ^ shiftIdent) tkey)
        (type_to_string ~options ~indent:(indent ^ shiftIdent) tvalue)
  | TToken -> "sp.TMutez"
  | TUnit -> "sp.TUnit"
  | TAddress -> "sp.TAddress"
  | TKeyHash -> "sp.TKeyHash"
  | TBakerHash -> "sp.TBakerHash"
  | TKey -> "sp.TKey"
  | TSecretKey -> "sp.TSecretKey"
  | TChainId -> "sp.TChainId"
  | TSignature -> "sp.TSignature"
  | TContract t ->
      sprintf
        "sp.TContract(%s)"
        (type_to_string ~options ~indent:(indent ^ shiftIdent) t)
  | TUnknown {contents = UExact t} -> type_to_string t
  | TUnknown {contents = UUnknown _} ->
      if options.html
      then "<span class='partialType'>sp.TUnknown()</span>"
      else sprintf "sp.TUnknown()"
  | TUnknown {contents = URecord l} ->
      sprintf "TRecord++(%s)" (pp_row ~options l)
  | TUnknown {contents = UTuple l} ->
      sprintf
        "TTuple++(%s)"
        (String.concat
           ", "
           (List.map
              (fun (i, t) -> sprintf "#%d = %s" i (type_to_string ~options t))
              l))
  | TUnknown {contents = UVariant l} ->
      sprintf
        "TVariant++(%s)"
        (String.concat
           " | "
           (List.map
              (fun (s, t) -> sprintf "%s %s" s (type_to_string ~options t))
              l))
  | TTuple [t1; t2] ->
      sprintf
        "sp.TPair(%s, %s)"
        (type_to_string ~options t1)
        (type_to_string ~options t2)
  | TTuple ts ->
      let ts = List.map type_to_string ts in
      sprintf "sp.TTuple(%s)" (String.concat ", " ts)
  | TList t ->
      sprintf
        "sp.TList(%s)"
        (type_to_string ~options ~indent:(indent ^ shiftIdent) t)
  | TTicket t ->
      sprintf
        "sp.TTicket(%s)"
        (type_to_string ~options ~indent:(indent ^ shiftIdent) t)
  | TLambda (t1, t2) ->
      sprintf
        "sp.TLambda(%s, %s)"
        (type_to_string ~options ~indent:(indent ^ shiftIdent) t1)
        (type_to_string ~options ~indent:(indent ^ shiftIdent) t2)
  | TOperation -> "sp.TOperation"
  | TSaplingState {memo} -> sprintf "sp.TSaplingState(%s)" (unknown_memo memo)
  | TSaplingTransaction {memo} ->
      sprintf "sp.TSaplingTransaction(%s)" (unknown_memo memo)
  | TNever -> "sp.TNever"
  | TBls12_381_g1 -> "sp.TBls12_381_g1"
  | TBls12_381_g2 -> "sp.TBls12_381_g2"
  | TBls12_381_fr -> "sp.TBls12_381_fr"

and pp_row ?(options = Options.string) row =
  String.concat
    ", "
    (List.map
       (fun (s, t) -> sprintf "%s = %s" s (type_to_string ~options t))
       row)

let type_to_string ?toplevel ?(options = Options.string) t =
  if options.html
  then
    sprintf "<span class='type'>%s</span>" (type_to_string ?toplevel ~options t)
  else type_to_string ?toplevel ~options t

let ppAmount html amount =
  let oneMillion = Bigint.of_int 1000000 in
  let quotient, modulo = Big_int.quomod_big_int amount oneMillion in
  if html
  then
    let mutez = Big_int.string_of_big_int amount in
    let mutez =
      if String.length mutez < 7
      then String.sub "0000000" 0 (7 - String.length mutez) ^ mutez
      else mutez
    in
    sprintf
      "%s.%s<img height=20 width=20 src='static/img/tezos.svg' alt='tz'/>"
      (String.sub mutez 0 (String.length mutez - 6))
      (String.sub mutez (String.length mutez - 6) 6)
  else if Big_int.compare_big_int modulo Big_int.zero_big_int = 0
  then sprintf "sp.tez(%s)" (Big_int.string_of_big_int quotient)
  else sprintf "sp.mutez(%s)" (Big_int.string_of_big_int amount)

let string_of_contract_id = function
  | Literal.C_static {static_id} -> sprintf "%i" static_id
  | Literal.C_dynamic {dynamic_id} -> sprintf "Dyn_%i" dynamic_id

let address_of_contract_id ~html contract_id entry_point =
  let address =
    match contract_id with
    | Literal.C_static {static_id} ->
        Bs58.address_of_contract_id ~static:true static_id entry_point
    | Literal.C_dynamic {dynamic_id} ->
        Bs58.address_of_contract_id ~static:false dynamic_id entry_point
  in
  if html then sprintf "<span class='address'>%s</span>" address else address

let literal_to_string ~html ?strip_strings =
  let to_string (l : Literal.t) =
    let open Format in
    let entry_point_opt ppf = function
      | None -> fprintf ppf ""
      | Some ep -> fprintf ppf "%%%s" ep
    in
    match l with
    | Unit -> "sp.unit"
    | Bool x -> String.capitalize_ascii (string_of_bool x)
    | Int {i} -> Big_int.string_of_big_int i
    | String s when strip_strings = Some () -> s
    | String s -> sprintf "'%s'" s
    | Bytes s -> sprintf "sp.bytes('0x%s')" (Misc.Hex.hexcape s)
    | Chain_id s -> sprintf "sp.chain_id('0x%s')" (Misc.Hex.hexcape s)
    | Mutez i -> ppAmount html i
    | Address (Real s, epo) ->
        asprintf "sp.address('%s%a')" s entry_point_opt epo
    | Address (Local l, epo) ->
        asprintf "sp.address('%s')" (address_of_contract_id ~html l epo)
    | Contract (Real s, epo, t) ->
        asprintf
          "sp.contract(%s, sp.address('%s%a')).open_some()"
          (type_to_string t)
          s
          entry_point_opt
          epo
    | Contract (Local l, epo, t) ->
        asprintf
          "sp.contract(%s, sp.address('%s')).open_some()"
          (type_to_string t)
          (address_of_contract_id ~html l epo)
    | Timestamp i -> sprintf "sp.timestamp(%s)" (Big_int.string_of_big_int i)
    | Key s -> sprintf "sp.key('%s')" s
    | Secret_key s -> sprintf "sp.secret_key('%s')" s
    | Key_hash s -> sprintf "sp.key_hash('%s')" s
    | Baker_hash s -> sprintf "sp.baker_hash('%s')" s
    | Signature s -> sprintf "sp.signature('%s')" s
    | Sapling_test_state _ -> "sapling test state"
    | Sapling_test_transaction _ -> "sapling test transaction"
    | Bls12_381_g1 s -> sprintf "sp.bls12_381_g1('0x%s')" (Misc.Hex.hexcape s)
    | Bls12_381_g2 s -> sprintf "sp.bls12_381_g2('0x%s')" (Misc.Hex.hexcape s)
    | Bls12_381_fr s -> sprintf "sp.bls12_381_fr('0x%s')" (Misc.Hex.hexcape s)
  in
  to_string

let unrec = function
  | {v = Record l} -> List.map snd l
  | x -> [x]

let is_range_keys l =
  let rec aux n = function
    | [] -> true
    | ({v = Literal (Int {i})}, _) :: l ->
        Big_int.eq_big_int (Bigint.of_int n) i && aux (n + 1) l
    | (_, _) :: _ -> false
  in
  aux 0 l

let is_vector v =
  match v.v with
  | Map l -> if is_range_keys l then Some (List.map snd l) else None
  | _ -> None

let is_matrix l =
  match is_vector l with
  | None -> None
  | Some vect ->
      let new_line lines a =
        match (lines, is_vector a) with
        | None, _ | _, None -> None
        | Some lines, Some v -> Some (v :: lines)
      in
      ( match List.fold_left new_line (Some []) vect with
      | Some lines
        when 2 <= List.length lines
             && List.exists (fun line -> 2 <= List.length line) lines ->
          Some ([], List.rev lines)
      | _ -> None )

let html_of_record_list (columns, data) f =
  let ppRow row =
    sprintf
      "<tr>%s</tr>"
      (String.concat
         ""
         (List.map (fun s -> sprintf "<td class='data'>%s</td>" (f s)) row))
  in
  sprintf
    "<table class='recordList'><tr>%s</tr>%s</table>"
    (String.concat
       "\n"
       (List.map
          (fun s ->
            sprintf "<td class='dataColumn'>%s</td>" (String.capitalize_ascii s))
          columns))
    (String.concat "\n" (List.map ppRow data))

let is_record_list = function
  | {v = Record _} :: _ -> true
  | _ -> false

let rec value_to_string
    ?(deep = false) ?(noEmptyList = false) ?(options = Options.string) v =
  match v.v with
  | Literal (Bool x) -> String.capitalize_ascii (string_of_bool x)
  | Literal (Int {i}) ->
      let pp = Big_int.string_of_big_int i in
      if options.html && not deep
      then sprintf "<input type='text' value='%s' readonly></input>" pp
      else pp
  | Literal (String s) -> if options.stripStrings then s else sprintf "'%s'" s
  | Literal (Bytes s) ->
      if options.html
      then sprintf "<span class='bytes'>0x%s</span>" (Misc.Hex.hexcape s)
      else sprintf "sp.bytes('0x%s')" (Misc.Hex.hexcape s)
  | Literal (Bls12_381_g1 s) ->
      if options.html
      then sprintf "<span class='bytes'>0x%s</span>" (Misc.Hex.hexcape s)
      else sprintf "sp.bls12_381_g1('0x%s')" (Misc.Hex.hexcape s)
  | Literal (Bls12_381_g2 s) ->
      if options.html
      then sprintf "<span class='bytes'>0x%s</span>" (Misc.Hex.hexcape s)
      else sprintf "sp.bls12_381_g2('0x%s')" (Misc.Hex.hexcape s)
  | Literal (Bls12_381_fr s) ->
      if options.html
      then sprintf "<span class='bytes'>0x%s</span>" (Misc.Hex.hexcape s)
      else sprintf "sp.bls12_381_fr('0x%s')" (Misc.Hex.hexcape s)
  | Literal (Chain_id s) ->
      if options.html
      then sprintf "<span class='bytes'>0x%s</span>" (Misc.Hex.hexcape s)
      else sprintf "sp.chain_id('0x%s')" (Misc.Hex.hexcape s)
  | Record l ->
      let layout =
        match Type.getRepr v.vt with
        | TRecord {layout} -> Some layout
        | _ -> None
      in
      let fields =
        let layout =
          match layout with
          | None -> None
          | Some layout -> Unknown.getRefOption layout
        in
        match layout with
        | None -> List.map (fun (x, _) -> (x, x)) l
        | Some layout ->
            let f Layout.{source; target} = (source, target) in
            List.map f (Binary_tree.to_list layout)
      in
      if options.html
      then
        html_of_record_list
          ( List.map snd fields
          , [List.map (fun (source, _) -> List.assoc source l) fields] )
          (fun s -> value_to_string ~noEmptyList:true ~options s)
      else
        sprintf
          "sp.record(%s)"
          (String.concat
             ", "
             (List.map
                (fun (n, _) ->
                  let x = List.assoc n l in
                  sprintf "%s = %s" n (value_to_string ~options x))
                fields))
  | Variant (name, v) when options.html ->
      sprintf
        "<div class='subtype'><select class='selection'><option \
         value='%s'>%s</option></select>%s</div>"
        name
        (String.capitalize_ascii name)
        (value_to_string v ~deep ~noEmptyList ~options)
  | Variant ("None", {v = Literal Unit}) ->
      if options.html then "None" else "sp.none"
  | Variant (name, {v = Literal Unit}) -> name
  | Variant ("Some", v) ->
      if options.html
      then value_to_string ~options v (* TODO ? *)
      else sprintf "sp.some(%s)" (value_to_string ~options v)
  | Variant (x, v) -> sprintf "%s(%s)" x (value_to_string ~options v)
  | List [] when noEmptyList -> ""
  | List l when deep && not (is_record_list l) ->
      if options.html
      then
        sprintf
          "[%s]"
          (String.concat
             ", "
             (List.map (value_to_string ~options ~deep:true) l))
      else
        sprintf
          "sp.list([%s])"
          (String.concat ", " (List.map (value_to_string ~options) l))
  | List l when options.html ->
      let l =
        match l with
        | {v = Record r} :: _ as l ->
            ( List.map fst r
            , List.map
                (function
                  | {v = Record r} -> List.map snd r
                  | _ -> assert false)
                l )
        | _ -> ([""], List.map (fun x -> [x]) l)
      in
      html_of_record_list l (fun s ->
          value_to_string ~noEmptyList:true ~deep:true ~options s)
  | List l ->
      sprintf
        "sp.list([%s])"
        (String.concat ", " (List.map (value_to_string ~options) l))
  | Ticket (ticketer, content, amount) ->
      let ticketer =
        match ticketer with
        | Real s -> s
        | Local s -> address_of_contract_id ~html:options.html s None
      in
      if options.html
      then
        html_of_record_list
          ( ["Ticketer"; "Content"; "Amount"]
          , [ [ sprintf "<span class='address'>%s</span>" ticketer
              ; value_to_string ~noEmptyList:true ~deep:true ~options content
              ; Bigint.string_of_big_int amount ] ] )
          (fun s -> s)
      else
        Format.asprintf
          "sp.ticket(sp.address(%S), %s, %a)"
          ticketer
          (value_to_string ~options content)
          Bigint.pp
          amount
  | Set [] when noEmptyList -> ""
  | Set set ->
      if options.html
      then
        html_of_record_list
          ([""], List.map (fun x -> [x]) set)
          (fun s -> value_to_string ~noEmptyList:true ~deep:true ~options s)
      else
        sprintf
          "sp.set([%s])"
          (String.concat ", " (List.map (value_to_string ~options) set))
  | Map map ->
      if options.html
      then
        match is_matrix v with
        | Some (columns, l) ->
            html_of_record_list (columns, l) (fun s ->
                value_to_string ~noEmptyList:true ~options s)
        | None ->
            let result =
              match Type.getRepr v.vt with
              | TMap {tvalue} ->
                ( match Type.getRepr tvalue with
                | TRecord {row} ->
                    Some
                      (html_of_record_list
                         ( "Key" :: List.map fst row
                         , List.map (fun (x, y) -> x :: unrec y) map )
                         (fun s -> value_to_string ~noEmptyList:true ~options s))
                | _ -> None )
              | _ -> None
            in
            ( match result with
            | None ->
                html_of_record_list
                  (["Key"; "Value"], List.map (fun (x, y) -> [x; y]) map)
                  (fun s -> value_to_string ~noEmptyList:true ~options s)
            | Some t -> t )
      else
        sprintf
          "{%s}"
          (String.concat
             ", "
             (List.map
                (fun (k, v) ->
                  sprintf
                    "%s : %s"
                    (value_to_string ~options k)
                    (value_to_string ~options v))
                map))
  | Literal Unit -> if options.html then "" else "Unit"
  | Literal (Key_hash s) ->
      if options.html
      then sprintf "<span class='key'>%s</span>" s
      else sprintf "sp.key_hash('%s')" s
  | Literal (Baker_hash s) ->
      if options.html
      then sprintf "<span class='key'>%s</span>" s
      else sprintf "sp.baker_hash('%s')" s
  | Literal (Key s) ->
      if options.html
      then sprintf "<span class='key'>%s</span>" s
      else sprintf "sp.key('%s')" s
  | Literal (Secret_key s) ->
      if options.html
      then sprintf "<span class='key'>%s</span>" s
      else sprintf "sp.secret_key('%s')" s
  | Literal (Signature s) ->
      if options.html
      then sprintf "<span class='signature'>%s</span>" s
      else sprintf "sp.signature('%s')" s
  | Literal ((Address (Real s, _) | Contract (Real s, _, _)) as lit) ->
      if options.html
      then sprintf "<span class='address'>%s</span>" s
      else literal_to_string ~html:false lit
  | Literal (Address (Local s, ep) | Contract (Local s, ep, _)) ->
      let s = address_of_contract_id ~html:options.html s ep in
      if options.html
      then sprintf "<span class='address'>%s</span>" s
      else sprintf "sp.address(%S)" s
  | Literal (Timestamp i) ->
      if options.html
      then
        sprintf
          "<span class='timestamp'>timestamp(%s)</span>"
          (Big_int.string_of_big_int i)
      else sprintf "sp.timestamp(%s)" (Big_int.string_of_big_int i)
  | Literal (Mutez i) ->
      let amount = ppAmount options.html i in
      if options.html
      then sprintf "<span class='token'>%s</span>" amount
      else amount
  | Tuple vs ->
      if options.html
      then
        html_of_record_list
          ([], [vs])
          (fun s -> value_to_string ~noEmptyList:true ~options s)
      else
        let vs = List.map (value_to_string ~noEmptyList:true ~options) vs in
        sprintf "(%s)" (String.concat ", " vs)
  | Closure _ -> sprintf "lambda(%s)" (type_to_string v.vt)
  | Operation operation -> operation_to_string ~options operation
  | Literal (Sapling_test_state {test = false}) ->
      if options.html
      then html_of_record_list (["SaplingState"], []) (fun s -> s)
      else sprintf "[%s]" (String.concat "; " (List.map (String.concat ",") []))
  | Literal (Sapling_test_state {elements}) ->
      let l =
        List.map
          (fun (key, amount) -> [key; Bigint.string_of_big_int amount])
          (List.sort compare elements)
      in
      if options.html
      then html_of_record_list (["key"; "amount"], l) (fun s -> s)
      else sprintf "[%s]" (String.concat "; " (List.map (String.concat ",") l))
  | Literal (Sapling_test_transaction {source; target; amount}) ->
      sprintf
        "sp.sapling_test_transaction(%S, %S, %s)"
        (Utils.Option.default "" source)
        (Utils.Option.default "" target)
        (Bigint.string_of_big_int amount)

and operation_to_string ?(options = Options.string) operation =
  match operation with
  | Transfer {arg; destination; amount} ->
      sprintf
        "<div class='operation'>Transfer %s to  %s<br>%s</div>"
        (value_to_string ~options amount)
        (value_to_string ~options destination)
        (value_to_string ~options arg)
  | SetDelegate None -> "<div class='operation'>Remove Delegate</div>"
  | SetDelegate (Some d) ->
      sprintf
        "<div class='operation'>Set Delegate(%s)</div>"
        (value_to_string ~options d)
  | CreateContract {id; baker; contract} ->
      sprintf
        "<div class='operation'>Create Contract(address: %s, baker: %s)%s</div>"
        (address_of_contract_id ~html:options.html id None)
        (value_to_string baker)
        (Base.Option.value_map
           ~f:(value_to_string ~options)
           ~default:""
           contract.storage)

let vclass_to_string = function
  | Storage -> "storage"
  | Local -> "local"
  | Param -> "param"
  | Iter -> "iter"
  | ListMap -> "list_map"
  | MatchCons -> "match_cons"

let with_vclass ~html vclass x =
  if html
  then sprintf "<span class='%s'>%s</span>" (vclass_to_string vclass) x
  else x

let tvariable_to_string ?(options = Options.string) ?protect (s, t) vclass =
  let prot s = if protect = Some () then sprintf "(%s)" s else s in
  match options with
  | {html = false; types = false} -> s
  | {html; types = true} ->
      prot
        (sprintf
           "%s : <span class='type'>%s</span>"
           (with_vclass ~html vclass s)
           (type_to_string t))
  | {types = false} ->
      sprintf "<span class='%s'>%s</span>" (vclass_to_string vclass) s

let variable_to_string ?(options = Options.string) ?protect s vclass =
  let prot s = if protect = Some () then sprintf "(%s)" s else s in
  match options with
  | {html = false; types = false} -> s
  | {html; types = true} -> prot (with_vclass ~html vclass s)
  | {types = false} ->
      sprintf "<span class='%s'>%s</span>" (vclass_to_string vclass) s

let string_of_binOpInfix = function
  | BNeq -> "!="
  | BEq -> "=="
  | BAnd -> "&"
  | BOr -> "|"
  | BAdd -> "+"
  | BSub -> "-"
  | BDiv -> "//"
  | BEDiv -> "ediv"
  | BMul _ -> "*"
  | BMod -> "%"
  | BLt -> "<"
  | BLe -> "<="
  | BGt -> ">"
  | BGe -> ">="
  | BLsl -> "<<"
  | BLsr -> ">>"
  | BXor -> "^"

let string_of_binOpPrefix = function
  | BMax -> "max"
  | BMin -> "min"

let collect_lambdas =
  let p_expr _ = function
    | ELambda {body = {c = CResult _}, _} -> []
    | ELambda {id; body} ->
        let l = sprintf "f%d" id in
        let v = sprintf "lparams_%d" id in
        [(l, v, fst body)]
    | e ->
        let e = map_expr_f snd (fun _ -> []) (fun _ -> []) e in
        fold_expr_f ( @ ) ( @ ) ( @ ) [] e
  in
  let p_command _ _ = [] in
  para_expr (para_alg ~p_expr ~p_command ~p_type:id)

let collect_lambdas {c} =
  let c = map_command_f collect_lambdas (fun _ -> []) (fun _ -> []) c in
  fold_command_f ( @ ) ( @ ) ( @ ) [] c

let rec expr_to_string ?(options = Options.string) ?protect e =
  let prot s = if protect = Some () then sprintf "(%s)" s else s in
  let htmlClass name s =
    if options.html then sprintf "<span class='%s'>%s</span>" name s else s
  in
  let putSelf s = sprintf "%s.%s" (htmlClass "self" "self") s in
  let to_string = expr_to_string ~options in
  match e.e with
  | EPrim0 prim ->
    begin
      match prim with
      | ECst v ->
          htmlClass
            "constant"
            (literal_to_string
               ~html:false
               ?strip_strings:(if options.stripStrings then Some () else None)
               v)
      | EBalance -> "sp.balance"
      | ESender -> "sp.sender"
      | ESource -> "sp.source"
      | EAmount -> "sp.amount"
      | ELevel -> "sp.level"
      | ENow -> "sp.now"
      | EChain_id -> "sp.chain_id"
      | EIter x -> with_vclass Iter ~html:options.html x
      | EMatchCons x -> with_vclass MatchCons ~html:options.html x
      | ELocal "__parameter__" -> sprintf "params"
      | ELocal "__operations__" -> "sp.operations()"
      | ELocal "__storage__" -> putSelf "data"
      | ELocal name -> sprintf "%s.value" name
      | EMetaLocal name -> sprintf "%s" name
      | EGlobal (name, _) -> sprintf "self.%s" name
      | EVariant_arg arg_name -> arg_name
      | ESelf -> "sp.self"
      | ESelf_address -> "sp.self_address"
      | ESelf_entry_point name -> sprintf "sp.self_entry_point('%s')" name
      | ESaplingEmptyState {memo} -> sprintf "sp.sapling_empty_state(%i)" memo
      | EContract_balance id ->
          sprintf "sp.contract_balance(%s)" (string_of_contract_id id)
      | EContract_baker id ->
          sprintf "sp.contract_baker(%s)" (string_of_contract_id id)
      | EContract_data id ->
          sprintf "sp.contract_data(%s)" (string_of_contract_id id)
      | EContract_typed id ->
          sprintf "sp.contract_typed(%s)" (string_of_contract_id id)
      | EScenario_var (id, _) -> sprintf "sp.scenario_var(%d)" id
      | EAccount_of_seed {seed} -> sprintf "sp.test_account(%S)" seed
      | ETotal_voting_power -> "sp.total_voting_power"
    end
  | EPrim1 (prim, x) ->
    begin
      match prim with
      | EReduce -> sprintf "sp.reduce(%s)" (to_string x)
      | EListRev -> sprintf "%s.rev()" (to_string ~protect:() x)
      | EListItems false -> sprintf "%s.items()" (to_string ~protect:() x)
      | EListKeys false -> sprintf "%s.keys()" (to_string ~protect:() x)
      | EListValues false -> sprintf "%s.values()" (to_string ~protect:() x)
      | EListElements false -> sprintf "%s.elements()" (to_string ~protect:() x)
      | EListItems true -> sprintf "%s.rev_items()" (to_string ~protect:() x)
      | EListKeys true -> sprintf "%s.rev_keys()" (to_string ~protect:() x)
      | EListValues true -> sprintf "%s.rev_values()" (to_string ~protect:() x)
      | EListElements true ->
          sprintf "%s.rev_elements()" (to_string ~protect:() x)
      | EPack -> sprintf "sp.pack(%s)" (to_string x)
      | ENot -> prot (sprintf "~ %s" (to_string ~protect:() x))
      | EAbs -> prot (sprintf "abs(%s)" (to_string x))
      | EToInt -> prot (sprintf "sp.to_int(%s)" (to_string x))
      | EIsNat -> prot (sprintf "sp.is_nat(%s)" (to_string x))
      | ENeg -> prot (sprintf "- %s" (to_string ~protect:() x))
      | ESign -> prot (sprintf "sp.sign(%s)" (to_string x))
      | ESum -> sprintf "sp.sum(%s)" (to_string x)
      | EUnpack t ->
          sprintf "sp.unpack(%s, %s)" (to_string x) (type_to_string t)
      | EHash_key -> sprintf "sp.hash_key(%s)" (to_string x)
      | EHash algo ->
          sprintf
            "sp.%s(%s)"
            (String.lowercase_ascii (string_of_hash_algo algo))
            (to_string x)
      | EContract_address ->
        begin
          match x.e with
          | EPrim0 ESelf -> "sp.self_address"
          | EPrim0 (ESelf_entry_point name) ->
              sprintf "sp.self_entry_point_address('%s')" name
          | _ -> sprintf "sp.to_address(%s)" (to_string x)
        end
      | EImplicit_account -> sprintf "sp.implicit_account(%s)" (to_string x)
      | EProject 0 -> sprintf "sp.fst(%s)" (to_string x)
      | EProject 1 -> sprintf "sp.snd(%s)" (to_string x)
      | EProject i -> sprintf "%s[%d]" (to_string x) i
      | EConcat_list -> sprintf "sp.concat(%s)" (to_string x)
      | ESize -> sprintf "sp.len(%s)" (to_string x)
      | ESetDelegate -> sprintf "sp.set_delegate_operation(%s)" (to_string x)
      | EType_annotation t ->
          sprintf "sp.set_type_expr(%s, %s)" (to_string x) (type_to_string t)
      | EAttr name -> sprintf "%s.%s" (to_string ~protect:() x) name
      | EIsVariant "Some" -> sprintf "%s.is_some()" (to_string ~protect:() x)
      | EIsVariant name -> sprintf "%s.is_variant('%s')" (to_string x) name
      | EVariant name ->
        begin
          match (name, x) with
          | "None", {e = EPrim0 (ECst Unit)} -> "sp.none"
          | "Some", x -> sprintf "sp.some(%s)" (to_string x)
          | name, x -> sprintf "variant('%s', %s)" name (to_string x)
        end
      | EReadTicket -> sprintf "sp.read_ticket_raw(%s)" (to_string x)
      | EJoinTickets -> sprintf "sp.join_tickets_raw(%s)" (to_string x)
      | EPairingCheck -> sprintf "sp.pairing_check(%s)" (to_string x)
      | EVotingPower -> sprintf "sp.voting_power(%s)" (to_string x)
    end
  | EPrim2 (prim2, x, y) ->
    begin
      match prim2 with
      | ECallLambda ->
          let lambda, parameter = (x, y) in
          sprintf
            "%s(%s)" (* "%s.__call__(%s)" *)
            (to_string ~protect:() lambda)
            (to_string parameter)
      | EApplyLambda ->
          let lambda, parameter = (x, y) in
          sprintf
            "%s.apply(%s)"
            (to_string ~protect:() lambda)
            (to_string parameter)
      | EBinOpPre f ->
          sprintf
            "sp.%s(%s, %s)"
            (string_of_binOpPrefix f)
            (to_string x)
            (to_string y)
      | EBinOpInf BEDiv ->
          prot (sprintf "sp.ediv(%s, %s)" (to_string x) (to_string y))
      | EBinOpInf (BMul {overloaded = true}) ->
          sprintf "sp.mul(%s, %s)" (to_string x) (to_string y)
      | EBinOpInf op ->
          prot
            (sprintf
               "%s %s %s"
               (to_string ~protect:() x)
               (string_of_binOpInfix op)
               (to_string ~protect:() y))
      | EGetOpt ->
          let m, k = (x, y) in
          sprintf "%s.get_opt(%s)" (to_string ~protect:() m) (to_string k)
      | ECons ->
          let e1, e2 = (x, y) in
          sprintf "sp.cons(%s, %s)" (to_string e1) (to_string e2)
      | EAdd_seconds ->
          let e1, e2 = (x, y) in
          sprintf "sp.add_seconds(%s, %s)" (to_string e1) (to_string e2)
      | EContains ->
          let items, member = (x, y) in
          prot
            (sprintf
               "%s.contains(%s)"
               (to_string ~protect:() items)
               (to_string member))
      | ETicket ->
          let content, amount = (x, y) in
          sprintf "sp.ticket(%s, %s)" (to_string content) (to_string amount)
      | ESplitTicket ->
          let ticket, decomposition = (x, y) in
          sprintf
            "sp.split_ticket_raw(%s, %s)"
            (to_string ticket)
            (to_string decomposition)
      | ETest_ticket contract_id ->
          sprintf
            "sp.test_ticket(%s, %s, %s)"
            (address_of_contract_id ~html:options.html contract_id None)
            (to_string x)
            (to_string y)
    end
  | EPrim3 (prim3, x, y, z) ->
    begin
      match prim3 with
      | ERange ->
        begin
          match (x, y, z) with
          | a, b, {e = EPrim0 (ECst (Int {i = step}))}
            when Big_int.eq_big_int step (Bigint.of_int 1) ->
              sprintf "sp.range(%s, %s)" (to_string a) (to_string b)
          | e1, e2, e3 ->
              sprintf
                "sp.range(%s, %s, %s)"
                (to_string e1)
                (to_string e2)
                (to_string e3)
        end
      | ECheck_signature ->
          sprintf
            "sp.check_signature(%s, %s, %s)"
            (to_string x)
            (to_string y)
            (to_string z)
      | EUpdate_map ->
          sprintf
            "sp.update_map(%s, %s, %s)"
            (to_string x)
            (to_string y)
            (to_string z)
      | EGet_and_update ->
          sprintf
            "sp.get_and_update(%s, %s, %s)"
            (to_string x)
            (to_string y)
            (to_string z)
      | EIf ->
          let cond, a, b = (x, y, z) in
          sprintf
            "sp.eif(%s, %s, %s)"
            (to_string cond)
            (to_string a)
            (to_string b)
      | ESplit_tokens ->
        begin
          match (x, y, z) with
          | ( {e = EPrim0 (ECst (Mutez tok))}
            , quantity
            , {e = EPrim0 (ECst (Int {i}))} )
            when Big_int.eq_big_int tok (Bigint.of_int 1000000)
                 && Big_int.eq_big_int i (Bigint.of_int 1) ->
              sprintf "sp.tez(%s)" (to_string quantity)
          | ( {e = EPrim0 (ECst (Mutez tok))}
            , quantity
            , {e = EPrim0 (ECst (Int {i}))} )
            when Big_int.eq_big_int tok (Bigint.of_int 1)
                 && Big_int.eq_big_int i (Bigint.of_int 1) ->
              sprintf "sp.mutez(%s)" (to_string quantity)
          | e1, e2, e3 ->
              sprintf
                "sp.split_tokens(%s, %s, %s)"
                (to_string e1)
                (to_string e2)
                (to_string e3)
        end
    end
  | EOpenVariant (name, x, missing_message) ->
      let missing_message =
        match missing_message with
        | None -> None
        | Some x -> Some (sprintf "message = %s" (to_string x))
      in
      ( match (name, x, missing_message) with
      | "Some", {e = EPrim1 (EIsNat, x)}, None ->
          sprintf "sp.as_nat(%s)" (to_string x)
      | "Some", {e = EPrim1 (EIsNat, x)}, Some message ->
          sprintf "sp.as_nat(%s, %s)" (to_string x) message
      | "Some", x, None -> sprintf "%s.open_some()" (to_string x)
      | "Some", x, Some message ->
          sprintf "%s.open_some(%s)" (to_string x) message
      | name, x, None -> sprintf "%s.open_variant('%s')" (to_string x) name
      | name, x, Some message ->
          sprintf "%s.open_variant('%s', %s)" (to_string x) name message )
  | ELambda
      { id
      ; body =
          { c =
              CResult
                {e = EMichelson (michelson, [{e = ELambdaParams {id = idl}}])}
          }
      ; tParams
      ; tResult }
    when id = idl ->
      sprintf
        "sp.lambda_michelson(%S, %s, %s)"
        michelson.name
        (type_to_string tParams)
        (type_to_string tResult)
  | ELambda {id; body = {c = CResult r}} ->
      sprintf "sp.build_lambda(lambda lparams_%i: %s)" id (to_string r)
  | ELambda {id} -> sprintf "sp.build_lambda(f%d)" id
  | EMapFunction {f; l} ->
      sprintf "%s.map(%s)" (to_string ~protect:() l) (to_string f)
  | ELambdaParams {id} -> sprintf "lparams_%i" id
  | ECreate_contract _ -> sprintf "create contract ..."
  | EItem {items; key; default_value = None; missing_message = None} ->
      sprintf "%s[%s]" (to_string ~protect:() items) (to_string key)
  | EItem {items; key; default_value = Some d} ->
      sprintf
        "%s.get(%s, default_value = %s)"
        (to_string ~protect:() items)
        (to_string key)
        (to_string d)
  | EItem {items; key; default_value = None; missing_message = Some message} ->
      sprintf
        "%s.get(%s, message = %s)"
        (to_string ~protect:() items)
        (to_string key)
        (to_string message)
  | ERecord entries ->
      sprintf
        "sp.record(%s)"
        (String.concat
           ", "
           (List.map (fun (n, e) -> sprintf "%s = %s" n (to_string e)) entries))
  | EList l ->
      sprintf "sp.list([%s])" (String.concat ", " (List.map to_string l))
  | EMap (_, l) ->
      sprintf
        "{%s}"
        (String.concat
           ", "
           (List.map
              (fun (k, e) -> sprintf "%s : %s" (to_string k) (to_string e))
              l))
  | ESet l -> sprintf "sp.set([%s])" (String.concat ", " (List.map to_string l))
  | EContract {arg_type; entry_point; address} ->
      sprintf
        "sp.contract(%s, %s%s)"
        (type_to_string arg_type)
        (to_string address)
        (Base.Option.value_map entry_point ~default:"" ~f:(fun ep ->
             sprintf ", entry_point='%s'" ep))
  | ETuple es ->
      let es = List.map to_string es in
      sprintf "(%s)" (String.concat ", " es)
  | ESlice {offset; length; buffer} ->
      prot
        (sprintf
           "sp.slice(%s, %s, %s)"
           (to_string buffer)
           (to_string offset)
           (to_string length))
  | EMake_signature {secret_key; message; message_format} ->
      sprintf
        "sp.make_signature(secret_key = %s, message = %s, message_format = %s)"
        (to_string secret_key)
        (to_string message)
        ( match message_format with
        | `Hex -> "'Hex'"
        | `Raw -> "'Raw'" )
  | EMichelson (michelson, exprs) ->
      sprintf
        "sp.michelson(%S)(%s)"
        michelson.name
        (String.concat ", " (List.map to_string exprs))
  | ETransfer {arg; amount; destination} ->
      sprintf
        "sp.transfer_operation(%s, %s, %s)"
        (to_string arg)
        (to_string amount)
        (to_string destination)
  | EMatch (scrutinee, clauses) ->
      sprintf
        "sp.ematch(%s, %s)"
        (to_string scrutinee)
        (String.concat
           ", "
           (List.map
              (fun (cons, rhs) -> sprintf "(\"%s\", %s)" cons (to_string rhs))
              clauses))
  | ESaplingVerifyUpdate {transaction; state} ->
      sprintf
        "sp.sapling_verify_update(%s, %s)"
        (to_string state)
        (to_string transaction)

let pps, ppS =
  let comma buf = bprintf buf ", " in
  ( List.buffer_sep comma (fun buf -> bprintf buf "%s")
  , List.buffer_sep comma (fun buf -> bprintf buf "%S") )

let rec pp_command ?(indent = "") ?(options = Options.string) =
  let pp_command = pp_command ~options in
  let shiftIdent = if options.html then "&nbsp;&nbsp;" else "  " in
  let newline = if options.html then "\n<br>" else "\n" in
  let print_lambda buf (l, v, body) =
    let ppc = pp_command ~indent:(indent ^ shiftIdent) in
    bprintf buf "%sdef %s(%s):%s%a%s" indent l v newline ppc body newline
  in
  let expr_to_string = expr_to_string ~options in
  let rec pp ~indent buf c =
    List.iter (print_lambda buf) (collect_lambdas c);
    match c.c with
    | CNever message ->
        bprintf buf "%ssp.never(%s)" indent (expr_to_string message)
    | CFailwith message ->
        bprintf buf "%ssp.failwith(%s)" indent (expr_to_string message)
    | CIf (c, t, e) ->
        if options.html
        then (
          bprintf
            buf
            "%ssp.if %s:<br>%a"
            indent
            (expr_to_string c)
            (pp ~indent:(indent ^ shiftIdent))
            t;
          match e.c with
          | CResult {e = EPrim0 (ECst Literal.Unit)} -> ()
          | _ ->
              bprintf
                buf
                "<br>%ssp.else:<br>%a"
                indent
                (pp ~indent:(indent ^ shiftIdent))
                e )
        else (
          bprintf
            buf
            "%ssp.if %s:\n%a"
            indent
            (expr_to_string c)
            (pp ~indent:(indent ^ shiftIdent))
            t;
          match e.c with
          | CResult {e = EPrim0 (ECst Literal.Unit)} -> ()
          | _ ->
              bprintf
                buf
                "\n%ssp.else:\n%a"
                indent
                (pp ~indent:(indent ^ shiftIdent))
                e )
    | CMatch (scrutinee, [(constructor, arg_name, c)]) ->
        if options.html
        then
          bprintf
            buf
            "%swith %s.match('%s') as %s:<br>%a"
            indent
            (expr_to_string ~protect:() scrutinee)
            constructor
            arg_name
            (pp ~indent:(indent ^ shiftIdent))
            c
        else
          bprintf
            buf
            "%swith %s.match('%s') as %s:\n%a"
            indent
            (expr_to_string ~protect:() scrutinee)
            constructor
            arg_name
            (pp ~indent:(indent ^ shiftIdent))
            c
    | CMatch (scrutinee, cases) ->
        if options.html
        then
          bprintf
            buf
            "%swith %s.match_cases() as arg:<br>%a"
            indent
            (expr_to_string ~protect:() scrutinee)
            (fun buf ->
              List.iter (fun (constructor, arg_name, c) ->
                  bprintf
                    buf
                    "%swith arg.match('%s') as %s:<br>%a<br>"
                    (indent ^ shiftIdent)
                    constructor
                    arg_name
                    (pp ~indent:(indent ^ shiftIdent ^ shiftIdent))
                    c))
            cases
        else
          bprintf
            buf
            "%swith %s.match_cases() as arg:\n%a"
            indent
            (expr_to_string ~protect:() scrutinee)
            (fun buf ->
              List.iter (fun (constructor, arg_name, c) ->
                  bprintf
                    buf
                    "%swith arg.match('%s') as %s:\n%a\n"
                    (indent ^ shiftIdent)
                    constructor
                    arg_name
                    (pp ~indent:(indent ^ shiftIdent ^ shiftIdent))
                    c))
            cases
    | CMatchProduct (_, Pattern_single _, _) -> assert false
    | CMatchProduct (s, Pattern_tuple ns, c) ->
        let f = bprintf buf "%s%a = sp.match_tuple(%s, %a)%s%a" in
        f indent pps ns (expr_to_string s) ppS ns newline (pp ~indent) c
    | CMatchProduct (s, Pattern_record (_name, bs), c) ->
        let vs = List.map (fun {var} -> var) bs in
        let fs = List.map (fun {field} -> field) bs in
        let f = bprintf buf "%s%a = sp.match_record(%s, %a)%s%a" in
        f indent pps vs (expr_to_string s) ppS fs newline (pp ~indent) c
    | CModifyProduct (s, Pattern_single x, c) ->
        let indent' = indent ^ shiftIdent in
        let f = bprintf buf "%swith sp.modify(%s, %S) as %S:%s%a" in
        f indent (expr_to_string s) x x newline (pp ~indent:indent') c
    | CModifyProduct (s, Pattern_tuple ls, c) ->
        let indent' = indent ^ shiftIdent in
        let f = bprintf buf "%swith sp.match_tuple(%s, %a) as %a:%s%a" in
        f indent (expr_to_string s) ppS ls pps ls newline (pp ~indent:indent') c
    | CModifyProduct (s, Pattern_record (name, _bs), c) ->
        let indent' = indent ^ shiftIdent in
        let f = bprintf buf "%swith sp.match_record(%s, %S) as %s:%s%a" in
        f indent (expr_to_string s) name name newline (pp ~indent:indent') c
    | CMatchCons matcher ->
        bprintf
          buf
          "%swith sp.match_cons(%s) as %s:%s%a%s%selse:%s%a"
          indent
          (expr_to_string ~protect:() matcher.expr)
          matcher.id
          newline
          (pp ~indent:(indent ^ shiftIdent))
          matcher.ok_match
          newline
          indent
          newline
          (pp ~indent:(indent ^ shiftIdent))
          matcher.ko_match
    | CBind (None, c1, c2) ->
        pp ~indent buf c1;
        bprintf buf "%s" newline;
        pp ~indent buf c2
    | CBind (Some x, {c = CResult e}, c2) ->
        if options.html
        then (
          bprintf
            buf
            "%s%s = sp.local(%S, %s)\n<br>"
            indent
            x
            x
            (expr_to_string e);
          pp ~indent buf c2 )
        else (
          bprintf buf "%s%s = sp.local(%S, %s)\n" indent x x (expr_to_string e);
          pp ~indent buf c2 )
    | CBind (Some x, c1, c2) ->
        if options.html
        then (
          bprintf buf "%s%s = sp.bind_block(%S):\n<br>" indent x x;
          bprintf buf "%swith %s:\n<br>" indent x;
          pp ~indent:(indent ^ shiftIdent) buf c1;
          bprintf buf "\n<br>";
          pp ~indent buf c2 )
        else (
          bprintf buf "%s%s = sp.bind_block(%S)\n" indent x x;
          bprintf buf "%swith %s:\n" indent x;
          pp ~indent:(indent ^ shiftIdent) buf c1;
          bprintf buf "\n";
          pp ~indent buf c2 )
    | CResult {e = EPrim0 (ECst Literal.Unit)} -> bprintf buf "%spass" indent
    | CResult r -> bprintf buf "%ssp.result(%s)" indent (expr_to_string r)
    | CFor (var, e, c) ->
        if options.html
        then
          bprintf
            buf
            "%ssp.for %s in %s:\n<br>%a"
            indent
            (variable_to_string ~options var Iter)
            (expr_to_string e)
            (pp ~indent:(indent ^ shiftIdent))
            c
        else
          bprintf
            buf
            "%ssp.for %s in %s:\n%a"
            indent
            (variable_to_string ~options var Iter)
            (expr_to_string e)
            (pp ~indent:(indent ^ shiftIdent))
            c
    | CWhile (e, c) ->
        if options.html
        then
          bprintf
            buf
            "%ssp.while %s:\n<br>%a"
            indent
            (expr_to_string e)
            (pp ~indent:(indent ^ shiftIdent))
            c
        else
          bprintf
            buf
            "%ssp.while %s:\n%a"
            indent
            (expr_to_string e)
            (pp ~indent:(indent ^ shiftIdent))
            c
    | CDefineLocal (name, {e = EPrim1 (EType_annotation t, e)}) ->
        bprintf
          buf
          "%s%s = sp.local(%S, %s, %s)"
          indent
          (tvariable_to_string ~options (name, t) Local)
          (tvariable_to_string ~options (name, t) Local)
          (expr_to_string e)
          (type_to_string t)
    | CDefineLocal (name, e) ->
        bprintf
          buf
          "%s%s = sp.local(%S, %s)"
          indent
          (variable_to_string ~options name Local)
          (variable_to_string ~options name Local)
          (expr_to_string e)
    | CSetVar (s, t) ->
      ( match t.e with
      | EPrim2 (ECons, u, s') when s = s' ->
        ( match (s.e, u.e) with
        | ( EPrim0 (ELocal "__operations__")
          , ETransfer
              { destination =
                  { e =
                      EOpenVariant
                        ( "Some"
                        , {e = EContract {arg_type = F TUnit; address}}
                        , None ) }
              ; amount } ) ->
            bprintf
              buf
              "%ssp.send(%s, %s)"
              indent
              (expr_to_string address)
              (expr_to_string amount)
        | EPrim0 (ELocal "__operations__"), ETransfer {arg; amount; destination}
          ->
            bprintf
              buf
              "%ssp.transfer(%s, %s, %s)"
              indent
              (expr_to_string arg)
              (expr_to_string amount)
              (expr_to_string destination)
        | EPrim0 (ELocal "__operations__"), EPrim1 (ESetDelegate, e) ->
            bprintf buf "%ssp.set_delegate(%s)" indent (expr_to_string e)
        | _ ->
            bprintf
              buf
              "%s%s.push(%s)"
              indent
              (expr_to_string s)
              (expr_to_string u) )
      | EPrim2
          ( EBinOpInf ((BAdd | BSub | BMul {overloaded = false} | BDiv) as op)
          , s'
          , u )
        when equal_expr_modulo_line_nos s s' ->
          bprintf
            buf
            "%s%s %s= %s"
            indent
            (expr_to_string s)
            (string_of_binOpInfix op)
            (expr_to_string u)
      | _ ->
        ( match s.e with
        | EItem _ | EPrim1 (EAttr _, _) ->
            bprintf buf "%s%s = %s" indent (expr_to_string s) (expr_to_string t)
        | EPrim0 (ELocal _) ->
            bprintf buf "%s%s = %s" indent (expr_to_string s) (expr_to_string t)
        | _ ->
            bprintf
              buf
              "%s%s.set(%s)"
              indent
              (expr_to_string s)
              (expr_to_string t) ) )
    | CDelItem (expr, item) ->
        bprintf
          buf
          "%s%s %s[%s]"
          indent
          (if options.html then "<span class='keyword'>del</span>" else "del")
          (expr_to_string expr)
          (expr_to_string item)
    | CUpdateSet (expr, element, add) ->
        bprintf
          buf
          "%s%s.%s(%s)"
          indent
          (expr_to_string expr)
          (if add then "add" else "remove")
          (expr_to_string element)
    | CVerify (e, message) ->
        bprintf
          buf
          "%ssp.verify(%s%s)"
          indent
          (expr_to_string e)
          ( match message with
          | None -> ""
          | Some msg -> Format.sprintf ", message = %s" (expr_to_string msg) )
    | CComment s -> bprintf buf "%s# %s" indent s
    | CSetType (e, t) ->
        bprintf
          buf
          "%ssp.set_type(%s, %s)"
          indent
          (expr_to_string e)
          (type_to_string t)
    | CSetResultType (c, t) ->
        bprintf
          buf
          "%swith sp.set_result_type(%s):\n<br>"
          indent
          (type_to_string t);
        pp ~indent:(indent ^ shiftIdent) buf c
    | CTrace e -> bprintf buf "%ssp.trace(%s)" indent (expr_to_string e)
  in
  pp ~indent

let command_to_string ?indent ?options c =
  let buf = Buffer.create 64 in
  pp_command ?indent ?options buf c;
  Buffer.contents buf

let unRecordList = function
  | {v = Record l} -> (List.map fst l, [List.map snd l])
  | _data -> (["data"], [])

let html_of_data options data =
  html_of_record_list (unRecordList data) (fun s ->
      value_to_string ~noEmptyList:true ~options s)
  ^
  (*  Value.html_of_record_list (unRecordList data) (fun s -> value_to_string ~noEmptyList:true ~options (!checkImport s)) ^*)
  match options with
  | {html = true; types = true} ->
      type_to_string ~options data.vt ^ "<br><br><br>"
  | _ -> ""

let init_html_of_data options (x, v) =
  sprintf "%s = %s" (tvariable_to_string ~options x Storage) (value_to_string v)

let init_html_of_data options : tvalue -> string list = function
  | {v = Record l} ->
      List.map (fun (n, v) -> init_html_of_data options ((n, v.vt), v)) l
  | {v = Literal Unit} -> []
  | v -> [value_to_string v]

let unMutez = function
  | {v = Literal (Mutez b)} -> b
  | _ -> failwith "Not a mutez"

let pp_contract
    ?(options = Options.string)
    ppf
    {value_contract = {balance; storage; tstorage; entry_points}} =
  if options.html
  then (
    let init =
      if options.stripped
      then ""
      else
        match storage with
        | None ->
            sprintf
              "<div class='on'><span class='keyword'>def</span> \
               __init__(self)<span class='keyword'>:</span><div \
               class='indent5'>&nbsp;&nbsp;self.init_type(%s)</div></div><br>"
              (type_to_string tstorage)
        | Some storage ->
            sprintf
              "<div class='on'><span class='keyword'>def</span> \
               __init__(self)<span class='keyword'>:</span><div \
               class='indent5'>&nbsp;&nbsp;self.init(%s)</div></div><br>"
              (String.concat
                 ",<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                 (init_html_of_data options storage))
    in
    Format.fprintf
      ppf
      "<div class='contract'><h3>Balance: %s</h3>\n\
       <h3>Storage:</h3>\n\
       %s\n\
       <h3>Entry points:</h3>\n\
       %s\n"
      (ppAmount true (unMutez balance))
      (Base.Option.value_map ~default:"" ~f:(html_of_data options) storage)
      init;

    List.iteri
      (fun i {channel; originate; body} ->
        if i > 0 then Format.fprintf ppf "\n<br>";
        Format.fprintf
          ppf
          "<div class='on'>%s<span class='keyword'>def</span> %s(<span \
           class='self'>self</span>, params)<span \
           class='keyword'>:</span><br><div class='indent5'>%a</div></div>"
          ( if options.stripped
          then ""
          else if originate
          then "@sp.entry_point<br>"
          else "@sp.private_entry_point<br>" )
          channel
          (fun ppf c ->
            let c = command_to_string ~options ~indent:"&nbsp;&nbsp;" c in
            Format.fprintf ppf "%s" c)
          body)
      entry_points;

    Format.fprintf ppf "</div>" )
  else
    let init =
      if options.stripped
      then ""
      else
        match storage with
        | None ->
            sprintf
              "  def __init__(self):\n    self.init_type(%s)\n"
              (type_to_string tstorage)
        | Some storage ->
            sprintf
              "  def __init__(self):\n    self.init(%s)\n"
              (String.concat ", " (init_html_of_data options storage))
    in
    Format.fprintf
      ppf
      "import smartpy as sp\n\nclass Contract(sp.Contract):\n%s"
      init;

    List.iteri
      (fun i {channel; body} ->
        if i > 0 then Format.fprintf ppf "\n";
        Format.fprintf
          ppf
          "%s  def %s(self, params):\n%a"
          (if options.stripped then "" else "\n  @sp.entry_point\n")
          channel
          (fun ppf c ->
            let c = command_to_string ~options ~indent:"    " c in
            Format.fprintf ppf "%s" c)
          body)
      entry_points

let contract_to_string ?options c =
  Format.asprintf "%a" (pp_contract ?options) c

let pp_tcontract ?options fmt c =
  pp_contract
    ?options
    fmt
    (erase_types_value_contract (layout_records_contract c))

let texpr_to_string ?options ?protect x =
  expr_to_string ?options ?protect (erase_types_expr (layout_records_expr x))

let tcommand_to_string ?indent ?options x =
  command_to_string
    ?indent
    ?options
    (erase_types_command (layout_records_command x))

let tcontract_to_string ?options c =
  contract_to_string
    ?options
    (erase_types_value_contract (layout_records_contract c))

let ppType html t =
  if html
  then sprintf "<span class='type'>%s</span>" (type_to_string t)
  else type_to_string t

let ppExpr html (e : texpr) =
  if html
  then
    let pp =
      match e.line_no with
      | Some l ->
          sprintf
            "<button class='text-button' onClick='showLine(%i)'>%s</button>"
            l
      | None -> id
    in
    pp (sprintf "(%s : %s)" (texpr_to_string e) (ppType html e.et))
  else
    sprintf
      "(%s : %s)%s"
      (texpr_to_string e)
      (ppType html e.et)
      (Option.cata "" (sprintf " (line %i)") e.line_no)

let ppExpr_untyped html (e : expr) =
  if html
  then
    let pp =
      match e.line_no with
      | Some l ->
          sprintf
            "<button class='text-button' onClick='showLine(%i)'>%s</button>"
            l
      | None -> id
    in
    pp (expr_to_string e)
  else
    sprintf
      "(%s)%s"
      (expr_to_string e)
      (Option.cata "" (sprintf " (line %i)") e.line_no)

let rec simplify acc (x : smart_except list) =
  match x with
  | [] -> acc
  | ( ( `Literal _ | `Value _ | `Expr _ | `Exprs _ | `Expr_untyped _ | `Text _
      | `Type _ ) as x )
    :: rest ->
      simplify (x :: acc) rest
  | `Br :: rest ->
      let acc =
        match acc with
        | `Br :: _ -> acc
        | _ -> `Br :: acc
      in
      simplify acc rest
  | `Line i :: rest ->
    begin
      match acc with
      | `Line j :: `Br :: _ when i = j -> simplify acc rest
      | `Br :: _ -> simplify (`Line i :: acc) rest
      | _ -> simplify (`Line i :: `Br :: acc) rest
    end
  | `Rec l :: rest -> simplify acc (l @ rest)

let simplify x = List.rev (simplify [] x)

let rec pp_smart_except html = function
  | `Literal literal -> literal_to_string ~html literal
  | `Value value -> value_to_string value
  | `Expr expr -> ppExpr html expr
  | `Exprs exprs -> String.concat ", " (List.map (ppExpr html) exprs)
  | `Expr_untyped expr -> ppExpr_untyped html expr
  | `Line i ->
      let i = Option.default (-1) i in
      if html
      then
        sprintf
          "<button class='text-button' onClick='showLine(%i)'>line %i</button>"
          i
          i
      else sprintf "(line %i)" i
  | `Text s -> s
  | `Type t -> ppType html t
  | `Br -> if html then "<br>" else "\n"
  | `Rec l -> String.concat " " (List.map (pp_smart_except html) l)

let pp_smart_except html l = pp_smart_except html (`Rec (simplify l))

let error_to_string ?(options = Options.string) operation =
  let open Execution in
  if options.html
  then
    match operation with
    | Exec_error v ->
        sprintf "<div class='error'>%s</div>" (pp_smart_except options.html v)
    | Exec_channel_not_found s ->
        sprintf "<div class='error'>ChannelNotFound: '%s'</div>" s
    | Exec_wrong_condition (e, line_no, message) ->
        let line_no = Option.default (-1) line_no in
        let message =
          match message with
          | None -> ""
          | Some msg -> sprintf " [%s]" (value_to_string msg)
        in
        sprintf
          "<div class='error'>WrongCondition in <button class='text-button' \
           onClick='showLine(%i)'>line %i</button>: %s%s</div>"
          line_no
          line_no
          (texpr_to_string ~options e)
          message
  else
    match operation with
    | Exec_error v -> sprintf "%s" (pp_smart_except false v)
    | Exec_channel_not_found s -> sprintf "ChannelNotFound: '%s'" s
    | Exec_wrong_condition (e, line_no, message) ->
        let line_no = Option.default (-1) line_no in
        let message =
          match message with
          | None -> ""
          | Some msg -> sprintf " [%s]" (value_to_string msg)
        in
        sprintf
          "WrongCondition in line %i: %s%s"
          line_no
          (texpr_to_string ~options e)
          message

let exception_to_string html = function
  | Failure s -> s
  | SmartExcept l -> pp_smart_except html l
  | Yojson.Basic.Util.Type_error (msg, _t) -> "Yojson exception - " ^ msg
  | e -> Printexc.to_string e
