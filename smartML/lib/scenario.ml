(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils.Misc
open Basics

type loaded_scenario =
  { scenario : tscenario
  ; scenario_state : Basics.scenario_state
  ; typing_env : Typing.env }

let action_of_json ~primitives ~scenario_state ~typing_env x =
  let module M = (val json_getter x : JsonGetter) in
  let open M in
  let import_expr_string s =
    Import.import_expr
      typing_env
      primitives
      scenario_state
      (Parsexp.Single.parse_string_exn (string s))
  in
  let import_type_string s =
    Import.import_type typing_env (Parsexp.Single.parse_string_exn (string s))
  in
  let import_contract_id_string s =
    Import.import_contract_id (Parsexp.Single.parse_string_exn (string s))
  in
  match string "action" with
  | "newContract" ->
      let contract =
        Import.import_contract
          ~primitives
          ~scenario_state
          typing_env
          (Parsexp.Single.parse_string_exn (string "export"))
      in
      let id = import_contract_id_string "id" in
      let line_no = Some (int "line_no") in
      New_contract
        { id
        ; contract = contract.contract
        ; line_no
        ; accept_unknown_types = bool "accept_unknown_types"
        ; baker = Expr_untyped.none ~line_no
        ; show = bool "show" }
  | "compute" ->
      let expression = import_expr_string "expression" in
      Compute {id = int "id"; expression; line_no = Some (int "line_no")}
  | "simulation" ->
      Simulation
        {id = import_contract_id_string "id"; line_no = Some (int "line_no")}
  | "message" ->
      let amount = import_expr_string "amount" in
      let of_seed id =
        match string id with
        | "none" -> None
        | seed ->
            if Base.String.is_prefix seed ~prefix:"seed:"
            then
              let module P = (val primitives : Primitives.Primitives) in
              Some
                (Account
                   (P.Crypto.account_of_seed
                      (String.sub seed 5 (String.length seed - 5))))
            else Some (Address (import_expr_string id))
      in
      let sender = of_seed "sender" in
      let source = of_seed "source" in
      let chain_id =
        if string "chain_id" = ""
        then None
        else Some (import_expr_string "chain_id")
      in
      let params = import_expr_string "params" in
      let line_no = Some (int "line_no") in
      let message = string "message" in
      let id = import_contract_id_string "id" in
      Message
        { id
        ; valid = import_expr_string "valid"
        ; params
        ; line_no
        ; title = string "title"
        ; messageClass = string "messageClass"
        ; sender
        ; source
        ; chain_id
        ; time = import_expr_string "time"
        ; amount
        ; level = import_expr_string "level"
        ; voting_powers = import_expr_string "voting_powers"
        ; message
        ; show = bool "show"
        ; export = bool "export" }
  | "error" -> ScenarioError {message = string "message"}
  | "html" ->
      Html
        { tag = string "tag"
        ; inner = string "inner"
        ; line_no = Some (int "line_no") }
  | "verify" ->
      Verify
        { condition = import_expr_string "condition"
        ; line_no = Some (int "line_no") }
  | "show" ->
      Show
        { expression = import_expr_string "expression"
        ; html = bool "html"
        ; stripStrings = bool "stripStrings"
        ; compile = bool "compile"
        ; line_no = Some (int "line_no") }
  | "dynamic_contract" ->
      DynamicContract
        { id =
            ( match import_contract_id_string "id" with
            | C_dynamic dyn -> dyn
            | _ -> assert false )
        ; tstorage = import_type_string "tcontract"
        ; tparameter = import_type_string "tparameter"
        ; line_no = Some (int "line_no") }
  | "flag" ->
    begin
      match Config.parse_flag (string_list "flag") with
      | None -> assert false
      | Some (flag, _) -> Add_flag {flag; line_no = Some (int "line_no")}
    end
  | action -> failwith ("Unknown action: '" ^ action ^ "'")

let load_from_string ~primitives config j =
  let j_actions = Yojson.Basic.Util.member "scenario" j in
  let shortname = Yojson.Basic.Util.(to_string (member "shortname" j)) in
  let decompile = Yojson.Basic.Util.(to_bool (member "decompile" j)) in
  let typing_env = Typing.init_env () in
  let scenario_state = Basics.scenario_state () in
  let kind =
    match Yojson.Basic.Util.(to_string (member "kind" j)) with
    | "test" -> Test
    | "compilation" -> Compilation
    | s -> Printf.ksprintf failwith "Bad scenario kind: %s" s
  in
  let actions = ref [] in
  ( try
      List.iter
        (fun action ->
          actions :=
            action_of_json ~primitives ~scenario_state ~typing_env action
            :: !actions)
        (Yojson.Basic.Util.to_list j_actions)
    with
  | SmartExcept exn -> actions := Exception exn :: !actions
  | exn ->
      actions :=
        Exception [`Text (Printer.exception_to_string false exn)] :: !actions );
  (* TODO *)
  let s =
    { shortname
    ; actions = List.rev !actions
    ; flags = (if decompile then [Config.Decompile true] else [])
    ; kind }
  in
  let s = Checker.check_scenario config s in
  Solver.apply typing_env;
  let scenario = Closer.close_scenario s in
  {scenario; typing_env; scenario_state}
