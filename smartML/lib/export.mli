(* Copyright 2019-2021 Smart Chain Arena LLC. *)

val export_type : Type.t -> string
