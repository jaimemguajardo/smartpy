(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Control
module Literal = Literal

type vClass =
  | Storage
  | Local
  | Param
  | Iter
  | ListMap
  | MatchCons

type 'v record_f = (string * 'v) list [@@deriving eq, ord, show]

type binOpInfix =
  | BNeq
  | BEq
  | BAnd
  | BOr
  | BAdd
  | BSub
  | BDiv
  | BEDiv
  | BMul  of {overloaded : bool}
  | BMod
  | BLt
  | BLe
  | BGt
  | BGe
  | BLsl
  | BLsr
  | BXor
[@@deriving show, eq]

type binOpPrefix =
  | BMax
  | BMin
[@@deriving show, eq]

type micheline =
  | Int       of string
  | String    of string
  | Bytes     of string
  | Primitive of
      { name : string
      ; annotations : string list
      ; arguments : micheline list }
  | Sequence  of micheline list
[@@deriving eq, show, map, ord]

type 't inline_michelson =
  { name : string
  ; parsed : micheline
  ; typesIn : 't list
  ; typesOut : 't list }
[@@deriving eq, show, map, ord]

type hash_algo =
  | BLAKE2B
  | SHA256
  | SHA512
  | KECCAK
  | SHA3
[@@deriving show, eq]

val string_of_hash_algo : hash_algo -> string

type 'command entry_point =
  { channel : string
  ; paramsType : Type.t
  ; originate : bool
  ; body : 'command }
[@@deriving show, map, fold]

type 'command offchain_view =
  { name : string
  ; tparameter : Type.t option
  ; pure : bool
  ; body : 'command
  ; doc : string }
[@@deriving show {with_path = false}, map, fold, eq, ord]

type ('expr, 'glob, 'command) contract_f =
  { balance : 'expr
  ; storage : 'expr option
  ; baker : 'expr
  ; tstorage : Type.t
  ; tparameter : Type.t
  ; entry_points : 'command entry_point list
  ; entry_points_layout : Layout.t option
  ; unknown_parts : string option
  ; flags : Config.flag list
  ; global_variables : (string * 'glob) list
  ; metadata : (string * 'expr Utils.Misc.meta) list
  ; views : 'command offchain_view list }
[@@deriving show, map, fold]

type ('command, 'type_) lambda_f =
  { id : int
  ; name : string
  ; tParams : 'type_
  ; tResult : 'type_
  ; body : 'command
  ; clean_stack : bool }
[@@deriving eq, ord, show, map, fold]

type 'type_ prim0 =
  | ECst                of 'type_ Literal.f
  | EBalance
  | ESender
  | ESource
  | EChain_id
  | ENow
  | EAmount
  | ELevel
  | ESelf
  | ESelf_address
  | ETotal_voting_power
  | ESelf_entry_point   of string
  | ELocal              of string
  | EMetaLocal          of string
  | EGlobal             of string * Literal.static_id
  | EVariant_arg        of string
  | EIter               of string
  | EMatchCons          of string
  | ESaplingEmptyState  of {memo : int}
  | EAccount_of_seed    of {seed : string}
  | EContract_balance   of Literal.contract_id
  | EContract_baker     of Literal.contract_id
  | EContract_data      of Literal.contract_id
  | EContract_typed     of Literal.contract_id
  | EScenario_var       of int * 'type_
[@@deriving eq, ord, show, map, fold]

type 'type_ prim1 =
  | EAbs
  | EConcat_list
  | EContract_address
  | EProject          of int
  | EHash_key
  | EImplicit_account
  | EIsNat
  | EListElements     of bool
  | EListItems        of bool
  | EListKeys         of bool
  | EListRev
  | EListValues       of bool
  | ENeg
  | ENot
  | EPack
  | EReduce
  | ESetDelegate
  | ESign
  | ESize
  | ESum
  | EToInt
  | EUnpack           of 'type_
  | EHash             of hash_algo
  | EType_annotation  of 'type_
  | EAttr             of string
  | EVariant          of string
  | EIsVariant        of string
  | EReadTicket
  | EJoinTickets
  | EPairingCheck
  | EVotingPower
[@@deriving eq, ord, show, map, fold]

type 'type_ prim2 =
  | EGetOpt
  | EBinOpInf    of binOpInfix
  | EBinOpPre    of binOpPrefix
  | EContains
  | ECallLambda
  | EApplyLambda
  | ECons
  | EAdd_seconds
  | ETicket
  | ESplitTicket
  | ETest_ticket of Literal.contract_id
[@@deriving eq, ord, show, map, fold]

type 'type_ prim3 =
  | ESplit_tokens
  | ERange
  | ECheck_signature
  | EUpdate_map
  | EGet_and_update
  | EIf
[@@deriving eq, ord, show, map, fold]

type ('expr, 'command, 'type_) expr_f =
  | EPrim0               of 'type_ prim0
  | EPrim1               of 'type_ prim1 * 'expr
  | EPrim2               of 'type_ prim2 * 'expr * 'expr
  | EPrim3               of 'type_ prim3 * 'expr * 'expr * 'expr
  | EOpenVariant         of string * 'expr * 'expr option
  | EItem                of
      { items : 'expr
      ; key : 'expr
      ; default_value : 'expr option
      ; missing_message : 'expr option }
  | ETuple               of 'expr list
  | ERecord              of (string * 'expr) list
  | EList                of 'expr list
  | EMap                 of bool * ('expr * 'expr) list
  | ESet                 of 'expr list
  | ESaplingVerifyUpdate of
      { state : 'expr
      ; transaction : 'expr }
  | EMichelson           of 'type_ inline_michelson * 'expr list
  | EMapFunction         of
      { f : 'expr
      ; l : 'expr }
  | ELambda              of ('command, 'type_) lambda_f
  | ELambdaParams        of
      { id : int
      ; name : string }
  | ECreate_contract     of
      { baker : 'expr
      ; contract_template : ('expr, 'expr, 'command) contract_f }
  | EContract            of
      { entry_point : string option
      ; arg_type : 'type_
      ; address : 'expr }
  | ESlice               of
      { offset : 'expr (* nat *)
      ; length : 'expr (* nat *)
      ; buffer : 'expr }
  | EMake_signature      of
      { secret_key : 'expr
      ; message : 'expr
      ; message_format : [ `Raw | `Hex ] }
  | ETransfer            of
      { arg : 'expr
      ; amount : 'expr
      ; destination : 'expr }
  | EMatch               of 'expr * (string * 'expr) list
[@@deriving eq, ord, show, map, fold]

type record_field_binding =
  { var : string
  ; field : string }
[@@deriving eq, ord, show]

type pattern =
  | Pattern_single of string
  | Pattern_tuple  of string list
  | Pattern_record of string * record_field_binding list
[@@deriving eq, ord, show]

type ('expr, 'command, 'type_) command_f =
  | CNever         of 'expr
  | CFailwith      of 'expr
  | CVerify        of 'expr * 'expr option (* message *)
  | CIf            of 'expr * 'command * 'command
  | CMatch         of 'expr * (string * string * 'command) list
  | CMatchProduct  of 'expr * pattern * 'command
  | CModifyProduct of 'expr * pattern * 'command
  | CMatchCons     of
      { expr : 'expr
      ; id : string
      ; ok_match : 'command
      ; ko_match : 'command }
  | CDefineLocal   of string * 'expr
  | CSetVar        of 'expr * 'expr
  | CDelItem       of 'expr * 'expr
  | CUpdateSet     of 'expr * 'expr * bool
  | CBind          of string option * 'command * 'command
  | CFor           of string * 'expr * 'command
  | CWhile         of 'expr * 'command
  | CResult        of 'expr
  | CComment       of string
  | CSetType       of 'expr * 'type_
  | CSetResultType of 'command * 'type_
  | CTrace         of 'expr
[@@deriving show, map, fold, eq, ord]

type line_no = int option [@@deriving eq, ord, show]

val string_of_line_no : line_no -> string

module Typed : sig
  type texpr =
    { e : (texpr, tcommand, Type.t) expr_f
    ; et : Type.t
    ; line_no : line_no }
  [@@deriving show, eq, ord]

  and tcommand =
    { c : (texpr, tcommand, Type.t) command_f
    ; ct : Type.t
    ; line_no : line_no }
  [@@deriving show, eq, ord]
end

open Typed

module Untyped : sig
  type expr =
    { e : (expr, command, Type.t) expr_f
    ; line_no : line_no }
  [@@deriving eq, ord, show]

  and command =
    { c : (expr, command, Type.t) command_f
    ; line_no : line_no }
  [@@deriving eq, ord, show]
end

open Untyped

val equal_expr_modulo_line_nos : expr -> expr -> bool

val equal_command_modulo_line_nos : command -> command -> bool

type ('e, 'c, 't) tsyntax_alg =
  { f_texpr : int option -> Type.t -> ('e, 'c, 't) expr_f -> 'e
  ; f_tcommand : int option -> Type.t -> ('e, 'c, 't) command_f -> 'c
  ; f_ttype : Type.t -> 't }

val cata_texpr : ('e, 'c, 't) tsyntax_alg -> texpr -> 'e

val cata_tcommand : ('e, 'c, 't) tsyntax_alg -> tcommand -> 'c

val monoid_talg : ('a -> 'a -> 'a) -> 'a -> ('a, 'a, 'a) tsyntax_alg

val para_texpr :
  (texpr * 'e, tcommand * 'c, Type.t * 't) tsyntax_alg -> texpr -> 'e

val para_tcommand :
  (texpr * 'e, tcommand * 'c, Type.t * 't) tsyntax_alg -> tcommand -> 'c

type ('e, 'c, 't) texpr_p = (texpr * 'e, tcommand * 'c, Type.t * 't) expr_f

type ('e, 'c, 't) tcommand_p =
  (texpr * 'e, tcommand * 'c, Type.t * 't) command_f

val para_talg :
     p_texpr:(line_no -> Type.t -> ('e, 'c, 't) texpr_p -> 'e)
  -> p_tcommand:(line_no -> Type.t -> ('e, 'c, 't) tcommand_p -> 'c)
  -> p_ttype:(Type.t -> 't)
  -> (texpr * 'e, tcommand * 'c, Type.t * 't) tsyntax_alg

type ('e, 'c, 't) syntax_alg =
  { f_expr : int option -> ('e, 'c, 't) expr_f -> 'e
  ; f_command : int option -> ('e, 'c, 't) command_f -> 'c
  ; f_type : Type.t -> 't }

val cata_expr : ('e, 'c, 't) syntax_alg -> expr -> 'e

val cata_command : ('e, 'c, 't) syntax_alg -> command -> 'c

val monoid_alg : ('a -> 'a -> 'a) -> 'a -> ('a, 'a, 'a) syntax_alg

type ('e, 'c, 't) expr_p = (expr * 'e, command * 'c, Type.t * 't) expr_f

type ('e, 'c, 't) command_p = (expr * 'e, command * 'c, Type.t * 't) command_f

val para_alg :
     p_expr:(line_no -> ('e, 'c, 't) expr_p -> 'e)
  -> p_command:(line_no -> ('e, 'c, 't) command_p -> 'c)
  -> p_type:(Type.t -> 't)
  -> (expr * 'e, command * 'c, Type.t * 't) syntax_alg

val para_expr : (expr * 'e, command * 'c, Type.t * 't) syntax_alg -> expr -> 'e

val para_command :
  (expr * 'e, command * 'c, Type.t * 't) syntax_alg -> command -> 'c

type lambda = (tcommand, Type.t) lambda_f [@@deriving eq, ord, show]

type 'v operation =
  | Transfer       of
      { arg : 'v
      ; destination : 'v
      ; amount : 'v }
  | SetDelegate    of 'v option
  | CreateContract of
      { id : Literal.contract_id
      ; baker : 'v
      ; contract : ('v, texpr, tcommand) contract_f }
[@@deriving eq, ord, show, map, fold]

type 'v value_f =
  | Literal   of Literal.t
  | Record    of (string * 'v) list
  | Variant   of string * 'v
  | List      of 'v list
  | Set       of 'v list
  | Map       of ('v * 'v) list
  | Tuple     of 'v list
  | Closure   of lambda * 'v list
  | Operation of 'v operation
  | Ticket    of Literal.address * 'v * Bigint.t
[@@deriving eq, ord, show, map, fold]

type tvalue =
  { v : tvalue value_f
  ; vt : Type.t }
[@@deriving show]

val equal_tvalue : tvalue -> tvalue -> bool

type tmessage =
  { channel : string
  ; params : tvalue }
[@@deriving show]

type contract = {contract : (expr, expr, command) contract_f} [@@deriving show]

type tcontract = {tcontract : (texpr, texpr, tcommand) contract_f}
[@@deriving show]

type value_contract = {value_contract : (tvalue, expr, command) contract_f}
[@@deriving show]

type value_tcontract = {value_tcontract : (tvalue, texpr, tcommand) contract_f}
[@@deriving show]

type smart_except =
  [ `Expr of texpr
  | `Exprs of texpr list
  | `Expr_untyped of expr
  | `Value of tvalue
  | `Literal of Literal.t
  | `Line of line_no
  | `Text of string
  | `Type of Type.t
  | `Br
  | `Rec of smart_except list
  ]
[@@deriving show]

type typing_constraint =
  | HasAdd           of texpr * texpr * texpr
  | HasMul           of texpr * texpr * texpr * bool (* overloaded *)
  | HasSub           of texpr * texpr * texpr
  | HasDiv           of texpr * texpr * texpr
  | HasBitArithmetic of texpr * texpr * texpr
  | HasMap           of texpr * texpr * texpr
  | IsComparable     of texpr
  | HasGetItem       of texpr * texpr * Type.t
  | HasContains      of texpr * texpr * line_no
  | HasSize          of texpr
  | HasSlice         of texpr
  | AssertEqual      of Type.t * Type.t * (unit -> smart_except list)
  | IsInt            of Type.t * (unit -> smart_except list)
  | RowHasEntry      of
      { kind : [ `Record | `Variant ]
      ; t : Type.t
      ; label : string
      ; entry : Type.t }
  | SaplingVerify    of texpr * texpr
  | HasNeg           of texpr * Type.t
  | HasInt           of texpr
  | IsNotHot         of string * Type.t
[@@deriving show]

(** Contract execution results, see also {!Contract.execMessageInner}. *)
module Execution : sig
  (** Execution errors, see also the {!Error} module. *)
  type error =
    | Exec_error             of smart_except list
    | Exec_channel_not_found of string
    | Exec_wrong_condition   of texpr * line_no * tvalue option
  [@@deriving show]

  type step =
    { command : tcommand
    ; iters : (string * (tvalue * string option)) list
    ; locals : (string * tvalue) list
    ; storage : tvalue
    ; balance : Bigint.t
    ; operations : string list
    ; substeps : step list ref
    ; elements : (string * tvalue) list }
  [@@deriving show]

  type 'html exec_message =
    { ok : bool
    ; contract : value_tcontract option
    ; operations : tvalue operation list
    ; error : error option
    ; html : 'html
    ; storage : tvalue
    ; steps : step list }
  [@@deriving show]
end

exception SmartExcept of smart_except list

val setEqualUnknownOption :
  pp:(unit -> smart_except list) -> 'a Unknown.t ref -> 'a Unknown.t ref -> unit

type scenario_state =
  { contracts : (Literal.contract_id, value_tcontract) Hashtbl.t
  ; variables : (int, tvalue) Hashtbl.t
  ; addresses : (Literal.contract_id, string) Hashtbl.t
  ; next_dynamic_address_id : int ref }

val scenario_state : unit -> scenario_state

val copy_scenario_state : scenario_state -> scenario_state

val get_parameter_type : tcontract -> string -> Type.t option

(* Checks whether there are exists given sub-expressions or sub-commands
   for which one of the given predicates hold.*)

type 'a exists_in =
     exclude_create_contract:bool
  -> (texpr -> bool)
  -> (tcommand -> bool)
  -> 'a
  -> bool

val exists_expr : texpr exists_in

val exists_command : tcommand exists_in

val exists_entry_point : tcommand entry_point exists_in

val exists_contract : ('v, texpr, tcommand) contract_f exists_in

type 'address account_or_address =
  | Account of Primitives.account
  | Address of 'address
[@@deriving show, map]

type ('expr, 'command) action_f =
  | New_contract    of
      { id : Literal.contract_id
      ; contract : ('expr, 'expr, 'command) contract_f
      ; line_no : line_no
      ; accept_unknown_types : bool
      ; baker : 'expr
      ; show : bool }
  | Compute         of
      { id : int
      ; expression : 'expr
      ; line_no : line_no }
  | Simulation      of
      { id : Literal.contract_id
      ; line_no : line_no }
  | Message         of
      { id : Literal.contract_id
      ; valid : 'expr
      ; params : 'expr
      ; line_no : line_no
      ; title : string
      ; messageClass : string
      ; source : 'expr account_or_address option
      ; sender : 'expr account_or_address option
      ; chain_id : 'expr option
      ; time : 'expr
      ; amount : 'expr
      ; level : 'expr
      ; voting_powers : 'expr
      ; message : string
      ; show : bool
      ; export : bool }
  | ScenarioError   of {message : string}
  | Html            of
      { tag : string
      ; inner : string
      ; line_no : line_no }
  | Verify          of
      { condition : 'expr
      ; line_no : line_no }
  | Show            of
      { expression : 'expr
      ; html : bool
      ; stripStrings : bool
      ; compile : bool
      ; line_no : line_no }
  | Exception       of smart_except list
  | Set_delegate    of
      { id : Literal.contract_id
      ; line_no : line_no
      ; baker : 'expr }
  | DynamicContract of
      { id : Literal.dynamic_id
      ; line_no : line_no
      ; tparameter : Type.t
      ; tstorage : Type.t }
  | Add_flag        of
      { flag : Config.flag
      ; line_no : line_no }
[@@deriving show]

type action = (expr, command) action_f

type taction = (texpr, tcommand) action_f

(** A scenario is a list of actions. *)
type scenario_kind =
  | Test
  | Compilation

type ('expr, 'command) scenario_f =
  { shortname : string
  ; actions : ('expr, 'command) action_f list
  ; flags : Config.flag list
  ; kind : scenario_kind }

type scenario = (expr, command) scenario_f

type tscenario = (texpr, tcommand) scenario_f

val map_action_contract : (tcontract -> tcontract) -> taction -> taction

val size_texpr : texpr -> int

val size_tcommand : tcommand -> int

val size_tcontract : ('v, 'e, tcommand) contract_f -> int

val erase_types_expr : texpr -> expr

val erase_types_command : tcommand -> command

val erase_types_contract : tcontract -> contract

val erase_types_value_contract : value_tcontract -> value_contract

val unerase_types_expr : expr -> texpr

val unerase_types_command : command -> tcommand

val unerase_types_contract : contract -> tcontract

val unerase_types_value_contract : value_contract -> value_tcontract

val layout_records_expr : texpr -> texpr

val layout_records_command : tcommand -> tcommand

val layout_records_contract : value_tcontract -> value_tcontract

module Syntax (M : MONAD) : sig
  open M

  val sequence_meta : 'a t Utils.Misc.meta -> 'a Utils.Misc.meta t

  val sequence_command_f :
    ('e t, 'c t, 't) command_f -> ('e, 'c, 't) command_f t

  val sequence_expr_f : ('e t, 'c t, 't) expr_f -> ('e, 'c, 't) expr_f t

  val mapM_action_contract :
       (tcontract -> tcontract t)
    -> (texpr, tcommand) action_f
    -> (texpr, tcommand) action_f t

  type ('e, 'c, 't) malg =
    { fm_expr : int option -> ('e, 'c, 't) expr_f -> 'e t
    ; fm_command : int option -> ('e, 'c, 't) command_f -> 'c t
    ; fm_type : Type.t -> 't }

  val cataM_expr : ('e, 'c, 't) malg -> expr -> 'e t

  val cataM_command : ('e, 'c, 'c) malg -> command -> 'c t
end
