(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Untyped

type t = command

val ifte : line_no:line_no -> expr -> t -> t -> t

val ifteSome : line_no:line_no -> Expr_untyped.t -> t -> t -> t

val mk_match : line_no:line_no -> expr -> (string * string * t) list -> t

val sp_failwith : line_no:line_no -> expr -> t

val never : line_no:line_no -> expr -> t

val verify : line_no:line_no -> expr -> expr option -> t

val forGroup : line_no:line_no -> string -> expr -> t -> t

val whileLoop : line_no:line_no -> expr -> t -> t

val delItem : line_no:line_no -> expr -> expr -> t

val updateSet : line_no:line_no -> expr -> expr -> bool -> t

val set : line_no:line_no -> expr -> expr -> t

val defineLocal : line_no:line_no -> string -> expr -> t

val bind : line_no:line_no -> string option -> t -> t -> t

val seq : line_no:line_no -> t list -> t

val setType : line_no:line_no -> expr -> Type.t -> t

val result : line_no:line_no -> expr -> t

val mk_match_cons : line_no:line_no -> expr -> string -> t -> t -> t

val mk_match_product : line_no:line_no -> expr -> pattern -> t -> t

val mk_modify_product : line_no:line_no -> expr -> pattern -> t -> t

val comment : line_no:line_no -> string -> t

val set_result_type : line_no:line_no -> t -> Type.t -> t

val set_type : line_no:line_no -> expr -> Type.t -> t

val trace : line_no:line_no -> expr -> t
