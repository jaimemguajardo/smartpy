(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Type

type env =
  { unknowns : (string, Type.t) Hashtbl.t
  ; constraints : (line_no * typing_constraint) list ref }

let init_env () = {unknowns = Hashtbl.create 10; constraints = ref []}

let unknown importEnv i =
  if i = ""
  then failwith "empty unknown"
  else
    match Hashtbl.find_opt importEnv.unknowns i with
    | Some t -> t
    | None ->
        let r = unknown_raw (ref (UUnknown i)) in
        Hashtbl.add importEnv.unknowns i r;
        r

let intType isNat =
  match Unknown.getRefOption isNat with
  | None -> `Unknown
  | Some true -> `Nat
  | Some false -> `Int

let add_constraint ~line_no env c =
  env.constraints := (line_no, c) :: !(env.constraints)

let assertEqual ~line_no ~pp ~env t1 t2 =
  if unF t1 != unF t2
  then add_constraint ~line_no env (AssertEqual (t1, t2, pp))

let reset_constraints env = {env with constraints = ref []}
