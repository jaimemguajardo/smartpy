(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Basics
open Typed
open Control

let empty_env = String.Map.empty

let mk_env xs = String.Map.of_list (List.map (fun (x, t) -> (x, (t, 0))) xs)

let string_of_static_id Literal.{static_id} =
  Printf.sprintf "contract%d" static_id

let string_of_dynamic_id Literal.{dynamic_id} =
  Printf.sprintf "created_contract%d" dynamic_id

let string_of_contract_id = function
  | Literal.C_static id -> string_of_static_id id
  | Literal.C_dynamic id -> string_of_dynamic_id id

let string_of_scenario_var i = Printf.sprintf "scenario_var%d" i

module Action = struct
  type state =
    { vars : (Type.t * int) String.Map.t
    ; constraints : (line_no * typing_constraint) list
    ; inside_contract : bool
    ; config : Config.t }

  type _ t =
    | Return : 'a -> 'a t
    | Bind   : 'a t * ('a -> 'b t) -> 'b t
    | Set    : state -> unit t
    | Get : state t

  let rec run : type a. a t -> state -> a * state = function
    | Return x -> fun state -> (x, state)
    | Bind (x, f) ->
        fun state ->
          let x', state = run x state in
          run (f x') state
    | Set state -> fun _ -> ((), state)
    | Get -> fun state -> (state, state)

  let return x = Return x

  let bind x f = Bind (x, f)

  let set vars = Set vars

  let get = Get

  let map f x = bind x (fun x -> return (f x))

  let apply f x = bind f (fun f -> bind x (fun x -> return (f x)))
end

module ActionM = struct
  include Action
  include Monad (Action)
end

open ActionM

open Syntax (ActionM)

let get_vars =
  let* s = get in
  return s.vars

let get_config =
  let* s = get in
  return s.config

let modify f =
  let* s = get in
  set (f s)

let set_vars vars = modify (fun s -> {s with vars})

let set_config config = modify (fun s -> {s with config})

let set_inside_contract inside_contract =
  modify (fun s -> {s with inside_contract})

let modify_vars f = modify (fun s -> {s with vars = f s.vars})

let pp_bin ~line_no op x y () = [`Text op; `Exprs [x; y]; `Br; `Line line_no]

let pp_bin_op ~line_no op x y () =
  [`Expr x; `Text op; `Expr y; `Br; `Line line_no]

let add_constraint ~line_no c =
  modify (fun st -> {st with constraints = (line_no, c) :: st.constraints})

let assertEqual ~line_no ~pp t1 t2 =
  if Type.unF t1 != Type.unF t2
  then add_constraint ~line_no (AssertEqual (t1, t2, pp))
  else return ()

let checkComparable ~line_no x y =
  let pp = pp_bin ~line_no "comparison between" x y in
  let* _ = assertEqual ~line_no x.et y.et ~pp in
  let* _ = add_constraint ~line_no (IsComparable x) in
  let* _ = add_constraint ~line_no (IsComparable y) in
  return ()

let solve_constraints =
  let* s = get in
  Solver.run s.constraints;
  modify (fun s -> {s with constraints = []})

let err ~line_no msg =
  raise (SmartExcept [`Text "Declaration Error"; `Br; `Text msg; `Line line_no])

let add_var ~line_no name t =
  modify_vars (fun vars ->
      match String.Map.find_opt name vars with
      | None -> String.Map.add name (t, 0) vars
      | Some _ ->
          err ~line_no (Printf.sprintf "Variable name %S already in use." name))

let check_var ~line_no ?meta kind name =
  let* {vars; inside_contract} = get in
  match String.Map.find_opt name vars with
  | Some (t, _occs) when not inside_contract -> return t
  | Some (t, occs) ->
      let* () =
        if occs = 1 && not (meta = Some ())
        then add_constraint ~line_no (IsNotHot (name, t))
        else return ()
      in
      let* () = set_vars (String.Map.add name (t, occs + 1) vars) in
      return t
  | None ->
      err
        ~line_no
        (Format.asprintf "%s variable %S escapes its scope." kind name)

let remove_var ~line_no name =
  modify_vars (fun vars ->
      match String.Map.find_opt name vars with
      | None -> err ~line_no (Printf.sprintf "Missing variable %S." name)
      | Some _ -> String.Map.filter (fun x _ -> not (String.equal name x)) vars)

let scoped x =
  let* old_vars = get_vars in
  let* r = x in
  let* () = set_vars old_vars in
  return r

let scoped_no_occs x =
  let* old_vars = get_vars in
  let* () = set_vars (String.Map.map (fun (n, _) -> (n, 0)) old_vars) in
  let* r = x in
  let* () = set_vars old_vars in
  return r

let scoped_branch x =
  let* old_vars = get_vars in
  let* r = x in
  let* new_vars = get_vars in
  let* () = set_vars old_vars in
  return (String.Map.map snd (String.Map.intersect old_vars new_vars), r)

let max_occs b1 b2 =
  let f _ x y =
    match (x, y) with
    | Some (t, occs1), Some (_, occs2) -> Some (t, max occs1 occs2)
    | Some (t, occs), None | None, Some (t, occs) -> Some (t, occs)
    | None, None -> None
  in
  String.Map.merge f b1 b2

let no_new_occs ~line_no old_vars new_vars =
  let f name x y =
    match (x, y) with
    | Some (t, 0), Some (_, 1) ->
        Some (add_constraint ~line_no (IsNotHot (name, t)))
        (* TODO better error message *)
    | _ -> None
  in
  iter_list id String.Map.(values (merge f old_vars new_vars))

let for_variant ~line_no name t =
  let open Type in
  match name with
  | "None" ->
      let* _ =
        assertEqual ~line_no t Type.unit ~pp:(fun () ->
            [`Text "Argument to None must be unit."])
      in
      return (option (full_unknown ()))
  | "Some" -> return (option t)
  | "Left" -> return (tor t (full_unknown ()))
  | "Right" -> return (tor (full_unknown ()) t)
  | _ -> return (uvariant name t)

let check_command_f line_no c =
  let assertEqual = assertEqual ~line_no in
  let add_var = add_var ~line_no in
  let remove_var = remove_var ~line_no in
  match c with
  | CMatch (scrutinee, cases) ->
      let* scrutinee = scrutinee in
      let rt = Type.full_unknown () in
      let check_case (constructor, arg_name, body) =
        scoped_branch
          (let at = Type.full_unknown () in
           let* vt = for_variant ~line_no constructor at in
           let* _ =
             assertEqual scrutinee.et vt ~pp:(fun () ->
                 [ `Text "match scrutinee has incompatible type:"
                 ; `Expr scrutinee
                 ; `Br
                 ; `Text "Expected type:"
                 ; `Type vt
                 ; `Line line_no ])
           in
           let* _ = add_var arg_name at in
           let* body = body in
           let* _ =
             assertEqual body.ct rt ~pp:(fun () ->
                 [ `Text "match branch has type"
                 ; `Type body.ct
                 ; `Text "instead of"
                 ; `Type rt
                 ; `Line line_no ])
           in
           let* _ = remove_var arg_name in
           return (constructor, arg_name, body))
      in
      let* cases = map_list check_case cases in
      let* () =
        set_vars List.(fold_left max_occs String.Map.empty (map fst cases))
      in
      let c = CMatch (scrutinee, List.map snd cases) in
      return {c; ct = rt; line_no}
  | CMatchCons {expr; id; ok_match; ko_match} ->
      let* expr = expr in
      let at = Type.full_unknown () in
      let vt = Type.list at in
      let* _ =
        assertEqual expr.et vt ~pp:(fun () ->
            [ `Text "match list scrutinee is not a list:"
            ; `Expr expr
            ; `Br
            ; `Text "Expected type:"
            ; `Type vt
            ; `Line line_no ])
      in
      let* vars1, ok_match =
        scoped_branch
          (let t = Type.record_default_layout [("head", at); ("tail", vt)] in
           let* () = add_var id t in
           ok_match)
      in
      let* vars2, ko_match = scoped_branch ko_match in
      let* () = set_vars (max_occs vars1 vars2) in
      let* _ =
        assertEqual ok_match.ct ko_match.ct ~pp:(fun () ->
            [`Text "sp.match_cons: cannot unify branches"; `Line line_no])
      in
      let c = CMatchCons {expr; id; ok_match; ko_match} in
      return {c; ct = ok_match.ct; line_no}
  | CMatchProduct (scrutinee, Pattern_record (name, bs), c) ->
      if List.length bs < 1
      then
        raise
          (SmartExcept
             [`Text "Must match at least one record field"; `Line line_no]);
      let bs' = List.map (fun x -> (x, Type.full_unknown ())) bs in
      let vars = List.map (fun ({var}, t) -> (var, t)) bs' in
      let fields = List.map (fun ({field}, t) -> (field, t)) bs' in
      let* scrutinee = scrutinee in
      let t = Type.urecord fields in
      let* _ =
        assertEqual scrutinee.et t ~pp:(fun () ->
            [ `Text "match tuple scrutinee is not a record:"
            ; `Expr scrutinee
            ; `Br
            ; `Text "Expected type:"
            ; `Type t
            ; `Line line_no ])
      in
      let* c =
        scoped
          (let* () = iter_list (uncurry add_var) vars in
           c)
      in
      return
        { c = CMatchProduct (scrutinee, Pattern_record (name, bs), c)
        ; ct = c.ct
        ; line_no }
  | CMatchProduct (scrutinee, Pattern_tuple ns, c) ->
      if List.length ns < 2
      then
        raise
          (SmartExcept
             [ `Text "Matched tuple must have at least two elements"
             ; `Line line_no ]);
      let* scrutinee = scrutinee in
      let ts = List.map (fun n -> (n, Type.full_unknown ())) ns in
      let t = Type.tuple (List.map snd ts) in
      let* _ =
        assertEqual scrutinee.et t ~pp:(fun () ->
            [ `Text "match tuple scrutinee is not a tuple:"
            ; `Expr scrutinee
            ; `Br
            ; `Text "Expected type:"
            ; `Type t
            ; `Line line_no ])
      in
      let* c =
        scoped
          (let* () = iter_list (uncurry add_var) ts in
           c)
      in
      return
        {c = CMatchProduct (scrutinee, Pattern_tuple ns, c); ct = c.ct; line_no}
  | CModifyProduct (e, Pattern_record (name, bs), body) ->
      let* e = e in
      let t = Type.urecord [] in
      let* () =
        assertEqual e.et t ~pp:(fun () ->
            [ `Text "sp.match_record scrutinee is not a record:"
            ; `Expr e
            ; `Br
            ; `Text "Expected type:"
            ; `Type t
            ; `Line line_no ])
      in
      let* () = add_var name e.et in
      let* body = body in
      let* () =
        assertEqual body.ct Type.unit ~pp:(fun () ->
            [ `Text "sp.modify_record result type must not have an sp.result"
            ; `Line line_no ])
      in
      return
        { c = CModifyProduct (e, Pattern_record (name, bs), body)
        ; ct = Type.unit
        ; line_no }
  | CModifyProduct (lhs, Pattern_tuple vars, body) ->
      if List.length vars < 2
      then
        raise
          (SmartExcept
             [ `Text "sp.modify_tuple requires at least two arguments "
             ; `Line line_no ]);
      let* lhs = lhs in
      let vars' = List.map (fun v -> (v, Type.full_unknown ())) vars in
      let t = Type.tuple (List.map snd vars') in
      let* () =
        assertEqual lhs.et t ~pp:(fun () ->
            [ `Text "scrutinee is not a tuple:"
            ; `Expr lhs
            ; `Br
            ; `Text "Expected type:"
            ; `Type t
            ; `Line line_no ])
      in
      let* body =
        scoped
          (let* () = iter_list (uncurry add_var) vars' in
           body)
      in
      let* () =
        assertEqual body.ct t ~pp:(fun () ->
            [ `Text "sp.match_record result type:"
            ; `Type body.ct
            ; `Br
            ; `Text "Expected type:"
            ; `Type t
            ; `Line line_no ])
      in
      return
        { c = CModifyProduct (lhs, Pattern_tuple vars, body)
        ; ct = Type.unit
        ; line_no }
  | CModifyProduct (lhs, Pattern_single var, body) ->
      let* lhs = lhs in
      let* body =
        scoped
          (let* () = add_var var lhs.et in
           body)
      in
      let* () =
        assertEqual body.ct lhs.et ~pp:(fun () ->
            [ `Text "sp.match result type:"
            ; `Type body.ct
            ; `Br
            ; `Text "Expected type:"
            ; `Type lhs.et
            ; `Line line_no ])
      in
      return
        { c = CModifyProduct (lhs, Pattern_single var, body)
        ; ct = Type.unit
        ; line_no }
  | CDefineLocal (name, e) ->
      let* e = e in
      let* _ = add_var name e.et in
      let c = CDefineLocal (name, e) in
      return {c; ct = Type.unit; line_no}
  | CBind (None, c1, c2) ->
      let* c1 = c1 in
      let* c2 = c2 in
      return {c = CBind (None, c1, c2); ct = c2.ct; line_no}
  | CBind (Some x, c1, c2) ->
      let* c1 = c1 in
      let* _ = add_var x c1.ct in
      let* c2 = c2 in
      return {c = CBind (Some x, c1, c2); ct = c2.ct; line_no}
  | CFor (name, e, body) ->
      let* e = e in
      let t = Type.full_unknown () in
      let* _ =
        assertEqual e.et (Type.list t) ~pp:(fun () ->
            [ `Text
                (Printf.sprintf
                   "for (%s : %s) in"
                   (Printer.variable_to_string name Iter)
                   (Printer.type_to_string t))
            ; `Expr e
            ; `Line line_no ])
      in
      let* old_vars = get_vars in
      let* new_vars, body =
        scoped_branch
          (let* () = add_var name t in
           body)
      in
      let* () = no_new_occs ~line_no old_vars new_vars in
      let* _ =
        assertEqual body.ct Type.unit ~pp:(fun () ->
            [ `Text "for-loop body has non-unit type"
            ; `Type body.ct
            ; `Line line_no ])
      in
      let c = CFor (name, e, body) in
      return {c; ct = Type.unit; line_no}
  | CIf (cond, c1, c2) ->
      let* cond = cond in
      let* vars1, c1 = scoped_branch c1 in
      let* vars2, c2 = scoped_branch c2 in
      let* () = set_vars (max_occs vars1 vars2) in
      let* () =
        assertEqual cond.et Type.bool ~pp:(fun () ->
            [`Text "sp.if"; `Expr cond; `Text ":"; `Line line_no])
      in
      let* () =
        assertEqual c1.ct c2.ct ~pp:(fun () ->
            [`Text "sp.if: cannot unify branches"; `Line line_no])
      in
      return {c = CIf (cond, c1, c2); ct = c1.ct; line_no}
  | CSetResultType (c, t) ->
      let* c = c in
      let* () =
        assertEqual c.ct t ~pp:(fun () ->
            [ `Type c.ct
            ; `Text "is not compatible with type"
            ; `Type t
            ; `Line line_no ])
      in
      return {c = CSetResultType (c, t); ct = t; line_no}
  | CWhile (e, body) ->
      let* e = e in
      let* _ =
        assertEqual e.et Type.bool ~pp:(fun () ->
            [`Text "while"; `Expr e; `Line line_no])
      in
      let* old_vars = get_vars in
      let* new_vars, body = scoped_branch body in
      let* () = no_new_occs ~line_no old_vars new_vars in
      let* () =
        assertEqual body.ct Type.unit ~pp:(fun () ->
            [`Text "while body has non-unit type"; `Type body.ct; `Line line_no])
      in
      return {c = CWhile (e, body); ct = Type.unit; line_no}
  | CSetType (e, t) ->
      let* e = scoped_no_occs e in
      let* _ =
        assertEqual e.et t ~pp:(fun () ->
            [ `Expr e
            ; `Text "is not compatible with type"
            ; `Type t
            ; `Line line_no ])
      in
      return {c = CSetType (e, t); ct = Type.unit; line_no}
  | CSetVar (lhs, rhs) ->
      let* lhs = scoped_no_occs lhs in
      let* rhs = rhs in
      let* _ =
        assertEqual lhs.et rhs.et ~pp:(fun () ->
            [`Text "set"; `Expr lhs; `Text "="; `Expr rhs; `Line line_no])
      in
      return {c = CSetVar (lhs, rhs); ct = Type.unit; line_no}
  | c ->
      (* Commands that don't modify the context. *)
      let* c = sequence_command_f c in
      let* t =
        match c with
        | CMatch _ | CMatchCons _ | CMatchProduct _ | CModifyProduct _
         |CDefineLocal _ | CFor _ | CBind _ | CIf _ | CSetType _
         |CSetResultType _ | CWhile _ | CSetVar _ ->
            assert false (* Already treated above. *)
        | CVerify (e, _message) ->
            let* _ =
              assertEqual e.et Type.bool ~pp:(fun () ->
                  [`Text "not a boolean expression"; `Line line_no])
            in
            return Type.unit
        | CDelItem (x, y) ->
            let pp () =
              [ `Text "del"
              ; `Expr x
              ; `Text "["
              ; `Expr y
              ; `Text "]"
              ; `Line line_no ]
            in
            let* _ =
              assertEqual
                x.et
                (Type.map
                   ~big:(ref (Unknown.UnUnknown ""))
                   ~tkey:y.et
                   ~tvalue:(Type.full_unknown ()))
                ~pp
            in
            return Type.unit
        | CUpdateSet (x, y, add) ->
            let pp () =
              [ `Expr x
              ; `Text "."
              ; `Text (if add then "add" else "remove")
              ; `Text "("
              ; `Expr y
              ; `Text ")"
              ; `Br
              ; `Line line_no ]
            in
            let* _ = assertEqual x.et (Type.set ~telement:y.et) ~pp in
            return Type.unit
        | CFailwith _ -> return (Type.full_unknown ())
        | CNever e ->
            let* _ =
              assertEqual e.et Type.never ~pp:(fun () ->
                  [`Text "Bad type in sp.never"; `Expr e; `Line line_no])
            in
            return (Type.full_unknown ())
        | CResult e -> return e.et
        | CComment _ | CTrace _ -> return Type.unit
      in
      return {c; ct = t; line_no}

let check_contract_f_generic
    ( { tstorage
      ; tparameter
      ; entry_points
      ; entry_points_layout
      ; global_variables
      ; views } as c ) =
  scoped
    (let line_no = None in
     let* () = set_inside_contract true in
     let* () = set_vars String.Map.empty in
     let* global_variables = map_list sequence_snd global_variables in
     let check_entry_point (ep : _ entry_point) =
       scoped_no_occs
         (let* () =
            add_var ~line_no "__operations__" (Type.list Type.operation)
          in
          let* () = add_var ~line_no "__storage__" tstorage in
          let* () = add_var ~line_no "__parameter__" ep.paramsType in
          let* () =
            add_var ~line_no "__contract__" (Type.contract tparameter)
          in
          let* () =
            iter_list (fun (n, e) -> add_var ~line_no n e.et) global_variables
          in
          let* body = ep.body in
          let* _ =
            assertEqual ~line_no body.ct Type.unit ~pp:(fun () ->
                [ `Text "Entry point "
                ; `Text ep.channel
                ; `Text " has a sp.result."
                ; `Br
                ; `Line body.line_no ])
          in
          return {ep with body})
     in
     let check_view (ov : _ offchain_view) =
       scoped_no_occs
         (let* () = add_var ~line_no "__storage__" tstorage in
          let* () =
            match ov.tparameter with
            | None -> return ()
            | Some t -> add_var ~line_no "__parameter__" t
          in
          let* () =
            iter_list (fun (n, e) -> add_var ~line_no n e.et) global_variables
          in
          let* body = ov.body in
          return {ov with body})
     in
     let* entry_points = map_list check_entry_point entry_points in
     let* views = map_list check_view views in
     let t_full =
       let row =
         flip List.map entry_points (fun ep -> (ep.channel, ep.paramsType))
       in
       match row with
       | [] -> Type.unit
       | r -> Type.variant_default_layout r
     in
     let entry_points' = List.filter (fun ep -> ep.originate) entry_points in
     let t =
       let row =
         flip List.map entry_points' (fun ep -> (ep.channel, ep.paramsType))
       in
       match (entry_points_layout, row) with
       | _, [] -> Type.unit
       | None, r -> Type.variant_default_layout r
       | Some l, r -> Type.variant (ref (Unknown.UnValue l)) r
     in
     let* _ =
       assertEqual ~line_no:None tparameter t ~pp:(fun () ->
           [`Text "incompatible parameter type"])
     in
     let* () = set_inside_contract false in
     return
       ( { c with
           tstorage
         ; entry_points
         ; entry_points_layout
         ; global_variables
         ; views }
       , t_full
       , t ))

let check_contract_f_expr
    ~line_no ({balance; storage; baker; tstorage; metadata} as c) =
  let* balance = balance in
  let* baker = baker in
  let* _ =
    assertEqual ~line_no balance.et Type.token ~pp:(fun () ->
        [ `Expr balance
        ; `Text "is not a valid amount in sp.create_contract"
        ; `Line line_no ])
  in
  let* _ =
    assertEqual ~line_no baker.et (Type.option Type.key_hash) ~pp:(fun () ->
        [ `Expr baker
        ; `Text "is not a valid optional baker in sp.create_contract"
        ; `Line line_no ])
  in
  let* storage =
    match storage with
    | None -> return None
    | Some storage ->
        let* storage = storage in
        let* _ =
          assertEqual ~line_no storage.et tstorage ~pp:(fun () ->
              [ `Expr storage
              ; `Text "is not a valid storage in sp.create_contract"
              ; `Type tstorage
              ; `Line line_no ])
        in
        return (Some storage)
  in
  let* metadata =
    map_list
      (fun (x, y) ->
        let+ y = sequence_meta y in
        (x, y))
      metadata
  in
  return {c with balance; storage; tstorage; baker; metadata}

let check_contract_f_value
    ~line_no ({balance; storage; tstorage; baker; metadata} as c) =
  let* balance = balance in
  let* baker = baker in
  let* _ =
    assertEqual ~line_no balance.vt Type.token ~pp:(fun () ->
        [ `Value balance
        ; `Text "is not a valid amount in sp.create_contract"
        ; `Line line_no ])
  in
  let* _ =
    assertEqual ~line_no baker.vt (Type.option Type.key_hash) ~pp:(fun () ->
        [ `Value baker
        ; `Text "is not a valid optional baker in sp.create_contract"
        ; `Line line_no ])
  in
  let* storage =
    match storage with
    | None -> return None
    | Some storage ->
        let* storage = storage in
        let* _ =
          assertEqual ~line_no storage.vt tstorage ~pp:(fun () ->
              [ `Value storage
              ; `Text "is not a valid storage in sp.create_contract"
              ; `Type tstorage
              ; `Line line_no ])
        in
        return (Some storage)
  in
  let* metadata =
    map_list
      (fun (x, y) ->
        let+ y = sequence_meta y in
        (x, y))
      metadata
  in
  return {c with balance; storage; tstorage; baker; metadata}

let check_contract_f ~line_no c =
  let* c = check_contract_f_expr ~line_no c in
  check_contract_f_generic c

let check_expr_f line_no ee =
  let assertEqual = assertEqual ~line_no in
  let add_constraint = add_constraint ~line_no in
  let check_var = check_var ~line_no in
  let pp_bin = pp_bin ~line_no in
  let pp_bin_op = pp_bin_op ~line_no in
  match ee with
  | ELambda {id; name; tParams; body; tResult; clean_stack} ->
      scoped_no_occs
        (let* () =
           if clean_stack then set_vars String.Map.empty else return ()
         in
         let* () = add_var ~line_no (Format.sprintf "lambda %d" id) tParams in
         let* body = body in
         let* _ =
           assertEqual body.ct tResult ~pp:(fun () ->
               [ `Text "lambda result type"
               ; `Type body.ct
               ; `Text "is not"
               ; `Type tResult
               ; `Line line_no ])
         in
         return
           { e = ELambda {id; name; tParams; body; tResult; clean_stack}
           ; et = Type.lambda tParams tResult
           ; line_no })
  | ECreate_contract {baker; contract_template} ->
      let* config = get_config in
      let* baker = baker in
      let* _ =
        assertEqual
          baker.et
          (Type.option (Type.baker_type_of_protocol config.protocol))
          ~pp:(fun () ->
            [ `Expr baker
            ; `Text "is not a valid optional baker in sp.contract"
            ; `Line line_no ])
      in
      let* contract_template, _t_full, _t =
        check_contract_f ~line_no contract_template
      in
      return
        { e = ECreate_contract {baker; contract_template}
        ; et =
            Type.record_default_layout
              [("operation", Type.operation); ("address", Type.address)]
        ; line_no }
  | e ->
      (* Expressions that don't modify the context. *)
      let* e = sequence_expr_f e in
      let* et =
        match e with
        | ELambda _ | ECreate_contract _ ->
            assert false (* Already treated above. *)
        | EList xs ->
            let t = Type.full_unknown () in
            let item x =
              assertEqual x.et t ~pp:(fun () ->
                  [ `Text "bad type for list item"
                  ; `Expr x
                  ; `Text "is not a"
                  ; `Type t
                  ; `Line line_no ])
            in
            let* _ = iter_list item xs in
            return (Type.list t)
        | EPrim0 prim ->
          ( match prim with
          | EAmount -> return Type.token
          | ELevel -> return (Type.nat ())
          | EBalance -> return Type.token
          | EChain_id -> return Type.chain_id
          | ETotal_voting_power -> return (Type.nat ())
          | ECst (Contract (Local cid, None, t)) ->
              let n = string_of_contract_id cid ^ "_full" in
              let* t' = check_var "contract variable" n in
              let t' =
                match Type.getRepr t' with
                | TContract t ->
                  ( match Type.getRepr t with
                  | TVariant {row = [(_, t)]} -> t
                  | t -> F t )
                | t -> Type.F t
              in
              let* () =
                assertEqual t t' ~pp:(fun () ->
                    [`Text "sp.contract: incompatible type"; `Line line_no])
              in
              return (Type.contract t)
          | ECst (Contract (Local cid, Some ep, t)) ->
              let n = string_of_contract_id cid ^ "_full" in
              let* t' = check_var "contract variable" n in
              let t_ep = Type.full_unknown () in
              let* _ =
                assertEqual t t' ~pp:(fun () ->
                    [`Text "sp.contract: incompatible type"; `Line line_no])
              in
              let* _ =
                assertEqual (Type.uvariant ep t_ep) t ~pp:(fun () ->
                    [`Text "sp.contract: incompatible type"; `Line line_no])
              in
              return (Type.contract t_ep)
          | ECst x -> return (Literal.type_of x)
          | EGlobal (n, _id) -> check_var "Global var" n
          | EIter name -> check_var "Iterator" name
          | ELocal name -> check_var "Local" name
          | EMetaLocal name -> check_var ~meta:() "Meta Local" name
          | EMatchCons name -> check_var "Match list" name
          | ENow -> return Type.timestamp
          | ESaplingEmptyState {memo} -> return (Type.sapling_state (Some memo))
          | ESelf_address -> return Type.address
          | ESelf -> check_var "Self parameter" "__parameter_contract__"
          | ESelf_entry_point name ->
              let* t_contract = check_var "Self parameter" "__contract__" in
              let t_ep = Type.full_unknown () in
              let* _ =
                assertEqual
                  (Type.contract (Type.uvariant name t_ep))
                  t_contract
                  ~pp:(fun () ->
                    [ `Text "sp.self_entry_point: incompatible type"
                    ; `Line line_no ])
              in
              return (Type.contract t_ep)
          | ESender -> return Type.address
          | ESource -> return Type.address
          | EVariant_arg name -> check_var "Pattern" name
          | EScenario_var (id, _) ->
              let name = string_of_scenario_var id in
              check_var "Scenario var" name
          | EContract_data id ->
              let name = string_of_contract_id id ^ "_storage" in
              check_var "Contract var" name
          | EAccount_of_seed {seed = _} -> return Type.account
          | EContract_balance _ -> return Type.token
          | EContract_baker _ ->
              let* config = get_config in
              return (Type.option (Type.baker_type_of_protocol config.protocol))
          | EContract_typed id ->
              let name = string_of_contract_id id in
              check_var "Contract var" name )
        | EPrim1 (prim, x) ->
          ( match prim with
          | ENot ->
              let* _ =
                assertEqual x.et Type.bool ~pp:(fun () ->
                    [`Text "not"; `Expr x; `Line line_no])
              in
              return Type.bool
          | ESum ->
              let t = Type.intOrNat () in
              let* _ =
                assertEqual x.et (Type.list t) ~pp:(fun () ->
                    [`Text "sum"; `Expr x; `Line line_no])
              in
              return t
          | EReduce -> return x.et
          | ESetDelegate ->
              let* config = get_config in
              let* _ =
                assertEqual
                  x.et
                  (Type.option (Type.baker_type_of_protocol config.protocol))
                  ~pp:(fun () -> [`Text "not an optional hash"; `Line line_no])
              in
              return Type.operation
          | EUnpack t ->
              let pp () = [`Text "unpack"; `Expr x; `Type t; `Line line_no] in
              let* _ = assertEqual x.et Type.bytes ~pp in
              return (Type.option t)
          | EPack -> return Type.bytes
          | EHash_key ->
              let* _ =
                assertEqual Type.key x.et ~pp:(fun () ->
                    [`Text "sp.hash_key("; `Expr x; `Text ")"; `Line line_no])
              in
              return Type.key_hash
          | EHash algo ->
              let* _ =
                assertEqual Type.bytes x.et ~pp:(fun () ->
                    [ `Text
                        (Printf.sprintf
                           "sp.%s("
                           (String.lowercase_ascii (string_of_hash_algo algo)))
                    ; `Expr x
                    ; `Text ")"
                    ; `Line line_no ])
              in
              return Type.bytes
          | EImplicit_account ->
              let* _ =
                assertEqual x.et Type.key_hash ~pp:(fun () ->
                    [ `Text "sp.implicit_account("
                    ; `Expr x
                    ; `Text ")"
                    ; `Line line_no ])
              in
              return (Type.contract Type.unit)
          | EToInt ->
              let* _ = add_constraint (HasInt x) in
              return (Type.int ())
          | EIsNat ->
              let* _ =
                assertEqual x.et (Type.int ()) ~pp:(fun () ->
                    [`Text "sp.isNat"; `Expr x; `Line line_no])
              in
              return (Type.option (Type.nat ()))
          | ENeg ->
              let t = Type.full_unknown () in
              let* _ = add_constraint (HasNeg (x, t)) in
              return t
          | ESign ->
              let* _ =
                assertEqual x.et (Type.int ()) ~pp:(fun () ->
                    [`Text "sp.sign"; `Expr x; `Line line_no])
              in
              return (Type.int ())
          | EAbs ->
              let* _ =
                assertEqual x.et (Type.int ()) ~pp:(fun () ->
                    [`Text "abs"; `Expr x; `Line line_no])
              in
              return (Type.nat ())
          | EConcat_list ->
              let t = Type.full_unknown () in
              let pp expr () = [`Text "sp.concat"; `Expr expr; `Line line_no] in
              let* _ = assertEqual x.et (Type.list t) ~pp:(pp x) in
              return t
          | ESize ->
              let* _ = add_constraint (HasSize x) in
              return (Type.nat ())
          | EListRev ->
              let t = Type.full_unknown () in
              let* _ =
                assertEqual x.et (Type.list t) ~pp:(fun () ->
                    [`Expr x; `Text ".rev()"; `Line line_no])
              in
              return (Type.list t)
          | EListItems _rev ->
              let tkey = Type.full_unknown () in
              let tvalue = Type.full_unknown () in
              let* _ =
                assertEqual
                  x.et
                  (Type.map ~big:(ref (Unknown.UnValue false)) ~tkey ~tvalue)
                  ~pp:(fun () -> [`Expr x; `Text ".items()"; `Line line_no])
              in
              return (Type.list (Type.key_value tkey tvalue))
          | EListKeys _rev ->
              let tkey = Type.full_unknown () in
              let tvalue = Type.full_unknown () in
              let* _ =
                assertEqual
                  x.et
                  (Type.map ~big:(ref (Unknown.UnValue false)) ~tkey ~tvalue)
                  ~pp:(fun () -> [`Expr x; `Text ".keys()"; `Line line_no])
              in
              return (Type.list tkey)
          | EListValues _rev ->
              let tkey = Type.full_unknown () in
              let tvalue = Type.full_unknown () in
              let* _ =
                assertEqual
                  x.et
                  (Type.map ~big:(ref (Unknown.UnValue false)) ~tkey ~tvalue)
                  ~pp:(fun () -> [`Expr x; `Text ".values()"; `Line line_no])
              in
              return (Type.list tvalue)
          | EListElements _rev ->
              let telement = Type.full_unknown () in
              let* _ =
                assertEqual x.et (Type.set ~telement) ~pp:(fun () ->
                    [`Expr x; `Text ".elements()"; `Line line_no])
              in
              return (Type.list telement)
          | EContract_address ->
              let* _ =
                assertEqual
                  x.et
                  (Type.contract (Type.full_unknown ()))
                  ~pp:(fun () ->
                    [`Text "sp.to_address("; `Expr x; `Text ")"; `Line line_no])
              in
              return Type.address
          | EType_annotation t ->
              let pp () =
                [`Text "type annotation"; `Expr x; `Type t; `Line line_no]
              in
              let* _ = assertEqual x.et t ~pp in
              return t
          | EVariant name -> for_variant ~line_no name x.et
          | EIsVariant name ->
              let tvariant =
                match name with
                | "None" | "Some" -> Type.option (Type.full_unknown ())
                | _ -> Type.uvariant name (Type.full_unknown ())
              in
              let* _ =
                assertEqual x.et tvariant ~pp:(fun () ->
                    [ `Text "Incompatible variant types"
                    ; `Br
                    ; `Type x.et
                    ; `Br
                    ; `Text "and"
                    ; `Br
                    ; `Type tvariant
                    ; `Br
                    ; `Line line_no ])
              in
              return Type.bool
          | EProject i ->
              let t = Type.full_unknown () in
              let* _ =
                assertEqual x.et (Type.utuple i t) ~pp:(fun () ->
                    [ `Text "Attribute error"
                    ; `Expr x
                    ; `Text "has no component"
                    ; `Text (string_of_int i)
                    ; `Text "of type"
                    ; `Type t
                    ; `Br
                    ; `Line line_no ])
              in
              return t
          | EAttr name ->
              let t = Type.full_unknown () in
              let* _ =
                assertEqual
                  x.et
                  (Type.urecord [(name, t)])
                  ~pp:(fun () ->
                    [ `Text "Attribute error"
                    ; `Expr x
                    ; `Text "has no field"
                    ; `Text name
                    ; `Text "of type"
                    ; `Type t
                    ; `Br
                    ; `Line line_no ])
              in
              return t
          | EReadTicket ->
              let t = Type.full_unknown () in
              let* _ =
                assertEqual x.et (Type.ticket t) ~pp:(fun () ->
                    [`Expr x; `Text "is not a valid ticket"; `Line line_no])
              in
              let t_data = Type.tuple [Type.address; t; Type.nat ()] in
              return (Type.pair t_data x.et)
          | EJoinTickets ->
              let ticket_t = Type.ticket (Type.full_unknown ()) in
              let* _ =
                assertEqual x.et (Type.pair ticket_t ticket_t) ~pp:(fun () ->
                    [`Expr x; `Text "is not a valid ticket list"; `Line line_no])
              in
              return (Type.option ticket_t)
          | EPairingCheck ->
              let* _ =
                assertEqual
                  x.et
                  (Type.list (Type.pair Type.bls12_381_g1 Type.bls12_381_g2))
                  ~pp:(fun () ->
                    [ `Expr x
                    ; `Text "is not a valid list of BLS12 pairs"
                    ; `Line line_no ])
              in
              return Type.bool
          | EVotingPower ->
              let* config = get_config in
              let* _ =
                assertEqual
                  x.et
                  (Type.baker_type_of_protocol config.protocol)
                  ~pp:(fun () ->
                    [ `Expr x
                    ; `Text
                        "voting_power requires tz1[1-3] address as parameter"
                    ; `Line line_no ])
              in
              return (Type.nat ()) )
        | EOpenVariant (name, x, _missing_message) ->
            let et =
              match name with
              | "None" -> Type.unit
              | _ -> Type.full_unknown ()
            in
            let tv =
              match name with
              | "Some" | "None" -> Type.option et
              | _ -> Type.uvariant name et
            in
            let* _ =
              assertEqual x.et tv ~pp:(fun () ->
                  [ `Text "Variant error"
                  ; `Expr x
                  ; `Text "has no constructor"
                  ; `Text name
                  ; `Br
                  ; `Line line_no ])
            in
            return et
        | ELambdaParams {id} ->
            check_var "Lambda" (Format.sprintf "lambda %d" id)
        | EPrim2 (prim2, x, y) ->
          begin
            match prim2 with
            | EBinOpInf op ->
              ( match op with
              | BLe | BLt | BGe | BGt | BEq | BNeq ->
                  let* _ = checkComparable ~line_no x y in
                  return Type.bool
              | BAdd ->
                  let* _ = assertEqual x.et y.et ~pp:(pp_bin_op "+" x y) in
                  let e = {e; et = x.et; line_no} in
                  let* _ = add_constraint (HasAdd (e, x, y)) in
                  return x.et
              | BMul {overloaded} ->
                  let t = Type.full_unknown () in
                  let e = {e; et = t; line_no} in
                  let* _ = add_constraint (HasMul (e, x, y, overloaded)) in
                  if overloaded
                  then return t
                  else
                    let pp = pp_bin_op "*" x y in
                    let* _ = assertEqual x.et y.et ~pp in
                    let* _ = assertEqual e.et x.et ~pp in
                    return x.et
              | BLsl ->
                  let pp = pp_bin_op "<<" x y in
                  let* _ = assertEqual x.et y.et ~pp in
                  let* _ = assertEqual x.et (Type.nat ()) ~pp in
                  return x.et
              | BLsr ->
                  let pp = pp_bin_op ">>" x y in
                  let* _ = assertEqual x.et y.et ~pp in
                  let* _ = assertEqual x.et (Type.nat ()) ~pp in
                  return x.et
              | BMod ->
                  let pp = pp_bin_op "%" x y in
                  let* _ = assertEqual x.et y.et ~pp in
                  let* _ = assertEqual x.et (Type.intOrNat ()) ~pp in
                  return (Type.nat ())
              | BEDiv ->
                  let a = Type.full_unknown () in
                  let b = Type.full_unknown () in
                  let et = Type.option (Type.pair a b) in
                  let e = {e; et; line_no} in
                  let* _ = add_constraint (HasDiv (e, x, y)) in
                  return et
              | BDiv ->
                  let pp () =
                    [ `Text "in expression"
                    ; `Expr x
                    ; `Text "/"
                    ; `Expr y
                    ; `Br
                    ; `Text "Both expressions must be of type"
                    ; `Type (Type.nat ())
                    ; `Br
                    ; `Line line_no ]
                  in
                  let* _ = assertEqual x.et y.et ~pp in
                  let* _ = assertEqual x.et (Type.nat ()) ~pp in
                  return (Type.nat ())
              | BSub ->
                  let pp = pp_bin_op "-" x y in
                  let* _ = assertEqual x.et y.et ~pp in
                  let et = Type.full_unknown () in
                  let e = {e; et; line_no} in
                  let* _ = add_constraint (HasSub (e, x, y)) in
                  return et
              | BOr ->
                  let pp () =
                    [ `Text "bad type for or:"
                    ; `Expr x
                    ; `Text "|"
                    ; `Expr y
                    ; `Text "is not a nat or a bool"
                    ; `Br
                    ; `Line line_no ]
                  in
                  let* _ = assertEqual x.et y.et ~pp in
                  let e = {e; et = x.et; line_no} in
                  let* _ = add_constraint (HasBitArithmetic (e, x, y)) in
                  return x.et
              | BAnd ->
                  let pp () =
                    [ `Text "bad type for and:"
                    ; `Expr x
                    ; `Text "&"
                    ; `Expr y
                    ; `Text "is not a nat or a bool"
                    ; `Br
                    ; `Line line_no ]
                  in
                  let* _ = assertEqual x.et y.et ~pp in
                  let e = {e; et = x.et; line_no} in
                  let* _ = add_constraint (HasBitArithmetic (e, x, y)) in
                  return x.et
              | BXor ->
                  let pp () =
                    [ `Text "bad type for xor:"
                    ; `Expr x
                    ; `Text "^"
                    ; `Expr y
                    ; `Text "is not a nat or a bool"
                    ; `Br
                    ; `Line line_no ]
                  in
                  let* _ = assertEqual x.et y.et ~pp in
                  let e = {e; et = x.et; line_no} in
                  let* _ = add_constraint (HasBitArithmetic (e, x, y)) in
                  return x.et )
            | EBinOpPre BMax ->
                let pp = pp_bin "max" x y in
                let* _ = assertEqual x.et y.et ~pp in
                let* _ = assertEqual x.et (Type.intOrNat ()) ~pp in
                return x.et
            | EBinOpPre BMin ->
                let pp = pp_bin "min" x y in
                let* _ = assertEqual x.et y.et ~pp in
                let* _ = assertEqual x.et (Type.intOrNat ()) ~pp in
                return x.et
            | EContains ->
                let* _ = add_constraint (HasContains (x, y, line_no)) in
                return Type.bool
            | ECons ->
                let x, l = (x, y) in
                let* _ =
                  assertEqual l.et (Type.list x.et) ~pp:(fun () ->
                      [`Text "cannot cons"; `Exprs [x; l]; `Line line_no])
                in
                return l.et
            | EGetOpt ->
                let r = Type.full_unknown () in
                let pp () =
                  [ `Text "getOpt expects map with values of type"
                  ; `Type r
                  ; `Br
                  ; `Line line_no ]
                in
                let* _ =
                  assertEqual
                    x.et
                    (Type.map
                       ~big:(ref (Unknown.UnUnknown ""))
                       ~tkey:y.et
                       ~tvalue:r)
                    ~pp
                in
                return (Type.option r)
            | EAdd_seconds ->
                let t, s = (x, y) in
                let pp () =
                  [ `Text "sp.add_seconds("
                  ; `Exprs [t; s]
                  ; `Text ")"
                  ; `Line line_no ]
                in
                let* _ = assertEqual Type.timestamp t.et ~pp in
                let* _ = assertEqual (Type.int ()) s.et ~pp in
                return Type.timestamp
            | ECallLambda ->
                let lambda, parameter = (x, y) in
                let t = Type.full_unknown () in
                let* _ =
                  assertEqual
                    lambda.et
                    (Type.lambda parameter.et t)
                    ~pp:(fun () ->
                      [ `Expr lambda
                      ; `Text "does not apply to"
                      ; `Expr parameter
                      ; `Line line_no ])
                in
                return t
            | EApplyLambda ->
                let lambda, parameter = (x, y) in
                let t1 = Type.full_unknown () in
                let t2 = Type.full_unknown () in
                let* _ =
                  assertEqual
                    lambda.et
                    (Type.lambda (Type.pair parameter.et t1) t2)
                    ~pp:(fun () ->
                      [ `Expr lambda
                      ; `Text "does not apply to"
                      ; `Expr parameter
                      ; `Line line_no ])
                in
                return (Type.lambda t1 t2)
            | ETicket | ETest_ticket _ ->
                let content, amount = (x, y) in
                let* _ =
                  assertEqual amount.et (Type.nat ()) ~pp:(fun () ->
                      [ `Expr amount
                      ; `Text "is not a valid ticket amount"
                      ; `Line line_no ])
                in
                return (Type.ticket content.et)
            | ESplitTicket ->
                let ticket, decomposition = (x, y) in
                let t = Type.full_unknown () in
                let ticket_t = Type.ticket t in
                let* _ =
                  assertEqual ticket.et ticket_t ~pp:(fun () ->
                      [`Expr ticket; `Text "is not a ticket"; `Line line_no])
                in
                let* _ =
                  assertEqual
                    decomposition.et
                    (Type.pair (Type.nat ()) (Type.nat ()))
                    ~pp:(fun () ->
                      [ `Expr decomposition
                      ; `Text "is not a ticket decomposition (nat * nat) for"
                      ; `Expr ticket
                      ; `Line line_no ])
                in
                return (Type.option (Type.pair ticket_t ticket_t))
          end
        | EPrim3 (prim3, x, y, z) ->
          begin
            match prim3 with
            | ERange ->
                let a, b, step = (x, y, z) in
                let pp () =
                  [ `Text "Range takes integers, it cannot be applied to"
                  ; `Exprs [a; b; step]
                  ; `Br
                  ; `Line line_no ]
                in
                let* _ = assertEqual a.et b.et ~pp in
                let* _ = assertEqual a.et step.et ~pp in
                let* _ = add_constraint (IsInt (a.et, pp)) in
                return (Type.list a.et)
            | ECheck_signature ->
                let pk, signature, message = (x, y, z) in
                let pp e () =
                  [`Text "sp.check_signature"; `Expr e; `Line line_no]
                in
                let* _ = assertEqual Type.key pk.et ~pp:(pp pk) in
                let* _ =
                  assertEqual Type.signature signature.et ~pp:(pp signature)
                in
                let* _ = assertEqual Type.bytes message.et ~pp:(pp message) in
                return Type.bool
            | ESplit_tokens ->
                let mutez, quantity, total = (x, y, z) in
                let pp () =
                  [ `Text "sp.split_tokens("
                  ; `Exprs [mutez; quantity; total]
                  ; `Text ")"
                  ; `Line line_no ]
                in
                let* _ = assertEqual Type.token mutez.et ~pp in
                let* _ = assertEqual (Type.nat ()) quantity.et ~pp in
                let* _ = assertEqual (Type.nat ()) total.et ~pp in
                return Type.token
            | EUpdate_map ->
                let map, key, value = (x, y, z) in
                let tvalue = Type.full_unknown () in
                let pp () =
                  [ `Expr value
                  ; `Text "is of type"
                  ; `Type value.et
                  ; `Text ", but should be an option."
                  ; `Line line_no ]
                in
                let* _ = assertEqual value.et (Type.option tvalue) ~pp in
                let pp () =
                  [ `Text "Map is of type"
                  ; `Type map.et
                  ; `Text ", but used as map from"
                  ; `Type key.et
                  ; `Text " to "
                  ; `Type tvalue
                  ; `Line line_no ]
                in
                let big = ref (Unknown.UnUnknown "") in
                let mt = Type.map ~big ~tkey:key.et ~tvalue in
                let* _ = assertEqual map.et mt ~pp in
                return map.et
            | EGet_and_update ->
                let map, key, value = (x, y, z) in
                let tvalue = Type.full_unknown () in
                let pp () =
                  [ `Expr value
                  ; `Text "is of type"
                  ; `Type value.et
                  ; `Text ", but should be an option."
                  ; `Line line_no ]
                in
                let* _ = assertEqual value.et (Type.option tvalue) ~pp in
                let pp () =
                  [ `Text "Map is of type"
                  ; `Type map.et
                  ; `Text ", but used as map from"
                  ; `Type key.et
                  ; `Text " to "
                  ; `Type tvalue
                  ; `Line line_no ]
                in
                let big = ref (Unknown.UnUnknown "") in
                let mt = Type.map ~big ~tkey:key.et ~tvalue in
                let* _ = assertEqual map.et mt ~pp in
                return (Type.pair value.et map.et)
            | EIf ->
                let cond, a, b = (x, y, z) in
                let* _ =
                  assertEqual a.et b.et ~pp:(fun () ->
                      [ `Text "incompatible result types in if-then-else"
                      ; `Line line_no ])
                in
                let* _ =
                  assertEqual cond.et Type.bool ~pp:(fun () ->
                      [ `Text "non-boolean condition in if-then-else"
                      ; `Line line_no ])
                in
                return a.et
          end
        | ESlice {offset; length; buffer} ->
            let pp expr () = [`Text "sp.slice"; `Expr expr; `Line line_no] in
            let* _ = assertEqual offset.et (Type.nat ()) ~pp:(pp offset) in
            let* _ = assertEqual length.et (Type.nat ()) ~pp:(pp length) in
            let* _ = add_constraint (HasSlice buffer) in
            return (Type.option buffer.et)
        | EItem {items; key; default_value} ->
            let et = Type.full_unknown () in
            let* _ = add_constraint (HasGetItem (items, key, et)) in
            let* _ =
              match default_value with
              | None -> return ()
              | Some e ->
                  assertEqual e.et et ~pp:(fun () ->
                      [ `Expr items
                      ; `Text "bad type for default value."
                      ; `Expr e
                      ; `Br
                      ; `Line line_no ])
            in
            return et
        | ERecord entries ->
            let layout = ref (Unknown.UnUnknown "") in
            let row = List.map (map_snd (fun {et} -> et)) entries in
            return (Type.record_or_unit layout row)
        | EMap (big, entries) ->
            let tkey = Type.full_unknown () in
            let tvalue = Type.full_unknown () in
            let item (k, v) =
              let* _ =
                assertEqual k.et tkey ~pp:(fun () ->
                    [ `Text "bad type for map key"
                    ; `Expr k
                    ; `Text "is not a"
                    ; `Type tkey
                    ; `Br
                    ; `Line line_no ])
              in
              let* _ =
                assertEqual v.et tvalue ~pp:(fun () ->
                    [ `Text "bad type for map value"
                    ; `Expr v
                    ; `Text "is not a"
                    ; `Type tvalue
                    ; `Br
                    ; `Line line_no ])
              in
              return ()
            in
            let* _ = iter_list item entries in
            return (Type.map ~big:(ref (Unknown.UnValue big)) ~tkey ~tvalue)
        | ESet entries ->
            let telement = Type.full_unknown () in
            let check k =
              assertEqual k.et telement ~pp:(fun () ->
                  [ `Text "bad type for set element"
                  ; `Expr k
                  ; `Text "is not an element of"
                  ; `Type (Type.set ~telement)
                  ; `Br
                  ; `Line line_no ])
            in
            let* _ = iter_list check entries in
            return (Type.set ~telement)
        | ETransfer {arg; amount; destination} ->
            let pp () =
              [ `Text "transfer("
              ; `Exprs [arg; amount; destination]
              ; `Text ")"
              ; `Br
              ; `Line line_no ]
            in
            let* _ = assertEqual destination.et (Type.contract arg.et) ~pp in
            let* _ = assertEqual amount.et Type.token ~pp in
            return Type.operation
        | EMake_signature {secret_key; message} ->
            let pp e () = [`Text "sp.make_signature"; `Expr e; `Line line_no] in
            let* _ =
              assertEqual Type.secret_key secret_key.et ~pp:(pp secret_key)
            in
            let* _ = assertEqual Type.bytes message.et ~pp:(pp message) in
            return Type.signature
        | EContract {arg_type; address} ->
            let* _ =
              assertEqual address.et Type.address ~pp:(fun () ->
                  [`Expr address])
            in
            return (Type.option (Type.contract arg_type))
        | EMapFunction {l; f} as e ->
            let s = Type.full_unknown () in
            let t = Type.full_unknown () in
            let target = Type.full_unknown () in
            let* _ =
              assertEqual f.et (Type.lambda s t) ~pp:(fun () ->
                  [`Text "map"; `Expr f; `Line line_no])
            in
            let e = {e; et = target; line_no} in
            let* _ = add_constraint (HasMap (e, l, f)) in
            return target
        | EMichelson (michelson, exprs) ->
            let pp () =
              [ `Text "sp.michelson"
              ; `Text michelson.name
              ; `Exprs exprs
              ; `Line line_no ]
            in
            let rec matchTypes types name tin out =
              match (types, tin) with
              | _, [] -> return (out @ types)
              | [], _ ->
                  raise
                    (SmartExcept
                       [`Text "Empty stack at instruction"; `Rec (pp ())])
              | tt1 :: types, tt2 :: tin ->
                  let* _ = assertEqual tt1 tt2 ~pp in
                  matchTypes types name tin out
            in
            let* typesOut =
              let* types = return (List.map (fun e -> e.et) exprs) in
              matchTypes
                types
                michelson.name
                michelson.typesIn
                michelson.typesOut
            in
            let err message =
              raise (SmartExcept [`Text message; `Rec (pp ()); `Line line_no])
            in
            ( match typesOut with
            | [] -> err "Empty output types"
            | [t] -> return t
            | _ -> err "Too many output types" )
        | ETuple es -> return (Type.tuple (List.map (fun {et} -> et) es))
        | EMatch (scrutinee, clauses) ->
            let result_t = Type.full_unknown () in
            let* row =
              clauses
              |> map_list (fun (constructor, rhs) ->
                     let arg_t = Type.full_unknown () in
                     let* _ =
                       assertEqual
                         rhs.et
                         (Type.lambda arg_t result_t)
                         ~pp:(fun () ->
                           [ `Text "incompatible result types in match"
                           ; `Line line_no ])
                     in
                     return (constructor, arg_t))
            in
            let* _ =
              assertEqual
                scrutinee.et
                (Type.variant (ref (Unknown.UnUnknown "")) row)
                ~pp:(fun () -> [`Text "incoherent scrutinee type"])
            in
            return result_t
        | ESaplingVerifyUpdate {state; transaction} ->
            let* _ =
              assertEqual
                transaction.et
                (Type.sapling_transaction None)
                ~pp:(fun () ->
                  [ `Expr transaction
                  ; `Text "is expected to be a sapling transaction "
                  ; `Line line_no ])
            in
            let* _ =
              assertEqual state.et (Type.sapling_state None) ~pp:(fun () ->
                  [ `Expr state
                  ; `Text "is expected to be a sapling state "
                  ; `Line line_no ])
            in
            let* _ = add_constraint (SaplingVerify (state, transaction)) in
            return (Type.option (Type.pair (Type.int ()) state.et))
      in
      return {e; et; line_no}

let check = {f_expr = check_expr_f; f_command = check_command_f; f_type = id}

let check_command x =
  let* _ = return () in
  let* r = cata_command check x in
  return r

let check_expr = cata_expr check

let check_texpr x = check_expr (erase_types_expr x)

let check_tcommand x = check_command (erase_types_command x)

let check_contract {contract} =
  let c = map_contract_f check_expr check_expr check_command contract in
  let* tcontract, t_full, t = check_contract_f ~line_no:None c in
  return ({tcontract}, t_full, t)

let check_action = function
  | New_contract {id; contract; line_no; accept_unknown_types; baker; show} ->
      let* contract, t_full, t = check_contract {contract} in
      let* baker = check_expr baker in
      let pp () = [`Text "scenario contract baker"; `Type baker.et] in
      let* _ = assertEqual ~line_no baker.et (Type.option Type.key_hash) ~pp in
      let* () = add_var ~line_no (string_of_contract_id id) (Type.contract t) in
      let* () =
        add_var
          ~line_no
          (string_of_contract_id id ^ "_full")
          (Type.contract t_full)
      in
      let* () =
        add_var
          ~line_no
          (string_of_contract_id id ^ "_storage")
          contract.tcontract.tstorage
      in
      return
        (New_contract
           { id
           ; contract = contract.tcontract
           ; line_no
           ; accept_unknown_types
           ; baker
           ; show })
  | Compute {id; expression; line_no} ->
      let* expression = check_expr expression in
      let* () = add_var ~line_no (string_of_scenario_var id) expression.et in
      return (Compute {id; expression; line_no})
  | Simulation {id; line_no} -> return (Simulation {id; line_no})
  | Message
      { id
      ; valid
      ; params
      ; line_no
      ; title
      ; messageClass
      ; source
      ; sender
      ; chain_id
      ; time
      ; amount
      ; level
      ; voting_powers
      ; message
      ; show
      ; export } ->
      let* valid = check_expr valid in
      let* params = check_expr params in
      let* voting_powers = check_expr voting_powers in
      let check_aa = function
        | Address a ->
            let* a = check_expr a in
            return (Address a)
        | Account a -> return (Account a)
      in
      let* source = map_option check_aa source in
      let* sender = map_option check_aa sender in
      let* chain_id = map_option check_expr chain_id in
      let* time = check_expr time in
      let* amount = check_expr amount in
      let* level = check_expr level in
      let id' = string_of_contract_id id in
      let* c =
        match id with
        | C_dynamic _ ->
            return
              (Type.contract
                 (Type.variant_default_layout [(message, params.et)]))
        | _ -> check_var ~line_no "contract id" (id' ^ "_full")
      in
      let tparameter =
        match c with
        | F (TContract tparameter) -> tparameter
        | _ ->
            raise
              (SmartExcept [`Text "Not a contract: "; `Text id'; `Line line_no])
      in
      let entry = Type.full_unknown () in
      let* _ =
        add_constraint
          ~line_no
          (RowHasEntry {kind = `Variant; t = tparameter; label = message; entry})
      in
      let pp () =
        [ `Text "entry point expects parameter"
        ; `Type entry
        ; `Text " but got "
        ; `Type params.et
        ; `Line line_no ]
      in
      let* _ = assertEqual ~line_no params.et entry ~pp in
      return
        (Message
           { id
           ; valid
           ; params
           ; line_no
           ; title
           ; messageClass
           ; source
           ; sender
           ; chain_id
           ; time
           ; amount
           ; level
           ; voting_powers
           ; message
           ; show
           ; export })
  | ScenarioError {message} -> return (ScenarioError {message})
  | Html {tag; inner; line_no} -> return (Html {tag; inner; line_no})
  | Verify {condition; line_no} ->
      let* condition = check_expr condition in
      let pp () = [`Text "not a boolean expression"; `Line line_no] in
      let* _ = assertEqual ~line_no condition.et Type.bool ~pp in
      return (Verify {condition; line_no})
  | Show {expression; html; stripStrings; compile; line_no} ->
      let* expression = check_expr expression in
      return (Show {expression; html; stripStrings; compile; line_no})
  | Exception es -> return (Exception es)
  | Add_flag flag ->
      let* config = get_config in
      let* () = set_config (Config.apply_flag flag.flag config) in
      return (Add_flag flag)
  | Set_delegate {id; line_no; baker} ->
      let* baker = check_expr baker in
      let id' = string_of_contract_id id in
      let* _ = check_var ~line_no "contract id" id' in
      let pp () = [`Text "scenario set_delegate"; `Type baker.et] in
      let* config = get_config in
      let* _ =
        assertEqual
          ~line_no:None
          baker.et
          (Type.option (Type.baker_type_of_protocol config.protocol))
          ~pp
      in
      return (Set_delegate {id; line_no; baker})
  | DynamicContract {id; line_no; tparameter; tstorage} ->
      let* () =
        add_var ~line_no (string_of_dynamic_id id) (Type.contract tparameter)
      in
      let* () =
        add_var ~line_no (string_of_dynamic_id id ^ "_storage") tstorage
      in
      return (DynamicContract {id; line_no; tparameter; tstorage})

let check_value_tcontract {value_tcontract = c} =
  let c = map_contract_f return check_texpr check_tcommand c in
  let* c = check_contract_f_value ~line_no:None c in
  let* value_tcontract, t_full, t = check_contract_f_generic c in
  return ({value_tcontract}, t_full, t)

let default_if condition ~line_no =
  let open Type in
  function
  | F (TUnknown {contents = UUnknown _}) as t when condition ->
      let* _ =
        assertEqual ~line_no t Type.unit ~pp:(fun () -> [`Text "defaulter"])
      in
      return Type.unit
  | t -> return t

let is_storage = function
  | {e = EPrim0 (ELocal "__storage__"); et = _} -> true
  | _ -> false

let is_param : texpr -> bool = function
  | {e = EPrim0 (ELocal "__parameter__"); et = _} -> true
  | _ -> false

let rec default_contract {tcontract = c} =
  let uses_storage =
    exists_contract ~exclude_create_contract:true is_storage (fun _ -> false) c
  in
  let* tstorage = default_if (not uses_storage) ~line_no:None c.tstorage in
  let* tparameter =
    default_if (c.entry_points = []) ~line_no:None c.tparameter
  in
  let* entry_points = map_list default_entry_point c.entry_points in
  let* global_variables =
    let f (n, e) =
      let* e = cata_texpr default_alg e in
      return (n, e)
    in
    map_list f c.global_variables
  in
  let* _ = solve_constraints in
  let c = {c with tstorage; tparameter; entry_points; global_variables} in
  match (c.storage, Type.getRepr c.tstorage) with
  | None, Type.TUnit -> return {tcontract = {c with storage = Some Expr.unit}}
  | _ -> return {tcontract = c}

and default_entry_point (ep : _ entry_point) =
  let uses_param =
    exists_command
      ~exclude_create_contract:true
      is_param
      (fun _ -> false)
      ep.body
  in
  let* paramsType = default_if (not uses_param) ~line_no:None ep.paramsType in
  let* body = cata_tcommand default_alg ep.body in
  return {ep with paramsType; body}

and default_alg =
  let f_texpr line_no et e =
    let* e = sequence_expr_f e in
    match e with
    | ECreate_contract {baker; contract_template = ct} ->
        let* ct = default_contract {tcontract = ct} in
        return
          { e = ECreate_contract {baker; contract_template = ct.tcontract}
          ; et
          ; line_no }
    | e -> return {e; et; line_no}
  in
  let f_tcommand line_no ct c =
    let* c = sequence_command_f c in
    return {c; ct; line_no}
  in
  {f_texpr; f_tcommand; f_ttype = (fun t -> t)}

let default_action = mapM_action_contract default_contract

(** {1 Non-monadic interface for the outside world} *)

let exec_with config f vars x =
  fst (run (f x) {vars; constraints = []; inside_contract = false; config})

let check_expr config vars =
  exec_with
    config
    (fun e ->
      let* e = check_expr e in
      let* () = solve_constraints in
      return e)
    (mk_env vars)

let check_command config vars =
  exec_with
    config
    (fun c ->
      let* c = check_command c in
      let* () = solve_constraints in
      return c)
    (mk_env vars)

let check_value_tcontract config =
  exec_with
    config
    (fun c ->
      let* c = check_value_tcontract c in
      let* () = solve_constraints in
      return c)
    empty_env

let check_contract config =
  exec_with
    config
    (fun c ->
      let* c, t_full, t = check_contract c in
      let* () = solve_constraints in
      let* c = default_contract c in
      let* () = solve_constraints in
      return (c, t_full, t))
    empty_env

let check_scenario config =
  exec_with
    config
    (fun s ->
      let* actions = map_list check_action s.actions in
      let* () = solve_constraints in
      let* actions = map_list default_action actions in
      return {s with actions})
    empty_env
