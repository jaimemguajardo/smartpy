(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Basics
open Typed
open Untyped

(** {1 Target Printer Options} *)

module Options : sig
  type t =
    { html : bool
    ; stripStrings : bool
    ; stripped : bool
    ; types : bool
    ; analysis : bool }

  val string : t

  val html : t

  val htmlStripStrings : t

  val types : t

  val analysis : t
end

(** {1 Types} *)

val type_to_string : ?toplevel:unit -> ?options:Options.t -> Type.t -> string

val pp_row : ?options:Options.t -> (string * Type.t) list -> string

(** {1 Values} *)

val html_of_data : Options.t -> tvalue -> string

val value_to_string :
  ?deep:bool -> ?noEmptyList:bool -> ?options:Options.t -> tvalue -> string

val literal_to_string : html:bool -> ?strip_strings:unit -> Literal.t -> string

val ppAmount : bool -> Bigint.t -> string

(** {1 Expressions, Commands, Contracts} *)

val layout_to_string : Layout.t Unknown.t ref -> string

val expr_to_string : ?options:Options.t -> ?protect:unit -> expr -> string

val texpr_to_string : ?options:Options.t -> ?protect:unit -> texpr -> string

val variable_to_string :
  ?options:Options.t -> ?protect:unit -> string -> vClass -> string

val tvariable_to_string :
  ?options:Options.t -> ?protect:unit -> string * Type.t -> vClass -> string

val command_to_string :
  ?indent:string -> ?options:Options.t -> command -> string

val tcommand_to_string :
  ?indent:string -> ?options:Options.t -> tcommand -> string

val contract_to_string : ?options:Options.t -> value_contract -> string

val tcontract_to_string : ?options:Options.t -> value_tcontract -> string

val pp_contract :
  ?options:Options.t -> Format.formatter -> value_contract -> unit

val pp_tcontract :
  ?options:Options.t -> Format.formatter -> value_tcontract -> unit

val html_of_record_list : string list * 'a list list -> ('a -> string) -> string

(** {1 Operations} *)

val operation_to_string : ?options:Options.t -> tvalue operation -> string

(** {1 Exceptions} *)

val error_to_string : ?options:Options.t -> Execution.error -> string

val exception_to_string : bool -> exn -> string

val pp_smart_except : bool -> Basics.smart_except list -> string

val string_of_contract_id : Literal.contract_id -> string

val address_of_contract_id :
  html:bool -> Literal.contract_id -> string option -> string
