(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils

type protocol =
  (* Order is IMPORTANT *)
  | Delphi (* Proto 7 *)
  | Edo (* Proto 8 *)
  | Florence (* Proto 9 *)
  | Proto10
[@@deriving eq, ord, show {with_path = false}]

type lazy_entry_points =
  | Single
  | Multiple
[@@deriving eq, ord, show {with_path = false}]

type exceptions =
  | FullDebug
  | Message
  | VerifyOrLine
  | DefaultLine
  | Line
  | DefaultUnit
  | Unit
[@@deriving eq, ord, show {with_path = false}]

type t =
  { simplify_via_michel : bool
  ; decompile : bool
  ; erase_comments : bool
  ; disable_dup_check : bool
  ; protocol : protocol
  ; lazy_entry_points : lazy_entry_points option
  ; exceptions : exceptions }
[@@deriving eq, show {with_path = false}]

type flag =
  | Simplify_via_michel of bool
  | Decompile           of bool
  | Erase_comments      of bool
  | Disable_dup_check   of bool
  | Protocol            of protocol
  | Lazy_entry_points   of lazy_entry_points option
  | Exceptions          of exceptions
[@@deriving eq, ord, show {with_path = false}]

let parse_bool_flag b = function
  | "simplify-via-michel" :: xs -> Some (Simplify_via_michel b, xs)
  | "decompile" :: xs -> Some (Decompile b, xs)
  | "erase-comments" :: xs -> Some (Erase_comments b, xs)
  | "disable-dup-check" :: xs -> Some (Disable_dup_check b, xs)
  | _ -> None

let parse_bool_flag = function
  | x :: xs ->
      if String.sub x 0 3 = "no-"
      then
        let x = String.sub x 3 (String.length x - 3) in
        parse_bool_flag false (x :: xs)
      else parse_bool_flag true (x :: xs)
  | _ -> None

let parse_flag = function
  | "protocol" :: "delphi" :: xs -> Some (Protocol Delphi, xs)
  | "protocol" :: "edo" :: xs -> Some (Protocol Edo, xs)
  | "protocol" :: "florence" :: xs -> Some (Protocol Florence, xs)
  | "protocol" :: "proto10" :: xs -> Some (Protocol Proto10, xs)
  | "protocol" :: protocol :: _xs ->
      Printf.ksprintf failwith "Unknown protocol: %S" protocol
  | "lazy-entry-points" :: "none" :: xs -> Some (Lazy_entry_points None, xs)
  | "lazy-entry-points" :: "single" :: xs ->
      Some (Lazy_entry_points (Some Single), xs)
  | "lazy-entry-points" :: "multiple" :: xs ->
      Some (Lazy_entry_points (Some Multiple), xs)
  | "exceptions" :: "full-debug" :: xs -> Some (Exceptions FullDebug, xs)
  | "exceptions" :: "debug-message" :: xs -> Some (Exceptions Message, xs)
  | "exceptions" :: "verify-or-line" :: xs -> Some (Exceptions VerifyOrLine, xs)
  | "exceptions" :: "default-line" :: xs -> Some (Exceptions DefaultLine, xs)
  | "exceptions" :: "line" :: xs -> Some (Exceptions Line, xs)
  | "exceptions" :: "default-unit" :: xs -> Some (Exceptions DefaultUnit, xs)
  | "exceptions" :: "unit" :: xs -> Some (Exceptions Unit, xs)
  | xs -> parse_bool_flag xs

let default =
  { simplify_via_michel = false
  ; decompile = false
  ; erase_comments = false
  ; disable_dup_check = false
  ; lazy_entry_points = None
  ; exceptions = VerifyOrLine
  ; protocol = Edo }

let apply_flag flag config =
  match flag with
  | Simplify_via_michel x -> {config with simplify_via_michel = x}
  | Decompile x -> {config with decompile = x}
  | Erase_comments x -> {config with erase_comments = x}
  | Disable_dup_check x -> {config with disable_dup_check = x}
  | Protocol x -> {config with protocol = x}
  | Lazy_entry_points x -> {config with lazy_entry_points = x}
  | Exceptions x -> {config with exceptions = x}

let apply_flags flags config =
  List.fold_left (fun flag config -> apply_flag config flag) config flags

let protocol_of_string = function
  | "Delphi" -> Delphi
  | "Edo" -> Edo
  | "Florence" -> Florence
  | "Proto10" -> Proto10
  | _ -> assert false
