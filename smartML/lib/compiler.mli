(* Copyright 2019-2021 Smart Chain Arena LLC. *)

(** Compiler from {!Basics.tcontract} to Michelson. *)

open Michelson

(*val mtype_of_record : ('a -> mtype) -> (string * 'a) list -> mtype*)
(** Compile a record to Michelson, see {!Scenario.run}, it useful to
    reproduce the same binary tree structure as normal compilation. *)

val mtype_of_type : Type.t -> mtype

val compile_value : config:Config.t -> Basics.tvalue -> literal
(** Compile a value into a Michelson literal. *)

val pack_value : config:Config.t -> Basics.tvalue -> string
(** Serialize a value into the same binary format as Michelson in the
    protocol.  *)

val unpack_value : Type.t -> string -> Basics.tvalue
(** Deserialize michelson bytes. *)

val compile_value_tcontract :
  config:Config.t -> Basics.value_tcontract -> Michelson.tcontract
(** Compile a smart-contract to the Michelson intermediate representation. *)

val compile_views :
     Basics.value_tcontract
  -> (string * (mtype option * mtype * instr * bool * string)) list
(** Compile the offchain views in a contract. Returns the parameter and the return type along with each view. *)
