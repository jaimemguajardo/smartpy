(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Control
open Basics
open Typed
open Michelson
open Utils.Misc
open Has_operations
open Printf
open Printer

let do_simplification = true

let debug_tags = false

let debug_adapt = false

let simplify ~config ~remove_comments =
  let r = remove_comments in
  let open Michelson_rewriter in
  if do_simplification
  then run (if r then remove_comments @ simplify ~config else simplify ~config)
  else id

let simplify_contract ~config ~remove_comments x =
  let r = remove_comments in
  let open Michelson_rewriter in
  if do_simplification
  then
    run_on_tcontract
      (if r then remove_comments @ simplify ~config else simplify ~config)
      x
  else x

(** {1 Actions for code construction} *)
type target =
  | T_var              of string
  | T_lambda_parameter of int
  | T_match_cons       of string * bool (* head *)
  | T_entry_points
  | T_self_address
[@@deriving eq, show {with_path = false}]

let tgt_parameter = T_var "__parameter__"

let tgt_storage = T_var "__storage__"

let tgt_operations = T_var "__operations__"

type stack_tag =
  | ST_none   of int
  | ST_target of target
[@@deriving eq, show]

let st_none =
  let c = ref 0 in
  fun () ->
    incr c;
    ST_none !c

let tag_operations = ST_target tgt_operations

let mk_instr instr = {instr}

let display_target = function
  | T_var "__parameter__" -> "params"
  | T_var "__storage__" -> "storage"
  | T_var "__operations__" -> "operations"
  | T_lambda_parameter id -> Printf.sprintf "lambda_parameter(%i)" id
  | T_var n -> Printf.sprintf "var(%s)" n
  | T_match_cons (n, b) ->
      Printf.sprintf "match_cons(%s).%s" n (if b then "head" else "tail")
  | T_entry_points -> "entry_points"
  | T_self_address -> "self_address"

let display_stack_tag = function
  | ST_none i -> "#" ^ string_of_int i
  | ST_target t -> display_target t

let tags_for_instr instr tags =
  let auto (a_in, a_out) =
    Some List.(map st_none (replicate a_out ()) @ drop a_in tags)
  in
  match (tags, instr.instr) with
  | _, MIdup _ -> auto (0, 1)
  | _, MIdig n ->
    ( match List.split_at n tags with
    | hi, x :: lo -> Some ((x :: hi) @ lo)
    | _ -> assert false )
  | _, MIdug n ->
    ( match List.split_at (n + 1) tags with
    | x :: hi, lo -> Some (hi @ (x :: lo))
    | _ -> assert false )
  | _ ->
    ( match arity instr with
    | Ok (_, None) -> None
    | Ok (m, Some n) -> auto (m, n)
    | Error e -> failwith ("arity: " ^ e) )

let find_stack_tag t =
  let f offset t' = if equal_stack_tag t t' then Some offset else None in
  Base.List.find_mapi ~f

let move_to_bottom x xs =
  let xs = List.rev xs in
  let last = List.length xs - 1 in
  match find_stack_tag x xs with
  | None -> None
  | Some i when i = last -> Some ([], List.rev xs)
  | Some i ->
    ( match List.nth_rem i xs with
    | Some (x', xs) when equal_stack_tag x x' ->
        Some ([MIdig i; MIdug last], List.rev (xs @ [x]))
    | _ -> assert false )

let move_to_top x xs =
  match find_stack_tag x xs with
  | None -> assert false
  | Some 0 -> ([], Some xs)
  | Some i ->
    ( match List.nth_rem i xs with
    | Some (x', xs) when equal_stack_tag x x' -> ([MIdig i], Some ([x] @ xs))
    | _ -> assert false )

let rec adapt_branches r rx ry xs ys =
  let last_x = List.length xs - 1 in
  let last_y = List.length ys - 1 in
  let drop i = [MIdig i; MIdrop] in
  match (xs, ys) with
  | [], [] -> (Some r, rx, ry)
  | [], _ :: ys -> adapt_branches r rx (ry @ drop last_y) xs ys
  | _ :: xs, [] -> adapt_branches r (rx @ drop last_x) ry xs ys
  | x :: xs, y :: ys when equal_stack_tag x y ->
      adapt_branches (x :: r) rx ry xs ys
  | (x :: xs' as xs), (y :: ys' as ys) ->
    ( match (move_to_bottom y xs, move_to_bottom x ys) with
    | Some (ix, xs), _ -> adapt_branches r (rx @ ix) ry xs ys
    | _, Some (iy, ys) -> adapt_branches r rx (ry @ iy) xs ys
    | _ -> adapt_branches r (rx @ drop last_x) (ry @ drop last_y) xs' ys' )

let mark_top = function
  | None -> None
  | Some [] -> failwith "mark_top: empty stack"
  | Some (_ :: xs) -> Some (ST_target (T_var "__top__") :: xs)

(** Finds the offset of the first occurence of the target. *)
let find_target target =
  let f offset = function
    | ST_none _ -> None
    | ST_target t -> if target = t then Some offset else None
  in
  Base.List.find_mapi ~f

(** Does the stack have the given target? *)
let has_target target tags = Option.is_some (find_target target tags)

let adapt_branches xs ys =
  let has_ops = has_target tgt_operations in
  match (xs, ys) with
  | None, None -> (None, [], [])
  | Some xs, None | None, Some xs -> (Some xs, [], [])
  | Some xs, Some ys ->
      let instrs_xs, xs, instrs_ys, ys =
        match (has_ops xs, has_ops ys) with
        | true, true | false, false -> ([], xs, [], ys)
        | true, false -> ([], xs, [MI0 (Nil mt_operation)], tag_operations :: ys)
        | false, true -> ([MI0 (Nil mt_operation)], tag_operations :: xs, [], ys)
      in
      let tags, instrs_xs, instrs_ys =
        adapt_branches [] instrs_xs instrs_ys (List.rev xs) (List.rev ys)
      in
      (tags, List.map mk_instr instrs_xs, List.map mk_instr instrs_ys)

let mk_seq block =
  match block with
  | [i] -> i
  | _ -> {instr = MIseq block}

(** A monad that can read the (immutable) parameter and storage types,
   as well as modify the state: (1) the instructions emitted within
   the current block and (2) the current stack. *)
module Action = struct
  type constant =
    { tparams : mtype
    ; config : Config.t }

  type state = {tags : stack_tag list option}

  type _ t =
    | Return    : 'a -> 'a t
    | Bind      : 'a t * ('a -> 'b t) -> 'b t
    | Ask : constant t
    | Instr     : instr -> unit t
    | Composite : bool * (unit t, literal) instr_f -> unit t
    | Block     :
        stack_tag list option * unit t
        -> (instr list * stack_tag list option) t
    | Set_tags  : stack_tag list option -> unit t
    | Get_tags : stack_tag list option t
    | Abort     : string -> 'a t

  let mk_cond
      ~protect_top cond (x_result, x_state, x_block) (y_result, y_state, y_block)
      =
    let result =
      match (x_result, y_result) with
      | Some (), Some () -> Some ()
      | _, _ -> None
    in
    let x_tags, y_tags =
      if protect_top
      then (mark_top x_state.tags, mark_top y_state.tags)
      else (x_state.tags, y_state.tags)
    in
    let tags, x_post, y_post = adapt_branches x_tags y_tags in
    let x = {instr = MIseq (x_block @ x_post)} in
    let y = {instr = MIseq (y_block @ y_post)} in
    match tags with
    | Some tags when protect_top ->
        let xs, tags = move_to_top (ST_target (T_var "__top__")) tags in
        (result, {tags}, List.map mk_instr (cond x y :: xs))
    | _ -> (result, {tags}, [mk_instr (cond x y)])

  let rec run :
      type a. constant -> state -> a t -> a option * state * instr list =
   fun c s -> function
    | Return x -> (Some x, s, [])
    | Bind (x, f) ->
      ( match run c s x with
      | Some x', s, i ->
          let r, s, i' = run c s (f x') in
          (r, s, i @ i')
      | None, si, i -> (None, si, i) )
    | Ask -> (Some c, s, [])
    | Instr i ->
      ( match s with
      | {tags = None} -> (Some (), s, [])
      | {tags = Some tags} -> (Some (), {tags = tags_for_instr i tags}, [i]) )
    | Composite (protect_top, i) -> emit_instr protect_top c s i
    | Block (tags, body) ->
        let _r, s', i = run c {tags} body in
        (Some (i, s'.tags), s, [])
    | Set_tags tags -> (Some (), {tags}, [])
    | Get_tags -> (Some s.tags, s, [])
    | Abort msg -> (None, s, [{instr = MIerror msg}])

  and emit_instr protect_top c s i : unit option * state * instr list =
    let err msg = (None, s, [{instr = MIerror msg}]) in
    match (s.tags, i) with
    | _, MIseq (x :: xs) ->
        run c s (Bind (x, fun _ -> Composite (false, MIseq xs)))
    | Some (_ :: tail), MIif (x, y) ->
        let x = run c {tags = Some tail} x in
        let y = run c {tags = Some tail} y in
        mk_cond ~protect_top (fun l r -> MIif (l, r)) x y
    | _, MIif _ -> err "IF"
    | Some (_ :: tail), MIif_some (x, y) ->
        let x = run c {tags = Some (st_none () :: tail)} x in
        let y = run c {tags = Some tail} y in
        mk_cond ~protect_top (fun l r -> MIif_some (l, r)) x y
    | _, MIif_some _ -> err "IF_SOME"
    | Some (_ :: tail), MIif_left (x, y) ->
        let x = run c {tags = Some (st_none () :: tail)} x in
        let y = run c {tags = Some (st_none () :: tail)} y in
        mk_cond ~protect_top (fun l r -> MIif_left (l, r)) x y
    | _, MIif_left _ -> err "IF_LEFT"
    | Some (_ :: tail), MIif_cons (x, y) ->
        let x = run c {tags = Some (st_none () :: st_none () :: tail)} x in
        let y = run c {tags = Some tail} y in
        mk_cond ~protect_top (fun l r -> MIif_cons (l, r)) x y
    | _, MIif_cons _ -> err "IF_CONS"
    | Some (_ :: tail), MImap body ->
        let result, _state, block =
          run c {tags = Some (st_none () :: tail)} body
        in
        ( result
        , {tags = Some (st_none () :: tail)}
        , [mk_instr (MImap (mk_seq block))] )
    | _, MImap _ -> err "MAP"
    | Some (_ :: tail), MIiter body ->
        let result, _state, block =
          run c {tags = Some (st_none () :: tail)} body
        in
        (result, {tags = Some tail}, [mk_instr (MIiter (mk_seq block))])
    | _, MIiter _ -> err "ITER"
    | Some (_ :: tail), MIloop body ->
        let result, _state, block = run c {tags = Some tail} body in
        (result, {tags = Some tail}, [mk_instr (MIloop (mk_seq block))])
    | Some tags, MIlambda (t_in, t_out, body) ->
        let result, _state, block = run c {tags = Some [st_none ()]} body in
        (* TODO raise error if output stack is not a singleton *)
        ( result
        , {tags = Some (st_none () :: tags)}
        , [mk_instr (MIlambda (t_in, t_out, mk_seq block))] )
    | _, MIlambda _ -> err "LAMBDA"
    | None, _ -> (Some (), s, [])
    | Some tags, i ->
        let dummy ppf _ = Format.fprintf ppf "..." in
        let pp = pp_instr_f dummy dummy in
        let f _ = failwith (Format.asprintf "emit_instr: %a" pp i) in
        let instr = map_instr_f f id i in
        (Some (), {tags = tags_for_instr {instr} tags}, [{instr}])

  let return x = Return x

  let bind x f = Bind (x, f)

  let ask = Ask

  let abort msg = Abort msg

  let map f x = bind x (fun x -> return (f x))

  let apply f x = bind f (fun f -> bind x (fun x -> return (f x)))

  let instr instr = Instr {instr}

  let rec _instr i =
    let i = map_instr_f (fun i -> _instr i.instr) id i in
    Composite (false, i)

  let composite ?protect_top instr = Composite (protect_top = Some (), instr)

  let block tags body = Block (tags, body)

  let set_tags tags = Set_tags tags

  let get_tags = Get_tags
end

module ActionM = struct
  include Action
  include Monad (Action)
end

module T = Binary_tree.Traversable (ActionM)
open Action
module A = Utils.Control.Monad (Action)
open A

let mi_seq = A.iter_list id

let display_ok_tags s =
  let tags = List.map display_stack_tag s in
  sprintf "[ %s ]" (String.concat " : " tags)

let display_tags = function
  | None -> sprintf "FAILED"
  | Some s -> display_ok_tags s

let instrs = iter_list instr

let comment msg = instr (MIcomment [msg])

let error msg = abort msg

let commentf fmt = ksprintf comment fmt

let errorf fmt = ksprintf error fmt

let comment_if c fmt =
  let comment msg = if c then comment msg else return () in
  ksprintf comment fmt

let _debug msg =
  let* tags = get_tags in
  commentf "%s %s" msg (display_tags tags)

let get_ok_stack lbl =
  let* tags = get_tags in
  match tags with
  | Some tags -> return tags
  | _ -> error ("get_ok_stack: " ^ lbl)

(** {1 Michelson helpers} *)

let mi_car = instr (MIfield [A])

let mi_cdr = instr (MIfield [D])

(* Equivalent to DUP n, but with zero-based index. *)
let mi_copy = function
  | 0 -> instr (MIdup 1)
  | n ->
      let* tags = get_tags in
      ( match tags with
      | None -> assert false
      | Some tags ->
          let* () = instrs [MIdig n; MIdup 1; MIdug (n + 1)] in
          set_tags (Some (st_none () :: tags)) )

let push_unit = instr (MIpush (mt_unit, MLiteral.unit))

let mi_failwith ?verify ~default ~line_no ~context ?(args = push_unit) s =
  let* {config = {exceptions}} = ask in
  let line_no = Option.default (-1) line_no in
  let i =
    match exceptions with
    | Unit -> push_unit
    | DefaultUnit -> if default then push_unit else args
    | Line -> instr (MIpush (mt_int, MLiteral.small_int line_no))
    | VerifyOrLine ->
        if (not default) || verify = Some ()
        then args
        else instr (MIpush (mt_int, MLiteral.small_int line_no))
    | DefaultLine ->
        if not default
        then args
        else instr (MIpush (mt_int, MLiteral.small_int line_no))
    | Message ->
        instrs
          [ MIpush (mt_int, MLiteral.small_int line_no)
          ; MIpush (mt_string, MLiteral.string s)
          ; MI2 (Pair (None, None)) ]
    | FullDebug ->
        let* () = args in
        instrs
          [ MIpush (mt_string, MLiteral.string context)
          ; MI2 (Pair (None, None))
          ; MIpush (mt_int, MLiteral.small_int line_no)
          ; MI2 (Pair (None, None))
          ; MIpush (mt_string, MLiteral.string s)
          ; MI2 (Pair (None, None)) ]
  in
  mi_seq [comment s; i; instr (MI1_fail Failwith)]

(** {1 Stack tags and targets} *)

let tag_top tags =
  let* ts = get_ok_stack "tag_top" in
  if List.length tags <= List.length ts
  then
    let* () = set_tags (Some (tags @ List.(drop (length tags) ts))) in
    comment_if debug_tags "tag_top"
  else error "tag_top: stack too short"

let fetch_target ~dup target =
  let* tags = get_ok_stack "fetch_target" in
  match find_target target tags with
  | None ->
      errorf
        "fetch_target %s in [%s]"
        (show_target target)
        (String.concat "; " (List.map show_stack_tag tags))
  | Some offset when dup -> mi_copy offset
  | Some offset -> instr (MIdig offset)

let rec equal_stack_tags xs ys =
  match (xs, ys) with
  | [], [] -> true
  | x :: xs, y :: ys -> equal_stack_tag x y && equal_stack_tags xs ys
  | _ -> false

let compatible_stack_tags xs ys =
  match (xs, ys) with
  | Some xs, Some ys -> equal_stack_tags xs ys
  | _ -> (* FAILED is compatible with anything *) true

let rec adapt_stack offset desired =
  let* current = get_tags in
  let* () = comment_if debug_adapt "adapt_stack %d" offset in
  match current with
  | None -> return ()
  | Some current ->
      let current = List.rdrop offset current in
      ( match (current, desired) with
      | [], [] -> return ()
      | [], _ -> assert false
      | _, [] ->
          let* () = comment_if debug_adapt "dropping" in
          let* () = instr MIdrop in
          adapt_stack offset desired
      | _ :: _, _ :: _ ->
          let ts, t = List.unsnoc desired in
          let f i c = if equal_stack_tag c t then Some i else None in
          let n = List.length current - 1 in
          let i = Base.List.find_mapi ~f current in
          let* () =
            comment_if
              debug_adapt
              "%s at %s (looking till %d)"
              (display_stack_tag t)
              (Option.cata "NA" string_of_int i)
              n
          in
          let* () =
            match i with
            | Some i when i = n -> return ()
            | Some i when i < n ->
                let* () = when_ (i <> 0) (instr (MIdig i)) in
                instr (MIdug n)
            | _ -> assert false
          in
          adapt_stack (offset + 1) ts )

let adapt_stack desired =
  let* current = get_tags in
  let* () = comment_if debug_adapt "current    : %s" (display_tags current) in
  let* () = comment_if debug_adapt "desired     : %s" (display_tags desired) in
  match (current, desired) with
  | Some current, Some desired ->
      let f t = Option.is_some (List.find_opt (equal_stack_tag t) current) in
      let available = List.filter f desired in
      let* () =
        comment_if debug_adapt "available  : %s" (display_ok_tags available)
      in
      let* () = adapt_stack 0 available in
      let* result = get_tags in
      (* TODO instead of printing, assert available = result *)
      let* () =
        comment_if debug_adapt "result     : %s" (display_tags result)
      in
      unless
        (compatible_stack_tags (Some available) result)
        (error "available /= result")
  | None, _ -> comment_if debug_adapt "not adapting failed stack"
  | _, None -> comment_if debug_adapt "desired stack failed"

let if_stack_ok x =
  let* tags = get_tags in
  match tags with
  | Some tags -> x tags
  | None -> return ()

let drop_target target =
  if_stack_ok (fun tags ->
      match find_target target tags with
      | None ->
          comment_if debug_adapt "drop_target skipping %s" (show_target target)
      | Some offset -> instrs [MIdig offset; MIdrop])

let unzip_target ~copy target =
  let err msg =
    let* () = errorf "unzip_target: %s" msg in
    return (false, return ())
  in
  let* tags = get_tags in
  match tags with
  | None -> err "stack failed"
  | Some tags ->
      let* stack_original = get_tags in
      ( match find_target target tags with
      | None -> return (false, tag_top [ST_target target])
      | Some offset when copy ->
          let* () = mi_copy offset in
          return
            ( true
            , let* () = drop_target target in
              let* () = tag_top [ST_target target] in
              adapt_stack stack_original )
      | Some offset ->
          let* () = instr (MIdig offset) in
          return
            ( true
            , let* () = tag_top [ST_target target] in
              adapt_stack stack_original ) )

(** {1 Stack unification with operations} *)

let ops_init =
  let* () = instr (MI0 (Nil mt_operation)) in
  tag_top [ST_target tgt_operations]

let mi_if ?protect_top l r = composite ?protect_top (MIif (l, r))

let mi_if_some ?protect_top l r = composite ?protect_top (MIif_some (l, r))

let mi_if_left ?protect_top l r = composite ?protect_top (MIif_left (l, r))

let mi_if_cons ?protect_top l r = composite ?protect_top (MIif_cons (l, r))

let mi_map body = composite (MImap body)

let mi_iter body = composite (MIiter body)

let mi_loop body = composite (MIloop body)

let tree_of_layout row =
  Binary_tree.map (fun Layout.{source; target} ->
      match List.assoc_opt source row with
      | Some t -> (source, target, t)
      | None ->
          Printf.ksprintf
            failwith
            "Missing layout field %S in [%s]"
            source
            (String.concat "; " (List.map fst row)))

let get_layout row layout =
  match Unknown.getRefOption layout with
  | None -> Type.default_layout_of_row row
  | Some layout -> layout

let rec compile_tuple f = function
  | [] | [_] -> failwith "compile_tuple"
  | [x; y] -> f x y
  | x :: xs -> f x (compile_tuple f xs)

let rec row_of_tree build tr =
  let l (_source, target, t) = (Some target, mtype_of_type t) in
  let n (annot1, t1) (annot2, t2) = (None, build ?annot1 ?annot2 t1 t2) in
  Binary_tree.cata l n tr

and mtype_of_layout build row layout =
  match row with
  | [] -> mt_unit
  | row ->
      let layout = get_layout row layout in
      snd (row_of_tree build (tree_of_layout row layout))

and mtype_of_type t =
  match Type.getRepr t with
  | TUnit -> mt_unit
  | TBool -> mt_bool
  | TInt {isNat} ->
    ( match Typing.intType isNat with
    | `Unknown -> mt_int
    | `Nat -> mt_nat
    | `Int -> mt_int )
  | TTimestamp -> mt_timestamp
  | TBytes -> mt_bytes
  | TString -> mt_string
  | TRecord {row; layout} -> mtype_of_layout mt_pair row layout
  | TVariant {row = [("None", u); ("Some", t)]} when u = Type.unit ->
      mt_option (mtype_of_type t)
  | TVariant {row; layout} ->
      if false
      then
        match row with
        | [(_name1, t1)] -> mtype_of_type t1
        | (annot1, left) :: (annot2, right) :: l ->
            List.fold_left
              (fun t (annot2, t2) -> mt_or ~annot2 t (mtype_of_type t2))
              (mt_or ~annot1 ~annot2 (mtype_of_type left) (mtype_of_type right))
              l
        | [] -> mt_unit
      else mtype_of_layout mt_or row layout
  | TSet {telement} -> mt_set (mtype_of_type telement)
  | TMap {tkey; tvalue} ->
      let tkey = mtype_of_type tkey in
      if Type.is_bigmap t
      then mt_big_map tkey (mtype_of_type tvalue)
      else mt_map tkey (mtype_of_type tvalue)
  | TAddress -> mt_address
  | TContract t -> mt_contract (mtype_of_type t)
  | TTicket t -> mt_ticket (mtype_of_type t)
  | TKeyHash -> mt_key_hash
  | TBakerHash -> mt_baker_hash
  | TKey -> mt_key
  | TSecretKey -> mt_missing "Secret keys are forbidden in contracts"
  | TChainId -> mt_chain_id
  | TToken -> mt_mutez
  | TSignature -> mt_signature
  | TUnknown _ -> mt_missing "Unknown Type"
  | TTuple ts -> compile_tuple mt_pair (List.map mtype_of_type ts)
  | TList t -> mt_list (mtype_of_type t)
  | TLambda (t1, t2) -> mt_lambda (mtype_of_type t1) (mtype_of_type t2)
  | TOperation -> mt_operation
  | TSaplingState {memo} ->
      Option.fold
        ~none:(mt_missing "sapling state with no explicit memo")
        ~some:(fun memo -> mt_sapling_state memo)
        (Unknown.getRefOption memo)
  | TSaplingTransaction {memo} ->
      Option.fold
        ~none:(mt_missing "sapling transaction with no explicit memo")
        ~some:(fun memo -> mt_sapling_transaction memo)
        (Unknown.getRefOption memo)
  | TNever -> mt_never
  | TBls12_381_g1 -> mt_bls12_381_g1
  | TBls12_381_g2 -> mt_bls12_381_g2
  | TBls12_381_fr -> mt_bls12_381_fr

let compile_literal ?for_error (l : Basics.Literal.t) =
  let module L = MLiteral in
  let for_error =
    Option.cata [] (fun (e : texpr) -> [`Expr e; `Line e.line_no]) for_error
  in
  match l with
  | Unit -> L.unit
  | Int {i} -> L.int i
  | Timestamp i ->
      let i = Bigint.int_of_big_int i in
      ( match Ptime.of_span (Ptime.Span.of_int_s i) with
      | None -> failwith (sprintf "timestamp %d out of range" i)
      | Some t -> L.string (Ptime.to_rfc3339 ~tz_offset_s:0 t) )
  | Mutez i -> L.int i
  | String s -> L.string s
  | Bytes s -> L.bytes s
  | Chain_id s -> L.bytes s
  | Key_hash s -> L.string s
  | Baker_hash h -> L.string h
  | Signature s -> L.string s
  | Key s -> L.string s
  | Address (Real s, ep) | Contract (Real s, ep, _) ->
      let s = sprintf "%s%s" s (Option.cata "" (sprintf "%%%s") ep) in
      L.string s
  | Bool b -> L.bool b
  | Address (Local i, ep) | Contract (Local i, ep, _) ->
      let static, contract_id =
        match i with
        | Literal.C_static {static_id} -> (true, static_id)
        | Literal.C_dynamic {dynamic_id} -> (false, dynamic_id)
      in
      L.string (Bs58.address_of_contract_id ~static contract_id ep)
      (*
raise
        (SmartExcept
           [ `Text "Local contract addresses are forbidden in contracts"
           ; `Expr e
           ; `Line e.line_no ])
 *)
  | Secret_key _ ->
      raise
        (SmartExcept
           ([`Text "Secret keys are forbidden in contracts"] @ for_error))
  | Sapling_test_state _ -> L.sapling_empty_state
  | Sapling_test_transaction _ -> L.string "FAKE_SAPLING_TRANSACTION"
  | Bls12_381_g1 s -> L.bytes s
  | Bls12_381_g2 s -> L.bytes s
  | Bls12_381_fr s -> L.bytes s

let op_of_attr (name : string) t =
  let rec get acc = function
    | Binary_tree.Leaf Layout.{target} ->
        if target = name then Some (List.rev acc) else None
    | Node (fst, snd) -> get (A :: acc) fst <|> fun () -> get (D :: acc) snd
  in
  match Type.getRepr t with
  | TRecord {row; layout} -> get [] (get_layout row layout)
  | _ -> assert false

(** {1 Expressions} *)

(* If x is missing, compile top += y *)
let compile_binop ~line_no ~context x y t =
  let t = mtype_of_type t in
  let strict i =
    let* () = y in
    let* () = Option.default (instr (MIdig 1)) x in
    i
  in
  function
  | BNeq -> strict (instrs [MI2 Compare; MI1 Neq])
  | BEq -> strict (instrs [MI2 Compare; MI1 Eq])
  | BLt -> strict (instrs [MI2 Compare; MI1 Lt])
  | BLe -> strict (instrs [MI2 Compare; MI1 Le])
  | BGt -> strict (instrs [MI2 Compare; MI1 Gt])
  | BGe -> strict (instrs [MI2 Compare; MI1 Ge])
  | BOr when equal_mtype (remove_annots t) mt_bool ->
      let* () = Option.default (return ()) x in
      mi_if ~protect_top:() (instr (MIpush (mt_bool, MLiteral.bool true))) y
  | BAnd when equal_mtype (remove_annots t) mt_bool ->
      let* () = Option.default (return ()) x in
      mi_if
        ~protect_top:()
        y
        (instr (MIpush (mtype_of_type Type.bool, MLiteral.bool false)))
  | BAnd -> strict (instr (MI2 And))
  | BOr -> strict (instr (MI2 Or))
  | BAdd when equal_mtype (remove_annots t) mt_string ->
      strict (instr (MI2 Concat2))
  | BAdd when equal_mtype (remove_annots t) mt_bytes ->
      strict (instr (MI2 Concat2))
  | BAdd -> strict (instr (MI2 Add))
  | BSub -> strict (instr (MI2 Sub))
  | BEDiv -> strict (instr (MI2 Ediv))
  | BDiv ->
      strict
        (let* () = instr (MI2 Ediv) in
         mi_if_some
           mi_car
           (mi_failwith ~default:true ~line_no ~context "DivisionByZero"))
  | BMul _ -> strict (instr (MI2 Mul))
  | BMod ->
      strict
        (let* () = instr (MI2 Ediv) in
         mi_if_some
           mi_cdr
           (mi_failwith ~default:true ~line_no ~context "DivisionByZero"))
  | BLsl -> strict (instr (MI2 Lsl))
  | BLsr -> strict (instr (MI2 Lsr))
  | BXor -> strict (instr (MI2 Xor))

let mi_rev_list t =
  let* () = instrs [MI0 (Nil (mtype_of_type t)); MIdig 1] in
  mi_iter (instr (MI2 Cons))

(** Ensure an operations element is on the stack if the given command
   may output any. *)
let ensure_ops =
  let* tags = get_ok_stack "ensure_ops_if_out" in
  if has_target tgt_operations tags then return () else ops_init

let ensure_ops_if c = if c then ensure_ops else return ()

type lstep =
  | LMapItem of texpr * Type.t
  | LAttr    of string * Type.t

type lroot =
  | LOperations
  | LStorage
  | LLocal      of string
  | LIter       of string

type lexpr =
  { lroot : lroot
  ; lsteps : lstep list }

let occurs_lroot_alg root =
  let a = monoid_talg ( || ) false in
  let f_texpr ln t = function
    | EPrim0 (ELocal n) ->
      ( match root with
      | LOperations -> n = "__operations__"
      | LStorage -> n = "__storage__"
      | LLocal n' -> n = n'
      | _ -> false )
    | EPrim0 (EIter n) ->
      ( match root with
      | LIter n' -> n = n'
      | _ -> false )
    | e -> a.f_texpr ln t e
  in
  {a with f_texpr}

let occurs_lroot_expr root = cata_texpr (occurs_lroot_alg root)

let occurs_lroot_command root = cata_tcommand (occurs_lroot_alg root)

let self_referential_lexpr {lroot; lsteps} =
  let f = function
    | LMapItem (e, _) -> occurs_lroot_expr lroot e
    | _ -> false
  in
  List.exists f lsteps

let extend_lexpr {lroot; lsteps} s = {lroot; lsteps = lsteps @ [s]}

let last_step_is_item {lroot; lsteps} =
  if List.length lsteps > 0
  then
    match List.unsnoc lsteps with
    | lsteps, LMapItem (key, tvalue) -> Some ({lroot; lsteps}, key, tvalue)
    | _ -> None
  else None

let lexpr_of_expr =
  let rec of_expr acc e =
    match e.e with
    | EPrim0 (ELocal "__operations__") ->
        Some {lroot = LOperations; lsteps = acc}
    | EPrim0 (ELocal "__storage__") -> Some {lroot = LStorage; lsteps = acc}
    | EPrim0 (ELocal name) -> Some {lroot = LLocal name; lsteps = acc}
    | EPrim1 (EAttr name, {e = EPrim0 (EMetaLocal loc)}) ->
        Some {lroot = LLocal (Printf.sprintf "%s.%s" loc name); lsteps = acc}
    | EPrim0 (EIter name) -> Some {lroot = LIter name; lsteps = acc}
    | EItem {items; key; default_value = None; missing_message = None} ->
      ( match items.et with
      | F (TMap {tvalue}) -> of_expr (LMapItem (key, tvalue) :: acc) items
      | _ -> assert false )
    | EPrim1 (EAttr name, expr) -> of_expr (LAttr (name, expr.et) :: acc) expr
    | _ -> None
  in
  of_expr []

(* There are two ways to modify an iterator variable: (1) assign to it
   directly; (2) iterate over it or one of its substructures (nested
   loop). *)
let rec modifies_iter i c =
  let touches lhs =
    match lexpr_of_expr lhs with
    | None ->
        raise
          (SmartExcept
             [ `Text "Forbidden expression in left position of an assignment."
             ; `Expr lhs
             ; `Line c.line_no ])
    | Some {lroot = LIter name} when name = i -> true
    | Some _ -> false
  in
  match c.c with
  | CSetVar (lhs, _) | CDelItem (lhs, _) | CUpdateSet (lhs, _, _) -> touches lhs
  | CModifyProduct (lhs, _, body) -> touches lhs || modifies_iter i body
  | CFor (name, container, body) ->
      modifies_iter i body
      ||
      ( match lexpr_of_expr container with
      | Some {lroot = LIter cname} when i = cname -> modifies_iter name body
      | _ -> false )
  | CIf (_, c1, c2) -> modifies_iter i c1 || modifies_iter i c2
  | CBind (_, c1, c2) -> modifies_iter i c1 || modifies_iter i c2
  | CWhile (_, c) -> modifies_iter i c
  | CNever _ | CFailwith _
   |CVerify (_, _)
   |CMatch (_, _)
   |CMatchProduct (_, _, _)
   |CMatchCons _
   |CDefineLocal (_, _)
   |CResult _ | CComment _
   |CSetType (_, _)
   |CSetResultType (_, _)
   |CTrace _ ->
      false

(** Brings the target into focus, leaves a zipper on the stack. *)
let rec mi_unzip = function
  | A :: rest -> mi_seq [instr (MIunpair [true; true]); mi_unzip rest]
  | D :: rest ->
      let* () = instrs [MIunpair [true; true]; MIdig 1] in
      mi_unzip rest
  | [] -> return ()

let rec mi_zip = function
  | A :: rest -> mi_seq [mi_zip rest; instr (MI2 (Pair (None, None)))]
  | D :: rest ->
      let* () = mi_zip rest in
      instrs [MIdig 1; MI2 (Pair (None, None))]
  | [] -> return ()

let target_of_lexpr = function
  | {lroot = LOperations; lsteps} -> (tgt_operations, lsteps)
  | {lroot = LStorage; lsteps} -> (tgt_storage, lsteps)
  | {lroot = LLocal name; lsteps} -> (T_var name, lsteps)
  | {lroot = LIter name; lsteps} -> (T_var name, lsteps)

let renaming_of_type ~rev t =
  match
    match Type.getRepr t with
    | TRecord {layout} -> Unknown.getRefOption layout
    | _ -> None
  with
  | None -> Hashtbl.create 5
  | Some layout ->
      let renaming = Hashtbl.create 5 in
      let rec aux = function
        | Binary_tree.Leaf Layout.{source; target} ->
            if rev
            then Hashtbl.add renaming target source
            else Hashtbl.add renaming source target
        | Binary_tree.Node (l1, l2) ->
            aux l1;
            aux l2
      in
      aux layout;
      renaming

let compile_record t entries =
  match Type.getRepr t with
  | TRecord {row; layout} ->
      get_layout row layout
      |> tree_of_layout row
      |> Binary_tree.map (fun (source, target, _) ->
             (target, List.assoc_exn ~msg:"compile_record" source entries))
  | _ -> failwith "ERecord"

(** Puts the specified fields on the stack and returns a list
   reflecting their order. *)
let explode_record ?name bs =
  let open Layout in
  let bs = List.map (fun {var; field} -> (field, var)) bs in
  let get_target t = List.assoc_opt t bs in
  let is_target {target} = Option.is_some (get_target target) in
  let build_target t =
    let t =
      match name with
      | None -> t
      | Some name -> Printf.sprintf "%s.%s" name t
    in
    ST_target (T_var t)
  in
  let rec explode (x : t) =
    let* {config} = ask in
    match x with
    | Leaf {target} ->
      ( match List.assoc_opt target bs with
      | Some var ->
          let* () = tag_top [build_target var] in
          return [var]
      | None ->
          let* () = instr MIdrop in
          return [] )
    | l
      when ( match config.protocol with
           | Delphi -> false
           | Edo | Florence | Proto10 -> true )
           && Binary_tree.(is_right_comb l && for_all is_target l) ->
        let l = Binary_tree.to_list l in
        let* () = instr (MIunpair (List.map (fun _ -> true) l)) in
        let f {target} = Option.get ~msg:"explode" (get_target target) in
        let targets = List.map f l in
        let* () = tag_top (List.map build_target targets) in
        return targets
    | Node (l, r) ->
        let* () = instr (MIunpair [true; true]) in
        let* l = explode l in
        let* () = instr (MIdig (List.length l)) in
        let* r = explode r in
        return (r @ l)
  in
  explode

let explode_single_pattern v =
  let* () = tag_top [ST_target (T_var v)] in
  return (drop_target (T_var v))

let explode_tuple_pattern ns =
  let* {config} = ask in
  let n = List.length ns in
  let open List in
  match config.protocol with
  | Edo | Florence | Proto10 ->
      let* () = instr (MIunpair (replicate n true)) in
      let* () = tag_top (map (fun n -> ST_target (T_var n)) ns) in
      return (iter_list (fun n -> drop_target (T_var n)) (rev ns))
  | Delphi ->
      let* () =
        (* "UNPAIR n" in reverse order: *)
        iter_list
          (fun _ -> instrs [MIunpair [true; true]; MIdig 1])
          (replicate (n - 1) ())
      in
      let* () = tag_top (rev (map (fun n -> ST_target (T_var n)) ns)) in
      return (iter_list (fun n -> drop_target (T_var n)) ns)

let explode_record_pattern ?name t bs =
  match Type.getRepr t with
  | TRecord {row; layout} ->
      let* targets = explode_record ?name bs (get_layout row layout) in
      return (iter_list (fun t -> drop_target (T_var t)) targets)
  | _ -> assert false

let explode_pattern ?name t = function
  | Pattern_single v -> explode_single_pattern v
  | Pattern_tuple ns -> explode_tuple_pattern ns
  | Pattern_record (_, bs) -> explode_record_pattern ?name t bs

let open_record_pattern name t bs =
  match Type.getRepr t with
  | TRecord {row; layout} ->
      let* _ = explode_record ~name bs (get_layout row layout) in
      let close =
        let l (field, _) =
          let var_name = Printf.sprintf "%s.%s" name field in
          (Some field, fetch_target ~dup:false (T_var var_name))
        in
        let p (field1, x1) (field2, x2) =
          (None, mi_seq [x2; x1; instr (MI2 (Pair (field1, field2)))])
        in
        let entries = List.map (fun {field} -> (field, field)) bs in
        let r = compile_record t entries in
        snd (Binary_tree.cata l p r)
      in
      return close
  | _ -> assert false

let compile_option t name x =
  match name with
  | "None" -> instr (MIpush (mt_option (mtype_of_type t), MLiteral.none))
  | "Some" -> mi_seq [x; instr (MI1 Some_)]
  | _ -> assert false

let compile_or tl tr name x =
  match name with
  | "Left" -> mi_seq [x; instr (MI1 (Left (None, None, mtype_of_type tr)))]
  | "Right" -> mi_seq [x; instr (MI1 (Right (None, None, mtype_of_type tl)))]
  | _ -> failwith (sprintf "Bad variant expecting Left/Right but got %s" name)

let compile_variant row layout name =
  let (_, target, _), ctxt =
    get_layout row layout
    |> tree_of_layout row
    |> Binary_tree.find_leaf (fun (_, target, _) -> target = name)
    |> Option.of_some ~msg:"compile_variant"
  in
  (target, ctxt)

let unzip_attr name t =
  let err =
    let* () = error "unzip_attr" in
    return (return ())
  in
  match op_of_attr name t with
  | None -> err
  | Some op ->
      let* () = mi_unzip op in
      return (mi_zip op)

let unzip_map key tvalue =
  let* () =
    if Type.is_hot tvalue = No
    then
      let* () = instr (MIdup 1) (* x x *) in
      let* () = key (* k x x *) in
      let* () = tag_top [st_none ()] in
      let* () = instr (MIdup 1) (* k k x x *) in
      let* () = instr (MIdug 2) (* k x k x *) in
      let* () = instr (MI2 Get) (* v k x *) in
      return ()
    else
      let* () = instr (MI0 (None_ (mtype_of_type tvalue))) in
      (* v x x *)
      let* () = key (* k v x *) in
      let* () = instr (MIdup 1) (* k k v x *) in
      let* () = instr (MIdug 3) (* k v x k *) in
      let* () = instr (MI3 Get_and_update) (* v x k *) in
      let* () = instr (MIdig 2) (* k v x *) in
      let* () = instr (MIdig 1) (* v k x *) in
      return ()
  in
  return
    (let* () = instr (MIdig 1) in
     instr (MI3 Update))

let unzip_option ~line_no ~context ~args =
  let* () =
    mi_if_some
      (return ())
      (mi_failwith ~default:true ~line_no ~context ~args "unzip_option")
  in
  return (instr (MI1 Some_))

let pps = List.pp_sep ", " (fun ppf -> Format.fprintf ppf "%s")

let ppS = List.pp_sep ", " (fun ppf -> Format.fprintf ppf "%S")

let print_match_product scrutinee p =
  let s = texpr_to_string scrutinee in
  match p with
  | Pattern_single _ -> assert false
  | Pattern_tuple ns ->
      let f = Format.asprintf "%a = sp.match_tuple(%s, %a)" in
      f pps ns s ppS ns
  | Pattern_record (_, bs) ->
      let vs = List.map (fun {var} -> var) bs in
      let fs = List.map (fun {field} -> field) bs in
      let f = Format.asprintf "%a = sp.match_record(%s, %a)" in
      f pps vs s ppS fs

let print_modify_product lhs p =
  let s = texpr_to_string lhs in
  match p with
  | Pattern_single x ->
      let f = Format.asprintf "%s = sp.modify(%s, %S)" in
      f x s x
  | Pattern_tuple ns ->
      let f = Format.asprintf "%a = sp.modify_tuple(%s, %a)" in
      f pps ns s ppS ns
  | Pattern_record (name, _bs) ->
      let f = Format.asprintf "with sp.modify_record(%s, %S) as %s:" in
      let s = texpr_to_string lhs in
      f s name name

let compile_prim0 outer =
  let dup = Type.is_hot outer.et <> Yes in
  function
  | EAmount -> instr (MI0 Amount)
  | EBalance -> instr (MI0 Balance)
  | EChain_id -> instr (MI0 Chain_id)
  | ETotal_voting_power -> instr (MI0 Total_voting_power)
  | ECst l ->
      instr
        (MIpush
           ( mtype_of_type (Literal.type_of l)
           , compile_literal ~for_error:outer l ))
  | EGlobal (name, _) -> fetch_target ~dup (T_var name)
  | EIter name -> fetch_target ~dup (T_var name)
  | ELocal name -> fetch_target ~dup (T_var name)
  | EMetaLocal _name -> assert false
  | EMatchCons name ->
      let* () = fetch_target ~dup (T_match_cons (name, false)) in
      let* () = fetch_target ~dup (T_match_cons (name, true)) in
      instr (MI2 (Pair (None, None)))
  | ENow -> instr (MI0 Now)
  | ELevel -> instr MIlevel
  | ESaplingEmptyState {memo} -> instr (MI0 (Sapling_empty_state {memo}))
  | ESelf -> instr (MI0 (Self None))
  | ESelf_address -> instr (MI0 Self_address)
  | ESelf_entry_point entry_point -> instr (MI0 (Self (Some entry_point)))
  | ESender -> instr (MI0 Sender)
  | ESource -> instr (MI0 Source)
  | EVariant_arg arg_name -> fetch_target ~dup (T_var arg_name)
  | EAccount_of_seed _ ->
      errorf
        "Expression %s cannot be converted to Michelson (missing \
         pre-evaluation)"
        (String.escaped (texpr_to_string outer))
  | EContract_balance _ | EContract_data _ | EContract_baker _
   |EContract_typed _ | EScenario_var _ ->
      errorf
        "No conversion for expression %s of type %s"
        (String.escaped (texpr_to_string outer))
        (String.escaped (type_to_string outer.et))

let rec compile_prim1 outer inner expr =
  let dup = Type.is_hot outer.et <> Yes in
  match expr with
  | ENot -> mi_seq [compile_expr inner; instr (MI1 Not)]
  | EAbs -> mi_seq [compile_expr inner; instr (MI1 Abs)]
  | EToInt -> mi_seq [compile_expr inner; instr (MI1 Int)]
  | EIsNat -> mi_seq [compile_expr inner; instr (MI1 IsNat)]
  | ENeg -> mi_seq [compile_expr inner; instr (MI1 Neg)]
  | ESign ->
      let* () = instr (MIpush (mt_int, MLiteral.small_int 0)) in
      let* () = compile_expr inner in
      instr (MI2 Compare)
  | ESum ->
      let proj, sub =
        match inner.e with
        | EPrim1 (EListValues _, inner2) -> (mi_cdr, inner2)
        | _ -> (return (), inner)
      in
      let* () = instr (MIpush (mt_int, MLiteral.small_int 0)) in
      let* () = compile_expr sub in
      mi_iter (mi_seq [proj; instr (MI2 Add)])
  | EProject 0 -> mi_seq [compile_expr inner; mi_car]
  | EProject 1 -> mi_seq [compile_expr inner; mi_cdr]
  | EProject _ -> failwith "compiler: TODO EProject"
  | EPack -> mi_seq [compile_expr inner; instr (MI1 Pack)]
  | EUnpack t ->
      mi_seq [compile_expr inner; instr (MI1 (Unpack (mtype_of_type t)))]
  | EConcat_list ->
    ( match inner.e with
    | EList [a] -> compile_expr a
    | EList [a; b] ->
        let* () = compile_expr b in
        let* () = compile_expr a in
        instr (MI2 Concat2)
    | _ -> mi_seq [compile_expr inner; instr (MI1 Concat1)] )
  | ESize -> mi_seq [compile_expr inner; instr (MI1 Size)]
  | EHash_key -> mi_seq [compile_expr inner; instr (MI1 Hash_key)]
  | EHash algo ->
      let algo =
        match algo with
        | BLAKE2B -> MI1 Blake2b
        | SHA256 -> MI1 Sha256
        | SHA512 -> MI1 Sha512
        | KECCAK -> MI1 Keccak
        | SHA3 -> MI1 Sha3
      in
      mi_seq [compile_expr inner; instr algo]
  | EContract_address ->
    ( match inner.e with
    | EPrim0 ESelf ->
        let* tags = get_ok_stack "self_address" in
        if has_target T_self_address tags
        then fetch_target ~dup T_self_address
        else instr (MI0 Self_address)
    | _ -> mi_seq [compile_expr inner; instr (MI1 Address)] )
  | EImplicit_account ->
      mi_seq [compile_expr inner; instr (MI1 Implicit_account)]
  | EListRev ->
    ( match Type.getRepr inner.et with
    | TList t -> mi_seq [compile_expr inner; mi_rev_list t]
    | _ -> errorf "map.items error: %s" (type_to_string inner.et) )
  | EListItems rev ->
    ( match Type.getRepr inner.et with
    | TMap {tkey; tvalue} ->
        compile_container_access_list
          inner
          (return ())
          (Type.key_value tkey tvalue)
          rev
    | _ -> errorf "map.items error: %s" (type_to_string inner.et) )
  | EListValues rev ->
    ( match Type.getRepr inner.et with
    | TMap {tvalue} -> compile_container_access_list inner mi_cdr tvalue rev
    | _ -> errorf "map.values error: %s" (type_to_string inner.et) )
  | EListKeys rev ->
    ( match Type.getRepr inner.et with
    | TMap {tkey} -> compile_container_access_list inner mi_car tkey rev
    | _ -> errorf "map.keys error: %s" (type_to_string inner.et) )
  | EListElements rev ->
    ( match Type.getRepr inner.et with
    | TSet {telement} ->
        compile_container_access_list inner (return ()) telement rev
    | _ -> errorf "set.elements error: %s" (type_to_string inner.et) )
  | EReduce ->
      errorf
        "Expression %s cannot be converted to Michelson (missing \
         pre-evaluation)"
        (String.escaped (texpr_to_string outer))
  | ESetDelegate -> mi_seq [compile_expr inner; instr (MI1 Set_delegate)]
  | EType_annotation _ -> compile_expr inner
  | EAttr name ->
    begin
      match (name, inner) with
      | "head", {e = EPrim0 (EMatchCons name)} ->
          fetch_target ~dup (T_match_cons (name, true))
      | "tail", {e = EPrim0 (EMatchCons name)} ->
          fetch_target ~dup (T_match_cons (name, false))
      | attr, {e = EPrim0 (EMetaLocal name)} ->
          fetch_target ~dup (T_var (name ^ "." ^ attr))
      | name, x ->
          let renaming = renaming_of_type ~rev:false x.et in
          let name =
            match Hashtbl.find_opt renaming name with
            | None -> name
            | Some x -> x
          in
          ( match op_of_attr name x.et with
          | None -> errorf "EAttr '%s', %s" name (Type.show x.et)
          | Some [] -> mi_seq [compile_expr x]
          | Some op -> mi_seq [compile_expr x; instr (MIfield op)] )
    end
  | EVariant name ->
      let x = compile_expr inner in
      ( match Type.getRepr outer.et with
      | TVariant {row = [("None", _); ("Some", t)]} -> compile_option t name x
      | TVariant {row = [("Left", tl); ("Right", tr)]} ->
          compile_or tl tr name x
      | TVariant {row; layout} ->
          let target, ctxt = compile_variant row layout name in
          let l (annot1, acc) t =
            let annot2, t = row_of_tree mt_or t in
            (None, instr (MI1 (Left (annot1, annot2, t))) :: acc)
          in
          let r t (annot2, acc) =
            let annot1, t = row_of_tree mt_or t in
            (None, instr (MI1 (Right (annot1, annot2, t))) :: acc)
          in
          let _, instrs =
            Binary_tree.context_cata (Some target, [x]) l r ctxt
          in
          mi_seq (List.rev instrs)
      | t ->
          let t = Type.show_f Type.pp t in
          errorf "EVariant: not a variant: '%s' '%s'" name t )
  | EIsVariant constructor ->
      compile_match
        inner
        [ ( constructor
          , "dummy"
          , mi_seq [instr (MIpush (mt_bool, MLiteral.bool true))] ) ]
        (mi_seq [instr (MIpush (mt_bool, MLiteral.bool false))])
  | EReadTicket ->
      let* () = compile_expr inner in
      let* () = instr (MI1 Read_ticket) in
      instr (MI2 (Pair (None, None)))
  | EJoinTickets -> mi_seq [compile_expr inner; instr (MI1 Join_tickets)]
  | EPairingCheck -> mi_seq [compile_expr inner; instr (MI1 Pairing_check)]
  | EVotingPower -> mi_seq [compile_expr inner; instr (MI1 Voting_power)]

and compile_expr e =
  match e.e with
  | EPrim0 prim -> compile_prim0 e prim
  | EPrim1 (prim, x) -> compile_prim1 e x prim
  | EOpenVariant (constructor, inner, missing_message) ->
      let default, args =
        match missing_message with
        | None ->
            let l = Option.default (-1) e.line_no in
            let l = instr (MIpush (mt_int, MLiteral.small_int l)) in
            (true, l)
        | Some missing_message -> (false, compile_expr missing_message)
      in
      compile_match
        ~no_drop:()
        inner
        [(constructor, "dummy", tag_top [st_none ()])]
        (mi_failwith
           ~default
           ~context:(texpr_to_string e)
           ~line_no:e.line_no
           ~args
           "OpenVariant")
  | EMake_signature _ ->
      errorf
        "Expression %s cannot be converted to Michelson (missing \
         pre-evaluation)"
        (String.escaped (texpr_to_string e))
  | EMichelson (michelson, exprs) ->
      let* () = iter_list id (List.rev_map compile_expr exprs) in
      let {name; parsed; typesIn; typesOut} = michelson in
      let x = Of_micheline.instruction parsed in
      begin
        match x.instr with
        | MIconcat_unresolved | MIerror _ ->
            let typesIn = List.map mtype_of_type typesIn in
            let typesOut = List.map mtype_of_type typesOut in
            instr (MImich {name; parsed; typesIn; typesOut})
        | _ -> instr x.instr
      end
  | EMapFunction {l; f} ->
    ( match f.e with
    | ELambda {id; name = _; body} ->
        let* () = compile_expr l in
        let* tags = get_tags in
        let go_fun () =
          match tags with
          | Some (_ :: ts) ->
              let tag = ST_target (T_lambda_parameter id) in
              let* () = set_tags (Some (tag :: ts)) in
              let* () = compile_command body in
              drop_target (T_lambda_parameter id)
          | _ -> errorf "map compilation error"
        in
        mi_map (go_fun ())
    | _ ->
        let* () = compile_expr f (* f *) in
        let* () = compile_expr l (* l f *) in
        let* tags = get_tags in
        ( match tags with
        | Some (_ :: ts) ->
            let go_fun =
              let* () = set_tags (Some (st_none () :: ts)) in
              let* () = mi_copy 1 (* f x f *) in
              instrs [MIdig 1 (* x f f *); MI2 Exec (* y f *)]
            in
            mi_seq [mi_map go_fun; instr (MIdig 1); instr MIdrop]
        | _ -> error "map compilation error" ) )
  | EItem {items; key; default_value; missing_message} ->
      let line_no = e.line_no in
      let on_option =
        let l = Option.default (-1) e.line_no in
        let missed =
          match default_value with
          | None ->
              let default, args =
                match missing_message with
                | None ->
                    let l = instr (MIpush (mt_int, MLiteral.small_int l)) in
                    (true, l)
                | Some missing_message -> (false, compile_expr missing_message)
              in
              mi_failwith
                ~default
                ~line_no
                ~context:(texpr_to_string e)
                ~args
                "GetItem"
          | Some v -> compile_expr v
        in
        let comment = commentf "of_some: Get-item:%d" l in
        mi_if_some ~protect_top:() comment missed
      in
      mi_seq [compile_expr items; compile_expr key; instr (MI2 Get); on_option]
  | EPrim2 (EGetOpt, m, k) ->
      mi_seq [compile_expr m; compile_expr k; instr (MI2 Get)]
  | EPrim2 (EBinOpInf op, x, y) ->
      compile_binop
        ~line_no:e.line_no
        ~context:(texpr_to_string e)
        (Some (compile_expr x))
        (compile_expr y)
        y.et
        op
  | ERecord entries ->
      let entries = List.map (map_snd compile_expr) entries in
      let l (a, i) = (Some a, i) in
      let p (a1, x1) (a2, x2) =
        (None, mi_seq [x2; x1; instr (MI2 (Pair (a1, a2)))])
      in
      snd (Binary_tree.cata l p (compile_record e.et entries))
  | ETuple es ->
      let f x y = mi_seq [y; x; instr (MI2 (Pair (None, None)))] in
      compile_tuple f (List.map compile_expr es)
  | EPrim2 (ECons, x, l) ->
      let x = compile_expr x in
      let l = compile_expr l in
      mi_seq [l; x; instr (MI2 Cons)]
  | EPrim2 (EBinOpPre op, x, y) ->
      let* () = compile_expr x (* x *) in
      let* () = instr (MIdup 1) (* x x *) in
      let* () = compile_expr y (* y x x *) in
      let* () = instr (MIdup 1) (* y y x x *) in
      let* () = instr (MIdug 2) (* y x y x *) in
      let* () = instr (MI2 Compare) in
      let* () = instr (MI1 Le) (* (y<=x) y x *) in
      let x = instr MIdrop in
      let y = mi_seq [instr (MIdig 1); instr MIdrop] in
      ( match op with
      | BMin -> mi_if ~protect_top:() y x
      | BMax -> mi_if ~protect_top:() x y )
  | EPrim2 (EAdd_seconds, t, s) ->
      mi_seq [compile_expr t; compile_expr s; instr (MI2 Add)]
  | EPrim3 (ECheck_signature, pub_key, signature, bytes) ->
      let* () = compile_expr bytes in
      let* () = compile_expr signature in
      let* () = compile_expr pub_key in
      instr (MI3 Check_signature)
  | ESlice {offset; length; buffer} ->
      let* () = compile_expr buffer in
      let* () = compile_expr length in
      let* () = compile_expr offset in
      instr (MI3 Slice)
  | EPrim2 (EContains, items, member) ->
    ( match Type.getRepr items.et with
    | TMap _ -> mi_seq [compile_expr items; compile_expr member; instr (MI2 Mem)]
    | TSet _ -> mi_seq [compile_expr items; compile_expr member; instr (MI2 Mem)]
    | _ -> error "EContains" )
  | EPrim3 (ESplit_tokens, e, quantity, {e = EPrim0 (ECst (Int {i}))})
    when Bigint.equal i (Bigint.of_int 1) ->
      mi_seq [compile_expr e; compile_expr quantity; instr (MI2 Mul)]
  | EPrim3 (ESplit_tokens, amount, quantity, total) ->
      let* () = compile_expr total in
      let* () = compile_expr amount in
      let* () = compile_expr quantity in
      let* () = instr (MI2 Mul) in
      let* () = instr (MI2 Ediv) in
      let* () = of_Some ~line_no:e.line_no ~context:(texpr_to_string e) in
      mi_car
  | EList elems ->
    ( match mtype_of_type e.et with
    | {mt = MT1 (T_list, t)} ->
        let* () = instr (MIpush (mt_list t, MLiteral.list [])) in
        mi_seq
          (List.rev_map
             (fun value -> mi_seq [compile_expr value; instr (MI2 Cons)])
             elems)
    | _ -> failwith "elems" )
  | EMap (_, entries) ->
      let* () =
        match mtype_of_type e.et with
        | {mt = MT2 (T_big_map, k, v)} -> instr (MI0 (Empty_bigmap (k, v)))
        | mt -> instr (MIpush (mt, MLiteral.mk_map []))
      in
      mi_seq
        (List.map
           (fun (key, value) ->
             let* () = compile_expr value in
             let* () = instr (MI1 Some_) in
             let* () = compile_expr key in
             instr (MI3 Update))
           entries)
  | ESet entries ->
      let* () = instr (MIpush (mtype_of_type e.et, MLiteral.set [])) in
      mi_seq
        (List.map
           (fun key ->
             let* () = instr (MIpush (mt_bool, MLiteral.bool true)) in
             let* () = compile_expr key in
             instr (MI3 Update))
           entries)
  | EContract {entry_point; arg_type; address} ->
      let* () = compile_expr address in
      instr (MI1 (Contract (entry_point, mtype_of_type arg_type)))
  | EPrim3 (ERange, a, b, step) ->
      let range_result_target = T_var "__range_result" in
      let body =
        if_stack_ok (fun tags ->
            match find_target range_result_target tags with
            | None -> errorf "find_target %s" (show_target range_result_target)
            | Some offset ->
                instrs
                  [MIdup 1; MIdig (offset + 1); MIdig 1; MI2 Cons; MIdug offset])
      in
      let* () = comment (texpr_to_string e) in
      let* () = instr (MI0 (Nil (mtype_of_type a.et))) in
      let* () = tag_top [ST_target range_result_target] in
      let* () =
        let line_no = e.line_no in
        let name = "RANGE" in
        let has_operations = false in
        compile_range ~line_no ~a ~b ~step ~name ~body ~has_operations
      in
      let* () = mi_rev_list a.et in
      tag_top [st_none ()]
  | EPrim2 (ECallLambda, lambda, parameter) ->
      mi_seq [compile_expr lambda; compile_expr parameter; instr (MI2 Exec)]
  | EPrim2 (EApplyLambda, lambda, parameter) ->
      mi_seq [compile_expr lambda; compile_expr parameter; instr (MI2 Apply)]
  | ELambda {id; name = _; tParams; body; tResult} ->
      let tParams = mtype_of_type tParams in
      let tResult = mtype_of_type tResult in
      let body =
        let* () = tag_top [ST_target (T_lambda_parameter id)] in
        let* () = compile_command body in
        drop_target (T_lambda_parameter id)
      in
      composite (MIlambda (tParams, tResult, body))
  | ELambdaParams {id} ->
      fetch_target ~dup:(Type.is_hot e.et <> Yes) (T_lambda_parameter id)
  | ECreate_contract {baker; contract_template} ->
      let* {config} = ask in
      let storage = Option.of_some contract_template.storage in
      let ccc =
        compile_contract
          ~config
          None
          { contract_template with
            global_variables = [] (* ************ todo **** *) }
      in
      let* () = compile_expr storage in
      let* () = compile_expr contract_template.balance in
      let* () = compile_expr baker in
      let* () =
        instr
          (MIcreate_contract
             { tparameter = ccc.tparameter
             ; tstorage = ccc.tstorage
             ; code = erase_types_instr ccc.code })
      in
      instr (MI2 (Pair (None, None)))
  | EPrim3 (EUpdate_map, m, k, v) ->
      let* () = compile_expr m in
      let* () = compile_expr v in
      let* () = compile_expr k in
      instr (MI3 Update)
  | EPrim3 (EGet_and_update, m, k, v) ->
      let* () = compile_expr m in
      let* () = compile_expr v in
      let* () = compile_expr k in
      instrs [MI3 Get_and_update; MI2 (Pair (None, None))]
  | ETransfer {arg; amount; destination} ->
      let* () = compile_expr destination in
      let* () = compile_expr amount in
      let* () = compile_expr arg in
      instr (MI3 Transfer_tokens)
  | EMatch _ -> error "ematch"
  | EPrim3 (EIf, c, x, y) ->
      mi_seq
        [compile_expr c; mi_if ~protect_top:() (compile_expr x) (compile_expr y)]
  | EPrim2 (ETest_ticket _, _, _) -> failwith "cannot compile test ticket"
  | ESaplingVerifyUpdate {state; transaction} ->
      let* () = compile_expr state in
      let* () = compile_expr transaction in
      instr (MI2 Sapling_verify_update)
  | EPrim2 (ETicket, content, amount) ->
      mi_seq [compile_expr amount; compile_expr content; instr (MI2 Ticket)]
  | EPrim2 (ESplitTicket, ticket, decomposition) ->
      mi_seq
        [ compile_expr decomposition
        ; compile_expr ticket
        ; instr (MI2 Split_ticket) ]

and compile_container_access_list container projection t rev =
  let* () = instr (MI0 (Nil (mtype_of_type t))) in
  let* () = compile_expr container in
  let* () = mi_iter (mi_seq [projection; instr (MI2 Cons)]) in
  if rev then return () else mi_rev_list t

and compile_match ?no_drop scrutinee cases (body_else : unit t) =
  let body_tagged arg_name body =
    let tag = T_var arg_name in
    let* () = tag_top [ST_target tag] in
    let* () = body in
    if no_drop = Some () then return () else drop_target tag
  in
  let drop_else = mi_seq [instr MIdrop; body_else] in
  let tree =
    match cases with
    | [("None", _arg_name, body)] -> mi_if_some ~protect_top:() drop_else body
    | [("Some", arg_name, body)] ->
        mi_if_some ~protect_top:() (body_tagged arg_name body) body_else
    | [("Some", arg_name, body1); ("None", _arg_name, body2)]
     |[("None", _arg_name, body2); ("Some", arg_name, body1)] ->
        mi_if_some ~protect_top:() (body_tagged arg_name body1) body2
    | [("Left", arg_name, body)] ->
        mi_if_left ~protect_top:() (body_tagged arg_name body) drop_else
    | [("Right", arg_name, body)] ->
        mi_if_left ~protect_top:() drop_else (body_tagged arg_name body)
    | _ ->
        let compile_match =
          let leaf Layout.{source} =
            match
              List.find_opt
                (fun (constructor, _, _) -> constructor = source)
                cases
            with
            | None -> drop_else
            | Some (_constructor, arg_name, body) -> body_tagged arg_name body
          in
          Binary_tree.cata leaf (mi_if_left ~protect_top:())
        in
        let layout =
          match Type.getRepr scrutinee.et with
          | TVariant {layout} -> Unknown.getRefOption layout
          | _ -> None
        in
        let tree =
          match layout with
          | None -> error "Not a variant type"
          | Some layout -> compile_match layout
        in
        tree
  in
  mi_seq [compile_expr scrutinee; tree]

and compile_match_cons expr id ok_match ko_match =
  let* () = compile_expr expr in
  mi_if_cons
    ~protect_top:()
    (let* () =
       tag_top
         [ ST_target (T_match_cons (id, true))
         ; ST_target (T_match_cons (id, false)) ]
     in
     let* () = ok_match in
     let* () = drop_target (T_match_cons (id, true)) in
     drop_target (T_match_cons (id, false)))
    ko_match

and of_Some ~line_no ~context =
  let comment = comment context in
  mi_if_some comment (mi_failwith ~default:true ~line_no ~context "OfSome")

(** Return an instruction that makes the value of [e] available. The
   instruction is either [dup_tag] or [PUSH], depending on whether [e]
   is a constant or not. Currently only implemented for int and
   nat. *)
and get_target_or_cst e tag =
  let put_tag = tag_top [ST_target tag] in
  match (e.e, mtype_of_type e.et) with
  | EPrim0 (ECst (Int {i})), {mt = MT0 T_int} ->
      return (mi_seq [instr (MIpush (mt_int, MLiteral.int i)); put_tag])
  | EPrim0 (ECst (Int {i})), {mt = MT0 T_nat} ->
      return (mi_seq [instr (MIpush (mt_nat, MLiteral.int i)); put_tag])
  | _ ->
      let* () = compile_expr e in
      let* () = put_tag in
      return (fetch_target ~dup:true tag)

and compile_range ~line_no ~a ~b ~step ~name ~body ~has_operations =
  let i = T_var name in
  let* () = ensure_ops_if has_operations in
  let* get_b = get_target_or_cst b (T_var (name ^ "#b")) in
  let* get_step = get_target_or_cst step (T_var (name ^ "#step")) in
  let cond cmp =
    mi_seq [fetch_target ~dup:true i; get_b; instr (MI2 Compare); cmp]
  in
  let loop cmp =
    let* () = cond cmp in
    mi_loop
      (let* () = body in
       let* () = comment "loop step" in
       let* () = get_step in
       let* () = instr (MI2 Add) in
       let* () = tag_top [ST_target i] in
       cond cmp)
  in
  let* () = compile_expr a in
  let* () = tag_top [ST_target i] (* i := a *) in
  let* () =
    let err =
      mi_failwith ~default:true ~line_no ~context:"range" "ZeroRangeStep"
    in
    match mtype_of_type step.et with
    | {mt = MT0 T_int} ->
        let* () = get_step in
        let* () = instr (MI1 Gt) in
        mi_if (* step > 0 *)
          (loop (instr (MI1 Gt)))
          (mi_seq
             [ get_step
             ; instr (MI1 Lt)
             ; mi_if (* step < 0 *) (loop (instr (MI1 Lt))) err ])
    | {mt = MT0 T_nat} ->
        let* () = get_step in
        let* () = instr (MIpush (mt_nat, MLiteral.small_int 0)) in
        let* () = instr (MI2 Compare) in
        let* () = instr (MI1 Eq) in
        mi_if err (loop (instr (MI1 Gt)))
    | _ -> assert false
  in
  let* () = drop_target i in
  let* () = drop_target (T_var (name ^ "#b")) in
  let* () = drop_target (T_var (name ^ "#step")) in
  return ()

and unzip_lstep ~line_no ~context = function
  | LMapItem (key, tvalue) ->
      let* zip_container = unzip_map (compile_expr key) tvalue in
      let* zip_option =
        unzip_option ~line_no ~context ~args:(instr (MI2 (Pair (None, None))))
      in
      return
        (let* () = zip_option in
         zip_container)
  | LAttr (name, t) ->
      let* zip_attr = unzip_attr name t in
      return zip_attr

and unzip_path ~line_no ~context = function
  | [] -> return (return ())
  | s :: rest ->
      let* zip_lstep = unzip_lstep ~line_no ~context s in
      let* zip_rest = unzip_path ~line_no ~context rest in
      return
        (let* () = zip_rest in
         zip_lstep)

and unzip_lexpr ~line_no ~context ~copy lexpr =
  let target, lsteps = target_of_lexpr lexpr in
  let* found, zip_target = unzip_target ~copy target in
  match (lsteps, found) with
  | [], false -> return (false, zip_target)
  | _, true ->
      let* zip_path = unzip_path ~line_no ~context lsteps in
      return
        ( true
        , let* () = zip_path in
          zip_target )
  | _ -> assert false

and compile_delete ~line_no ~context lexpr =
  match last_step_is_item lexpr with
  | None -> assert false
  | Some (lexpr', key, tvalue) ->
      let copy = self_referential_lexpr lexpr in
      let* found, zip_lexpr = unzip_lexpr ~line_no ~context ~copy lexpr' in
      assert found;
      let* () =
        instr (MIpush (mt_option (mtype_of_type tvalue), MLiteral.none))
      in
      let* () = compile_expr key in
      let* () = instr (MI3 Update) in
      zip_lexpr

and compile_assign ~line_no ~context ~copy lexpr rhs =
  match last_step_is_item lexpr with
  | None ->
      let* found, zip_lexpr = unzip_lexpr ~line_no ~context ~copy lexpr in
      let* () = when_ found (instr MIdrop) in
      let* () = rhs in
      let* () = zip_lexpr in
      return ()
  | Some (lexpr', key, _) ->
      (* Do not look up the key. *)
      let* found, zip_lexpr = unzip_lexpr ~line_no ~context ~copy lexpr' in
      assert found;
      let* () = rhs in
      let* () = instr (MI1 Some_) in
      let* () = compile_expr key in
      let* () = instr (MI3 Update) in
      zip_lexpr

and compile_update ~line_no ~context ~copy lexpr rhs =
  let* found, zip_lexpr = unzip_lexpr ~line_no ~context ~copy lexpr in
  assert found;
  let* () = rhs in
  zip_lexpr

and compile_command x =
  match x.c with
  | CNever inner -> mi_seq [compile_expr inner; instr (MI1_fail Never)]
  | CDefineLocal (_, e) ->
      let* () = comment (tcommand_to_string x) in
      let* () = compile_expr e in
      let* () = instr MIdrop in
      push_unit
  | CBind (x, ({c = CDefineLocal (name, expr)} as def_local), c) ->
      assert (x = None);
      let* () = comment (tcommand_to_string def_local) in
      let* () = compile_expr expr in
      let* () = tag_top [ST_target (T_var name)] in
      let* () = compile_command c in
      drop_target (T_var name)
  | CBind (_, c, ({c = CDefineLocal (_, e)} as def_local)) ->
      let* () = compile_command c in
      let* () = instr MIdrop in
      let* () = comment (tcommand_to_string def_local) in
      let* () = compile_expr e in
      let* () = instr MIdrop in
      push_unit
  | CBind (None, c1, c2) ->
      let* () = compile_command c1 in
      let* () = instr MIdrop in
      compile_command c2
  | CBind (x, c1, c2) ->
      let* () = compile_command c1 in
      ( match x with
      | None ->
          let* () = instr MIdrop in
          compile_command c2
      | Some x ->
          let* () = tag_top [ST_target (T_var x)] in
          let* () = compile_command c2 in
          drop_target (T_var x) )
  | CResult {e = EPrim0 (ECst Literal.Unit)} -> push_unit
  | CResult e ->
      let* () = commentf "sp.result(%s)" (texpr_to_string e) in
      let* () = compile_expr e in
      tag_top [st_none ()]
  | CIf (c, t, e) ->
      let* () = commentf "if %s:" (texpr_to_string c) in
      let* () = compile_expr c in
      mi_if ~protect_top:() (compile_command t) (compile_command e)
  | CMatch (scrutinee, cases) ->
      let cases =
        List.map
          (fun (constructor, name, body) ->
            (constructor, name, compile_command body))
          cases
      in
      let* () =
        commentf "with %s.match_cases(...):" (texpr_to_string scrutinee)
      in
      compile_match scrutinee cases push_unit
  | CMatchCons {expr; id; ok_match; ko_match} ->
      let* () =
        commentf "with sp.match_cons(%s) as %s:" (texpr_to_string expr) id
      in
      compile_match_cons
        expr
        id
        (compile_command ok_match)
        (compile_command ko_match)
  | CMatchProduct (scrutinee, p, c) ->
      let* () = comment (print_match_product scrutinee p) in
      let* () = compile_expr scrutinee in
      let* drop_vars = explode_pattern scrutinee.et p in
      let* () = compile_command c in
      drop_vars
  | CModifyProduct (lhs, Pattern_record (name, p), body) ->
      let* () = comment (print_modify_product lhs (Pattern_record (name, p))) in
      ( match lexpr_of_expr lhs with
      | None -> error "invalid lexpr: CModifyProduct"
      | Some lexpr ->
          let p =
            match Type.getRepr lhs.et with
            | TRecord {row} -> List.map (fun (var, _) -> {var; field = var}) row
            | _ -> assert false
          in
          let* () = ensure_ops_if (has_operations body <> HO_none) in
          let line_no = x.line_no in
          let context = tcommand_to_string x in
          let copy = occurs_lroot_command lexpr.lroot body in
          let* found, zip_lexpr = unzip_lexpr ~line_no ~context ~copy lexpr in
          assert found;
          let* close_record = open_record_pattern name lhs.et p in
          let* () = compile_command body in
          let* () = instr MIdrop in
          let* () = close_record in
          let* () = zip_lexpr in
          push_unit )
  | CModifyProduct (lhs, p, c) ->
      let* () = comment (print_modify_product lhs p) in
      ( match lexpr_of_expr lhs with
      | None -> error "invalid lexpr: CModifyProduct"
      | Some lexpr ->
          let* () = ensure_ops_if (has_operations c <> HO_none) in
          let line_no = x.line_no in
          let context = tcommand_to_string x in
          let copy = occurs_lroot_command lexpr.lroot c in
          let* found, zip_lexpr = unzip_lexpr ~line_no ~context ~copy lexpr in
          assert found;
          let* drop_vars = explode_pattern lhs.et p in
          let* () = compile_command c in
          let* () = drop_vars in
          let* () = zip_lexpr in
          push_unit )
  | CFailwith message ->
      mi_failwith
        ~default:false
        ~line_no:x.line_no
        ~context:(tcommand_to_string x)
        ~args:(compile_expr message)
        "failwith"
  | CVerify (e, message) ->
      let error =
        let default, message =
          match message with
          | None ->
              ( true
              , instr
                  (MIpush
                     ( mt_string
                     , MLiteral.string
                         (sprintf "WrongCondition: %s" (texpr_to_string e)) ))
              )
          | Some message -> (false, compile_expr message)
        in
        mi_failwith
          ~default
          ~verify:()
          ~line_no:x.line_no
          ~context:(texpr_to_string e)
          ~args:message
          "verify"
      in
      let* () = comment (tcommand_to_string x) in
      let* () = compile_expr e in
      let* () = mi_if (return ()) error in
      push_unit
  | CSetVar (lhs, e) ->
      let* () = comment (tcommand_to_string x) in
      let* () =
        match lexpr_of_expr lhs with
        | None -> errorf "Invalid lexpr (CSetVar): %s" (texpr_to_string lhs)
        | Some lexpr_lhs ->
            let* () = ensure_ops_if (lexpr_lhs.lroot = LOperations) in
            let line_no = x.line_no in
            let context = tcommand_to_string x in
            ( match e with
            | {e = EPrim2 (EBinOpInf op, e1, e2)} when equal_texpr lhs e1 ->
                let copy =
                  occurs_lroot_expr lexpr_lhs.lroot e2
                  || self_referential_lexpr lexpr_lhs
                in
                let e2' = compile_expr e2 in
                let e = compile_binop ~line_no ~context None e2' e2.et op in
                compile_update ~line_no ~context ~copy lexpr_lhs e
            | _ ->
                let copy =
                  occurs_lroot_expr lexpr_lhs.lroot e
                  || self_referential_lexpr lexpr_lhs
                in
                compile_assign
                  ~line_no
                  ~context
                  ~copy
                  lexpr_lhs
                  (compile_expr e) )
      in
      push_unit
  | CUpdateSet (lhs, key, add) ->
      let* () = comment (tcommand_to_string x) in
      ( match lexpr_of_expr lhs with
      | None -> errorf "Invalid lexpr (CSetVar): %s" (texpr_to_string lhs)
      | Some lhs ->
          let copy =
            occurs_lroot_expr lhs.lroot key || self_referential_lexpr lhs
          in
          let* found, zip_lexpr =
            unzip_lexpr
              ~line_no:x.line_no
              ~context:(tcommand_to_string x)
              ~copy
              lhs
          in
          assert found;
          let* () = instr (MIpush (mt_bool, MLiteral.bool add)) in
          let* () = compile_expr key in
          let* () = instr (MI3 Update) in
          let* () = zip_lexpr in
          push_unit )
  | CFor (name, ({e = EPrim3 (ERange, a, b, step)} as range), body)
    when List.mem (mtype_of_type a.et) [mt_nat; mt_int] ->
      let* () =
        commentf
          "for %s in %s: ... (%s)"
          name
          (texpr_to_string range)
          (type_to_string step.et)
      in
      let line_no = x.line_no in
      let has_operations = has_operations body <> HO_none in
      let body = compile_command body >> instr MIdrop in
      let* () =
        compile_range ~line_no ~a ~b ~step ~name ~body ~has_operations
      in
      push_unit
  | CFor (name, container, body) ->
      let projection, map, onMap =
        match container.e with
        | EPrim1 (EListItems false, map) ->
          ( match Type.getRepr map.et with
          | TMap _ -> (return (), map, mi_cdr)
          | _ ->
              ( errorf "map.items error: %s" (type_to_string map.et)
              , map
              , return () ) )
        | EPrim1 (EListValues false, map) -> (mi_cdr, map, return ())
        | EPrim1 (EListKeys false, map) -> (mi_car, map, return ())
        | EPrim1 (EListElements false, set) -> (return (), set, return ())
        | _ -> (return (), container, return ())
      in
      let lcontainer = lexpr_of_expr map in
      let iterName = T_var name in
      let writes = modifies_iter name body in
      let* () = commentf "for %s in %s: ..." name (texpr_to_string container) in
      let* () = ensure_ops_if (has_operations body <> HO_none) in
      let* () = compile_expr map in
      let* () =
        (if writes then mi_map else mi_iter)
          (let* () = projection in
           let* () = tag_top [ST_target iterName] in
           let* () = compile_command body in
           let* () = instr MIdrop in
           let* () = if writes then onMap else return () in
           if writes then return () else drop_target iterName)
      in
      let* () =
        match lcontainer with
        | None when writes ->
            errorf "Invalid lexpr (CFor): %s" (texpr_to_string map)
        | Some lhs when writes ->
            let t = T_var (name ^ "#iteratee") in
            let* () = tag_top [ST_target t] in
            let* () =
              compile_assign
                ~line_no:x.line_no
                ~context:"for"
                ~copy:true
                lhs
                (fetch_target ~dup:true t)
            in
            drop_target t
        | _ -> return ()
      in
      push_unit
  | CDelItem (map, key) ->
    ( match (lexpr_of_expr map, map.et) with
    | Some lexpr, F (TMap {tvalue}) ->
        let lexpr = extend_lexpr lexpr (LMapItem (key, tvalue)) in
        let context = tcommand_to_string x in
        let* () = comment (tcommand_to_string x) in
        let* () = compile_delete ~line_no:x.line_no ~context lexpr in
        push_unit
    | _ -> errorf "invalid lexpr" )
  | CWhile (e, c) ->
      let* () = commentf "while %s : ..." (texpr_to_string e) in
      let* () = ensure_ops_if (has_operations c <> HO_none) in
      let* () = compile_expr e in
      let* () =
        mi_loop
          (let* () = compile_command c in
           let* () = instr MIdrop in
           let* () = commentf "check for next loop: %s" (texpr_to_string e) in
           compile_expr e)
      in
      push_unit
  | CComment s -> comment s
  | CSetResultType (c, _) -> compile_command c
  | CSetType _ | CTrace _ -> push_unit

and compile_entry_point (ep : _ entry_point) =
  let* () = commentf "== %s ==" ep.channel in
  let* () = tag_top [ST_target tgt_parameter; ST_target tgt_storage] in
  let* () = compile_command ep.body in
  let* () = if_stack_ok (fun _ -> instr MIdrop) in
  drop_target tgt_parameter

(** Tidies up the stack at the end of the contract. *)

and pre_finalize_contract has_operations =
  let* tags = get_tags in
  match tags with
  | None -> return ()
  | Some _ ->
    ( match has_operations with
    | Has_operations.HO_none ->
        mi_seq [ops_init; instr (MI2 (Pair (None, None)))]
    | HO_at_most_one -> instr (MI2 (Pair (None, None)))
    | HO_many ->
        mi_seq [mi_rev_list Type.operation; instr (MI2 (Pair (None, None)))] )

and pre_finalize_contract_lazy =
  let* () =
    instrs [MIunpair [true; true]; MIdug 2; MI2 (Pair (None, None)); MIdig 1]
  in
  let* () = tag_top [ST_target tgt_operations] in
  let* () = mi_rev_list Type.operation in
  instr (MI2 (Pair (None, None)))

and assert_singleton_stack =
  let* tags = get_tags in
  match tags with
  | Some [_] -> return ()
  | None -> return ()
  | _ -> error "assert_singleton_stack"

and lazify_multiple_body cmd =
  let* instrs, _ =
    block
      (Some [st_none ()])
      (let* () = instrs [MIunpair [true; true]; MIunpair [true; true]] in
       let* () =
         tag_top
           [ ST_target tgt_parameter
           ; ST_target tgt_storage
           ; ST_target T_self_address ]
       in
       let* () = cmd in
       let* () = drop_target T_self_address in
       let* () =
         get_tags
         >>= function
         | Some tags when not (has_target tgt_operations tags) -> ops_init
         | _ -> return ()
       in
       instr (MI2 (Pair (None, None))))
  in
  let body = [MLiteral.(small_int 0, instr {instr = MIseq instrs})] in
  return (MLiteral.mk_map body)

and lazify_multiple_call path =
  let* () = instr (MI2 (Pair (None, None))) in
  let* () = fetch_target ~dup:true T_entry_points in
  let* () = instr (MIfield path) in
  let* () = instr (MIpush (mt_nat, MLiteral.small_int 0)) in
  let* () = instr (MI2 Get) in
  let* () = of_Some ~line_no:None ~context:"missing entry point" in
  let* () = instrs [MIdig 1; MI0 (Self None)] in
  let* () = instr (MI1 Address) in
  let* () = tag_top [ST_target T_self_address] in
  let* () = instrs [MIdig 1; MI2 (Pair (None, None))] in
  instr (MI2 Exec)

and lazify_single_body id path cmd =
  let rec proj = function
    | [] -> return ()
    | A :: rest ->
        mi_if_left
          (proj rest)
          (mi_failwith ~default:true ~line_no:None ~context:"" "")
    | D :: rest ->
        mi_if_left
          (mi_failwith ~default:true ~line_no:None ~context:"" "")
          (proj rest)
  in
  let* instrs, _ =
    block
      (Some [st_none ()])
      (let* () = instrs [MIunpair [true; true]; MIunpair [true; true]] in
       let* () =
         tag_top
           [ ST_target tgt_parameter
           ; ST_target tgt_storage
           ; ST_target T_self_address ]
       in
       let* () = proj path in
       let* () = cmd in
       let* () = drop_target T_self_address in
       let* () =
         get_tags
         >>= function
         | Some tags when not (has_target tgt_operations tags) -> ops_init
         | _ -> return ()
       in
       instr (MI2 (Pair (None, None))))
  in
  let body = {instr = MIseq instrs} in
  return [(MLiteral.small_int id, MLiteral.instr body)]

and lazify_single_call id =
  let* () = instr MIdrop in
  instr (MIpush (mt_nat, MLiteral.small_int id))

and compile_entry_points eps layout global_variables lazification =
  let* () =
    A.iter_list
      (fun (name, expr) ->
        let* () = commentf "Global variable: %s" name in
        let* () = compile_expr expr in
        let* () = tag_top [ST_target (T_var name)] in
        instr (MIdig 1))
      global_variables
  in
  let* () =
    match lazification with
    | None -> instr (MIunpair [true; true])
    | Some (Config.Multiple | Single) ->
        let* () =
          instrs [MIunpair [true; true]; MIdig 1; MIunpair [true; true]; MIdig 2]
        in
        tag_top
          [ ST_target tgt_parameter
          ; ST_target tgt_storage
          ; ST_target T_entry_points ]
  in
  let layout =
    Binary_tree.map
      (fun Layout.{source} ->
        List.find (fun ({channel} : _ entry_point) -> channel = source) eps)
      layout
  in
  let has_ops =
    let f (ep : _ entry_point) = has_operations ep.body in
    Binary_tree.cata f Has_operations.or_ layout
  in
  let layout =
    Binary_tree.(
      cata
        (fun ep path -> Leaf (path, compile_entry_point ep))
        (fun l1 l2 path -> Node (l1 (path @ [A]), l2 (path @ [D])))
        layout
        [])
  in
  let* lazy_entry_points =
    match lazification with
    | None ->
        let* () =
          Binary_tree.cata
            (fun (_, ep) -> ep)
            (fun l1 l2 -> void (mi_if_left l1 l2))
            layout
        in
        let* () = pre_finalize_contract has_ops in
        return None
    | Some Multiple ->
        let* layout =
          T.map
            (fun (path, ep) ->
              let* body = lazify_multiple_body ep in
              return (path, body))
            layout
        in
        let* () =
          Binary_tree.cata
            (fun (path, _) -> lazify_multiple_call path)
            (mi_if_left ~protect_top:())
            layout
        in
        let* () = pre_finalize_contract_lazy in
        let l = Binary_tree.cata snd MLiteral.pair layout in
        return (Some l)
    | Some Single ->
        let* () = instr (MIdup 1) in
        let* () = tag_top [st_none ()] in
        let* layout =
          T.map
            (fun (id, (path, ep)) ->
              let* body = lazify_single_body id path ep in
              return (id, body))
            (snd (Binary_tree.map_accum (fun i x -> (i + 1, (i, x))) 0 layout))
        in
        let* () =
          Binary_tree.cata
            (fun (id, _) -> lazify_single_call id)
            (mi_if_left ~protect_top:())
            layout
        in
        let l = Binary_tree.cata snd (fun l1 l2 -> l1 @ l2) layout in
        let* () = instrs [MIdig 3; MIdup 1; MIdug 4; MIdig 1; MI2 Get] in
        let* () = of_Some ~line_no:None ~context:"missing entry point" in
        let* () =
          instrs
            [ MIdig 2
            ; MIdig 2
            ; MI2 (Pair (None, None))
            ; MI0 (Self None)
            ; MI1 Address ]
        in
        let* () = tag_top [ST_target T_self_address] in
        let* () = instrs [MIdig 1; MI2 (Pair (None, None)); MI2 Exec] in
        let* () = pre_finalize_contract_lazy in
        return (Some (MLiteral.mk_map l))
  in
  let* () =
    A.iter_list
      (fun (name, _) -> drop_target (T_var name))
      (List.rev global_variables)
  in
  let* () = assert_singleton_stack in
  return lazy_entry_points

and compile_value_f ~config vt = function
  | Literal l -> compile_literal l
  | Record entries ->
      Binary_tree.cata snd MLiteral.pair (compile_record vt entries)
  | Variant (name, x) ->
    ( match name with
    | "None" -> MLiteral.none
    | "Some" -> MLiteral.some x
    | "Left" -> MLiteral.left x
    | "Right" -> MLiteral.right x
    | _ ->
      ( match Type.getRepr vt with
      | TVariant {row; layout} ->
          let _, ctxt = compile_variant row layout name in
          let l x _ = MLiteral.left x in
          let r _ x = MLiteral.right x in
          Binary_tree.context_cata x l r ctxt
      | _ -> failwith "compile_value: Variant" ) )
  | List xs -> MLiteral.list xs
  | Ticket _ -> MLiteral.string "(some ticket)"
  | Set xs -> MLiteral.set xs
  | Map xs -> MLiteral.mk_map xs
  | Tuple xs -> compile_tuple MLiteral.pair xs
  | Operation _ -> failwith "compile_value: operation"
  | Closure ({id; body}, args) ->
    ( match args with
    | [] ->
        let tags = Some [ST_target (T_lambda_parameter id)] in
        let body =
          mi_seq [compile_command body; drop_target (T_lambda_parameter id)]
        in
        let _, _, instrs = run {tparams = mt_unit; config} {tags} body in
        let body =
          (* Comments are not properly pretty-printed in lambdas so we
           * need to remove them. *)
          simplify ~config ~remove_comments:true (mk_seq instrs)
        in
        MLiteral.instr body
    | _ -> failwith "compile_value: closure with arguments" )

and compile_value ~config v = Value.cata (compile_value_f ~config) v

and simplify_via_michel ~config c =
  match has_error_tcontract ~accept_missings:true c with
  | _ :: _ -> c
  | [] ->
      let st = Michel.Transformer.{var_counter = ref 0} in
      let ({storage; lazy_entry_points} : contract) = erase_types_contract c in
      let c = Michel_decompiler.decompile_contract st c in
      let c = Michel.Transformer.michelsonify st c in
      Michel_compiler.compile_contract ?storage ?lazy_entry_points ~config c

and compile_contract : 'a. config:_ -> _ -> ('a, _, _) contract_f -> tcontract =
 fun ~config
     storage
     { entry_points
     ; tstorage
     ; tparameter
     ; entry_points_layout
     ; flags = contract_flags
     ; global_variables } ->
  let config = Config.apply_flags contract_flags config in
  let layout =
    match entry_points_layout with
    | None ->
        if entry_points = []
        then None
        else
          Some
            (Type.default_layout
               (List.map
                  (fun (ep : _ entry_point) -> ep.channel)
                  (List.filter (fun x -> x.originate) entry_points)))
    | Some l -> Some l
  in
  let tparameter = mtype_of_type tparameter in
  let tstorage = mtype_of_type tstorage in
  let lazification =
    match entry_points with
    | [] -> None
    | _ -> config.lazy_entry_points
  in
  let tlazy_storage =
    match lazification with
    | None -> None
    | Some Multiple ->
        let leaf Layout.{source} =
          let ep =
            List.find
              (fun ({channel} : _ entry_point) -> channel = source)
              entry_points
          in
          mt_big_map
            mt_nat
            (mt_lambda
               (mt_pair
                  (mt_pair (mtype_of_type ep.paramsType) tstorage)
                  mt_address)
               (mt_pair (mt_list mt_operation) tstorage))
        in
        begin
          match layout with
          | None -> assert false
          | Some layout ->
              let t = Binary_tree.cata leaf mt_pair layout in
              Some {t with annot_variable = None}
        end
    | Some Single ->
        let lazy_storage =
          mt_big_map
            mt_nat
            (mt_lambda
               (mt_pair (mt_pair tparameter tstorage) mt_address)
               (mt_pair (mt_list mt_operation) tstorage))
        in
        Some lazy_storage
  in
  let tstorage_with_entry_points =
    match tlazy_storage with
    | None -> tstorage
    | Some t -> mt_pair tstorage t
  in
  let tags = Some [st_none ()] in
  let code =
    match layout with
    | None ->
        let* () = mi_cdr in
        let* () = pre_finalize_contract HO_none in
        return None
    | Some layout ->
        compile_entry_points entry_points layout global_variables lazification
  in
  let lazy_entry_points, _, instrs =
    run {tparams = tparameter; config} {tags} code
  in
  let lazy_entry_points = Option.join lazy_entry_points in
  let code = mk_seq instrs in
  let storage =
    match storage with
    | None -> None
    | Some storage ->
        let storage = compile_value ~config storage in
        ( match lazy_entry_points with
        | None -> Some storage
        | Some entry_points ->
            let f = simplify ~config ~remove_comments:false in
            Some (MLiteral.pair storage (on_instrs f entry_points)) )
  in
  let c =
    typecheck_contract
      ~strict_dup:true
      { tparameter
      ; tstorage = tstorage_with_entry_points
      ; lazy_entry_points
      ; storage
      ; code }
  in
  let c = simplify_contract ~config ~remove_comments:config.erase_comments c in
  let c =
    if config.simplify_via_michel then simplify_via_michel ~config c else c
  in
  let c = erase_types_contract c in
  let c = typecheck_contract ~strict_dup:(not config.disable_dup_check) c in
  c

let compile_value_tcontract ~config {value_tcontract = c} =
  compile_contract ~config c.storage c

let compile_view ~tstorage ~config {name; tparameter; pure; body; doc} =
  let tstorage = mtype_of_type tstorage in
  let stack, tparameter =
    match tparameter with
    | None -> (Stack_ok [{tstorage with annot_variable = Some "storage"}], None)
    | Some tparameter ->
        let tparameter = mtype_of_type tparameter in
        (initial_stack ~tparameter ~tstorage, Some tparameter)
  in
  let tags = Some [st_none ()] in
  let tresult = mtype_of_type body.ct in
  let body =
    match tparameter with
    | None ->
        let* () = tag_top [ST_target tgt_storage] in
        let* () = compile_command body in
        drop_target tgt_storage
    | Some _ ->
        let* () = instr (MIunpair [true; true]) in
        let* () = tag_top [ST_target tgt_parameter; ST_target tgt_storage] in
        let* () = compile_command body in
        let* () = drop_target tgt_parameter in
        drop_target tgt_storage
  in
  let _, _, instrs =
    let tparams = mt_unit (* Only relevant for MIself. *) in
    run {tparams; config} {tags} body
  in
  let code = mk_seq instrs in
  let _ = typecheck_instr ~strict_dup:true ~tparameter:mt_unit stack code in
  let code = simplify ~config ~remove_comments:true code in
  let _ =
    typecheck_instr
      ~strict_dup:(not config.disable_dup_check)
      ~tparameter:mt_unit
      stack
      code
  in
  (name, (tparameter, tresult, code, pure, doc))

let compile_views {value_tcontract = {tstorage; flags; views}} =
  let config = Config.apply_flags flags Config.default in
  List.map (compile_view ~tstorage ~config) views

let _canonicalize_instr, canonicalize_literal =
  let f_tliteral ~t tliteral =
    let t = Result.get_ok_exn t in
    let literal =
      match (t.mt, tliteral) with
      | MT0 T_timestamp, String s ->
        ( match Ptime.of_rfc3339 s with
        | Ok (t, tz, _chars_read) ->
            assert (tz = Some 0);
            let t = Ptime.to_span t in
            ( match Ptime.Span.to_int_s t with
            | None -> assert false
            | Some t -> Int (Bigint.big_int_of_int t) )
        | Error _ -> Int (Bigint.big_int_of_string s) )
      | MT0 T_key_hash, String s ->
          Bytes (Misc.Hex.unhex (Bs58.decode_key_hash s))
      | MT0 T_baker_hash, String s ->
          Bytes (Misc.Hex.unhex (Bs58.decode_baker_hash s))
      | MT0 T_signature, String s ->
          Bytes (Misc.Hex.unhex (Bs58.decode_signature s))
      | MT0 T_address, String s ->
          Bytes (Misc.Hex.unhex (Bs58.decode_address s))
      | MT0 T_key, String s -> Bytes (Misc.Hex.unhex (Bs58.decode_key s))
      | ( ( MT0
              ( T_unit | T_bool | T_nat | T_int | T_mutez | T_string | T_bytes
              | T_chain_id | T_timestamp | T_address | T_key | T_key_hash
              | T_baker_hash | T_signature | T_operation | T_sapling_state _
              | T_sapling_transaction _ | T_never | T_bls12_381_g1
              | T_bls12_381_g2 | T_bls12_381_fr )
          | MT1 ((T_option | T_list | T_set | T_contract | T_ticket), _)
          | MT2 ((T_lambda | T_map | T_big_map), _, _)
          | MTpair _ | MTor _ | MTmissing _ )
        , ( ( Int _ | Bool _ | String _ | Bytes _ | Unit | Pair _ | None_
            | Left _ | Right _ | Some_ _ | Seq _ | Elt _ | AnyMap _ | Instr _ )
          as l ) ) ->
          l
    in
    {literal}
  in
  let f_tinstr ~stack:_ instr = {instr} in
  tcata {f_tinstr; f_tliteral}

let pack_value ~config v =
  let t = mtype_of_type v.vt in
  let tparameter = mt_unit in
  let v = compile_value ~config v in
  let v = typecheck_literal ~strict_dup:true ~tparameter t v in
  let v = canonicalize_literal v in
  let mich = Michelson.To_micheline.literal v in
  Micheline_encoding.(pack_prefix ^ pack_node_expression (node_of_mich mich))

let unpack_value t b =
  let bytes =
    Base.String.chop_prefix_exn b ~prefix:Micheline_encoding.pack_prefix
  in
  let mich = Micheline_encoding.mich_of_bytes (Misc.Hex.hexcape bytes) in
  let pp_mich t i =
    Michelson.display_instr
      (mtype_of_type t)
      (Michelson.Of_micheline.instruction i)
  in
  let value = Micheline.to_value ~pp_mich t mich in
  value
