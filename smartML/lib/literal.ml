(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils

type dynamic_id = {dynamic_id : int}
[@@deriving eq, ord, show {with_path = false}]

type static_id = {static_id : int}
[@@deriving eq, ord, show {with_path = false}]

type contract_id =
  | C_static  of static_id
  | C_dynamic of dynamic_id
[@@deriving eq, ord, show {with_path = false}]

type address =
  | Real  of string
  | Local of contract_id
[@@deriving show, eq, ord]

type sapling_test_state =
  { test : bool
  ; memo : int
  ; elements : (string * Bigint.t) list }
[@@deriving show, eq, ord]

type sapling_test_transaction =
  { source : string option
  ; target : string option
  ; amount : Bigint.t
  ; memo : int }
[@@deriving show, eq, ord]

type 'type_ f =
  | Unit
  | Bool                     of bool
  | Int                      of
      { i : Bigint.t
      ; is_nat : bool Unknown.t_ref
            [@equal fun _ _ -> true] [@compare fun _ _ -> 0] }
  | String                   of string
  | Bytes                    of string
  | Chain_id                 of string
  | Timestamp                of Bigint.t
  | Mutez                    of Bigint.t
  | Address                  of address * string option (* <- entry-point *)
  | Contract                 of
      address * string option (* <- entry-point *) * 'type_
  | Key                      of string
  | Secret_key               of string
  | Key_hash                 of string
  | Baker_hash               of string
  | Signature                of string
  | Sapling_test_state       of sapling_test_state
  | Sapling_test_transaction of sapling_test_transaction
  | Bls12_381_g1             of string
  | Bls12_381_g2             of string
  | Bls12_381_fr             of string
[@@deriving eq, ord, show {with_path = false}, map, fold]

type t = Type.t f [@@deriving show {with_path = false}]

let equal = equal_f (fun _ _ -> true)

let compare = compare_f (fun _ _ -> 0)

let map_type = map_f

let type_of = function
  | Unit -> Type.unit
  | Bool _ -> Type.bool
  | Int {is_nat} -> Type.int_raw ~isNat:is_nat
  | String _ -> Type.string
  | Bytes _ -> Type.bytes
  | Chain_id _ -> Type.chain_id
  | Timestamp _ -> Type.timestamp
  | Mutez _ -> Type.token
  | Address _ -> Type.address
  | Contract (_, _, t) -> Type.contract t
  | Key _ -> Type.key
  | Secret_key _ -> Type.secret_key
  | Key_hash _ -> Type.key_hash
  | Baker_hash _ -> Type.baker_hash
  | Signature _ -> Type.signature
  | Sapling_test_state {memo} -> Type.sapling_state (Some memo)
  | Sapling_test_transaction {memo} -> Type.sapling_transaction (Some memo)
  | Bls12_381_g1 _ -> Type.bls12_381_g1
  | Bls12_381_g2 _ -> Type.bls12_381_g2
  | Bls12_381_fr _ -> Type.bls12_381_fr

let unit = Unit

let bool x = Bool x

let int i = Int {i; is_nat = ref (Unknown.UnValue false)}

let nat i = Int {i; is_nat = ref (Unknown.UnValue true)}

let intOrNat t i =
  match Type.getRepr t with
  | TInt {isNat} -> Int {i; is_nat = isNat}
  | _ -> assert false

let small_int i = int (Bigint.of_int i)

let small_nat i = nat (Bigint.of_int i)

let string s = String s

let bytes s = Bytes s

let chain_id s = Chain_id s

let timestamp i = Timestamp i

let mutez i = Mutez i

let meta_address ?entry_point s = Address (s, entry_point)

let address ?entry_point s = meta_address ?entry_point (Real s)

let local_address ?entry_point s = meta_address ?entry_point (Local s)

let meta_contract ?entry_point s t = Contract (s, entry_point, t)

let contract ?entry_point s = meta_contract ?entry_point (Real s)

let local_contract ?entry_point s = meta_contract ?entry_point (Local s)

let key s = Key s

let secret_key s = Secret_key s

let key_hash s = Key_hash s

let baker_hash h = Baker_hash h

let signature s = Signature s

let sapling_test_state memo elements =
  Sapling_test_state {test = true; elements; memo}

let sapling_state_real memo =
  Sapling_test_state {test = false; elements = []; memo}

let sapling_test_transaction memo source target amount =
  Sapling_test_transaction {source; target; amount; memo}

let unBool = function
  | Bool b -> Some b
  | _ -> None

let unInt = function
  | Int {i} -> Some i
  | _ -> None

let bls12_381_g1 s = Bls12_381_g1 s

let bls12_381_g2 s = Bls12_381_g2 s

let bls12_381_fr s = Bls12_381_fr s
