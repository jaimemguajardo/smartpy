(* Copyright 2019-2021 Smart Chain Arena LLC. *)
open Basics
open Typed

type mangle_env =
  { substContractData : (Literal.contract_id, texpr) Hashtbl.t
  ; reducer : line_no:line_no -> texpr -> texpr }

val init_env :
     ?substContractData:(Literal.contract_id, Expr.t) Hashtbl.t
  -> reducer:(line_no:line_no -> Expr.t -> Expr.t)
  -> unit
  -> mangle_env

val mangle_expr : mangle_env -> Typing.env -> texpr -> texpr

val mangle_command : mangle_env -> Typing.env -> tcommand -> tcommand

val mangle_contract : mangle_env -> Typing.env -> tcontract -> tcontract

val mangle_value_contract :
  mangle_env -> Typing.env -> value_tcontract -> value_tcontract
