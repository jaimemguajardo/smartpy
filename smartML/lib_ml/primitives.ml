(* Copyright 2019-2021 Smart Chain Arena LLC. *)

module Bls12 = struct
  (* G1 Point *)
  let negateG1 _b = assert false

  let addG1 _b1 _b2 = assert false

  let multiplyG1ByFr _b1 _b2 = assert false

  (* G2 Point *)
  let negateG2 _b = assert false

  let addG2 _b1 _b2 = assert false

  let multiplyG2ByFr _b1 _b2 = assert false

  (* Fr *)
  let negateFr _b = assert false

  let addFr _b1 _b2 = assert false

  let multiplyFrByFr _b1 _b2 = assert false

  let multiplyFrByInt _b _ = assert false

  let convertFrToInt _ = assert false

  (* Pairing check *)
  let pairingCheck _ = assert false
end

module Crypto = struct
  let hash prefix s = prefix ^ Digest.string s

  let fake_pk_prefix = "edpkFake"

  let fake_pkh_prefix = "tz0Fake"

  let account_of_seed seed =
    let open Smartml.Primitives in
    { pkh = fake_pkh_prefix ^ seed
    ; pk = fake_pk_prefix ^ seed
    ; sk = "edskFake" ^ seed }

  let sign ~secret_key:_ _message =
    failwith "Primitive.sign not supported in native."

  let hash_key _s = failwith "Primitive.hash_key not supported in native."

  let check_signature ~public_key:_ ~signature:_ _bytes =
    failwith "Primitive.check_signature not supported in native."

  (* Dbg.p
   *   Format.(
   *     fun ppf () ->
   *       pp_open_hovbox ppf 2;
   *       fprintf ppf "fake-check-signature:";
   *       pp_print_space ppf ();
   *       fprintf ppf "pk: %S@ sig: %S@ bytes: %S" public_key signature bytes);
   * try
   *   let secret_key =
   *     let pos = String.length fake_pk_prefix in
   *     let seed = String.sub public_key pos (String.length public_key - pos) in
   *     (account_of_seed seed).sk
   *   in
   *   let redo = sign ~secret_key bytes in
   *   redo = signature
   * with
   * | _ -> false *)

  let blake2b = hash "b2b"

  let sha256 = hash "s56"

  let sha512 = hash "s12"

  let keccak = hash "keccak"

  let sha3 = hash "s3"
end
