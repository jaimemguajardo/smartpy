(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Control
open Type
open Binary_tree
open Expr
open Typing
module TO = Binary_tree.Traversable (Option)

let debug_level = 0

let typecheck_before_and_after = false

let typecheck_after_each_step = false

let check_conditions = false

type transformer =
  | Atom_typed    of (texpr -> expr)
  | Atom_untyped  of (expr -> expr)
  | Fixpoint      of transformer
  | Sequence      of transformer list
  | Label         of string * transformer
  | Precondition  of (expr -> string option) * transformer
  | Postcondition of (expr -> expr -> string option) * transformer

let mk_t label transform conditions =
  Label (label, List.fold_left ( |> ) (Atom_typed transform) conditions)

let mk label transform conditions =
  Label (label, List.fold_left ( |> ) (Atom_untyped transform) conditions)

let _pre (lbl, c) t =
  let c x = if c x then None else Some ("pre: " ^ lbl) in
  Precondition (c, t)

let post (lbl, c) t =
  let c _ y = if c y then None else Some ("post: " ^ lbl) in
  Postcondition (c, t)

let pre_post (lbl, c) t =
  let c x y =
    if c x
    then if c y then None else Some ("post: " ^ lbl)
    else Some ("pre: " ^ lbl)
  in
  Postcondition (c, t)

let preserves (lbl, c) t =
  let c x y = if (not (c x)) || c y then None else Some ("preserves: " ^ lbl) in
  Postcondition (c, t)

let log_time s f x =
  if debug_level > 0
  then (
    let t0 = Unix.gettimeofday () in
    let y = f x in
    let dt = Unix.gettimeofday () -. t0 in
    Printf.eprintf "%s %3.3fms\n" s (dt *. 1000.0);
    y )
  else f x

let typecheck_or_fail lbl x =
  let y = log_time (lbl ^ ": typecheck") typecheck_precontract x in
  match y with
  | Ok y -> y
  | Error msg ->
      let pp = print_precontract print_expr in
      failwith (Format.asprintf "@[<v>%s: typecheck: %s\n@]%a" lbl msg pp x)

let typecheck_or_fail_b lbl before after =
  let y = log_time (lbl ^ ": typecheck") typecheck_precontract after in
  match y with
  | Ok y -> y
  | Error msg ->
      let pp = print_precontract print_expr in
      let err =
        Format.asprintf "@[<v>%s: typecheck: %s@;Before:@;%a@;After:@;%a@]"
      in
      failwith (err lbl msg pp before pp after)

let rec run_transformer
    path t ({tparameter; tstorage; parameter_and_storage; body} as x) =
  let path_str = List.fold_left (fun x y -> x ^ " > " ^ y) "." path in
  let ppc = print_precontract print_expr in
  match t with
  | Atom_typed transform ->
      let x' = typecheck_or_fail path_str x in
      let y =
        let body = transform x'.checked_precontract.body in
        {tparameter; tstorage; parameter_and_storage; body}
      in
      if typecheck_after_each_step
      then (
        ignore (typecheck_or_fail_b path_str x y);
        y )
      else y
  | Atom_untyped transform ->
      let y =
        let body = transform body in
        {tparameter; tstorage; parameter_and_storage; body}
      in
      if typecheck_after_each_step
      then (
        ignore (typecheck_or_fail_b path_str x y);
        y )
      else y
  | Fixpoint t ->
      let rec fixpoint n x =
        if n = 0
        then failwith (Printf.sprintf "%s: failed to converge" path_str)
        else
          let y = run_transformer path t x in
          if equal_precontract equal_expr x y then x else fixpoint (n - 1) y
      in
      fixpoint 1000 x
  | Sequence ts -> List.fold_left ( |> ) x (List.map (run_transformer path) ts)
  | Label (lbl, t) ->
      let y =
        log_time
          (path_str ^ " > " ^ lbl)
          (fun () -> run_transformer (path @ [lbl]) t x)
          ()
      in
      if debug_level >= 2 && not (equal_precontract equal_expr x y)
      then print_endline "  progress";
      y
  | Precondition (_, t) when not check_conditions -> run_transformer path t x
  | Precondition (c, t) ->
    ( match c x.body with
    | None -> run_transformer path t x
    | Some msg ->
        failwith
          (Format.asprintf
             "%s: precondition violated: %s\n%a"
             path_str
             msg
             ppc
             x) )
  | Postcondition (_, t) when not check_conditions -> run_transformer path t x
  | Postcondition (c, t) ->
      let y = run_transformer path t x in
      ( match c body y.body with
      | None -> y
      | Some msg ->
          let msg = path_str ^ ": postcondition violated: " ^ msg in
          failwith
            (Format.asprintf
               "@[<v>%s@;@;Before:@;%a@;After:@;%a@]"
               msg
               ppc
               x
               ppc
               y) )

let run_transformer t c =
  if typecheck_before_and_after
  then ignore (typecheck_or_fail "initial check" c);
  let c = log_time "run_transformer" (run_transformer [] t) c in
  if debug_level >= 1 then prerr_endline "";
  if typecheck_before_and_after then ignore (typecheck_or_fail "final check" c);
  c

type state = {var_counter : int ref}

let fresh {var_counter} x =
  let i = !var_counter in
  incr var_counter;
  x ^ string_of_int i

let freshen st s = List.map (fun _ -> fresh st s)

let may_fail =
  cata_expr (function
      | Loop _ -> true
      | Prim1_fail _ -> true
      | Prim2 (Exec, _, _) -> true
      | Prim2 ((Lsl | Lsr | Add | Sub | Mul), _, _) ->
          true (* overflow on some types *)
      | e -> fold_expr_f ( || ) false e)

let is_tmutez = function
  | Stack_ok (T0 T_mutez) -> true
  | _ -> false

let may_fail_typed_f = function
  | Loop _ -> true
  | Prim1_fail _ -> true
  | Prim2 (Exec, _, _) -> true
  | Prim2 ((Lsl | Lsr), _, _) -> true
  | Prim2 ((Add | Sub | Mul), (_, t1), (_, t2)) -> is_tmutez t1 || is_tmutez t2
  | e -> fold_expr_f ( || ) false (map_expr_f fst e)

let sum_map = String.Map.union (fun _ x y -> Some (x + y))

let sum_maps = List.fold_left sum_map String.Map.empty

let map_of_list f xs =
  List.fold_left
    (String.Map.union (fun _k x y -> Some (f x y)))
    String.Map.empty
    (List.map (fun (k, v) -> String.Map.singleton k v) xs)

(** How many times is each variable used? *)
let occs_f = function
  | Var v -> String.Set.singleton v
  | e -> fold_expr_f String.Set.union String.Set.empty e

(** How many times is each variable used? *)
let count_occs_f = function
  | Var v -> String.Map.singleton v 1
  | e -> fold_expr_f sum_map String.Map.empty e

let count_occs = cata_expr count_occs_f

(** How many times is each variable bound? *)
let count_bindings =
  let open String.Map in
  let count_var = Option.cata empty (fun x -> singleton x 1) in
  let count_tree f x = Binary_tree.(fold sum_map empty (map f x)) in
  let f = function
    | Let_in (xs, e1, e2) ->
        let xs =
          match xs with
          | P_var x -> [x]
          | P_vector xs -> xs
        in
        sum_maps (e1 :: e2 :: List.map count_var xs)
    | Lambda (x, _, _, e) -> sum_map (count_var x) e
    | Match_record (rp, e1, e2) -> sum_maps [count_tree count_var rp; e1; e2]
    | Match_variant (e, clauses) ->
        let count_clause {var; rhs} = sum_map (count_var var) rhs in
        sum_map e (count_tree count_clause clauses)
    | Loop (xs, step, init)
     |Iter_over (xs, step, init)
     |Map_over (xs, step, init) ->
        sum_maps ((step :: init) @ List.map count_var xs)
    | If_some (e1, x, e2, e3) -> sum_maps [e1; count_var x; e2; e3]
    | If_left (e1, x1, e2, x2, e3) ->
        sum_maps [e1; count_var x1; e2; count_var x2; e3]
    | If_cons (e1, x1, x2, e2, e3) ->
        sum_maps [e1; count_var x1; count_var x2; e2; e3]
    | e -> fold_expr_f sum_map String.Map.empty e
  in
  cata_expr f

(** Does every variable binding have a unique name? *)
let bindings_unique e = String.Map.for_all (fun _ n -> n = 1) (count_bindings e)

let bindings_unique = ("bindings_unique", bindings_unique)

let bindings_used = ("bindings_used", Analyser.bindings_used)

let is_linear = ("is_linear", Analyser.is_linear)

let loops_closed = ("loops_closed", Analyser.loops_closed)

(*let bindings_unique_and_used_once*)

let is_simple =
  let f = function
    | Var _ -> true
    | Lit _ -> true
    | Prim0 _ -> true
    | Prim1 ((Car | Cdr), x) -> x
    | Prim2 (Pair _, x, y) -> x && y
    | Record r -> for_all (fun (_, e) -> e) r
    | Vector xs -> List.for_all id xs
    | Variant (_, _, e) -> e
    | _ -> false
  in
  cata_expr f

let is_var {expr} =
  match expr with
  | Var _ -> true
  | _ -> false

(* Inlines let-bindings that are used exactly once. *)
let inline =
  let f = function
    | Var v -> fun subst -> Option.default (var v) (String.Map.find_opt v subst)
    | Let_in (P_var (Some x), ((_, may_fail1, e1), _), ((occs2, _, e2), _))
      when not may_fail1 ->
        fun subst ->
          let e1 = e1 subst in
          let single_occ = String.Map.lookup ~default:0 x occs2 = 1 in
          let simple = is_simple e1 in
          if (simple || single_occ) && String.sub x 0 2 <> "__"
          then e2 (String.Map.add x e1 subst)
          else let_in_var (Some x) e1 (e2 subst)
    | e -> fun subst -> {expr = map_expr_f (fun ((_, _, x), _) -> x subst) e}
  in
  let f e =
    ( count_occs_f (map_expr_f (fun ((occs, _, _), _) -> occs) e)
    , may_fail_typed_f (map_expr_f (fun ((_, mf, _), tys) -> (mf, tys)) e)
    , f e )
  in
  let f e =
    let _, _, r = cata_texpr (fun e _ -> f e) e in
    r String.Map.empty
  in
  mk_t "inline" f [preserves bindings_unique; preserves is_linear]

(* Inlines let-bindings to variables. *)
let inline_vars =
  let f = function
    | Var v -> fun subst -> Option.default (var v) (String.Map.find_opt v subst)
    | Let_in (P_var (Some x), e1, e2) ->
        fun subst ->
          let e1 = e1 subst in
          ( match e1.expr with
          | Var _ -> e2 (String.Map.add x e1 subst)
          | _ -> let_in_var (Some x) e1 (e2 subst) )
    | e -> fun subst -> {expr = map_expr_f (fun x -> x subst) e}
  in
  let f e = cata_expr f e String.Map.empty in
  mk "inline_vars" f [preserves bindings_unique; preserves is_linear]

(* Replace unused variables with a '_'. *)
let mark_unused =
  let underscore_var occs = function
    | Some x when not (String.Set.mem x occs) -> None
    | x -> x
  in
  let underscore_rp occs = Binary_tree.map (underscore_var occs) in
  let underscore_vars occs = List.map (underscore_var occs) in
  let underscore_pattern occs = function
    | P_var x -> P_var (underscore_var occs x)
    | P_vector xs -> P_vector (underscore_vars occs xs)
  in
  let underscore_clause {cons; var; rhs = occs, rhs} =
    {cons; var = underscore_var occs var; rhs}
  in
  let f = function
    | Let_in (xs, (_, e1), (occs, e2)) ->
        let_in (underscore_pattern occs xs) e1 e2
    | Match_record (rp, (_, e1), (occs, e2)) ->
        match_record (underscore_rp occs rp) e1 e2
    | Match_variant ((_, scrutinee), clauses) ->
        match_variant scrutinee (Binary_tree.map underscore_clause clauses)
    | Lambda (x, a, b, (occs, e)) ->
        {expr = Lambda (underscore_var occs x, a, b, e)}
    | Loop (xs, (occs, step), init) ->
        {expr = Loop (underscore_vars occs xs, step, List.map snd init)}
    | Create_contract (c, (_, e1), (_, e2), (_, e3)) ->
        let {tparameter; tstorage; parameter_and_storage; body = occs, body} =
          c
        in
        let parameter_and_storage = underscore_var occs parameter_and_storage in
        let c = {tparameter; tstorage; parameter_and_storage; body} in
        {expr = Create_contract (c, e1, e2, e3)}
    | If_some ((_, e1), x, (occs, e2), (_, e3)) ->
        if_some e1 (underscore_var occs x, e2) e3
    | If_left ((_, e1), x1, (occs2, e2), x2, (occs3, e3)) ->
        if_left e1 (underscore_var occs2 x1, e2) (underscore_var occs3 x2, e3)
    | If_cons ((_, e1), x1, x2, (occs, e2), (_, e3)) ->
        if_cons e1 (underscore_var occs x1, underscore_var occs x2, e2) e3
    | Iter_over (xs, (occs, e), es) ->
        iter_over (underscore_vars occs xs) e (List.map snd es)
    | Map_over (xs, (occs, e), es) ->
        map_over (underscore_vars occs xs) e (List.map snd es)
    | e -> {expr = map_expr_f snd e}
  in
  let f e = (occs_f (map_expr_f fst e), f e) in
  let f e = snd (cata_expr f e) in
  mk "mark_unused" f [preserves bindings_unique; post bindings_used]

let field_access =
  let f = function
    | Prim1 (Car, (x, Stack_ok (Type.T_record (Node (Leaf (Some lbl, _), _)))))
      ->
        proj_field lbl x
    | Prim1 (Cdr, (x, Stack_ok (Type.T_record (Node (_, Leaf (Some lbl, _))))))
      ->
        proj_field lbl x
    | e -> {expr = map_expr_f fst e}
  in
  cata_texpr (fun e _ -> f e)

let field_access = mk_t "field_access" field_access [preserves bindings_unique]

let flatten_matches =
  let f e =
    let occs = count_occs e in
    let extract = function
      | { var = Some var
        ; rhs = {expr = Match_variant ({expr = Var scrutinee'}, clauses')} }
        when var = scrutinee' && String.Map.find_opt var occs = Some 1 ->
          clauses'
      | c -> leaf c
    in
    let f = function
      | Match_variant (scrutinee, clauses) ->
          (* TODO do not transcend type boundaries *)
          {expr = Match_variant (scrutinee, join (map extract clauses))}
      | expr -> {expr}
    in
    cata_expr f e
  in
  mk "flatten_matches" f [preserves bindings_unique]

let fresh_vars st v = List.map (Option.map (fun _ -> fresh st v))

let singleton_or_record = function
  | Leaf (None, x) -> x
  | xs -> {expr = Record xs}

let is_failwith = function
  (* TODO use types instead of syntactic is_failwith *)
  | {expr = Prim1_fail (Failwith, _)} -> true
  | _ -> false

(** A "mostly failing" match is one where at most one branch succeeds. *)
let mostly_fails clauses =
  Option.cata 0 size (filter (fun {rhs} -> not (is_failwith rhs)) clauses) <= 1

let _let_protect st e f =
  let v = fresh st "p" in
  let_in_var (Some v) e (f (var v))

(** Deletes bindings to '_'. Linearizes tuple-bindings. Propagates '_'-components in let-bindings below match nodes. *)
let mangle st =
  let f = function
    | Let_in (P_vector [Some x], e1, {expr = Vector [{expr = Var x'}]})
      when x = x' ->
        e1
    | Let_in (P_var (Some x), e1, {expr = Var x'}) when x = x' -> e1
    | Let_in (P_var None, e1, e2) when not (may_fail e1) -> e2
    | Let_in (_, ({expr = Prim1_fail (Failwith, _)} as fail), _) -> fail
    | Let_in (rp1, {expr = Let_in (rp2, e2, e1)}, e3) ->
        let_in rp2 e2 (let_in rp1 e1 e3)
    | Let_in (P_vector [rp1; rp2], {expr = Record (Node (r1, r2))}, e2) ->
        let unwrap = function
          | Leaf (_, x) -> x
          | Node _ as x -> {expr = Record x}
        in
        let_in_var rp1 (unwrap r1) (let_in_var rp2 (unwrap r2) e2)
    | Let_in (rp, {expr = Match_variant (e1, clauses)}, e2)
      when mostly_fails clauses ->
        (* Pull let into match when at most one branch succeeds. *)
        let mk_clause ({cons; var; rhs} as c) =
          if is_failwith rhs then c else {cons; var; rhs = let_in rp rhs e2}
        in
        let r = match_variant e1 (map mk_clause clauses) in
        r
    | Let_in (P_vector xs, {expr = Match_variant (e1, clauses)}, e2)
      when List.exists Option.is_none xs ->
        (* Push discarding of unused variables inside clauses. *)
        (* TODO Same thing for Match_record instead of Let_in *)
        let mk_clause ({cons; var; rhs} as c) =
          if is_failwith rhs
          then c
          else
            let xs = fresh_vars st "s" xs in
            let xs_used =
              vector ((List.map Expr.var) (List.filter_map id xs))
            in
            let rhs = let_in_vector xs rhs xs_used in
            {cons; var; rhs}
        in
        let xs = List.map Option.some (List.somes xs) in
        let_in_vector xs (match_variant e1 (map mk_clause clauses)) e2
    | expr -> {expr}
  in
  mk "mangle" (cata_expr f) [preserves bindings_unique]

let distribute_lets =
  let f = function
    | Let_in
        ( P_var (Some x)
        , {expr = Match_variant (({expr = Var _} as scrutinee), clauses)}
        , { expr =
              Prim2
                ( Pair _
                , ({expr = Prim0 (Nil _toperation)} as l)
                , {expr = Var x'} ) } )
      when x = x' ->
        let f {cons; var; rhs} = {cons; var; rhs = pair None None l rhs} in
        let clauses = Binary_tree.map f clauses in
        match_variant scrutinee clauses
    | Let_in (xs, {expr = Match_variant (scrutinee, clauses)}, e)
      when is_var scrutinee && is_simple e ->
        let f {cons; var; rhs} = {cons; var; rhs = let_in xs rhs e} in
        let clauses = Binary_tree.map f clauses in
        match_variant scrutinee clauses
    | Prim2 (Pair (a1, a2), l, {expr = Match_variant (scrutinee, clauses)})
      when is_var scrutinee && is_simple l ->
        let f {cons; var; rhs} = {cons; var; rhs = pair a1 a2 l rhs} in
        let clauses = Binary_tree.map f clauses in
        match_variant scrutinee clauses
    | expr -> {expr}
  in
  (* Does not preserve 'bindings_unique'. *)
  mk "distribute_lets" (cata_expr f) []

let fold_constants =
  let f e subst =
    match e with
    | Let_in (P_var (Some x), e1, e2) ->
        let e1 = e1 subst in
        let subst = if may_fail e1 then subst else String.Map.add x e1 subst in
        let_in_var (Some x) e1 (e2 subst)
    | e ->
        let e = map_expr_f (fun e -> e subst) e in
        let lookup_var = function
          | {expr = Var x} as e -> String.Map.lookup ~default:e x subst
          | e -> e
        in
        let open Big_int in
        let bool b = lit (Bool b) in
        let nat x = lit (Nat x) in
        let int x = lit (Int x) in
        ( match map_expr_f lookup_var e with
        | Prim1 (((Car | Cdr) as op), {expr = Vector [x]}) ->
            {expr = Prim1 (op, x)}
        | Prim1 (Car, {expr = Record (Node (Leaf (_, e1), _))}) -> e1
        | Prim1 (Car, {expr = Record (Node (t1, _))}) -> {expr = Record t1}
        | Prim1 (Cdr, {expr = Record (Node (_, Leaf (_, e2)))}) -> e2
        | Prim1 (Cdr, {expr = Record (Node (_, t2))}) -> {expr = Record t2}
        | Prim1 (Car, {expr = Prim2 (Pair _, x, _)}) -> x
        | Prim1 (Cdr, {expr = Prim2 (Pair _, _, y)}) -> y
        | Unpair (2, {expr = Prim2 (Pair _, x, y)}) -> vector [x; y]
        | Prim2 (Pair _, {expr = Lit _}, {expr = Lit _}) as e -> {expr = e}
        | Prim1 (op, {expr = Lit x}) ->
          ( match (op, x) with
          | Not, Bool x -> bool (not x)
          | Abs, Int x -> nat (abs_big_int x)
          | Eq, Int x -> bool (eq_big_int x zero_big_int)
          | Neq, Int x -> bool (not (eq_big_int x zero_big_int))
          | Lt, Int x -> bool (lt_big_int x zero_big_int)
          | Gt, Int x -> bool (gt_big_int x zero_big_int)
          | Le, Int x -> bool (le_big_int x zero_big_int)
          | Ge, Int x -> bool (ge_big_int x zero_big_int)
          | _ -> {expr = e} )
        | Prim2 (op, {expr = Lit x}, {expr = Lit y}) ->
          ( match (op, x, y) with
          | And, Bool x, Bool y -> bool (x && y)
          | Or, Bool x, Bool y -> bool (x || y)
          | Add, Nat x, Nat y -> nat (add_big_int x y)
          | Add, Int x, Int y -> int (add_big_int x y)
          | Add, Mutez x, Mutez y -> int (add_big_int x y)
          | Mul, Nat x, Nat y -> nat (mult_big_int x y)
          | Mul, Int x, Int y -> int (mult_big_int x y)
          | Compare, Mutez x, Mutez y
           |Compare, Nat x, Nat y
           |Compare, Int x, Int y ->
              int (big_int_of_int (compare_big_int x y))
          | _ -> {expr = e} )
        | If ({expr = Lit (Bool b)}, x, y) -> if b then x else y
        | Iter_over
            ( xs
            , body
            , {expr = Prim2 (Cons, x0, {expr = Prim0 (Nil _)}) | List (_, [x0])}
              :: init ) ->
            let f x e = Option.map (fun x -> (x, e)) x in
            let s = List.somes (List.map2 f xs (x0 :: init)) in
            Expr.substitute s body
        | _ -> {expr = e} )
  in
  let f e = cata_expr f e String.Map.empty in
  mk "fold_constants" f [preserves bindings_unique]

let pp_tree_skel x =
  print
    (fun ppf -> Format.fprintf ppf " | ")
    (fun ppf _ -> Format.fprintf ppf "_")
    x

let infer_constructors =
  let open Type in
  let f = function
    | Match_variant ((scrutinee, Stack_ok (T_variant r_scrutinee)), clauses) ->
      ( match matches clauses r_scrutinee with
      | None ->
          failwith
            (Format.asprintf
               "infer_constructors:\n  <%a>\n  (%a)"
               (print_row ~sep:";")
               r_scrutinee
               pp_tree_skel
               clauses)
      | Some clauses ->
          let put_cons = function
            | {cons = None; var; rhs = rhs, _}, Leaf (Some cons, _) ->
                {cons = Some cons; var; rhs}
            | {cons; var; rhs = rhs, _}, _ -> {cons; var; rhs}
          in
          let clauses = map put_cons clauses in
          {expr = Match_variant (scrutinee, clauses)} )
    | e -> {expr = map_expr_f fst e}
  in
  let f = cata_texpr (fun e _ -> f e) in
  mk_t "infer_constructors" f [preserves bindings_unique]

let cons x xs = x :: xs

(* A lens that allows us to look through a chain of lets (as well as
   mostly failing matches). Returns a list of bound variables. *)
let rec lens_lets bound wrap {expr} =
  match expr with
  | Comment (xs, e) ->
      let wrap e' = wrap {expr = Comment (xs, e')} in
      lens_lets bound wrap e
  | Let_in (p, e1, e2) ->
      let wrap e2' = wrap {expr = Let_in (p, e1, e2')} in
      let xs =
        match p with
        | P_var x -> [x]
        | P_vector xs -> xs
      in
      lens_lets (List.somes xs @ bound) wrap e2
  | Match_variant (e, clauses) when mostly_fails clauses ->
    ( match
        List.find_opt (fun {rhs} -> not (is_failwith rhs)) (to_list clauses)
      with
    | None -> (bound, {expr}, wrap)
    | Some {var; rhs} ->
        let wrap rhs' =
          let f ({cons; var; rhs} as c) =
            if is_failwith rhs then c else {cons; var; rhs = rhs'}
          in
          wrap (match_variant e (map f clauses))
        in
        lens_lets (Option.cata id cons var bound) wrap rhs )
  | If_left (s, xl, l, xr, r) ->
    ( match (is_failwith l, is_failwith r) with
    | true, false ->
        let bound = Option.cata [] (fun x -> [x]) xr @ bound in
        let wrap r = {expr = If_left (s, xl, l, xr, r)} in
        lens_lets bound wrap r
    | false, true -> assert false
    | _ -> (bound, {expr}, wrap) )
  | _ -> (bound, {expr}, wrap)

let lens_lets = lens_lets [] id

let apply_deep f x =
  let _, focus, close = lens_lets x in
  close (f focus)

(* Substitute invariant loop states with initial state.  Does not
   preserve linearity. *)
let inline_invariant_loop_state =
  let f = function
    | Loop (ts, step, {expr = Let_in (xs, e1, e2)} :: rs) ->
        {expr = Let_in (xs, e1, {expr = Loop (ts, step, e2 :: rs)})}
    | Loop (xs, step, init) as expr ->
      (* If a variable is the same in xs and in step, we can replace
         it with its initial value. *)
      ( match (init, lens_lets step) with
      | _ :: init, (_, {expr = Vector (_ :: step)}, _) ->
          let s =
            let f x s i =
              match (x, s, i) with
              | Some x, {expr = Var s}, {expr = Var _} when x = s ->
                  Some (x, i) (* x is invariant *)
              (* TODO If x is not a variable, wrap it in a let. *)
              | _ -> None
            in
            assert (eq3 (List.length xs) (List.length step) (List.length init));
            List.map3 f xs step init |> List.somes
          in
          Expr.substitute s {expr}
      | _ -> {expr} )
    | expr -> {expr}
  in
  mk "inline_invariant_loop_state" (cata_expr f) [preserves bindings_unique]

let remove_in_vector st used e =
  let xs = List.map (fun u -> if u then Some (fresh st "s") else None) used in
  let_in (P_vector xs) e (vector (List.map var (List.somes xs)))

let remove used xs =
  assert (List.length used = List.length xs);
  List.map2 Control.pair used xs |> List.filter fst |> List.map snd

(* Remove unused parts from the loop state. *)
let unused_loop_state st =
  let f = function
    | Let_in (P_vector rs, {expr = Loop (xs, step, init)}, e) as expr ->
        assert (List.length rs = List.length xs);
        let used = List.map2 Option.(fun x r -> is_some x || is_some r) rs xs in
        if List.exists not used
        then
          let rs = remove used rs in
          let xs = remove used xs in
          let init = remove (true :: used) init in
          let step = remove_in_vector st (true :: used) step in
          let_in_vector rs (loop xs step init) e
        else {expr}
    | expr -> {expr}
  in
  mk "unused_loop_state" (cata_expr f) [preserves bindings_unique]

let is_needed body r x s =
  match (r, x, s) with
  | None, Some x, {expr = Var x'} ->
      x <> x' || String.Map.lookup ~default:0 x (count_occs body) <> 1
  | _ -> true

let remove_invariant_loop_state =
  let f = function
    | Let_in (P_vector rs, {expr = Loop (xs, body, init)}, e) as expr ->
        let _, focus, close_body = lens_lets body in
        ( match focus.expr with
        | Vector (br0 :: body_res) ->
            assert (eq3 (List.length rs) (List.length xs) (List.length body_res));
            let needed = List.map3 (is_needed body) rs xs body_res in
            if List.exists not needed
            then
              let rs = remove needed rs in
              let xs = remove needed xs in
              let init = remove (true :: needed) init in
              let body_res = remove needed body_res in
              let body_res = close_body (vector (br0 :: body_res)) in
              let_in_vector rs (loop xs body_res init) e
            else {expr}
        | _ -> {expr} )
    | Let_in (P_vector rs, {expr = Iter_over (x0 :: xs, body, i0 :: init)}, e)
      as expr ->
        let _, focus, close_body = lens_lets body in
        ( match focus.expr with
        | Vector body_res ->
            assert (eq3 (List.length rs) (List.length xs) (List.length body_res));
            let needed = List.map3 (is_needed body) rs xs body_res in
            if List.exists not needed
            then
              let rs = remove needed rs in
              let xs = x0 :: remove needed xs in
              let init = i0 :: remove needed init in
              let body_res = remove needed body_res in
              let body_res = close_body (vector body_res) in
              let_in_vector rs (iter_over xs body_res init) e
            else {expr}
        | _ -> {expr} )
    | Let_in
        (P_vector (r0 :: rs), {expr = Map_over (x0 :: xs, body, i0 :: init)}, e)
      as expr ->
        let _, focus, close_body = lens_lets body in
        ( match focus.expr with
        | Vector (br0 :: body_res) ->
            assert (eq3 (List.length rs) (List.length xs) (List.length body_res));
            let needed = List.map3 (is_needed body) rs xs body_res in
            if List.exists not needed
            then
              let rs = r0 :: remove needed rs in
              let xs = x0 :: remove needed xs in
              let init = i0 :: remove needed init in
              let body_res = remove needed body_res in
              let body_res = close_body (vector (br0 :: body_res)) in
              let_in_vector rs (map_over xs body_res init) e
            else {expr}
        | _ -> {expr} )
    | expr -> {expr}
  in
  mk "remove_invariant_loop_state" (cata_expr f) [preserves bindings_unique]

let unify_fields x y =
  match (x, y) with
  | Some x, Some y -> if x = y then Some x else failwith "unify_fields"
  | x, None -> x
  | None, y -> y

let common_parts x y =
  common x y
  |> map (function
         | Leaf (fld_x, Some x), Leaf (fld_y, Some y) when equal_expr x y ->
             Leaf (unify_fields fld_x fld_y, Some x)
         | Leaf (fld_x, _), Leaf (fld_y, _) ->
             Leaf (unify_fields fld_x fld_y, None)
         | _ -> Leaf (None, None))
  |> join

let prune pat x =
  matches pat x
  |> Option.of_some ~msg:"prune"
  |> map_some (function p, x -> if p then Some x else None)
  |> Option.map join

let factorise_clauses st =
  let f = function
    | Match_variant (scrutinee, clauses) as expr ->
        let open Option.Monad_syntax in
        (let* results =
           (* TODO use lens_lets *)
           TO.map
             (function
               | {rhs = {expr = Record r}} -> Some (Some r)
               | {rhs = {expr = Prim1_fail (Failwith, _)}} -> Some None
               | _ -> None)
             clauses
         in
         let* results = map_some id results in
         let* _ = if mostly_fails clauses then None else Some () in
         (* This avoids pulling out bound variables. TODO Be more precise. *)
         let shared =
           fold1 common_parts (map (map (map_snd Option.some)) results)
         in
         let b = exists (fun x -> Option.is_some (snd x)) shared in
         let* _ = if b then Some () else None in
         let open Either in
         let out =
           shared
           |> map (function
                  | _, None -> Left (fresh st "o")
                  | fld, Some x -> Right (fld, x))
         in
         let x =
           map_some of_left out |> Option.cata (Leaf None) (map Option.some)
         in
         let clauses =
           let pat = map Either.is_left out in
           let prune_rhs = function
             | {expr = Record r} ->
                 singleton_or_record
                   (prune pat r |> Option.default (Leaf (None, unit)))
             | {expr = Prim1_fail (Failwith, _)} as rhs -> rhs
             | _ -> assert false
           in
           clauses |> map (map_match_clause prune_rhs)
         in
         let out =
           out
           |> map (function
                  | Left x -> (None, var x)
                  | Right e -> e)
         in
         Some
           (match_record
              x
              (match_variant scrutinee clauses)
              (singleton_or_record out)))
        |> Option.default {expr}
    | expr -> {expr}
  in
  mk "factorise_clauses" (cata_expr f) [preserves bindings_unique]

let reify_booleans =
  let f = function
    | Match_variant
        ( x
        , Node
            ( Leaf {cons = Some "True"; var = None; rhs = r1}
            , Leaf {cons = Some "False"; var = None; rhs = r2} ) ) as expr ->
      ( match () with
      | _ when equal_expr r2 false_ -> prim2 And x r1
      | _ when equal_expr r1 true_ -> prim2 Or x r1
      | _ -> {expr} )
    | expr -> {expr}
  in
  mk "reify_booleans" (cata_expr f) [preserves bindings_unique]

let binarize_matches st =
  let f = function
    | Match_variant (s, Node ((Node _ as l), r)) ->
        let ls = fresh st "l" in
        let rhs = {expr = Match_variant ({expr = Var ls}, l)} in
        let l = Leaf {cons = None; var = Some ls; rhs} in
        {expr = Match_variant (s, Node (l, r))}
    | Match_variant (s, Node (l, (Node _ as r))) ->
        let rs = fresh st "r" in
        let rhs = {expr = Match_variant ({expr = Var rs}, r)} in
        let r = Leaf {cons = None; var = Some rs; rhs} in
        {expr = Match_variant (s, Node (l, r))}
    | expr -> {expr}
  in
  mk "binarize_matches" (cata_expr f) [preserves bindings_unique]

let two_clauses c1 c2 = Binary_tree.Node (Leaf c1, Leaf c2)

let unfold_ifs =
  let f = function
    | If (cond, l, r) ->
        match_variant
          cond
          (two_clauses
             {cons = Some "True"; var = None; rhs = l}
             {cons = Some "False"; var = None; rhs = r})
    | If_left (scrutinee, xl, l, xr, r) ->
        match_variant
          scrutinee
          (two_clauses
             {cons = None; var = xl; rhs = l}
             {cons = None; var = xr; rhs = r})
    | If_some (scrutinee, xl, l, r) ->
        match_variant
          scrutinee
          (two_clauses
             {cons = Some "Some"; var = xl; rhs = l}
             {cons = Some "None"; var = None; rhs = r})
    | expr -> {expr}
  in
  mk "unfold_ifs" (cata_expr f) [preserves bindings_unique]

let fold_ifs =
  let f = function
    | Match_variant
        ( scrutinee
        , Node
            ( Leaf {cons = Some "True"; var = None; rhs = l}
            , Leaf {cons = Some "False"; var = None; rhs = r} ) ) ->
        if_ scrutinee l r
    | Match_variant
        ( scrutinee
        , Node
            ( Leaf {cons = Some "Some"; var = xl; rhs = l}
            , Leaf {cons = Some "None"; var = None; rhs = r} ) ) ->
        if_some scrutinee (xl, l) r
    | Match_variant
        (scrutinee, Node (Leaf {var = xl; rhs = l}, Leaf {var = xr; rhs = r}))
      ->
        if_left scrutinee (xl, l) (xr, r)
    | Match_variant (_, Node _) -> assert false (* run binarize_matches first *)
    | Match_variant (_, Leaf _) -> assert false
    | Variant (Some "True", Node_left (Hole, Leaf (Some "False", _)), _) ->
        lit (Bool true)
    | Variant (Some "False", Node_right (Leaf (Some "True", _), Hole), _) ->
        lit (Bool false)
    | Variant (Some "None", Node_left (Hole, Leaf (Some "Some", _)), e) ->
        some e
    | Variant (Some "Some", Node_right (Leaf (Some "None", _), Hole), _) -> unit
    | Variant (a1, Node_left (Hole, Leaf (a2, t)), e) -> left a2 a1 t e
    | Variant (a1, Node_right (Leaf (a2, t), Hole), e) -> right a2 a1 t e
    | Record (Node (Leaf (_, x), Leaf (_, y))) -> pair None None x y
    | expr -> {expr}
  in
  mk "fold_ifs" (cata_expr f) [preserves bindings_unique]

let always_fails e =
  let _, e, _ = lens_lets e in
  is_failwith e

let nest_ifs =
  let f = function
    | Let_in (x, {expr = If (c, a, b)}, e) when always_fails b ->
        {expr = If (c, {expr = Let_in (x, a, e)}, b)}
    | expr -> {expr}
  in
  mk "nest_ifs" (cata_expr f) [preserves bindings_unique]

let factorise_failwith =
  let f = function
    | If (c, x, y) as expr ->
        let _, x, close_x = lens_lets x in
        let _, y, close_y = lens_lets y in
        ( match (x.expr, y.expr) with
        | Prim1_fail (Failwith, x), Prim1_fail (Failwith, y) ->
            let cond = if_ c (close_x x) (close_y y) in
            {expr = Prim1_fail (Failwith, cond)}
        | _ -> {expr} )
    | If_left (c, l, x, r, y) as expr ->
        let _, x, close_x = lens_lets x in
        let _, y, close_y = lens_lets y in
        ( match (x.expr, y.expr) with
        | Prim1_fail (Failwith, x), Prim1_fail (Failwith, y) ->
            let cond = if_left c (l, close_x x) (r, close_y y) in
            {expr = Prim1_fail (Failwith, cond)}
        | _ -> {expr} )
    | expr -> {expr}
  in
  mk "factorise_failwith" (cata_expr f) [preserves bindings_unique]

let flatten_ifs =
  let f = function
    | If (c, {expr = Let_in (x, a, e)}, b) when always_fails b ->
        {expr = Let_in (x, {expr = If (c, a, b)}, e)}
    | expr -> {expr}
  in
  mk "flatten_ifs" (cata_expr f) [preserves bindings_unique]

let unvectorize_lets st =
  let f = function
    | Let_in (p1, {expr = Let_in (p2, e2, e1)}, e3) ->
        let_in p2 e2 (let_in p1 e1 e3)
    | Let_in (P_vector xs, {expr = Vector es}, e) ->
        assert (List.length xs = List.length es);
        let bs = List.map2 (fun x e -> (x, e)) xs es in
        List.fold_right (fun (x, e1) e2 -> let_in_var x e1 e2) bs e
    | Let_in (P_vector [x], {expr = Match_variant (scrutinee, clauses)}, e) ->
        let un1 e =
          let s = fresh st "s" in
          let_in_vector [Some s] e (var s)
        in
        let f = function
          | {cons; var; rhs = {expr = Prim1_fail (Failwith, _)} as rhs} ->
              {cons; var; rhs} (* TODO detect Failwith via type *)
          | {cons; var; rhs} -> {cons; var; rhs = un1 rhs}
        in
        let clauses = Binary_tree.map f clauses in
        let_in_var x (match_variant scrutinee clauses) e
    | expr -> {expr}
  in
  mk "unvectorize_lets" (cata_expr f) [preserves bindings_unique]

let single_lets st =
  let f = function
    | Match_record ((Node _ as rp), e1, e2) ->
        let x = fresh st "x" in
        let rec mk_vars v = function
          | Leaf n -> [(Option.default (fresh st "x") n, v)]
          | Node (x, y) -> mk_vars (prim1 Car v) x @ mk_vars (prim1 Cdr v) y
        in
        let bindings = (x, e1) :: mk_vars (var x) rp in
        List.fold_right (fun (x, e) r -> let_in_var (Some x) e r) bindings e2
    | expr -> {expr}
  in
  mk "single_lets" (cata_expr f) [preserves bindings_unique]

let binarize_projs =
  let f = function
    | Proj_field (fld, (e, Stack_ok (T_record r))) ->
        let rec proj r p other =
          match r with
          | Leaf (Some n, _) when fld = n -> p
          | Leaf _ -> other ()
          | Node (x, y) ->
              proj
                x
                {expr = Prim1 (Car, p)}
                (fun () -> proj y {expr = Prim1 (Cdr, p)} other)
        in
        proj r e (fun () -> assert false)
    | e -> {expr = map_expr_f fst e}
  in
  let f = cata_texpr (fun e _ -> f e) in
  mk_t "binarize_projs" f [preserves bindings_unique]

let _name_all st =
  let name_var x = Some (Option.default (fresh st "m") x) in
  let name_rp = Binary_tree.map name_var in
  let name_pattern = function
    | P_var x -> P_var (name_var x)
    | P_vector x -> P_vector (List.map name_var x)
  in
  let f = function
    | Let_in (p, e1, e2) -> {expr = Let_in (name_pattern p, e1, e2)}
    | Match_variant (scrutinee, clauses) ->
        let f c = {c with var = name_var c.var} in
        {expr = Match_variant (scrutinee, map f clauses)}
    | Match_record (rp, e1, e2) -> {expr = Match_record (name_rp rp, e1, e2)}
    | Lambda (rp, a, b, e) -> {expr = Lambda (name_var rp, a, b, e)}
    | Loop (xs, step, init) -> {expr = Loop (List.map name_var xs, step, init)}
    | expr -> {expr}
  in
  mk "name_all" (cata_expr f) [preserves bindings_unique]

let subst_fresh st xs =
  xs |> String.Set.elements |> List.map (fun x -> (x, fresh st (x ^ "d")))

let build_dup_chain st x xs =
  let open List in
  let tmps = init (length xs - 2) (fun _ -> fresh st "tmp") in
  let xs', xs_last = unsnoc xs in
  let ys = tmps @ [xs_last] in
  let zs = x :: tmps in
  map3
    ~err:"build_dup_chain"
    (fun x y z -> (P_vector [Some x; Some y], dup 1 [var z]))
    xs'
    ys
    zs

(* Converts a list of replacements into let-bindings and puts them in
   front of the given expression. *)
let make_dups st bs e =
  let chains =
    bs
    |> List.map (map_snd (fun x -> [x]))
    |> map_of_list ( @ )
    |> String.Map.bindings
    |> List.map (fun (x, xs) -> build_dup_chain st x xs)
    |> List.concat
  in
  lets chains e

(* Add elements to the end of a vector. *)
let extend st xs len e =
  if len = 1
  then vector (e :: xs)
  else
    match e.expr with
    | Vector es -> vector (es @ xs)
    | _ ->
        let es = List.init len (fun _ -> fresh st "e") in
        let_in_vector
          (List.map Option.some es)
          e
          (vector (List.map var es @ xs))

let extend st xs len e = apply_deep (extend st xs len) e

(* List.take on vectors *)
let take_vector st n len xs =
  if n = len
  then xs
  else
    let es = List.init n (fun _ -> fresh st "t") in
    let ignored = List.init (len - n) (fun _ -> None) in
    let_in_vector
      (List.map Option.some es @ ignored)
      xs
      (vector (List.map var (List.take n es)))

let set_of_map m =
  let f (k, v) =
    assert (v >= 0);
    if v > 0 then Some k else None
  in
  String.Set.of_list (List.filter_map f (String.Map.bindings m))

(* Modify loops so that their bodies don't refer to the outer scope. *)
let close_loops st =
  let f e =
    let open Analyser in
    let open String.Set in
    let generic () =
      ( count_free_vars_f mode_occurrences (map_expr_f fst e)
      , {expr = map_expr_f snd e} )
    in
    match e with
    | Loop (xs, (occs_step, step), init) ->
        let n = List.length xs in
        let outer =
          let xs = of_list (List.somes xs) in
          elements (diff (set_of_map occs_step) xs)
        in
        if outer = []
        then generic ()
        else
          let outer_fresh = List.map (fun x -> (x, fresh st "v")) outer in
          let init = List.map var outer @ List.map snd init in
          let xs = xs @ List.map (fun (_, v) -> Option.some v) outer_fresh in
          let step =
            let sigma = List.map (fun (x, v) -> (x, var v)) outer_fresh in
            substitute sigma step
          in
          let step =
            extend st (List.map (fun (_, v) -> var v) outer_fresh) (n + 1) step
          in
          let e = take_vector st n (List.length xs) (loop xs step init) in
          (count_free_vars mode_occurrences e, e)
    | _ -> generic ()
  in
  let f e = snd (cata_expr f e) in
  mk "close_loops" f [preserves bindings_unique; post loops_closed]

let drop_vars xs =
  List.fold_right
    (fun x e -> let_in_var None (var x) e)
    (String.Set.elements xs)

let without fs xs =
  let open String.Set in
  diff fs (of_list (List.somes xs))

(** Insert 'let _ = x in ...' for unused variables. *)
let drop_unused =
  let open String.Set in
  let cond bound_l (frees_l, l) bound_r (frees_r, r) =
    let xsl = of_list (List.somes bound_l) in
    let xsr = of_list (List.somes bound_r) in
    let dl = diff (union xsl (diff frees_r xsr)) frees_l in
    let dr = diff (union xsr (diff frees_l xsl)) frees_r in
    (drop_vars dl l, drop_vars dr r)
  in
  let bound_pattern = function
    | P_var x -> Option.cata empty singleton x
    | P_vector xs -> of_list (List.somes xs)
  in
  let f = function
    | Match_variant _ -> assert false
    | Let_in (p, (_, e1), (frees2, e2)) ->
        let d = diff (bound_pattern p) frees2 in
        Let_in (p, e1, drop_vars d e2)
    | Lambda (x, t1, t2, (frees, body)) ->
        let d = diff (Option.cata empty singleton x) frees in
        Lambda (x, t1, t2, drop_vars d body)
    | Loop (rp, (frees, step), init) ->
        let d = diff (of_list (List.somes rp)) frees in
        Loop (rp, drop_vars d step, List.map snd init)
    | Iter_over (xs, (frees, body), cont) ->
        let d = diff (of_list (List.somes xs)) frees in
        Iter_over (xs, drop_vars d body, List.map snd cont)
    | If ((_, s), l, r) ->
        let l, r = cond [] l [] r in
        If (s, l, r)
    | If_some ((_, s), xl, l, r) ->
        let l, r = cond [xl] l [] r in
        If_some (s, xl, l, r)
    | If_left ((_, s), xl, l, xr, r) ->
        let l, r = cond [xl] l [xr] r in
        If_left (s, xl, l, xr, r)
    | If_cons ((_, s), xl, xsl, l, r) ->
        let l, r = cond [xl; xsl] l [] r in
        If_cons (s, xl, xsl, l, r)
    | ( Var _ | Map_over _ | Match_record _ | Create_contract _ | Lit _
      | Prim0 _ | Prim1 _ | Prim1_fail _ | Prim2 _ | Prim3 _ | Proj_field _
      | Stack_op _ | Record _ | Comment _ | Variant _ | List _ | Set _ | Map _
      | Vector _ | Nth _ | Unpair _ | Record_of_tree _ ) as e ->
        map_expr_f snd e
  in
  let f x = (Analyser.free_vars_f (map_expr_f fst x), {expr = f x}) in
  let f e =
    let fr, r = cata_expr f e in
    if String.Set.mem "ps0" fr
    then r
    else drop_vars (String.Set.singleton "ps0") r
  in
  (* TODO precondition: loops have closed bodies *)
  mk "drop_unused" f [preserves bindings_unique]

(* Returns the expression wrapped in the appropriate chain
   of DUPs. *)
let make_unique st =
  let open String.Set in
  let cond mk (fs, s) xsl (fl, l) xsr (fr, r) subst =
    (* Variables that are used both in the scrutinee and at least one
       of the branches: *)
    let dups = inter fs (union (without fl xsl) (without fr xsr)) in
    let subst_scrut = subst_fresh st dups in
    let subst_branches = subst_fresh st dups in
    let s = s (subst @ subst_scrut) in
    let l = l (subst @ subst_branches) in
    let r = r (subst @ subst_branches) in
    let expr = mk s l r in
    make_dups st (subst_scrut @ subst_branches) {expr}
  in
  let f = function
    | Var x ->
        fun subst -> {expr = Var (Option.default x (List.assoc_opt x subst))}
    | Match_variant ((scrutinee_frees, scrutinee), clauses) ->
        fun subst ->
          let rhs_frees =
            clauses
            |> Binary_tree.map (fun {var; rhs = frees, _} ->
                   Option.cata id remove var frees)
            |> Binary_tree.fold union empty
          in
          let dups = inter scrutinee_frees rhs_frees in
          let subst_scrut = subst_fresh st dups in
          let subst_rhs = subst_fresh st dups in
          let scrutinee = scrutinee (subst @ subst_scrut) in
          let clauses =
            Binary_tree.map
              (fun {cons; var; rhs = _, e} ->
                {cons; var; rhs = e (subst @ subst_rhs)})
              clauses
          in
          let expr = Match_variant (scrutinee, clauses) in
          make_dups st (subst_scrut @ subst_rhs) {expr}
    | If (s, l, r) ->
        let mk s l r = If (s, l, r) in
        cond mk s [] l [] r
    | If_some (s, xl, l, r) ->
        let mk s l r = If_some (s, xl, l, r) in
        cond mk s [xl] l [] r
    | If_left (s, xl, l, xr, r) ->
        let mk s l r = If_left (s, xl, l, xr, r) in
        cond mk s [xl] l [xr] r
    | If_cons (s, xl, xsl, l, r) ->
        let mk s l r = If_cons (s, xl, xsl, l, r) in
        cond mk s [xl; xsl] l [] r
    | ( Loop _ | Map_over _ | Iter_over _ | Let_in _ | Match_record _
      | Create_contract _ | Lambda _ | Lit _ | Prim0 _ | Prim1 _ | Prim1_fail _
      | Prim2 _ | Prim3 _ | Proj_field _ | Stack_op _ | Record _ | Comment _
      | Variant _ | List _ | Set _ | Map _ | Vector _ | Nth _ | Unpair _
      | Record_of_tree _ ) as e ->
        fun subst ->
          (* Generates and applies replacements for variables that occur both in
             e (i.e. in occs) and elsewhere (i.e. in all_occs). *)
          let all_occs =
            map_expr_f (fun (occs, _) -> String.map_of_set (fun _ -> 1) occs) e
            |> fold_expr_f sum_map String.Map.empty
            |> String.Map.filter (fun _ v -> v > 1)
            |> set_of_map
          in
          let dup_expr occs = subst_fresh st (String.Set.inter all_occs occs) in
          let e = map_expr_f (fun (fs, e) -> (dup_expr fs, e)) e in
          let bs = fold_expr_f ( @ ) [] (map_expr_f fst e) in
          let expr = map_expr_f (fun (dups, e) -> e (subst @ dups)) e in
          make_dups st bs {expr}
  in
  let f x = (Analyser.free_vars_f (map_expr_f fst x), f x) in
  let f e = snd (cata_expr f e) [] in
  mk "make_unique" f [preserves bindings_unique]

let _linearize st =
  post
    is_linear
    (Sequence [Fixpoint (make_unique st); drop_unused; close_loops st])

let _linearize_tuples st =
  let f = function
    | Match_record (Node (rp1, rp2), e1, e2) ->
        let x = fresh st "t" in
        let_in_var
          (Some x)
          e1
          (match_record
             rp1
             (prim1 Car (var x))
             (match_record rp2 (prim1 Cdr (var x)) e2))
    | Record t -> Binary_tree.cata snd (pair None None) t
    | expr -> {expr}
  in
  mk "linearize_tuples" (cata_expr f) [preserves bindings_unique]

let unfold_stack_ops ~discarded_only =
  let err = "unfold_stack_ops" in
  let f = function
    | Let_in
        ( P_vector (None :: x :: xs | x :: None :: xs)
        , {expr = Stack_op (Dup 1, es)}
        , e2 ) ->
        let_in_vector (x :: xs) (vector es) e2
    | Let_in
        ( P_vector (Some x1 :: Some x2 :: xs)
        , {expr = Stack_op (Dup 1, e1 :: es)}
        , e2 )
      when not discarded_only ->
        let_in_var
          (Some x1)
          e1
          (let_in_var (Some x2) (var x1) (let_in (P_vector xs) (vector es) e2))
    | Let_in (P_vector (x0 :: xs), {expr = Stack_op (Dup n, es)}, rest)
      when n > 1 && not discarded_only ->
        assert (List.length xs = List.length es);
        assert (List.length xs >= n);
        ( match
            (List.split_at ~err (n - 1) xs, List.split_at ~err (n - 1) es)
          with
        | (x_hi, x :: x_lo), (e_hi, e :: e_lo) ->
            let_in_vector
              ((x0 :: x :: x_hi) @ x_lo)
              (dup 1 ((e :: e_hi) @ e_lo))
              rest
        | _ -> assert false )
    | Let_in (P_vector xs, {expr = Stack_op (Dig n, es)}, e)
      when (not discarded_only) || List.exists Option.is_none xs ->
      ( match List.split_at ~err n es with
      | es1, e' :: es2 -> let_in_vector xs (vector ((e' :: es1) @ es2)) e
      | _ -> assert false )
    | Let_in (P_vector xs, {expr = Stack_op (Dug n, es)}, e)
      when (not discarded_only) || List.exists Option.is_none xs ->
      ( match List.split_at ~err (n + 1) es with
      | e' :: es1, es2 -> let_in_vector xs (vector (es1 @ (e' :: es2))) e
      | _ -> assert false )
    | Let_in (P_vector (x1 :: x2 :: xs as xxs), {expr = Stack_op (Swap, es)}, e)
      when (not discarded_only) || List.exists Option.is_none xxs ->
        let_in_vector (x2 :: x1 :: xs) (vector es) e
    | Let_in (xs, {expr = Stack_op (Drop n, es)}, e) when List.length es > n ->
        let es1, es2 = List.split_at ~err n es in
        let_in_vector
          (List.map (fun _ -> None) es1)
          (vector es1)
          (let_in xs (vector es2) e)
    | expr -> {expr}
  in
  mk "unfold_stack_ops" (cata_expr f) [pre_post bindings_unique]

let chop_stack_ops =
  let err = "chop_stack_ops" in
  let f = function
    | Let_in
        (P_vector (x1 :: x2 :: xs), {expr = Stack_op (Swap, e1 :: e2 :: es)}, e)
      when List.length xs > 0 ->
        let_in_vector
          [x1; x2]
          (stack_op Swap [e1; e2])
          (let_in_vector xs (vector es) e)
    | Let_in (P_vector (x1 :: x2 :: xs), {expr = Stack_op (Dup 1, e1 :: es)}, e)
      when List.length xs > 0 ->
        let_in_vector
          [x1; x2]
          (stack_op (Dup 1) [e1])
          (let_in_vector xs (vector es) e)
    | Let_in (P_vector xs, {expr = Stack_op (Dug n, es)}, e)
      when List.length xs > n + 1 ->
        let xs1, xs2 = List.split_at ~err (n + 1) xs in
        let es1, es2 = List.split_at ~err (n + 1) es in
        let_in_vector
          xs1
          (stack_op (Dug n) es1)
          (let_in_vector xs2 (vector es2) e)
    | Let_in (P_vector xs, {expr = Stack_op (Dig n, es)}, e)
      when List.length xs > n + 1 ->
        let xs1, xs2 = List.split_at ~err (n + 1) xs in
        let es1, es2 = List.split_at ~err (n + 1) es in
        let_in_vector
          xs1
          (stack_op (Dig n) es1)
          (let_in_vector xs2 (vector es2) e)
    | expr -> {expr}
  in
  mk "chop_stack_ops" (cata_expr f) [pre_post bindings_unique]

let elim_drops =
  let f = function
    | Let_in (P_vector [], {expr = Vector []}, e) -> e
    | Let_in (P_vector xs, {expr = Vector es}, e) ->
        lets (List.map2 ~err:"let" (fun x e -> (P_var x, e)) xs es) e
    | Let_in (P_var None, e1, e2) when not (may_fail e1) -> e2
    | Let_in
        ( P_vector rs
        , {expr = Match_variant (scrutinee, Binary_tree.(Node (Leaf x, Leaf y)))}
        , e ) as expr ->
        let x_bound, x_rhs, close_x = lens_lets x.rhs in
        let y_bound, y_rhs, close_y = lens_lets y.rhs in
        let x_bound = Option.cata id cons x.var x_bound in
        let y_bound = Option.cata id cons y.var y_bound in
        let common, xs, ys =
          let is_free_in bound = function
            | {expr = Var x} when not (List.mem x bound) -> Some x
            | _ -> None
          in
          let same_free_var x y =
            match (x.expr, y.expr) with
            | Var x, Var y
              when List.(x = y && (not (mem x x_bound)) && not (mem y y_bound))
              ->
                Some x
            | _ -> None
          in
          match (x_rhs.expr, y_rhs.expr) with
          (* TODO Treat non-vector case of a single variable. *)
          | Vector xs, Vector ys ->
              (List.map2 ~err:"common" same_free_var xs ys, Some xs, Some ys)
          | Vector xs, Prim1_fail (Failwith, _) ->
              (List.map (is_free_in x_bound) xs, Some xs, None)
          | Prim1_fail (Failwith, _), Vector ys ->
              (List.map (is_free_in y_bound) ys, None, Some ys)
          | _ -> ([], None, None)
        in
        if List.exists Option.is_some common
        then
          let commons_only xs =
            let f p x = if Option.is_none p then None else Some x in
            List.somes (List.map2 ~err:"remove" f common xs)
          in
          let non_commons_only xs =
            let f p x = if Option.is_some p then None else Some x in
            List.somes (List.map2 ~err:"remove" f common xs)
          in
          let f x = vector (non_commons_only x) in
          let xs = Option.map f xs in
          let ys = Option.map f ys in
          let x = {x with rhs = Option.cata x.rhs close_x xs} in
          let y = {y with rhs = Option.cata y.rhs close_y ys} in
          let_in_vector
            (commons_only rs)
            (vector (List.map var (List.somes common)))
            (let_in_vector
               (non_commons_only rs)
               (match_variant scrutinee Binary_tree.(Node (Leaf x, Leaf y)))
               e)
        else {expr}
    | expr -> {expr}
  in
  mk "elim_drops" (cata_expr f) [pre_post bindings_unique]

exception No_label

let tree_of_record tree e =
  let leaf = function
    | None -> raise No_label
    | Some fld -> proj_field fld e
  in
  try Binary_tree.cata leaf (pair None None) tree with
  | No_label -> e

let eta_expand_parameter_and_storage st {tstorage; parameter_and_storage} =
  let pre, post =
    match tstorage with
    | T_record r ->
        let r = Binary_tree.map fst r in
        let post body =
          let ops' = fresh st "ops" in
          let s' = fresh st "s" in
          match_record
            Binary_tree.(Node (Leaf (Some ops'), Leaf (Some s')))
            body
            (tuple [var ops'; record_of_tree r (var s')])
        in
        (tree_of_record r, post)
    | _ -> (id, id)
  in
  let f body =
    match parameter_and_storage with
    | None -> body
    | Some ps ->
        let gp = "__parameter" in
        let s = "__storage" in
        let body =
          substitute [(ps, pair None None (var gp) (pre (var s)))] body
        in
        let body = post body in
        let body = let_in_var (Some s) (prim1 Cdr (var ps)) body in
        let body = let_in_var (Some gp) (prim1 Car (var ps)) body in
        body
  in
  mk "eta_expand_parameter_and_storage" f [preserves bindings_unique]

let erase_comments =
  let f = function
    | Comment (_, e) -> e
    | expr -> {expr}
  in
  mk "tuplefy" (cata_expr f) [preserves bindings_unique]

let tuplefy =
  let has_missing_label r =
    List.exists (fun (lbl, _) -> Option.is_none lbl) (Binary_tree.to_list r)
  in
  let f = function
    | Record r when has_missing_label r ->
        Binary_tree.cata (fun (_, t) -> t) (Expr.pair None None) r
    | expr -> {expr}
  in
  mk "tuplefy" (cata_expr f) [preserves bindings_unique]

let recordify st =
  let f = function
    | Match_record (lhs, {expr = Match_variant (s, clauses)}, e) ->
        let f {cons; var; rhs} =
          let fresh = Binary_tree.map (fun x -> (x, fresh st "x")) lhs in
          let lhs = Binary_tree.map (fun (_, x) -> Some x) fresh in
          let subst =
            let g = function
              | Some x, x' -> Some (x, Expr.var x')
              | _ -> None
            in
            List.map_some g (Binary_tree.to_list fresh)
          in
          let e = substitute subst e in
          let rhs =
            match rhs with
            | {expr = Prim1_fail (Failwith, _)} ->
                rhs (* TODO look at stack type, not syntax *)
            | _ -> match_record lhs rhs e
          in
          {cons; var; rhs}
        in
        match_variant s (Binary_tree.map f clauses)
    | Match_record (Node (Leaf x, Leaf y), {expr = Prim2 (Pair (_, _), t, u)}, e)
      ->
        let_in_var x t (let_in_var y u e)
    (* ------------------------------------------------------------------------ *)
    (* Distribute record_of_tree: *)
    | Let_in
        ( P_var (Some x)
        , e1
        , { expr =
              Record
                Binary_tree.(
                  Node
                    ( Leaf
                        ( _
                        , ({expr = Prim0 (Nil _)} as e2)
                        (* TODO generalize to any expression not mentioning x *)
                        )
                    , Leaf (_, {expr = Record_of_tree (row, {expr = Var x'})})
                    )) } )
      when x = x' ->
        let_in_var (Some x) (record_of_tree row e1) (tuple [e2; var x'])
    | Record_of_tree (row, {expr = Match_variant (scrutinee, clauses)}) ->
        let f c = {c with rhs = record_of_tree row c.rhs} in
        match_variant scrutinee (Binary_tree.map f clauses)
    | Record_of_tree (_row, ({expr = Prim1_fail (Failwith, _)} as e)) -> e
    (* Fallback: convert to record if possible: *)
    | Record_of_tree (row, tree) as expr ->
        let open Option in
        let rec parse_tree row tree =
          match (row, tree.expr) with
          | Leaf l, _ -> Some (Binary_tree.Leaf (l, tree))
          | Node (r1, r2), Prim2 (Pair _, x1, x2) ->
              let* x1 = parse_tree r1 x1 in
              let* x2 = parse_tree r2 x2 in
              Some (Binary_tree.Node (x1, x2))
          | _ -> None
        in
        Option.cata {expr} record (parse_tree row tree)
    | expr -> {expr}
  in
  mk "recordify" (cata_expr f) [preserves bindings_unique]

let unfold_unpair =
  let f = function
    | Unpair (2, x) -> vector [prim1 Car x; prim1 Cdr x]
    | expr -> {expr}
  in
  mk "unfold_unpair" (cata_expr f) [preserves bindings_unique]

let simplify st =
  let main =
    Sequence
      [ unfold_ifs
      ; unfold_stack_ops ~discarded_only:false
      ; inline
      ; fold_constants
      ; mark_unused
      ; unvectorize_lets st
      ; field_access
      ; flatten_matches
      ; Label ("fix_mangle", Fixpoint (mangle st))
      ; infer_constructors
      ; remove_invariant_loop_state
      ; unused_loop_state st
      ; factorise_clauses st
      ; reify_booleans ]
  in
  let t = Fixpoint main in
  run_transformer t

let smartMLify st pc =
  let t =
    Sequence
      [ erase_comments
      ; unfold_ifs
      ; Fixpoint
          (Sequence [unfold_stack_ops ~discarded_only:false; chop_stack_ops])
      ; unfold_unpair
      ; Fixpoint
          (Sequence
             [ inline
             ; fold_constants
             ; mark_unused
             ; unvectorize_lets st
             ; field_access
             ; flatten_matches
             ; Label ("fix_mangle", Fixpoint (mangle st))
             ; infer_constructors
             ; inline_invariant_loop_state
             ; remove_invariant_loop_state
             ; unused_loop_state st
             ; factorise_clauses st
             ; reify_booleans ])
      ; binarize_matches st
      ; binarize_projs
      ; single_lets st
      ; eta_expand_parameter_and_storage st pc
      ; flatten_matches
      ; Fixpoint
          (Sequence
             [ recordify st
             ; fold_constants
             ; inline
             ; distribute_lets
             ; unvectorize_lets st
             ; tuplefy ]) ]
  in
  run_transformer t pc

let michelsonify _st =
  let t =
    Sequence
      [ unfold_ifs
      ; chop_stack_ops
      ; Fixpoint
          (Sequence
             [ mark_unused
             ; inline_vars
             ; remove_invariant_loop_state
             ; unfold_stack_ops ~discarded_only:true
             ; fold_constants
             ; elim_drops ])
      ; fold_ifs
      ; nest_ifs
      ; drop_unused
        (* NB We don't need do full-blown linearization here because
           none of the above transformations create variables with
           several occurrences. *)
      ; Fixpoint (Sequence [factorise_failwith; flatten_ifs]) ]
  in
  run_transformer t
