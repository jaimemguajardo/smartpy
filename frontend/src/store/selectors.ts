import * as editorSelectors from '../features/editor/selectors';
import * as michelsonEditorSelectors from '../features/michelson-editor/selectors';
import * as themeSelectors from '../features/theme/selectors';

const selectors = {
    editor: editorSelectors,
    michelsonEditor: michelsonEditorSelectors,
    theme: themeSelectors,
};

export default selectors;
