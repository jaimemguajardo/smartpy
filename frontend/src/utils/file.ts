import httpRequester from '../services/httpRequester';

/**
 * Get content from file through HTTP request.
 * @param {string} fileLocation - File Location.
 * @return {Promise<string | void>} A promise with the file contents, or null if the file doesn't exist.
 */
export const getFileContent = async (fileLocation: string): Promise<string> => {
    try {
        return (await httpRequester.get(fileLocation)).data;
    } catch (e) {
        throw new Error(`Failed to load file: ${fileLocation}. (${e})`);
    }
};

export const downloadFile = (fileName: string, data: string) => {
    const blob = new Blob([data], { type: 'text/plain' });
    if (typeof navigator.msSaveOrOpenBlob !== 'undefined') {
        navigator.msSaveBlob(blob, fileName);
    } else {
        const elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = fileName;
        document.body.appendChild(elem);
        elem.click();
        document.body.removeChild(elem);
    }
};

const file = {
    getFileContent,
    downloadFile,
};

export default file;
