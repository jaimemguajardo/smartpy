/**
 * Verify if an object is not empty.
 * @param {Record<string, any>} obj - The object to validate
 * @return {boolean} True if the object is not empty or false otherwise
 */
export const objectIsNotEmpty = (obj: Record<string, any>) =>
    obj && obj.constructor === Object && Object.keys(obj).length >= 1;

/**
 * Returns the parameter or an empty array.
 * @param arr any array which can be null.
 * @return {Array<T>} The parameter or [] if parameter is null.
 */
export const arrayOrEmptyArray = <T>(arr: T[]) => arr || [];
