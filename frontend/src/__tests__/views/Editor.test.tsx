import React, { Suspense } from 'react';
import ReactAce from 'react-ace/lib/ace';
import { act } from 'react-dom/test-utils';
import { IDELayout } from 'SmartPyModels';

import Editor from '../../features/editor/views/EditorView';
import renderWithStore from '../test-helpers/renderWithStore';

const props = {
    htmlOutput: {
        __html: '',
    },
    settings: {
        layout: 'side-by-side' as IDELayout,
    },
    tests: [] as string[],
    editorRef: React.createRef() as React.RefObject<ReactAce>,
    updateContract: (value: string) => null,
    getCommands: () => ({}),
    showError: (error: string) => null,
    clearOutputs: () => null,
    contract: '',
    sharedContract: '',
};

describe('Editor Page', () => {
    it('Editor renders correctly', async () => {
        let container: Element;
        await act(async () => {
            container = renderWithStore(
                <Suspense fallback={'...'}>
                    <Editor {...props} />
                </Suspense>,
            ).container;

            while (!container.innerHTML.includes('run-code')) {
                // Wait for the content to be available or fail with timeout
                await new Promise((r) => setTimeout(r, 1000));
            }
            expect(container).toMatchSnapshot();
        });
    });
});
