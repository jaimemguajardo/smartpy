import { waitFor } from '@testing-library/react';

import renderWithRouter from '../test-helpers/renderWithRouter';
import routes from '../../features/navigation/constants/routes';

jest.mock('../../utils/rand.ts', () => ({
    generateID: (): string => {
        return '123'; // Mock random value for tests
    },
}));

describe('Application Routing', () => {
    for (const route of routes) {
        let path = route.routeProps.path as string;
        let testDescription = `Test Route ${route.title}`;

        if (route.routeProps.path === '*') {
            path = '/not-found';
            testDescription = 'Test Non Existent Route';
        }

        it(testDescription, async () => {
            const { container } = renderWithRouter({
                route: path,
            });
            expect(container.innerHTML).toMatchSnapshot();
            await waitFor(() => expect(document.title).toMatch(route.title));
        });
    }
});
