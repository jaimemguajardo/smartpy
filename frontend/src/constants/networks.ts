import { Protocol, ProtocolHash, DefaultProtocol } from './protocol';

export enum Network {
    MAINNET = 'Mainnet',
    DELPHINET = 'Delphinet',
    EDONET = 'Edonet',
    FLORENCENET = 'Florencenet',
}

export const NetworkProtocolHash = {
    [String(Network.MAINNET)]: ProtocolHash[DefaultProtocol],
    [String(Network.DELPHINET)]: ProtocolHash[Protocol.DELPHI],
    [String(Network.EDONET)]: ProtocolHash[Protocol.EDO],
    [String(Network.FLORENCENET)]: ProtocolHash[Protocol.FLORENCE],
};

export const DeprecatedNetworks = [Network.DELPHINET];
