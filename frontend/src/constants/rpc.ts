import { Network } from './networks';

export const smartpy = {
    [Network.MAINNET]: 'https://mainnet.smartpy.io',
    [Network.DELPHINET]: 'https://delphinet.smartpy.io',
    [Network.EDONET]: 'https://edonet.smartpy.io',
    [Network.FLORENCENET]: 'https://florencenet.smartpy.io',
};

const giga = {
    [Network.MAINNET]: 'https://mainnet-tezos.giganode.io',
    [Network.DELPHINET]: 'https://testnet-tezos.giganode.io',
    [Network.EDONET]: 'https://edonet-tezos.giganode.io',
};

export const Nodes = {
    // SMARTPY
    [smartpy.Mainnet]: Network.MAINNET,
    [smartpy.Delphinet]: Network.DELPHINET,
    [smartpy.Edonet]: Network.EDONET,
    [smartpy.Florencenet]: Network.FLORENCENET,
    // GIGANODE
    [giga.Mainnet]: Network.MAINNET,
    [giga.Delphinet]: Network.DELPHINET,
    [giga.Edonet]: Network.EDONET,
};

export enum Endpoint {
    HEADER = '/chains/main/blocks/head/header',
    ERRORS = '/chains/main/blocks/head/context/constants/errors',
    VERSION = '/version',
    CHECKPOINT = '/chains/main/checkpoint',
    CONSTANTS = '/chains/main/blocks/head/context/constants',
    METADATA = '/chains/main/blocks/head/metadata',
}

export default Nodes;
