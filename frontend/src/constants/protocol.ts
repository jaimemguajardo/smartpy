export enum Protocol {
    DELPHI = 'Delphi',
    EDO = 'Edo',
    FLORENCE = 'Florence',
}

export const ProtocolHash = {
    [String(Protocol.DELPHI)]: 'PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo',
    [String(Protocol.EDO)]: 'PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA',
    [String(Protocol.FLORENCE)]: 'PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i',
};

export const DefaultProtocol = Protocol.EDO;
