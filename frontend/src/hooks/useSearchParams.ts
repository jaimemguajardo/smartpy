import { useLocation } from 'react-router';
import queryString from 'query-string';

export const useSearchParams = () => {
    const location = useLocation();
    return queryString.parse(location.search);
};
