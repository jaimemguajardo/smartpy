import { createAction } from 'typesafe-actions';
export enum Actions {
    CHANGE_LANGUAGE = 'CHANGE_LANGUAGE',
}

export const changeLanguage = createAction(Actions.CHANGE_LANGUAGE, (language: string) => language)<string>();

const actions = {
    changeLanguage,
};

export default actions;
