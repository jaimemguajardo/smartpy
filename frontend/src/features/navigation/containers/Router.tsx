import React from 'react';
import { Switch } from 'react-router';
import { Helmet } from 'react-helmet-async';

import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import selectors from '../../../store/selectors';
import routes from '../constants/routes';
import Loader from '../../loader/components/CircularProgressWithText';

const Router = () => {
    const isDarkMode = selectors.theme.useThemeMode();
    const theme = React.useMemo(
        () =>
            createMuiTheme({
                palette: {
                    mode: isDarkMode ? 'dark' : 'light',
                    background: isDarkMode
                        ? {}
                        : {
                              default: '#F1F9FF',
                          },
                    primary: {
                        light: '#69a9ff',
                        main: '#007bff',
                        dark: '#0050cb',
                    },
                    secondary: {
                        light: '#FFF',
                        main: '#FFF',
                        dark: '#cccccc',
                    },
                },
            }),
        [isDarkMode],
    );

    return (
        <ThemeProvider theme={theme}>
            <Switch>
                {routes.map(({ Route, routeProps, Component, title }, index) => (
                    <Route key={index} {...routeProps}>
                        <React.Suspense fallback={<Loader loading={true} />}>
                            <Helmet>
                                <title>{title}</title>
                            </Helmet>
                            <Component />
                        </React.Suspense>
                    </Route>
                ))}
            </Switch>
        </ThemeProvider>
    );
};

export default Router;
