import React from 'react';

// Material UI
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import ErrorIcon from '@material-ui/icons/Error';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        failureIcon: {
            width: 48,
            height: 48,
            backgroundColor: 'rgb(237, 102, 99)',
        },
    }),
);

const FailureIndicator = () => {
    const classes = useStyles();

    return (
        <Avatar className={classes.failureIcon}>
            <ErrorIcon fontSize="large" />
        </Avatar>
    );
};

export default FailureIndicator;
