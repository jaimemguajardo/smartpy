import React from 'react';

// Material UI
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import SuccessIcon from '@material-ui/icons/Check';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        successIcon: {
            width: 48,
            height: 48,
            backgroundColor: 'rgb(131, 179, 0)',
        },
    }),
);

const SuccessIndicator = () => {
    const classes = useStyles();

    return (
        <Avatar className={classes.successIcon}>
            <SuccessIcon fontSize="large" />
        </Avatar>
    );
};

export default SuccessIndicator;
