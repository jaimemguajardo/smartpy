import React from 'react';

// Material UI
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Slide from '@material-ui/core/Slide';
import { TransitionProps } from '@material-ui/core/transitions/transition';

// Utils
import { prettifyJsonString } from '../../../utils/json';
import { objectIsNotEmpty } from '../../../utils/object';
// Components
import CodeBlockWithCopy from '../components/CodeBlock';
// State Management
import useTranslation from '../../i18n/hooks/useTranslation';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            height: '70vh',
            overflowY: 'auto',
        },
        fullHeight: {
            height: '100%',
        },
    }),
);

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & { children?: React.ReactElement<any, any> },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
    className: string;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && children}
        </div>
    );
}

/**
 * @summary Accessibility props (a11y)
 */
const a11yProps = (index: number) => {
    return {
        id: `tab-${index}`,
        'aria-controls': `tabpanel-${index}`,
    };
};

export interface NodeData {
    version: JSON;
    header: JSON;
    checkpoint: JSON;
    constants: JSON;
    explorerTip: JSON;
    explorerConfig: JSON;
    metaData: JSON;
    errors: JSON;
}

interface OwnProps {
    nodeData: NodeData;
}

const NodeDataView: React.FC<OwnProps> = ({ nodeData }) => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [open, setOpen] = React.useState(false);
    const t = useTranslation();

    const handleSwitch = () => {
        setOpen((state) => !state);
    };

    const handleChange = (event: React.SyntheticEvent<Element, Event>, newValue: number) => {
        setValue(newValue);
    };

    const tabs = React.useMemo(
        () =>
            Object.keys(nodeData)
                .filter((key) => objectIsNotEmpty((nodeData as Record<string, any>)[key]))
                .sort(),
        [nodeData],
    );

    return (
        <>
            <Button fullWidth variant="contained" color="primary" className={classes.fullHeight} onClick={handleSwitch}>
                View Node Data
            </Button>
            <Dialog maxWidth="md" fullWidth onClose={handleSwitch} open={open} TransitionComponent={Transition}>
                <AppBar position="static" color="default">
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="scrollable"
                        scrollButtons="auto"
                        aria-label="Node Data Dialog"
                    >
                        {tabs.map((section, index) => (
                            <Tab label={t(section)} {...a11yProps(index)} key={index} />
                        ))}
                    </Tabs>
                </AppBar>

                {tabs.map((section, index) => (
                    <TabPanel value={value} index={index} className={classes.root} key={index}>
                        <CodeBlockWithCopy
                            language="json"
                            showLineNumbers
                            text={
                                prettifyJsonString(JSON.stringify((nodeData as Record<string, any>)[section]), 4) || ''
                            }
                        />
                    </TabPanel>
                ))}

                <Divider />
                <DialogActions>
                    <Button autoFocus color="primary" onClick={handleSwitch}>
                        {t('common.close')}
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default NodeDataView;
