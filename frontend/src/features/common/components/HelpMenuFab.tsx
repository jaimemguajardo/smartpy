import React from 'react';

// Material UI
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import HelpIcon from '@material-ui/icons/HelpOutlineOutlined';
import Fab from '@material-ui/core/Fab';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        zoomRoot: {
            position: 'fixed',
            bottom: theme.spacing(2),
            right: theme.spacing(3),
        },
    }),
);

const HelpMenuFab: React.FC = ({ children }) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef<HTMLButtonElement>(null);

    const handleToggle = () => {
        setOpen((prevState) => !prevState);
    };

    const handleClose = (event: React.MouseEvent<EventTarget>) => {
        if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
            return;
        }

        setOpen(false);
    };

    return (
        <React.Fragment>
            <div role="presentation" className={classes.zoomRoot} aria-controls="help-menu" onClick={handleToggle}>
                <Fab color="primary" size="small" ref={anchorRef}>
                    <HelpIcon />
                </Fab>
            </div>
            <Menu
                anchorEl={anchorRef.current}
                keepMounted
                open={Boolean(open)}
                onClose={handleClose}
                onClick={handleClose}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
            >
                {children}
            </Menu>
        </React.Fragment>
    );
};

export default HelpMenuFab;
