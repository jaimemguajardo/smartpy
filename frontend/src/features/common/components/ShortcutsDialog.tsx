import React from 'react';

// Material UI
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';

import { Ace } from 'ace-builds';

interface OwnProps {
    platform?: string;
    hideDialog: () => void;
    getCommands: () => Ace.CommandMap;
}

const ShortcutsDialog: React.FC<OwnProps> = (props) => {
    const { platform, hideDialog, getCommands } = props;

    const commands = getCommands();

    const handleListItemClick = () => {
        hideDialog();
    };

    const getBindKey = (command: Ace.Command) => {
        if (platform && Object.getOwnPropertyNames(command?.bindKey || {}).includes(platform)) {
            return (command?.bindKey as { [index: string]: string })[platform];
        }
    };

    return platform ? (
        <Dialog onClose={handleListItemClick} aria-labelledby={`shortcuts-dialog-${platform}`} open>
            <DialogTitle>{`${platform.toUpperCase()} - Shortcuts`}</DialogTitle>
            <DialogContent>
                <List>
                    {Object.keys(commands)
                        .filter((key) => getBindKey(commands[key]))
                        .map((key) => (
                            <ListItem button onClick={handleListItemClick} key={key}>
                                <ListItemText
                                    primary={(commands[key] as any).description || key}
                                    secondary={getBindKey(commands[key])}
                                />
                            </ListItem>
                        ))}
                </List>
            </DialogContent>
        </Dialog>
    ) : null;
};

export default ShortcutsDialog;
