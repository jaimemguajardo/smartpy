import React from 'react';

// Material UI
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Fab from '@material-ui/core/Fab';
import TelegramIcon from '@material-ui/icons/Telegram';

// Local Components
import Footer from '../../navigation/components/Footer';
import ReferenceManual from '../components/ReferenceManual';
import Faq from '../components/Faq';
import ArticlesGuides from '../../common/components/ArticlesGuides';

// Local Elements
import StackExchangeIcon from '../../common/elements/icons/StackExchange';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            backgroundColor: theme.palette.background.paper,
        },
        extendedIcon: {
            marginRight: theme.spacing(1),
        },
        fabLabel: {
            paddingRight: 5,
            textTransform: 'none',
        },
        topButtons: {
            display: 'flex',
            justifyContent: 'center',
            margin: 20,
            [theme.breakpoints.down('xs')]: {
                display: 'flex',
                flexWrap: 'wrap',
            },
        },
        fabButton: {
            margin: 5,
            borderWidth: 1,
            borderStyle: 'solid',
            borderColor: theme.palette.primary.main,
            [theme.breakpoints.down('xs')]: {
                flexGrow: 1,
            },
        },
    }),
);

const Help = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div className={classes.topButtons}>
                <Fab
                    variant="extended"
                    color="secondary"
                    aria-label="Telegram"
                    href="https://t.me/SmartPy_io"
                    target="_blank"
                    className={classes.fabButton}
                    classes={{ label: classes.fabLabel }}
                >
                    <TelegramIcon className={classes.extendedIcon} color="primary" />
                    Telegram
                </Fab>
                <Fab
                    variant="extended"
                    color="secondary"
                    aria-label="Tezos Stack Exchange"
                    href="https://tezos.stackexchange.com/questions/ask?tags=smartpy"
                    target="_blank"
                    className={classes.fabButton}
                    classes={{ label: classes.fabLabel }}
                >
                    <StackExchangeIcon className={classes.extendedIcon} color="primary" />
                    Tezos Stack Exchange
                </Fab>
            </div>
            <Divider />
            <ReferenceManual />
            <Divider />
            <Faq />
            <Divider />
            <ArticlesGuides />
            <Footer />
        </div>
    );
};

export default Help;
