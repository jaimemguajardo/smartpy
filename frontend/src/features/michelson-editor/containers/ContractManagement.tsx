import React from 'react';
import { useDispatch } from 'react-redux';

// State Management
import { useContracts, useSelectedContract } from '../selectors';

// Local Components
import ContractManagementComponent from '../../common/components/ContractManagementComponent';

// State Management
import actions from '../actions';

interface OwnProps {
    onResize: () => void;
}

const ContractManagement: React.FC<OwnProps> = ({ onResize }) => {
    const contract = useSelectedContract();
    const contracts = useContracts();
    const dispatch = useDispatch();

    const handleCreateContract = (name: string) => {
        dispatch(
            actions.addContract(
                {
                    name,
                    shared: false,
                },
                false,
            ),
        );
    };

    const handleUpdateContract = (contractId: string, name: string) => {
        actions.updateContract({
            id: contractId,
            name,
        });
    };

    const handleDeleteContract = (contractId: string) => {
        dispatch(actions.removeContract(contractId));
    };

    const handleSelectContract = (contractId: string) => {
        dispatch(actions.selectContract(contractId));
    };

    return (
        <ContractManagementComponent
            onResize={onResize}
            contract={contract}
            contracts={contracts}
            handleDeleteContract={handleDeleteContract}
            handleUpdateContract={handleUpdateContract}
            handleSelectContract={handleSelectContract}
            handleCreateContract={handleCreateContract}
        />
    );
};

export default ContractManagement;
