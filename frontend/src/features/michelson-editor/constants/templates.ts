export const templates = [
    {
        name: 'Empty',
        code: `parameter type;
storage   type;
code
    {
    
    };`,
    },
    {
        name: 'Minimal',
        code: `parameter int;
storage   int;
code
    {
    CAR;
    NIL operation;
    PAIR;
    };`,
    },
];
