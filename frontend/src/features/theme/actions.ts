import { createAction } from 'typesafe-actions';

export enum Actions {
    SET_DARK_MODE = 'SET_DARK_MODE',
}

export const setDarkMode = createAction(Actions.SET_DARK_MODE, (isDarkMode: boolean) => isDarkMode)<boolean>();

const actions = {
    setDarkMode,
};

export default actions;
