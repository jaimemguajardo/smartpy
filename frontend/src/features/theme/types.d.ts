declare module 'SmartPyModels' {
    export type ThemeSettings = {
        darkMode: boolean;
    };
}
