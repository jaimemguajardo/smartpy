import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { makeStyles, fade, createStyles, Theme, useTheme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ShareOutlinedIcon from '@material-ui/icons/ShareOutlined';
import Tooltip from '@material-ui/core/Tooltip';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarIcon from '@material-ui/icons/Star';

// Local Services
import toast from '../../../services/toast';
// Local Components
import Loader from '../../loader/components/CircularProgressWithText';
// State Management
import actions from '../actions';
import { useFavoriteTemplates } from '../selectors';
import useTranslation from '../../i18n/hooks/useTranslation';
// Local Constants
import { TemplateProps, getAllTemplates } from '../constants/templates';
// Utils
import { copyToClipboard } from '../../../utils/clipboard';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        secondaryText: {
            color: theme.palette.mode === 'dark' ? 'rgba(255, 255, 255, 0.54)' : 'rgba(0, 0, 0, 0.54)',
        },
        list: {
            width: '100%',
        },
        item: {
            display: 'flex',
            width: '100%',
        },
        favoriteButton: {
            width: 48,
            height: 48,
            alignSelf: 'center',
        },
        itemContainer: {
            width: '100%',
        },
        title: {
            flexGrow: 1,
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                display: 'block',
            },
        },
        search: {
            position: 'relative',
            display: 'flex',
            alignItems: 'center',
            paddingLeft: 5,
            borderRadius: theme.shape.borderRadius,
            backgroundColor: fade(theme.palette.common.white, 0.15),
            '&:hover': {
                backgroundColor: fade(theme.palette.common.white, 0.25),
            },
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(1),
                width: 'auto',
            },
        },
        searchIcon: {
            alignSelf: 'center',
        },
        inputRoot: {
            color: 'inherit',
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                width: '12ch',
                '&:focus': {
                    width: '20ch',
                },
            },
        },
        emptyListText: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: 20,
        },
    }),
);

const allTemplates = getAllTemplates();

const FavoriteContracts: React.FC = () => {
    const classes = useStyles();
    const theme = useTheme();
    const [search, setSearch] = React.useState('');
    const [favorites, setFavorites] = React.useState([] as TemplateProps[]);
    const [loading, setLoading] = React.useState(true);
    const favoriteTemplates = useFavoriteTemplates();
    const dispatch = useDispatch();
    const t = useTranslation();

    const handleListItemClick = async (fileName: string) => {
        dispatch(actions.loadTemplate(fileName));
    };

    const handleTemplateShare = (fileName: string) => {
        copyToClipboard(`${window.location.origin}/ide/?template=${fileName}`);
        toast.info('The template url was copied!');
    };

    const handleFavoriteToggle = (event: React.MouseEvent<HTMLButtonElement>, template: string) => {
        dispatch(actions.toggleFavoriteTemplate(template));
    };

    const updateFilteredTemplates = React.useCallback(
        () =>
            setFavorites(() => {
                setLoading(true);
                const newState = Object.keys(allTemplates).reduce<TemplateProps[]>((state, templateName) => {
                    const template = allTemplates[templateName];
                    if (
                        favoriteTemplates.includes(template.fileName) &&
                        (template.description?.match(search) || template.name.match(search))
                    ) {
                        state.push(template);
                    }
                    return state;
                }, []);

                setLoading(false);
                return newState;
            }),
        [favoriteTemplates, search],
    );

    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value);
    };

    React.useEffect(() => {
        updateFilteredTemplates();
    }, [updateFilteredTemplates]);

    return (
        <React.Fragment>
            <AppBar position="static" color={theme.palette.mode === 'dark' ? 'default' : 'primary'}>
                <Toolbar>
                    <Typography className={classes.title} variant="h6" noWrap>
                        {t('ide.templatesMenu.favoriteTemplates')}
                    </Typography>
                    <div className={classes.search}>
                        <SearchIcon className={classes.searchIcon} />
                        <InputBase
                            defaultValue={search}
                            onChange={handleSearch}
                            placeholder={`${t('common.search')}…`}
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </div>
                </Toolbar>
            </AppBar>

            <Loader loading={loading} margin={50} />
            {favorites.length === 0 && !loading ? (
                <div className={classes.emptyListText}>
                    <Typography variant="overline">{t('ide.templatesMenu.emptyFavoritesList')}</Typography>
                </div>
            ) : (
                <List className={classes.list}>
                    {favorites.map((favorite) => (
                        <div key={favorite.name} className={classes.item}>
                            <IconButton
                                key={favorite.fileName}
                                className={classes.favoriteButton}
                                onClick={(e) => handleFavoriteToggle(e, favorite.fileName)}
                            >
                                {favoriteTemplates.includes(favorite.fileName) ? <StarIcon /> : <StarBorderIcon />}
                            </IconButton>
                            <ListItem
                                button
                                onClick={() => handleListItemClick(favorite.fileName)}
                                classes={{ container: classes.itemContainer }}
                            >
                                <div>
                                    <ListItemText primary={favorite.name} />
                                    {favorite.description ? (
                                        <div
                                            className={classes.secondaryText}
                                            dangerouslySetInnerHTML={{ __html: favorite.description }}
                                        />
                                    ) : null}
                                </div>
                                <ListItemSecondaryAction onClick={() => handleTemplateShare(favorite.fileName)}>
                                    <Tooltip title="Share Template" aria-label="share-template" placement="left">
                                        <IconButton edge="end">
                                            <ShareOutlinedIcon />
                                        </IconButton>
                                    </Tooltip>
                                </ListItemSecondaryAction>
                            </ListItem>
                        </div>
                    ))}
                </List>
            )}
        </React.Fragment>
    );
};

export default FavoriteContracts;
