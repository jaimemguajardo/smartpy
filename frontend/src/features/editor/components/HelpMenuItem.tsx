import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Material UI
import MenuItem from '@material-ui/core/MenuItem';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';

import { Ace } from 'ace-builds';

// Local Views
import ShortcutsDialog from '../../common/components/ShortcutsDialog';

// Redux logic
import { RootState } from 'SmartPyTypes';
import actions from '../actions';

// Local Components
import HelpMenuFab from '../../common/components/HelpMenuFab';

// Local Utils
import { getBase } from '../../../utils/url';
import useTranslation from '../../i18n/hooks/useTranslation';

interface OwnProps {
    getCommands: () => Ace.CommandMap;
}

const HelpMenuItem: React.FC<OwnProps> = (props) => {
    const t = useTranslation();
    const dispatch = useDispatch();
    const platform = useSelector((state: RootState) => state.michelsonEditor.shortcuts);

    const { getCommands } = props;

    const handleShowShortcuts = (platform: string) => {
        dispatch(actions.showShortcuts(platform));
    };

    const handleHideShortcuts = () => {
        dispatch(actions.hideShortcuts());
    };

    return (
        <>
            <HelpMenuFab>
                <MenuItem component={Link} href={`${getBase()}/reference.html`} target="_blank">
                    <Typography variant="inherit">{t('ide.helpMenu.openReferenceManual')}</Typography>
                </MenuItem>
                <MenuItem component={Link} href={`${getBase()}/cli`} target="_blank">
                    <Typography variant="inherit">{t('ide.helpMenu.installSmartpyCli')}</Typography>
                </MenuItem>
                <MenuItem component={Link} href={`${getBase()}/releases.html`} target="_blank">
                    <Typography variant="inherit">{t('ide.helpMenu.viewReleases')}</Typography>
                </MenuItem>
                <MenuItem onClick={() => handleShowShortcuts('mac')}>
                    <Typography variant="inherit">Mac - {t('common.shortcuts')}</Typography>
                </MenuItem>
                <MenuItem onClick={() => handleShowShortcuts('win')}>
                    <Typography variant="inherit">Win - {t('common.shortcuts')}</Typography>
                </MenuItem>
            </HelpMenuFab>
            <ShortcutsDialog getCommands={getCommands} platform={platform} hideDialog={handleHideShortcuts} />
        </>
    );
};

export default HelpMenuItem;
