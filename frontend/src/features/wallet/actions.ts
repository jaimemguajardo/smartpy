import { NetworkInformation, AccountInformation } from 'SmartPyModels';
import { createAction } from 'typesafe-actions';

export enum Actions {
    // Network
    UPDATE_NETWORK_INFO = 'UPDATE_NETWORK_INFO',
    // Account
    CLEAR_ACCOUNT_INFO = 'CLEAR_ACCOUNT_INFO',
    UPDATE_ACCOUNT_INFO = 'UPDATE_ACCOUNT_INFO',
    LOADING_ACCOUNT_INFO = 'LOADING_ACCOUNT_INFO',
}

// Network

export const updateNetworkInfo = createAction(Actions.UPDATE_NETWORK_INFO)<NetworkInformation>();

// Account

export const updateAccountInfo = createAction(Actions.UPDATE_ACCOUNT_INFO)<AccountInformation>();
export const clearAccountInfo = createAction(Actions.CLEAR_ACCOUNT_INFO)();
export const loadingAccountInfo = createAction(Actions.LOADING_ACCOUNT_INFO)<boolean>();

const actions = {
    updateNetworkInfo,
    updateAccountInfo,
    clearAccountInfo,
    loadingAccountInfo,
};

export default actions;
