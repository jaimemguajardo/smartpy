import React from 'react';
import { useDispatch } from 'react-redux';

import { makeStyles, createStyles } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Chip from '@material-ui/core/Chip';
import Typography from '@material-ui/core/Typography';
import Alert from '@material-ui/core/Alert';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';

import CircularProgressWithText from '../../loader/components/CircularProgressWithText';
import { useAccountInfo } from '../selectors/account';
import { convertUnitWithSymbol, AmountUnit } from '../../../utils/units';
import AddressAvatar from '../../common/components/AddressAvatar';
import useTranslation from '../../i18n/hooks/useTranslation';
import { copyToClipboard } from '../../../utils/clipboard';
import { shortenText } from '../../../utils/style/shortenner';
import { appendClasses } from '../../../utils/style/classes';
import { updateAccountInfo } from '../actions';
import WalletServices from '../services';
import logger from '../../../services/logger';
import { AccountSource } from '../constants/sources';
import debounce from '../../../utils/debounce';

const useStyles = makeStyles((theme) =>
    createStyles({
        centralizedContainer: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            padding: 10,
        },
        divider: {
            margin: theme.spacing(1, 0, 1, 0),
        },
        chip: {
            minHeight: 32,
        },
        card: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'stretch',
            backgroundColor: theme.palette.background.default,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
            padding: theme.spacing(1),
        },
        avatarSection: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
            borderRadius: 4,
            padding: 10,
            height: 'calc(100% - 20px)',
            backgroundColor: theme.palette.background.paper,
        },
        infoSection: {
            width: 424,
        },
        alignSelf: {
            alignSelf: 'stretch',
        },
        marginTop: {
            marginTop: 10,
        },
        section: {
            padding: 10,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
    }),
);

const debouncer = debounce(1000);

const AccountInfo: React.FC = () => {
    const isMobile = useMediaQuery('(max-width:600px)');
    const classes = useStyles();
    const accountInformation = useAccountInfo();
    const t = useTranslation();
    const dispatch = useDispatch();

    React.useEffect(() => {
        debouncer(async () => {
            if (accountInformation?.pkh && accountInformation.source === AccountSource.SMARTPY_FAUCET) {
                const needsActivation = await WalletServices[AccountSource.SMARTPY_FAUCET].requiresActivation();
                if (accountInformation.activationRequired !== needsActivation) {
                    dispatch(updateAccountInfo({ activationRequired: needsActivation }));
                }
            }
        });
    }, [
        accountInformation.activating,
        accountInformation.network,
        accountInformation.activationRequired,
        accountInformation.source,
        dispatch,
        accountInformation?.pkh,
    ]);

    const revealAccount = React.useCallback(async () => {
        dispatch(updateAccountInfo({ revealing: true }));
        try {
            if (accountInformation?.source && WalletServices[accountInformation.source]) {
                await WalletServices[accountInformation.source].reveal();
                dispatch(updateAccountInfo({ revealRequired: false }));
            }
        } catch (e) {
            logger.debug('Failed to reveal account.', e);
            dispatch(updateAccountInfo({ errors: e.message }));
        }
        dispatch(updateAccountInfo({ revealing: false }));
    }, [accountInformation.source, dispatch]);

    const activateAccount = React.useCallback(async () => {
        dispatch(updateAccountInfo({ activating: true }));
        try {
            if (accountInformation.source && WalletServices[accountInformation.source]) {
                await WalletServices[AccountSource.SMARTPY_FAUCET].activateAccount();
                const balance = await WalletServices[accountInformation.source].getBalance();
                dispatch(updateAccountInfo({ activationRequired: false, balance }));
            }
        } catch (e) {
            logger.debug('Failed to activate account.', e);
            dispatch(updateAccountInfo({ errors: e.message }));
        }
        dispatch(updateAccountInfo({ activating: false }));
    }, [accountInformation.source, dispatch]);

    const ActivationOrRevealButtons = React.useMemo(() => {
        if (accountInformation.activationRequired) {
            return (
                <>
                    <Divider className={classes.divider} />
                    {accountInformation.activating ? (
                        <CircularProgressWithText size={24} msg={t('wallet.accountInfo.activating')} />
                    ) : (
                        <Button variant="outlined" fullWidth onClick={activateAccount}>
                            {t('wallet.accountInfo.activate')}
                        </Button>
                    )}
                </>
            );
        }

        if (accountInformation.revealRequired) {
            return (
                <>
                    <Divider className={classes.divider} />
                    {accountInformation.revealing ? (
                        <CircularProgressWithText size={24} msg={t('wallet.accountInfo.revealing')} />
                    ) : (
                        <Button variant="outlined" fullWidth onClick={revealAccount}>
                            {t('wallet.accountInfo.reveal')}
                        </Button>
                    )}
                </>
            );
        }
    }, [
        accountInformation.activationRequired,
        accountInformation.revealRequired,
        accountInformation.activating,
        accountInformation.revealing,
        classes.divider,
        t,
        activateAccount,
        revealAccount,
    ]);

    const copyAddress = React.useCallback(() => {
        if (accountInformation?.pkh) {
            copyToClipboard(accountInformation.pkh);
        }
    }, [accountInformation]);

    if (accountInformation.errors) {
        return (
            <div className={classes.centralizedContainer}>
                <Alert severity="error">{accountInformation.errors}</Alert>
            </div>
        );
    }

    return accountInformation.isLoading ? (
        <CircularProgressWithText size={64} margin={40} msg={t('common.loading')} />
    ) : accountInformation.pkh ? (
        <div className={classes.centralizedContainer}>
            <Typography variant="overline">{t('wallet.accountInfo.title')}</Typography>
            <div className={classes.centralizedContainer}>
                <Alert severity="success">{t('wallet.accountInfo.loadedWithSuccess')}</Alert>
            </div>
            <Paper className={classes.card}>
                <Grid container alignItems="stretch" justifyContent="center">
                    <Grid item xs={12} md={5}>
                        <div className={classes.avatarSection}>
                            <AddressAvatar address={accountInformation.pkh} size="large" />
                            <Divider className={classes.divider} />
                            <Chip label={accountInformation.network} size="small" variant="outlined" color="primary" />
                        </div>
                    </Grid>
                    <Grid
                        item
                        xs={12}
                        md={7}
                        className={appendClasses(classes.centralizedContainer, isMobile ? '' : classes.infoSection)}
                    >
                        <Tooltip title={t('common.copy') as string} aria-label="copy" placement="top">
                            <Chip
                                className={appendClasses(classes.alignSelf, classes.chip)}
                                label={isMobile ? shortenText(accountInformation.pkh, 20) : accountInformation.pkh}
                                color="primary"
                                clickable
                                onClick={copyAddress}
                            />
                        </Tooltip>
                        <Divider className={classes.divider} />
                        <Chip
                            className={appendClasses(classes.alignSelf, classes.chip)}
                            label={convertUnitWithSymbol(
                                accountInformation.balance || 0,
                                AmountUnit.uTez,
                                AmountUnit.tez,
                            )}
                        />
                        {ActivationOrRevealButtons}
                    </Grid>
                </Grid>
            </Paper>
        </div>
    ) : null;
};

export default AccountInfo;
