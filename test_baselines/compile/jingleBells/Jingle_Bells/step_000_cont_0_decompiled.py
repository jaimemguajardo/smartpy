import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(current = sp.TString, played = sp.TInt, rules = sp.TList(sp.TString), verse = sp.TInt).layout((("current", "played"), ("rules", "verse"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TInt)
    sp.failwith('[Error: to_cmd: let [_; _; _; r160] =
          loop [ Gt(Compare(__parameter, 0))
               ; 0
               ; __parameter
               ; __parameter
               ; Pair
                   ( Pair(__storage.current, __storage.played)
                   , Pair(__storage.rules, __storage.verse)
                   )
               ]
          step s15; s16; s17; s18 ->
            let [s54; s55; s56; s57] =
              match Eq(Compare((Getn 4)(s18), 16)) with
              | True _ ->
                  [ s15
                  ; s16
                  ; s17
                  ; Pair
                      ( Pair("", Add(1, Cdr(Car(s18))))
                      , Pair(Car(Cdr(s18)), 0)
                      )
                  ]
              | False _ -> [ s15; s16; s17; s18 ]
              end
            let [s87; s88; s89; s90] =
              match Neq(Compare(Car(Car(s57)), "")) with
              | True _ ->
                  [ s54
                  ; s55
                  ; s56
                  ; Pair
                      ( Pair(Concat2(Car(Car(s57)), "\n"), Cdr(Car(s57)))
                      , Cdr(s57)
                      )
                  ]
              | False _ -> [ s54; s55; s56; s57 ]
              end
            match Get
                    ( (Getn 4)(s90)
                    , [0: "Dashing through the snow"; 1: "In a one-horse open sleigh"; 2: "O'er the fields we go"; 3: "Laughing all the way"; 4: "Bells on bob tail ring"; 5: "Making spirits bright"; 6: "What fun it is to ride and sing"; 7: "A sleighing song tonight!"; 8: "Jingle bells, jingle bells,"; 9: "Jingle all the way."; 10: "Oh! what fun it is to ride"; 11: "In a one-horse open sleigh."; 12: "Jingle bells, jingle bells,"; 13: "Jingle all the way;"; 14: "Oh! what fun it is to ride"; 15: "In a one-horse open sleigh."]#map(int,string)
                    ) with
            | Some s116 ->
                let x149 = Add(1, s87)
                [ Gt(Compare(s88, x149))
                ; x149
                ; s88
                ; s89
                ; Pair
                    ( Pair(Concat2(Car(Car(s90)), s116), Cdr(Car(s90)))
                    , Pair(Car(Cdr(s90)), Add(1, Cdr(Cdr(s90))))
                    )
                ]
            | None _ -> Failwith(48)
            end
        Pair(Nil<operation>, r160)]')

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
