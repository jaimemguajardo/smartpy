import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TOption(sp.TPair(sp.TPair(sp.TPair(sp.TList(sp.TInt), sp.TList(sp.TInt)), sp.TPair(sp.TList(sp.TPair(sp.TString, sp.TPair(sp.TString, sp.TBool))), sp.TPair(sp.TList(sp.TPair(sp.TString, sp.TPair(sp.TString, sp.TBool))), sp.TList(sp.TString)))), sp.TPair(sp.TPair(sp.TList(sp.TString), sp.TList(sp.TPair(sp.TString, sp.TBool))), sp.TPair(sp.TList(sp.TPair(sp.TString, sp.TBool)), sp.TPair(sp.TList(sp.TInt), sp.TList(sp.TInt)))))), b = sp.TInt, c = sp.TString, d = sp.TInt, e = sp.TString, f = sp.TList(sp.TInt), g = sp.TList(sp.TInt), head = sp.TString, tail = sp.TList(sp.TString)).layout(((("a", "b"), ("c", "d")), (("e", "f"), ("g", ("head", "tail"))))))

  @sp.entry_point
  def test(self, params):
    sp.set_type(params, sp.TPair(sp.TList(sp.TInt), sp.TPair(sp.TMap(sp.TString, sp.TPair(sp.TString, sp.TBool)), sp.TSet(sp.TInt))))
    sp.failwith('[Error: to_cmd: let [r181; r182; r183] =
          iter [ (Getn 4)(l3)
               ; Nil<int>
               ; Pair
                   ( Pair(__storage.e, __storage.f)
                   , Pair(__storage.g, Pair(__storage.head, __storage.tail))
                   )
               ; l3
               ]
          step s176; s177; s178; s179 ->
            [ Cons(s176, s177); s178; s179 ]
        let [r197; r198; r199; r200] =
          iter [ (Getn 4)(r183); Nil<int>; r181; r182; r183 ]
          step s191; s192; s193; s194; s195 ->
            [ Cons(s191, s192); s193; s194; s195 ]
        let [r210; r211; r212; r213] =
          iter [ r197; Nil<int>; r198; r199; r200 ]
          step s204; s205; s206; s207; s208 ->
            [ Cons(s204, s205); s206; s207; s208 ]
        let [r229; r230; r231; r232] =
          iter [ (Getn 3)(r213)
               ; Nil<{ string ; < True : unit | False : unit > }>
               ; Pair<s,sr>(r210, r211)
               ; r212
               ; r213
               ]
          step s222; s223; s224; s225; s226 ->
            [ Cons(Cdr(s222), s223); s224; s225; s226 ]
        let [r248; r249; r250; r251] =
          iter [ (Getn 3)(r232)
               ; Nil<{ string ; < True : unit | False : unit > }>
               ; Pair<mvr,_>(r229, r230)
               ; r231
               ; r232
               ]
          step s241; s242; s243; s244; s245 ->
            [ Cons(Cdr(s241), s242); s243; s244; s245 ]
        let [r261; r262; r263; r264] =
          iter [ r248
               ; Nil<{ string ; < True : unit | False : unit > }>
               ; r249
               ; r250
               ; r251
               ]
          step s255; s256; s257; s258; s259 ->
            [ Cons(s255, s256); s257; s258; s259 ]
        let [r281; r282; r283; r284; r285] =
          iter [ (Getn 3)(r264); Nil<string>; r261; r262; r263; r264 ]
          step s273; s274; s275; s276; s277; s278 ->
            [ Cons(Car(s273), s274); s275; s276; s277; s278 ]
        let [r302; r303; r304; r305] =
          iter [ (Getn 3)(r285)
               ; Nil<string>
               ; Pair(Pair<mkr,mv>(r281, r282), r283)
               ; r284
               ; r285
               ]
          step s295; s296; s297; s298; s299 ->
            [ Cons(Car(s295), s296); s297; s298; s299 ]
        let [r315; r316; r317; r318] =
          iter [ r302; Nil<string>; r303; r304; r305 ]
          step s309; s310; s311; s312; s313 ->
            [ Cons(s309, s310); s311; s312; s313 ]
        let [r334; r335; r336; r337; r338] =
          iter [ (Getn 3)(r318)
               ; Nil<{ key : string ; value : { string ; < True : unit | False : unit > } }>
               ; r315
               ; r316
               ; r317
               ; r318
               ]
          step s327; s328; s329; s330; s331; s332 ->
            [ Cons(s327, s328); s329; s330; s331; s332 ]
        let [r355; r356; r357; r358; r359] =
          iter [ (Getn 3)(r338)
               ; Nil<{ key : string ; value : { string ; < True : unit | False : unit > } }>
               ; Pair<mir,mk>(r334, r335)
               ; r336
               ; r337
               ; r338
               ]
          step s348; s349; s350; s351; s352; s353 ->
            [ Cons(s348, s349); s350; s351; s352; s353 ]
        let [r370; r371; r372; r373; r374] =
          iter [ r355
               ; Nil<{ key : string ; value : { string ; < True : unit | False : unit > } }>
               ; r356
               ; r357
               ; r358
               ; r359
               ]
          step s363; s364; s365; s366; s367; s368 ->
            [ Cons(s363, s364); s365; s366; s367; s368 ]
        let [r392; r393; r394; r395; r396] =
          iter [ Car(r374)
               ; Nil<int>
               ; Pair<mi,_>(r370, r371)
               ; r372
               ; r373
               ; r374
               ]
          step s385; s386; s387; s388; s389; s390 ->
            [ Cons(s385, s386); s387; s388; s389; s390 ]
        let [r421; r422; r423; r424] =
          iter [ Car(r396)
               ; 0
               ; Some_(Pair(Pair(Pair<l,lr>(Car(r396), r392), r393), r394))
               ; r395
               ; r396
               ]
          step s415; s416; s417; s418; s419 ->
            [ Add(s415, s416); s417; s418; s419 ]
        let [r442; r443; r444; r445] =
          iter [ (Getn 3)(r424); Nil<string>; Pair(r422, r421); r423; r424 ]
          step s435; s436; s437; s438; s439 ->
            [ Cons(Car(s435), s436); s437; s438; s439 ]
        let [r455; r456; r457; r458] =
          iter [ r442; Nil<string>; r443; r444; r445 ]
          step s449; s450; s451; s452; s453 ->
            [ Cons(s449, s450); s451; s452; s453 ]
        let [r478; r479; r480; r481; r482; r483] =
          iter [ (Getn 4)(r458)
               ; Nil<int>
               ; 0
               ; Concat1(r455)
               ; r456
               ; r457
               ; r458
               ]
          step s470; s471; s472; s473; s474; s475; s476 ->
            [ Cons(s470, s471); s472; s473; s474; s475; s476 ]
        let [r491; r492; r493; r494; r495] =
          iter [ r478; r479; r480; r481; r482; r483 ]
          step s484; s485; s486; s487; s488; s489 ->
            [ Add(s484, s485); s486; s487; s488; s489 ]
        let [_; r557] =
          iter [ (Getn 3)(r495)
               ; r495
               ; Pair
                   ( Pair(r493, Pair(r492, r491))
                   , Pair(Pair("", Cdr(Car(r494))), Cdr(r494))
                   )
               ]
          step s518; s519; s520 ->
            let [s554; s555] =
              match Cdr(Cdr(s518)) with
              | True _ ->
                  [ s519
                  ; Pair
                      ( Car(s520)
                      , Pair
                          ( Pair
                              ( Concat2(Car(Car(Cdr(s520))), Car(Cdr(s518)))
                              , Cdr(Car(Cdr(s520)))
                              )
                          , Cdr(Cdr(s520))
                          )
                      )
                  ]
              | False _ -> [ s519; s520 ]
              end
            [ s554; s555 ]
        let [_; r621] =
          loop [ True; 0; r557 ]
          step s561; s563 ->
            let x614 = Add(1, s561)
            [ Gt(Compare(5, x614))
            ; x614
            ; Pair
                ( Car(s563)
                , Pair
                    ( Pair
                        ( Car(Car(Cdr(s563)))
                        , Cons(Mul(s561, s561), Cdr(Car(Cdr(s563))))
                        )
                    , Cdr(Cdr(s563))
                    )
                )
            ]
        let [_; r659; r660; r661; r662] =
          loop [ True
               ; 1
               ; Nil<int>
               ; Cdr(Cdr(Cdr(r621)))
               ; Car(Cdr(r621))
               ; Car(r621)
               ]
          step s636; s637; s638; _; _ ->
            let x653 = Add(1, s636)
            [ Gt(Compare(12, x653))
            ; x653
            ; Cons(s636, s637)
            ; s638
            ; Car(Cdr(r621))
            ; Car(r621)
            ]
        Pair
          ( Nil<operation>
          , record_of_tree(..., let [r676; r677; r678; r679] =
                                  iter [ r659; Nil<int>; r660; r661; r662 ]
                                  step s670; s671; s672; s673; s674 ->
                                    [ Cons(s670, s671); s672; s673; s674 ]
                                Pair(r679, Pair(r678, Pair(r676, r677))))
          )]')

  @sp.entry_point
  def test_match(self, params):
    sp.set_type(params, sp.TList(sp.TString))
    sp.failwith('[Error: to_cmd: record_of_tree(..., let [s691] =
                              if l5 is x106 :: xs107
                              then
                                [ Pair
                                    ( Pair
                                        ( Pair(__storage.a, __storage.b)
                                        , Pair(__storage.c, __storage.d)
                                        )
                                    , Pair
                                        ( Pair(__storage.e, __storage.f)
                                        , Pair(__storage.g, Pair(x106, xs107))
                                        )
                                    )
                                ]
                              else
                                [ Pair
                                    ( Pair
                                        ( Pair(__storage.a, __storage.b)
                                        , Pair(__storage.c, __storage.d)
                                        )
                                    , Pair
                                        ( Pair(__storage.e, __storage.f)
                                        , Pair
                                            ( __storage.g
                                            , Pair("abc", __storage.tail)
                                            )
                                        )
                                    )
                                ]
                            s691)]')

  @sp.entry_point
  def test_match2(self, params):
    sp.set_type(params, sp.TList(sp.TString))
    sp.failwith('[Error: to_cmd: record_of_tree(..., let [s690] =
                              if r6 is x9 :: xs10
                              then
                                if xs10 is x43 :: xs44
                                then
                                  [ Pair
                                      ( Pair
                                          ( Pair(__storage.a, __storage.b)
                                          , Pair(__storage.c, __storage.d)
                                          )
                                      , Pair
                                          ( Pair(__storage.e, __storage.f)
                                          , Pair
                                              ( __storage.g
                                              , Pair(Concat2(x9, x43), xs44)
                                              )
                                          )
                                      )
                                  ]
                                else
                                  [ Pair
                                      ( Pair
                                          ( Pair(__storage.a, __storage.b)
                                          , Pair(__storage.c, __storage.d)
                                          )
                                      , Pair
                                          ( Pair(__storage.e, __storage.f)
                                          , Pair
                                              ( __storage.g
                                              , Pair
                                                  ( __storage.head
                                                  , __storage.tail
                                                  )
                                              )
                                          )
                                      )
                                  ]
                              else
                                [ Pair
                                    ( Pair
                                        ( Pair(__storage.a, __storage.b)
                                        , Pair(__storage.c, __storage.d)
                                        )
                                    , Pair
                                        ( Pair(__storage.e, __storage.f)
                                        , Pair
                                            ( __storage.g
                                            , Pair("abc", __storage.tail)
                                            )
                                        )
                                    )
                                ]
                            s690)]')

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
