import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(x = 2, y = 'aaa', z = 0)

  @sp.entry_point
  def f(self, params):
    y6 = sp.local("y6", self.a(sp.record(in_param = 5, in_storage = self.data)))
    self.data = y6.value.storage
    sp.for op in y6.value.operations:
      sp.operations().push(op)
    y7 = sp.local("y7", self.a(sp.record(in_param = 10, in_storage = self.data)))
    self.data = y7.value.storage
    sp.for op in y7.value.operations:
      sp.operations().push(op)
    self.data.z = y6.value.result + y7.value.result

  @sp.entry_point
  def g(self, params):
    y8 = sp.local("y8", self.a(sp.record(in_param = 6, in_storage = self.data)))
    self.data = y8.value.storage
    sp.for op in y8.value.operations:
      sp.operations().push(op)
    self.data.z = y8.value.result