import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TInt, y = sp.TString, z = sp.TInt).layout(("x", ("y", "z"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right")))
    sp.failwith('[Error: to_cmd: (fun (x1 : { in_param : int ; in_storage : { x : int ; (y : string ; z : int) } }) : { operations : list(operation) ; (result : int ; storage : { x : int ; (y : string ; z : int) }) } ->
let x23 = Pair(Add(1, Car(Cdr(x1))), Cdr(Cdr(x1)))
Pair<operations,_>
  ( Cons(Set_delegate(None<key_hash>), Nil<operation>)
  , Pair<result,storage>(Mul(Car(x1), Car(x23)), x23)
  ))]')

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
