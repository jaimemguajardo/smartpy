import smartpy as sp


class Neg__BLS12_381_fr(sp.Contract):
    def __init__(self, **a):
        self.init_type(sp.TRecord(fr= sp.TOption(sp.TBls12_381_fr)))
        self.init(fr = sp.none)

    """
    NEG: Negate a curve point or field element.

    :: bls12_381_fr : 'S -> bls12_381_fr : 'S
    """
    @sp.entry_point
    def negate(self, params):
        sp.set_type(params.fr, sp.TBls12_381_fr);
        self.data.fr = sp.some(- params.fr)

@sp.add_test(name = "Neg__BLS12_381_fr")
def test():
    c1 = Neg__BLS12_381_fr();

    scenario = sp.test_scenario()
    scenario += c1

    scenario += c1.negate(fr = sp.bls12_381_fr("0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221"));
