import smartpy as sp

class Tester(sp.Contract):
    def __init__(self, f):
        self.f = sp.inline_result(f.f)
        self.init(x = sp.none)

    @sp.entry_point
    def test(self, params):
        self.data.x = sp.some(self.f(params))

    @sp.entry_point
    def check(self, params, result):
        sp.verify(self.f(params) == result)

class MyContract(sp.Contract):
    def __init__(self, **kargs):
        self.init(x = 12, y = 0)

    @sp.global_lambda
    def f(x):
        sp.result(x * 2)

@sp.add_test(name = "Bind")
def test():
    scenario = sp.test_scenario()
    c1 = MyContract()
    scenario += c1
    tester = Tester(c1.f)
    scenario += tester
    scenario += tester.test(12)
    scenario.verify(tester.data.x.open_some() == 24)
    scenario += tester.check(params = 12, result = 24)
    scenario += tester.check(params = 11, result = 22)
