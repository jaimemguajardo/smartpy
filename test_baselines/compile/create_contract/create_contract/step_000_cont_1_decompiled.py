import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TOption(sp.TAddress))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TUnit, Right = sp.TList(sp.TInt)).layout(("Left", "Right"))).layout(("Left", "Right")))
    with params.match_cases() as arg:
      with arg.match('Left') as l3:
        with l3.match_cases() as arg:
          with arg.match('Left') as l108:
            sp.failwith('[Error: to_cmd: let [] = (Drop 2)([ l108; __storage ])
        let [x202; x203] =
          create_contract(parameter :  { x : int ; y : nat }
                          storage   :  { a : int ; b : nat }
                          ps204 ->
                          Pair
                            ( Nil<operation>
                            , Pair
                                ( Add(Car(Car(ps204)), Car(Cdr(ps204)))
                                , Add(Cdr(Car(ps204)), Cdr(Cdr(ps204)))
                                )
                            ), None<key_hash>, 0#mutez, Pair<a,b>(12, 15#nat))
[ Cons(x202, Nil<operation>); Some_(x203) ]]')
          with arg.match('Right') as _x10:
            sp.failwith('[Error: to_cmd: create_contract(parameter :  { x : int ; y : nat }
                        storage   :  { a : int ; b : nat }
                        ps115 ->
                        Pair
                          ( Nil<operation>
                          , Pair
                              ( Add(Car(Car(ps115)), Car(Cdr(ps115)))
                              , Add(Cdr(Car(ps115)), Cdr(Cdr(ps115)))
                              )
                          ), None<key_hash>, 2000000#mutez, Pair<a,b>(12, 15#nat))]')

      with arg.match('Right') as r4:
        with r4.value.match_cases() as arg:
          with arg.match('Left') as l5:
            sp.failwith('[Error: to_cmd: let [] = (Drop 2)([ l5; __storage ])
        let [x65; x66] =
          create_contract(parameter :  { x : int ; y : nat }
                          storage   :  { a : int ; b : nat }
                          ps67 ->
                          Pair
                            ( Nil<operation>
                            , Pair
                                ( Add(Car(Car(ps67)), Car(Cdr(ps67)))
                                , Add(Cdr(Car(ps67)), Cdr(Cdr(ps67)))
                                )
                            ), None<key_hash>, 0#mutez, Pair<a,b>(12, 15#nat))
[ Cons(x65, Nil<operation>); Some_(x66) ]]')
          with arg.match('Right') as r6:
            sp.failwith('[Error: to_cmd: iter [ r6; Nil<operation>; __storage ]
        step s10; s11; s12 ->
          let [x21; _] =
            create_contract(parameter :  { x : int ; y : nat }
                            storage   :  { a : int ; b : nat }
                            ps23 ->
                            Pair
                              ( Nil<operation>
                              , Pair
                                  ( Add(Car(Car(ps23)), Car(Cdr(ps23)))
                                  , Add(Cdr(Car(ps23)), Cdr(Cdr(ps23)))
                                  )
                              ), None<key_hash>, 0#mutez, Pair<a,b>(s10, 15#nat))
[ Cons(x21, s11); s12 ]]')



@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
