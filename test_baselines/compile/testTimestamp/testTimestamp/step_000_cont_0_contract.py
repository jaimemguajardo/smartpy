import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(next = sp.timestamp(0), out = False)

  @sp.entry_point
  def ep(self, params):
    self.data.out = sp.now > sp.add_seconds(sp.now, 1)
    sp.verify((sp.add_seconds(sp.now, 12) - sp.now) == 12)
    sp.verify((sp.now - sp.add_seconds(sp.now, 12)) == -12)
    sp.verify((sp.now - sp.add_seconds(sp.now, 12)) == -12)
    self.data.next = sp.add_seconds(sp.now, 86400)