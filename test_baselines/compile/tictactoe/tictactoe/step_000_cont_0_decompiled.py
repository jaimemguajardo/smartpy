import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), draw = sp.TBool, nbMoves = sp.TInt, nextPlayer = sp.TInt, winner = sp.TInt).layout((("deck", "draw"), ("nbMoves", ("nextPlayer", "winner")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TInt)))
    sp.failwith('[Error: to_cmd: let [s22; s23; s24] =
          match Eq(Compare
                     ( (Getn 6)(Pair
                                  ( Pair(__storage.deck, __storage.draw)
                                  , Pair
                                      ( __storage.nbMoves
                                      , Pair
                                          ( __storage.nextPlayer
                                          , __storage.winner
                                          )
                                      )
                                  ))
                     , 0
                     )) with
          | True _ ->
              [ Not(__storage.draw)
              ; __parameter
              ; Pair
                  ( Pair(__storage.deck, __storage.draw)
                  , Pair
                      ( __storage.nbMoves
                      , Pair(__storage.nextPlayer, __storage.winner)
                      )
                  )
              ]
          | False _ ->
              [ False
              ; __parameter
              ; Pair
                  ( Pair(__storage.deck, __storage.draw)
                  , Pair
                      ( __storage.nbMoves
                      , Pair(__storage.nextPlayer, __storage.winner)
                      )
                  )
              ]
          end
        match s22 with
        | True _ ->
            let [s43; s44; s45] =
              match Ge(Compare(Car(s23), 0)) with
              | True _ -> [ Gt(Compare(3, Car(s23))); s23; s24 ]
              | False _ -> [ False; s23; s24 ]
              end
            match s43 with
            | True _ ->
                let [s64; s65; s66] =
                  match Ge(Compare((Getn 3)(s44), 0)) with
                  | True _ -> [ Gt(Compare(3, (Getn 3)(s44))); s44; s45 ]
                  | False _ -> [ False; s44; s45 ]
                  end
                match s64 with
                | True _ ->
                    match Eq(Compare((Getn 4)(s65), (Getn 5)(s66))) with
                    | True _ ->
                        match Get(Car(s65), Car(Car(s66))) with
                        | Some s104 ->
                            match Get((Getn 3)(s65), s104) with
                            | Some s116 ->
                                match Eq(Compare(s116, 0)) with
                                | True _ ->
                                    match Get(Car(s65), Car(Car(s66))) with
                                    | Some s148 ->
                                        let x188 =
                                          Pair
                                            ( Add(1, Car(Cdr(s66)))
                                            , Cdr(Cdr(s66))
                                            )
                                        let x187 =
                                          Pair
                                            ( Update(Car(s65), Some_(Update((
                                          Getn 3)(s65), Some_((Getn 4)(s65)), s148)), Car(Car(s66)))
                                            , Cdr(Car(s66))
                                            )
                                        let x220 =
                                          Pair
                                            ( x187
                                            , Pair
                                                ( Car(x188)
                                                , Pair
                                                    ( Sub
                                                        ( 3
                                                        , (Getn 5)(Pair
                                                                    ( x187
                                                                    , x188
                                                                    ))
                                                        )
                                                    , Cdr(Cdr(x188))
                                                    )
                                                )
                                            )
                                        match Get(Car(s65), Car(Car(x220))) with
                                        | Some s234 ->
                                            match Get(0, s234) with
                                            | Some s242 ->
                                                let [s309; s310; s311] =
                                                  match Neq(Compare(s242, 0)) with
                                                  | True _ ->
                                                      match Get
                                                              ( Car(s65)
                                                              , Car(Car(x220))
                                                              ) with
                                                      | Some s269 ->
                                                          match Get(1, s269) with
                                                          | Some s276 ->
                                                              match Get
                                                                    ( Car(s65)
                                                                    , Car(Car(x220))
                                                                    ) with
                                                              | Some s293 ->
                                                                  match 
                                                                  Get
                                                                    ( 0
                                                                    , s293
                                                                    ) with
                                                                  | Some s301 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s301
                                                                    , s276
                                                                    ))
                                                                    ; s65
                                                                    ; x220
                                                                    ]
                                                                  | None _ ->
                                                                    Failwith(31)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(23)
                                                              end
                                                          | None _ ->
                                                              Failwith(31)
                                                          end
                                                      | None _ ->
                                                          Failwith(23)
                                                      end
                                                  | False _ ->
                                                      [ False; s65; x220 ]
                                                  end
                                                let [s371; s372; s373] =
                                                  match s309 with
                                                  | True _ ->
                                                      match Get
                                                              ( Car(s310)
                                                              , Car(Car(s311))
                                                              ) with
                                                      | Some s331 ->
                                                          match Get(2, s331) with
                                                          | Some s338 ->
                                                              match Get
                                                                    ( Car(s310)
                                                                    , Car(Car(s311))
                                                                    ) with
                                                              | Some s355 ->
                                                                  match 
                                                                  Get
                                                                    ( 0
                                                                    , s355
                                                                    ) with
                                                                  | Some s363 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s363
                                                                    , s338
                                                                    ))
                                                                    ; s310
                                                                    ; s311
                                                                    ]
                                                                  | None _ ->
                                                                    Failwith(31)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(23)
                                                              end
                                                          | None _ ->
                                                              Failwith(31)
                                                          end
                                                      | None _ ->
                                                          Failwith(23)
                                                      end
                                                  | False _ ->
                                                      [ False; s310; s311 ]
                                                  end
                                                let [s432; s433] =
                                                  match s371 with
                                                  | True _ ->
                                                      match Get
                                                              ( Car(s372)
                                                              , Car(Car(s373))
                                                              ) with
                                                      | Some s405 ->
                                                          match Get(0, s405) with
                                                          | Some s414 ->
                                                              [ s372
                                                              ; Pair
                                                                  ( Car(s373)
                                                                  , Pair
                                                                    ( Car(Cdr(s373))
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(Cdr(s373)))
                                                                    , s414
                                                                    )
                                                                    )
                                                                  )
                                                              ]
                                                          | None _ ->
                                                              Failwith(32)
                                                          end
                                                      | None _ ->
                                                          Failwith(23)
                                                      end
                                                  | False _ -> [ s372; s373 ]
                                                  end
                                                match Get(0, Car(Car(s433))) with
                                                | Some s443 ->
                                                    match Get
                                                            ( (Getn 3)(s432)
                                                            , s443
                                                            ) with
                                                    | Some s455 ->
                                                        let [s522; s523; s524
                                                              ] =
                                                          match Neq(Compare
                                                                    ( s455
                                                                    , 0
                                                                    )) with
                                                          | True _ ->
                                                              match Get
                                                                    ( 1
                                                                    , Car(Car(s433))
                                                                    ) with
                                                              | Some s475 ->
                                                                  match 
                                                                  Get
                                                                    ( (
                                                                  Getn 3)(s432)
                                                                    , s475
                                                                    ) with
                                                                  | Some s489 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s433))
                                                                    ) with
                                                                    | Some s502 ->
                                                                    match 
                                                                    Get
                                                                    ( (
                                                                    Getn 3)(s432)
                                                                    , s502
                                                                    ) with
                                                                    | Some s514 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s514
                                                                    , s489
                                                                    ))
                                                                    ; s432
                                                                    ; s433
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(24)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(24)
                                                                    end
                                                                  | None _ ->
                                                                    Failwith(24)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(24)
                                                              end
                                                          | False _ ->
                                                              [ False
                                                              ; s432
                                                              ; s433
                                                              ]
                                                          end
                                                        let [s584; s585; s586
                                                              ] =
                                                          match s522 with
                                                          | True _ ->
                                                              match Get
                                                                    ( 2
                                                                    , Car(Car(s524))
                                                                    ) with
                                                              | Some s537 ->
                                                                  match 
                                                                  Get
                                                                    ( (
                                                                  Getn 3)(s523)
                                                                    , s537
                                                                    ) with
                                                                  | Some s551 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s524))
                                                                    ) with
                                                                    | Some s564 ->
                                                                    match 
                                                                    Get
                                                                    ( (
                                                                    Getn 3)(s523)
                                                                    , s564
                                                                    ) with
                                                                    | Some s576 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s576
                                                                    , s551
                                                                    ))
                                                                    ; s523
                                                                    ; s524
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(24)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(24)
                                                                    end
                                                                  | None _ ->
                                                                    Failwith(24)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(24)
                                                              end
                                                          | False _ ->
                                                              [ False
                                                              ; s523
                                                              ; s524
                                                              ]
                                                          end
                                                        let s642 =
                                                          match s584 with
                                                          | True _ ->
                                                              match Get
                                                                    ( 0
                                                                    , Car(Car(s586))
                                                                    ) with
                                                              | Some s613 ->
                                                                  match 
                                                                  Get
                                                                    ( (
                                                                  Getn 3)(s585)
                                                                    , s613
                                                                    ) with
                                                                  | Some s627 ->
                                                                    Pair
                                                                    ( Car(s586)
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(s586))
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(Cdr(s586)))
                                                                    , s627
                                                                    )
                                                                    )
                                                                    )
                                                                  | None _ ->
                                                                    Failwith(24)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(24)
                                                              end
                                                          | False _ -> s586
                                                          end
                                                        match Get
                                                                ( 0
                                                                , Car(Car(s642))
                                                                ) with
                                                        | Some s652 ->
                                                            match Get(0, s652) with
                                                            | Some s659 ->
                                                                let [s708;
                                                                    s709] =
                                                                  match Neq(
                                                                  Compare
                                                                    ( s659
                                                                    , 0
                                                                    )) with
                                                                  | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , Car(Car(s642))
                                                                    ) with
                                                                    | Some s673 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , s673
                                                                    ) with
                                                                    | Some s679 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s642))
                                                                    ) with
                                                                    | Some s694 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s694
                                                                    ) with
                                                                    | Some s701 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s701
                                                                    , s679
                                                                    ))
                                                                    ; s642
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                  | False _ ->
                                                                    [ False
                                                                    ; s642
                                                                    ]
                                                                  end
                                                                let [s752;
                                                                    s753] =
                                                                  match s708 with
                                                                  | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , Car(Car(s709))
                                                                    ) with
                                                                    | Some s717 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s717
                                                                    ) with
                                                                    | Some s723 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s709))
                                                                    ) with
                                                                    | Some s738 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s738
                                                                    ) with
                                                                    | Some s745 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s745
                                                                    , s723
                                                                    ))
                                                                    ; s709
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                  | False _ ->
                                                                    [ False
                                                                    ; s709
                                                                    ]
                                                                  end
                                                                let s796 =
                                                                  match s752 with
                                                                  | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s753))
                                                                    ) with
                                                                    | Some s773 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s773
                                                                    ) with
                                                                    | Some s781 ->
                                                                    Pair
                                                                    ( Car(s753)
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(s753))
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(Cdr(s753)))
                                                                    , s781
                                                                    )
                                                                    )
                                                                    )
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                  | False _ ->
                                                                    s753
                                                                  end
                                                                match 
                                                                Get
                                                                  ( 0
                                                                  , Car(Car(s796))
                                                                  ) with
                                                                | Some s806 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s806
                                                                    ) with
                                                                    | Some s813 ->
                                                                    let 
                                                                    [s862;
                                                                    s863] =
                                                                    match Neq(
                                                                    Compare
                                                                    ( s813
                                                                    , 0
                                                                    )) with
                                                                    | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , Car(Car(s796))
                                                                    ) with
                                                                    | Some s827 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , s827
                                                                    ) with
                                                                    | Some s833 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s796))
                                                                    ) with
                                                                    | Some s848 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s848
                                                                    ) with
                                                                    | Some s855 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s855
                                                                    , s833
                                                                    ))
                                                                    ; s796
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | False _ ->
                                                                    [ False
                                                                    ; s796
                                                                    ]
                                                                    end
                                                                    let 
                                                                    [s906;
                                                                    s907] =
                                                                    match s862 with
                                                                    | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , Car(Car(s863))
                                                                    ) with
                                                                    | Some s871 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s871
                                                                    ) with
                                                                    | Some s877 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s863))
                                                                    ) with
                                                                    | Some s892 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s892
                                                                    ) with
                                                                    | Some s899 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s899
                                                                    , s877
                                                                    ))
                                                                    ; s863
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | False _ ->
                                                                    [ False
                                                                    ; s863
                                                                    ]
                                                                    end
                                                                    let s950 =
                                                                    match s906 with
                                                                    | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s907))
                                                                    ) with
                                                                    | Some s927 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s927
                                                                    ) with
                                                                    | Some s935 ->
                                                                    Pair
                                                                    ( Car(s907)
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(s907))
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(Cdr(s907)))
                                                                    , s935
                                                                    )
                                                                    )
                                                                    )
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | False _ ->
                                                                    s907
                                                                    end
                                                                    let 
                                                                    [s964;
                                                                    s965] =
                                                                    match Eq(
                                                                    Compare
                                                                    ( 9
                                                                    , (
                                                                    Getn 3)(s950)
                                                                    )) with
                                                                    | True _ ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( 0
                                                                    , (
                                                                    Getn 6)(s950)
                                                                    ))
                                                                    ; s950
                                                                    ]
                                                                    | False _ ->
                                                                    [ False
                                                                    ; s950
                                                                    ]
                                                                    end
                                                                    match s964 with
                                                                    | True _ ->
                                                                    Pair
                                                                    ( Nil<operation>
                                                                    , 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s965))
                                                                    , True
                                                                    )
                                                                    , Cdr(s965)
                                                                    )
                                                                    )
                                                                    | False _ ->
                                                                    Pair
                                                                    ( Nil<operation>
                                                                    , s965
                                                                    )
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                | None _ ->
                                                                    Failwith(26)
                                                                end
                                                            | None _ ->
                                                                Failwith(25)
                                                            end
                                                        | None _ ->
                                                            Failwith(25)
                                                        end
                                                    | None _ -> Failwith(24)
                                                    end
                                                | None _ -> Failwith(24)
                                                end
                                            | None _ -> Failwith(31)
                                            end
                                        | None _ -> Failwith(23)
                                        end
                                    | None _ -> Failwith(20)
                                    end
                                | False _ ->
                                    Failwith("WrongCondition: self.data.deck[params.i][params.j] == 0")
                                end
                            | None _ -> Failwith(19)
                            end
                        | None _ -> Failwith(19)
                        end
                    | False _ ->
                        Failwith("WrongCondition: params.move == self.data.nextPlayer")
                    end
                | False _ ->
                    Failwith("WrongCondition: (params.j >= 0) & (params.j < 3)")
                end
            | False _ ->
                Failwith("WrongCondition: (params.i >= 0) & (params.i < 3)")
            end
        | False _ ->
            Failwith("WrongCondition: (self.data.winner == 0) & (~ self.data.draw)")
        end]')

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
