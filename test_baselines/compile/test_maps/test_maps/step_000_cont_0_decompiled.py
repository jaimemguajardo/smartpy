import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(m = sp.TMap(sp.TInt, sp.TString), x = sp.TString, y = sp.TOption(sp.TString), z = sp.TOption(sp.TString)).layout((("m", "x"), ("y", "z"))))

  @sp.entry_point
  def test_get_and_update(self, params):
    sp.set_type(params, sp.TUnit)
    sp.failwith('[Error: to_cmd: record_of_tree(..., let [x153; _] =
                              Get_and_update(1, Some_("one"), __storage.m)
                            Pair
                              ( Pair(__storage.m, __storage.x)
                              , Pair(__storage.y, x153)
                              ))]')

  @sp.entry_point
  def test_map_get(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    with sp.some(params[12]).match_cases() as arg:
      with arg.match('Some') as s133:
        x17 = sp.local("x17", ((self.data.m, s133), (self.data.y, self.data.z)))
        x214 = sp.local("x214", x17.value)
        sp.failwith('[Error: to_cmd: record_of_tree(..., x214)]')
      with arg.match('None') as _x3:
        sp.failwith(12)


  @sp.entry_point
  def test_map_get2(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    with sp.some(params[12]).match_cases() as arg:
      with arg.match('Some') as s111:
        x35 = sp.local("x35", ((self.data.m, s111), (self.data.y, self.data.z)))
        x212 = sp.local("x212", x35.value)
        sp.failwith('[Error: to_cmd: record_of_tree(..., x212)]')
      with arg.match('None') as _x21:
        sp.failwith(17)


  @sp.entry_point
  def test_map_get_default_values(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    sp.failwith('[Error: to_cmd: record_of_tree(..., let x88 = __storage.m
                            let [s94; s95; s96] =
                              match Get(12, r81) with
                              | Some s92 ->
                                  [ s92
                                  ; x88
                                  ; Pair(__storage.y, __storage.z)
                                  ]
                              | None _ ->
                                  [ "abc"
                                  ; x88
                                  ; Pair(__storage.y, __storage.z)
                                  ]
                              end
                            Pair(Pair(s95, s94), s96))]')

  @sp.entry_point
  def test_map_get_missing_value(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    with sp.some(params[12]).match_cases() as arg:
      with arg.match('Some') as s67:
        x55 = sp.local("x55", ((self.data.m, s67), (self.data.y, self.data.z)))
        x208 = sp.local("x208", x55.value)
        sp.failwith('[Error: to_cmd: record_of_tree(..., x208)]')
      with arg.match('None') as _x41:
        sp.failwith('missing 12')


  @sp.entry_point
  def test_map_get_missing_value2(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    with sp.some(params[12]).match_cases() as arg:
      with arg.match('Some') as s48:
        x73 = sp.local("x73", ((self.data.m, s48), (self.data.y, self.data.z)))
        x206 = sp.local("x206", x73.value)
        sp.failwith('[Error: to_cmd: record_of_tree(..., x206)]')
      with arg.match('None') as _x59:
        sp.failwith(1234)


  @sp.entry_point
  def test_map_get_opt(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data = sp.record(m = self.data.m, x = self.data.x, y = sp.some(params[12]), z = self.data.z)

  @sp.entry_point
  def test_update_map(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = sp.record(m = sp.update_map(self.data.m, 1, sp.some('one')), x = self.data.x, y = self.data.y, z = self.data.z)

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
