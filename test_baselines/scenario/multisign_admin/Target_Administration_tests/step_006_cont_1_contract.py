import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(addrVoterId = {sp.address('tz0Fakesigner1') : 0, sp.address('tz0Fakesigner2') : 1}, keyVoterId = {sp.key('edpkFakesigner1') : 0, sp.key('edpkFakesigner2') : 1}, lastVoteTimestamp = sp.timestamp(0), lastVoterId = 1, nbVoters = 2, proposals = {}, quorum = 1, target = sp.address("KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate"), timeout = 5, voters = {0 : sp.record(addr = sp.address('tz0Fakesigner1'), lastProposalId = 0, publicKey = sp.key('edpkFakesigner1')), 1 : sp.record(addr = sp.address('tz0Fakesigner2'), lastProposalId = 0, publicKey = sp.key('edpkFakesigner2'))})

  @sp.entry_point
  def cancelProposal(self, params):
    sp.verify(self.data.addrVoterId.contains(sp.sender), message = 'MultisignAdmin_VoterUnknown')
    sp.verify((self.data.proposals.contains(self.data.addrVoterId[sp.sender])) & (self.data.proposals[self.data.addrVoterId[sp.sender]].id == params), message = 'MultisignAdmin_ProposalUnknown')
    self.data.proposals[self.data.addrVoterId[sp.sender]].canceled = True

  @sp.entry_point
  def multiVote(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(initiatorId = sp.TNat, proposalId = sp.TNat, votes = sp.TList(sp.TRecord(signature = sp.TSignature, voterId = sp.TNat, yay = sp.TBool).layout(("signature", ("voterId", "yay"))))).layout(("initiatorId", ("proposalId", "votes")))))
    sp.for proposalVotes in params:
      sp.for vote in proposalVotes.votes:
        sp.verify(self.data.voters.contains(vote.voterId), message = 'MultisignAdmin_VoterUnknown')
        sp.verify(sp.check_signature(self.data.voters[vote.voterId].publicKey, vote.signature, sp.pack((sp.self_address, (proposalVotes.initiatorId, proposalVotes.proposalId)))), message = 'MultisignAdmin_Badsig')
        y10 = sp.local("y10", self.registerVote(sp.record(in_param = sp.record(initiatorId = proposalVotes.initiatorId, proposalId = proposalVotes.proposalId, voterId = vote.voterId, yay = vote.yay), in_storage = self.data)))
        self.data = y10.value.storage
        sp.for op in y10.value.operations:
          sp.operations().push(op)
      sp.if (sp.len(self.data.proposals[proposalVotes.initiatorId].nay) + sp.len(self.data.proposals[proposalVotes.initiatorId].yay)) >= self.data.quorum:
        sp.if sp.len(self.data.proposals[proposalVotes.initiatorId].nay) >= sp.len(self.data.proposals[proposalVotes.initiatorId].yay):
          self.data.proposals[proposalVotes.initiatorId].canceled = True
        sp.else:
          y11 = sp.local("y11", self.onVoted(sp.record(in_param = self.data.proposals[proposalVotes.initiatorId], in_storage = self.data)))
          self.data = y11.value.storage
          sp.for op in y11.value.operations:
            sp.operations().push(op)

  @sp.entry_point
  def newProposal(self, params):
    sp.verify(self.data.addrVoterId.contains(sp.sender), message = 'MultisignAdmin_VoterUnknown')
    self.data.voters[self.data.addrVoterId[sp.sender]].lastProposalId += 1
    self.data.proposals[self.data.addrVoterId[sp.sender]] = sp.record(batchs = params, canceled = False, id = self.data.voters[self.data.addrVoterId[sp.sender]].lastProposalId, nay = sp.set([]), startedAt = sp.now, yay = sp.set([self.data.addrVoterId[sp.sender]]))
    sp.if self.data.quorum < 2:
      y12 = sp.local("y12", self.onVoted(sp.record(in_param = self.data.proposals[self.data.addrVoterId[sp.sender]], in_storage = self.data)))
      self.data = y12.value.storage
      sp.for op in y12.value.operations:
        sp.operations().push(op)
    sp.else:
      self.data.proposals[self.data.addrVoterId[sp.sender]].yay.add(self.data.addrVoterId[sp.sender])

  @sp.entry_point
  def vote(self, params):
    sp.verify(self.data.addrVoterId.contains(sp.sender), message = 'MultisignAdmin_VoterUnknown')
    sp.for vote in params:
      y13 = sp.local("y13", self.registerVote(sp.record(in_param = sp.record(initiatorId = vote.initiatorId, proposalId = vote.proposalId, voterId = self.data.addrVoterId[sp.sender], yay = vote.yay), in_storage = self.data)))
      self.data = y13.value.storage
      sp.for op in y13.value.operations:
        sp.operations().push(op)
      sp.if (sp.len(self.data.proposals[vote.initiatorId].nay) + sp.len(self.data.proposals[vote.initiatorId].yay)) >= self.data.quorum:
        sp.if sp.len(self.data.proposals[vote.initiatorId].nay) >= sp.len(self.data.proposals[vote.initiatorId].yay):
          self.data.proposals[vote.initiatorId].canceled = True
        sp.else:
          y14 = sp.local("y14", self.onVoted(sp.record(in_param = self.data.proposals[vote.initiatorId], in_storage = self.data)))
          self.data = y14.value.storage
          sp.for op in y14.value.operations:
            sp.operations().push(op)