Comment...
 h1: Self Administration tests
Table Of Contents

 Self Administration tests
# Init contracts
## Administrated
## multisignAdmin
# Auto-accepted proposal when quorum is 1
## signer1 propose to change quorum to 2
# Invalid quorum proposal
## signer1 new proposal to change quorum to 3
## signer2 votes the proposal
# Adding new voters
## signer2 new proposal to include signer3
## signer1 votes the proposal
# Newly included signer starts a proposal
## New proposal by signer 3 to increase quorum to 3
## signer1 votes the proposal
# Proposal cancellation
## New proposal
## Signer 2 tries to cancel the proposal
## Signer 1 cancels the proposal
## Signer 2 tries to vote the canceled proposal
# Proposal rejection
## New proposal
## Signer 2 votes against the proposal
## Signer 3 votes against the proposal
# Invalid Removed signer proposal
## Signer 1 new proposal: remove signer 3
## Signer 2 votes the remove proposal
## Signer 3 tries to vote the remove proposal
# 2 actions proposal
## Signer 1 new proposal: change quorum to 2 and remove signer 3
## Signer 2 votes the proposal
## Signer 3 votes the proposal
# Vote for past proposal
## Signer 1 new proposal: change timeout to 2
## Signer 2 new proposal: add new signer
## Signer 2 tries to vote signer1's proposal after timedout
## Signer 1 votes for signer2's proposal
## Signer 2 tries to vote signer1's proposal while a more recent one was accepted
Comment...
 h2: Init contracts
Comment...
 h3: Administrated
Creating contract
 -> (Pair False (Pair "tz0Fakeadmin" None))
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_004_cont_0_storage.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_004_cont_0_storage.json 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_004_cont_0_sizes.csv 2
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_004_cont_0_storage.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_004_cont_0_types.py 7
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_004_cont_0_contract.tz 67
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_004_cont_0_contract.json 87
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_004_cont_0_contract.py 21
Comment...
 h3: multisignAdmin
Creating contract
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1}) (Pair "1970-01-01T00:00:00Z" (Pair 1 2))) (Pair (Pair {} 1) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 0 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 0 "edpkFakesigner2"))}))))
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_006_cont_1_storage.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_006_cont_1_storage.json 52
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_006_cont_1_sizes.csv 2
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_006_cont_1_storage.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_006_cont_1_types.py 7
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_006_cont_1_contract.tz 1718
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_006_cont_1_contract.json 2500
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_006_cont_1_contract.py 61
Comment...
 h2: Auto-accepted proposal when quorum is 1
Comment...
 h3: signer1 propose to change quorum to 2
Verifying sp.contract_data(1).quorum == 1...
 OK
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_010_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_010_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_010_cont_1_params.json 1
Executing newProposal(sp.list([variant('selfAdmin', sp.list([variant('changeQuorum', 2)]))]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1}) (Pair "1970-01-01T00:00:01Z" (Pair 1 2))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Left (Left 2)}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:01Z" {0})))} 2) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 1 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 0 "edpkFakesigner2"))}))))
Verifying sp.contract_data(1).quorum == 2...
 OK
Comment...
 h2: Invalid quorum proposal
Comment...
 h3: signer1 new proposal to change quorum to 3
Verifying sp.contract_data(1).quorum != 3...
 OK
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_015_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_015_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_015_cont_1_params.json 1
Executing newProposal(sp.list([variant('selfAdmin', sp.list([variant('changeQuorum', 3)]))]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1}) (Pair "1970-01-01T00:00:01Z" (Pair 1 2))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Left (Left 3)}} (Pair False 2)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0})))} 2) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 2 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 0 "edpkFakesigner2"))}))))
Verifying sp.contract_data(1).quorum == 2...
 OK
Comment...
 h3: signer2 votes the proposal
Comment...
 p: proposal is rejected because nbSigners < proposed quorum
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_019_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_019_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_019_cont_1_params.json 1
Executing vote(sp.list([sp.record(initiatorId = sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)], proposalId = sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].id, yay = True)]))...
 -> --- Expected failure in transaction --- WrongCondition in line 262: quorum <= self.data.nbVoters ['MultisignAdmin_Quorum<Voters']
Verifying sp.contract_data(1).quorum != 3...
 OK
Comment...
 h2: Adding new voters
Comment...
 h3: signer2 new proposal to include signer3
Verifying ~ (sp.contract_data(1).voters.contains(2))...
 OK
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_024_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_024_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_024_cont_1_params.json 18
Executing newProposal(sp.list([variant('selfAdmin', sp.list([variant('changeVoters', sp.record(added = sp.list([sp.record(addr = sp.reduce(sp.test_account("signer3").address), publicKey = sp.reduce(sp.test_account("signer3").public_key))]), removed = sp.set([])))]))]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1}) (Pair "1970-01-01T00:00:01Z" (Pair 1 2))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Left (Left 3)}} (Pair False 2)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {1})))} 2) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 2 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2"))}))))
Comment...
 h3: signer1 votes the proposal
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_026_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_026_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_026_cont_1_params.json 1
Executing vote(sp.list([sp.record(initiatorId = sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer2").address)], proposalId = sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer2").address)]].id, yay = True)]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1; Elt "tz0Fakesigner3" 2} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1; Elt "edpkFakesigner3" 2}) (Pair "1970-01-01T00:00:02Z" (Pair 2 3))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Left (Left 3)}} (Pair False 2)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1})))} 2) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 2 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2")); Elt 2 (Pair "tz0Fakesigner3" (Pair 0 "edpkFakesigner3"))}))))
Verifying sp.contract_data(1).voters.contains(2)...
 OK
Comment...
 h2: Newly included signer starts a proposal
Comment...
 h3: New proposal by signer 3 to increase quorum to 3
Verifying sp.contract_data(1).quorum != 3...
 OK
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_031_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_031_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_031_cont_1_params.json 1
Executing newProposal(sp.list([variant('selfAdmin', sp.list([variant('changeQuorum', 3)]))]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1; Elt "tz0Fakesigner3" 2} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1; Elt "edpkFakesigner3" 2}) (Pair "1970-01-01T00:00:02Z" (Pair 2 3))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Left (Left 3)}} (Pair False 2)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1}))); Elt 2 (Pair (Pair {Left {Left (Left 3)}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:03Z" {2})))} 2) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 2 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2")); Elt 2 (Pair "tz0Fakesigner3" (Pair 1 "edpkFakesigner3"))}))))
Comment...
 h3: signer1 votes the proposal
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_033_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_033_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_033_cont_1_params.json 1
Executing vote(sp.list([sp.record(initiatorId = sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer3").address)], proposalId = sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer3").address)]].id, yay = True)]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1; Elt "tz0Fakesigner3" 2} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1; Elt "edpkFakesigner3" 2}) (Pair "1970-01-01T00:00:03Z" (Pair 2 3))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Left (Left 3)}} (Pair False 2)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1}))); Elt 2 (Pair (Pair {Left {Left (Left 3)}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:03Z" {0; 2})))} 3) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 2 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2")); Elt 2 (Pair "tz0Fakesigner3" (Pair 1 "edpkFakesigner3"))}))))
Verifying sp.contract_data(1).quorum == 3...
 OK
Comment...
 h2: Proposal cancellation
Comment...
 h3: New proposal
Verifying sp.contract_data(1).timeout != 10...
 OK
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_038_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_038_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_038_cont_1_params.json 1
Executing newProposal(sp.list([variant('selfAdmin', sp.list([variant('changeTimeout', 10)]))]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1; Elt "tz0Fakesigner3" 2} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1; Elt "edpkFakesigner3" 2}) (Pair "1970-01-01T00:00:03Z" (Pair 2 3))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Right (Left 10)}} (Pair False 3)) (Pair {} (Pair "1970-01-01T00:00:04Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1}))); Elt 2 (Pair (Pair {Left {Left (Left 3)}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:03Z" {0; 2})))} 3) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 3 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2")); Elt 2 (Pair "tz0Fakesigner3" (Pair 1 "edpkFakesigner3"))}))))
Comment...
 h3: Signer 2 tries to cancel the proposal
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_040_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_040_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_040_cont_1_params.json 1
Executing cancelProposal(sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].id)...
 -> --- Expected failure in transaction --- WrongCondition in line 220: (self.data.proposals.contains(self.data.addrVoterId[sp.sender])) & (self.data.proposals[self.data.addrVoterId[sp.sender]].id == params) ['MultisignAdmin_ProposalUnknown']
Comment...
 h3: Signer 1 cancels the proposal
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_042_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_042_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_042_cont_1_params.json 1
Executing cancelProposal(sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].id)...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1; Elt "tz0Fakesigner3" 2} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1; Elt "edpkFakesigner3" 2}) (Pair "1970-01-01T00:00:03Z" (Pair 2 3))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Right (Left 10)}} (Pair True 3)) (Pair {} (Pair "1970-01-01T00:00:04Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1}))); Elt 2 (Pair (Pair {Left {Left (Left 3)}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:03Z" {0; 2})))} 3) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 3 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2")); Elt 2 (Pair "tz0Fakesigner3" (Pair 1 "edpkFakesigner3"))}))))
Comment...
 h3: Signer 2 tries to vote the canceled proposal
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_044_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_044_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_044_cont_1_params.json 1
Executing vote(sp.list([sp.record(initiatorId = sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)], proposalId = sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].id, yay = True)]))...
 -> --- Expected failure in transaction --- WrongCondition in line 234: (sp.now > self.data.lastVoteTimestamp) & (~ self.data.proposals[lparams_1.in_param.initiatorId].canceled) ['MultisignAdmin_ProposalClosed']
Verifying sp.contract_data(1).timeout != 10...
 OK
Comment...
 h2: Proposal rejection
Comment...
 h3: New proposal
Verifying sp.contract_data(1).timeout != 10...
 OK
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_049_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_049_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_049_cont_1_params.json 1
Executing newProposal(sp.list([variant('selfAdmin', sp.list([variant('changeTimeout', 10)]))]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1; Elt "tz0Fakesigner3" 2} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1; Elt "edpkFakesigner3" 2}) (Pair "1970-01-01T00:00:03Z" (Pair 2 3))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Right (Left 10)}} (Pair False 4)) (Pair {} (Pair "1970-01-01T00:00:04Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1}))); Elt 2 (Pair (Pair {Left {Left (Left 3)}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:03Z" {0; 2})))} 3) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 4 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2")); Elt 2 (Pair "tz0Fakesigner3" (Pair 1 "edpkFakesigner3"))}))))
Comment...
 h3: Signer 2 votes against the proposal
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_051_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_051_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_051_cont_1_params.json 1
Executing vote(sp.list([sp.record(initiatorId = sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)], proposalId = sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].id, yay = False)]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1; Elt "tz0Fakesigner3" 2} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1; Elt "edpkFakesigner3" 2}) (Pair "1970-01-01T00:00:03Z" (Pair 2 3))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Right (Left 10)}} (Pair False 4)) (Pair {1} (Pair "1970-01-01T00:00:04Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1}))); Elt 2 (Pair (Pair {Left {Left (Left 3)}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:03Z" {0; 2})))} 3) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 4 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2")); Elt 2 (Pair "tz0Fakesigner3" (Pair 1 "edpkFakesigner3"))}))))
Comment...
 h3: Signer 3 votes against the proposal
Verifying sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].canceled == False...
 OK
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_054_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_054_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_054_cont_1_params.json 1
Executing vote(sp.list([sp.record(initiatorId = sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)], proposalId = sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].id, yay = False)]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1; Elt "tz0Fakesigner3" 2} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1; Elt "edpkFakesigner3" 2}) (Pair "1970-01-01T00:00:03Z" (Pair 2 3))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Right (Left 10)}} (Pair True 4)) (Pair {1; 2} (Pair "1970-01-01T00:00:04Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1}))); Elt 2 (Pair (Pair {Left {Left (Left 3)}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:03Z" {0; 2})))} 3) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 4 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2")); Elt 2 (Pair "tz0Fakesigner3" (Pair 1 "edpkFakesigner3"))}))))
Verifying sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].canceled == True...
 OK
Verifying sp.contract_data(1).timeout != 10...
 OK
Comment...
 h2: Invalid Removed signer proposal
Comment...
 h3: Signer 1 new proposal: remove signer 3
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_059_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_059_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_059_cont_1_params.json 1
Executing newProposal(sp.list([variant('selfAdmin', sp.list([variant('changeVoters', sp.record(added = sp.list([]), removed = sp.set([2])))]))]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1; Elt "tz0Fakesigner3" 2} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1; Elt "edpkFakesigner3" 2}) (Pair "1970-01-01T00:00:03Z" (Pair 2 3))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Right (Right (Pair {} {2}))}} (Pair False 5)) (Pair {} (Pair "1970-01-01T00:00:04Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1}))); Elt 2 (Pair (Pair {Left {Left (Left 3)}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:03Z" {0; 2})))} 3) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 5 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2")); Elt 2 (Pair "tz0Fakesigner3" (Pair 1 "edpkFakesigner3"))}))))
Comment...
 h3: Signer 2 votes the remove proposal
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_061_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_061_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_061_cont_1_params.json 1
Executing vote(sp.list([sp.record(initiatorId = sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)], proposalId = sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].id, yay = True)]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1; Elt "tz0Fakesigner3" 2} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1; Elt "edpkFakesigner3" 2}) (Pair "1970-01-01T00:00:03Z" (Pair 2 3))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Right (Right (Pair {} {2}))}} (Pair False 5)) (Pair {} (Pair "1970-01-01T00:00:04Z" {0; 1}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1}))); Elt 2 (Pair (Pair {Left {Left (Left 3)}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:03Z" {0; 2})))} 3) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 5 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2")); Elt 2 (Pair "tz0Fakesigner3" (Pair 1 "edpkFakesigner3"))}))))
Comment...
 h3: Signer 3 tries to vote the remove proposal
Comment...
 p: Fails because quorum would be > number of signers
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_064_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_064_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_064_cont_1_params.json 1
Executing vote(sp.list([sp.record(initiatorId = sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)], proposalId = sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].id, yay = True)]))...
 -> --- Expected failure in transaction --- WrongCondition in line 291: self.data.quorum <= self.data.nbVoters ['MultisignAdmin_Quorum<Voters']
Verifying sp.contract_data(1).voters.contains(2)...
 OK
Comment...
 h2: 2 actions proposal
Comment...
 h3: Signer 1 new proposal: change quorum to 2 and remove signer 3
Verifying sp.contract_data(1).quorum == 3...
 OK
Verifying sp.contract_data(1).voters.contains(2)...
 OK
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_070_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_070_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_070_cont_1_params.json 4
Executing newProposal(sp.list([variant('selfAdmin', sp.list([variant('changeQuorum', 2)])), variant('selfAdmin', sp.list([variant('changeVoters', sp.record(added = sp.list([]), removed = sp.set([2])))]))]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1; Elt "tz0Fakesigner3" 2} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1; Elt "edpkFakesigner3" 2}) (Pair "1970-01-01T00:00:03Z" (Pair 2 3))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Left (Left 2)}; Left {Right (Right (Pair {} {2}))}} (Pair False 6)) (Pair {} (Pair "1970-01-01T00:00:04Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1}))); Elt 2 (Pair (Pair {Left {Left (Left 3)}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:03Z" {0; 2})))} 3) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 6 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2")); Elt 2 (Pair "tz0Fakesigner3" (Pair 1 "edpkFakesigner3"))}))))
Comment...
 h3: Signer 2 votes the proposal
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_072_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_072_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_072_cont_1_params.json 1
Executing vote(sp.list([sp.record(initiatorId = sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)], proposalId = sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].id, yay = True)]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1; Elt "tz0Fakesigner3" 2} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1; Elt "edpkFakesigner3" 2}) (Pair "1970-01-01T00:00:03Z" (Pair 2 3))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Left (Left 2)}; Left {Right (Right (Pair {} {2}))}} (Pair False 6)) (Pair {} (Pair "1970-01-01T00:00:04Z" {0; 1}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1}))); Elt 2 (Pair (Pair {Left {Left (Left 3)}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:03Z" {0; 2})))} 3) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 6 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2")); Elt 2 (Pair "tz0Fakesigner3" (Pair 1 "edpkFakesigner3"))}))))
Comment...
 h3: Signer 3 votes the proposal
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_074_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_074_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_074_cont_1_params.json 1
Executing vote(sp.list([sp.record(initiatorId = sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)], proposalId = sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].id, yay = True)]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1}) (Pair "1970-01-01T00:00:04Z" (Pair 2 2))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Left (Left 2)}; Left {Right (Right (Pair {} {2}))}} (Pair False 6)) (Pair {} (Pair "1970-01-01T00:00:04Z" {0; 1; 2}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1})))} 2) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 6 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2"))}))))
Verifying sp.contract_data(1).quorum == 2...
 OK
Verifying ~ (sp.contract_data(1).voters.contains(2))...
 OK
Comment...
 h2: Vote for past proposal
Comment...
 h3: Signer 1 new proposal: change timeout to 2
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_079_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_079_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_079_cont_1_params.json 1
Executing newProposal(sp.list([variant('selfAdmin', sp.list([variant('changeTimeout', 2)]))]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1}) (Pair "1970-01-01T00:00:04Z" (Pair 2 2))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Right (Left 2)}} (Pair False 7)) (Pair {} (Pair "1970-01-01T00:00:04Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner3" "edpkFakesigner3"} {}))}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:02Z" {0; 1})))} 2) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 7 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 1 "edpkFakesigner2"))}))))
Comment...
 h3: Signer 2 new proposal: add new signer
Verifying sp.contract_data(1).timeout != 3...
 OK
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_082_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_082_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_082_cont_1_params.json 18
Executing newProposal(sp.list([variant('selfAdmin', sp.list([variant('changeVoters', sp.record(added = sp.list([sp.record(addr = sp.reduce(sp.test_account("signer4").address), publicKey = sp.reduce(sp.test_account("signer4").public_key))]), removed = sp.set([])))]))]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1}) (Pair "1970-01-01T00:00:04Z" (Pair 2 2))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Right (Left 2)}} (Pair False 7)) (Pair {} (Pair "1970-01-01T00:00:04Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner4" "edpkFakesigner4"} {}))}} (Pair False 2)) (Pair {} (Pair "1970-01-01T00:00:04Z" {1})))} 2) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 7 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 2 "edpkFakesigner2"))}))))
Comment...
 h3: Signer 2 tries to vote signer1's proposal after timedout
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_084_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_084_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_084_cont_1_params.json 1
Executing vote(sp.list([sp.record(initiatorId = sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)], proposalId = sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].id, yay = True)]))...
 -> --- Expected failure in transaction --- WrongCondition in line 237: sp.now < sp.add_seconds(self.data.proposals[lparams_1.in_param.initiatorId].startedAt, self.data.timeout * 60) ['MultisignAdmin_ProposalTimedout']
Comment...
 h3: Signer 1 votes for signer2's proposal
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_086_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_086_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_086_cont_1_params.json 1
Executing vote(sp.list([sp.record(initiatorId = sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer2").address)], proposalId = sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer2").address)]].id, yay = True)]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1; Elt "tz0Fakesigner4" 3} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1; Elt "edpkFakesigner4" 3}) (Pair "1970-01-01T00:00:05Z" (Pair 3 3))) (Pair (Pair {Elt 0 (Pair (Pair {Left {Right (Left 2)}} (Pair False 7)) (Pair {} (Pair "1970-01-01T00:00:04Z" {0}))); Elt 1 (Pair (Pair {Left {Right (Right (Pair {Pair "tz0Fakesigner4" "edpkFakesigner4"} {}))}} (Pair False 2)) (Pair {} (Pair "1970-01-01T00:00:04Z" {0; 1})))} 2) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 7 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 2 "edpkFakesigner2")); Elt 3 (Pair "tz0Fakesigner4" (Pair 0 "edpkFakesigner4"))}))))
Comment...
 h3: Signer 2 tries to vote signer1's proposal while a more recent one was accepted
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_088_cont_1_params.py 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_088_cont_1_params.tz 1
 => test_baselines/scenario/multisign_admin/Self_Administration_tests/step_088_cont_1_params.json 1
Executing vote(sp.list([sp.record(initiatorId = sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)], proposalId = sp.contract_data(1).proposals[sp.contract_data(1).addrVoterId[sp.reduce(sp.test_account("signer1").address)]].id, yay = True)]))...
 -> --- Expected failure in transaction --- WrongCondition in line 234: (sp.now > self.data.lastVoteTimestamp) & (~ self.data.proposals[lparams_1.in_param.initiatorId].canceled) ['MultisignAdmin_ProposalClosed']
