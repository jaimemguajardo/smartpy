import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(active = False, admin = sp.address('tz0Fakeadmin'), value = sp.none)

  @sp.entry_point
  def administrate(self, params):
    sp.verify(sp.sender == self.data.admin, message = 'NOT ADMIN')
    sp.set_type(params, sp.TList(sp.TVariant(setActive = sp.TBool, setAdmin = sp.TAddress).layout(("setActive", "setAdmin"))))
    sp.for action in params:
      with action.match_cases() as arg:
        with arg.match('setActive') as arg:
          self.data.active = arg
        with arg.match('setAdmin') as arg:
          self.data.admin = arg


  @sp.entry_point
  def setValue(self, params):
    sp.verify(self.data.active, message = 'NOT ACTIVE')