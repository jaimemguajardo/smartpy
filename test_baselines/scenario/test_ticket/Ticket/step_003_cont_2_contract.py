import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(t = sp.TTicket(sp.TNat), x = sp.TInt).layout(("t", "x")))

  @sp.entry_point
  def run(self, params):
    with sp.match_record(self.data, "data") as data:
      ticket_52_data, ticket_52_copy = sp.match_tuple(sp.read_ticket_raw(data.t), "ticket_52_data", "ticket_52_copy")
      ticket_52_ticketer, ticket_52_content, ticket_52_amount = sp.match_tuple(ticket_52_data, "ticket_52_ticketer", "ticket_52_content", "ticket_52_amount")
      sp.verify(ticket_52_content == 42)
      ticket1_54, ticket2_54 = sp.match_tuple(sp.split_ticket_raw(ticket_52_copy, (ticket_52_amount // 2, ticket_52_amount // 2)).open_some(), "ticket1_54", "ticket2_54")
      data.t = sp.join_tickets_raw((ticket2_54, ticket1_54)).open_some()