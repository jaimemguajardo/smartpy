Comment...
 h1: Variants
Creating contract
 -> (Pair (Pair 0 (Pair 0 1)) (Pair (Left (Left -1)) (Pair (Some -42) (Left -10))))
 => test_baselines/scenario/testVariant/variant/step_001_cont_0_storage.tz 1
 => test_baselines/scenario/testVariant/variant/step_001_cont_0_storage.json 13
 => test_baselines/scenario/testVariant/variant/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario/testVariant/variant/step_001_cont_0_storage.py 1
 => test_baselines/scenario/testVariant/variant/step_001_cont_0_types.py 7
 => test_baselines/scenario/testVariant/variant/step_001_cont_0_contract.tz 344
 => test_baselines/scenario/testVariant/variant/step_001_cont_0_contract.json 550
 => test_baselines/scenario/testVariant/variant/step_001_cont_0_contract.py 60
 => test_baselines/scenario/testVariant/variant/step_002_cont_0_params.py 1
 => test_baselines/scenario/testVariant/variant/step_002_cont_0_params.tz 1
 => test_baselines/scenario/testVariant/variant/step_002_cont_0_params.json 1
Executing options(sp.record())...
 -> (Pair (Pair 2 (Pair 0 1)) (Pair (Left (Left -1)) (Pair None (Left -10))))
 => test_baselines/scenario/testVariant/variant/step_003_cont_0_params.py 1
 => test_baselines/scenario/testVariant/variant/step_003_cont_0_params.tz 1
 => test_baselines/scenario/testVariant/variant/step_003_cont_0_params.json 1
Executing ep1(sp.record())...
 -> (Pair (Pair -1 (Pair 0 1)) (Pair (Right (Left 3)) (Pair None (Left -10))))
 => test_baselines/scenario/testVariant/variant/step_004_cont_0_params.py 1
 => test_baselines/scenario/testVariant/variant/step_004_cont_0_params.tz 1
 => test_baselines/scenario/testVariant/variant/step_004_cont_0_params.json 1
Executing ep3(sp.record())...
 -> (Pair (Pair -10 (Pair 0 1)) (Pair (Right (Left 3)) (Pair None (Left -10))))
 => test_baselines/scenario/testVariant/variant/step_005_cont_0_params.py 1
 => test_baselines/scenario/testVariant/variant/step_005_cont_0_params.tz 1
 => test_baselines/scenario/testVariant/variant/step_005_cont_0_params.json 1
Executing ep4(sp.record())...
 -> (Pair (Pair -10 (Pair 0 1)) (Pair (Right (Left 3)) (Pair None (Left -10))))
 => test_baselines/scenario/testVariant/variant/step_006_cont_0_params.py 1
 => test_baselines/scenario/testVariant/variant/step_006_cont_0_params.tz 1
 => test_baselines/scenario/testVariant/variant/step_006_cont_0_params.json 1
Executing ep5(sp.record())...
 -> (Pair (Pair -10 (Pair 0 1)) (Pair (Right (Left 3)) (Pair None (Left -10))))
 => test_baselines/scenario/testVariant/variant/step_007_cont_0_params.py 1
 => test_baselines/scenario/testVariant/variant/step_007_cont_0_params.tz 1
 => test_baselines/scenario/testVariant/variant/step_007_cont_0_params.json 1
Executing ep6(sp.record())...
 -> --- Expected failure in transaction --- Not the proper variant constructor [Toto] != [C] in [self.data.x.open_variant('Toto', message = 'no toto')].
