import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(x1 = sp.record(a = 0, b = 1, c = True, d = 'abc'), x2 = sp.record(a = 0, b = 1, c = True, d = 'abc'), x3 = sp.record(a = 0, b = 1, c = True, d = 'abc'), x4 = sp.record(a = 0, b = 1, c = True, d = 'abc'), y = (0, 1, True, 'abc'), z = sp.record(a = 0, b = 1, c = True, d = sp.record(e = 1, f = 'x')))

  @sp.entry_point
  def ep1(self, params):
    sp.set_type(self.data.x2, sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(("a", ("b", ("c", "d")))))
    sp.set_type(self.data.x3, sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(((("a", "b"), "c"), "d")))
    sp.set_type(self.data.x4, sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout((("a", "b"), ("c", "d"))))
    with sp.match_record(self.data.x1, "modify_record_21") as modify_record_21:
      sp.verify((abs(modify_record_21.b)) == (modify_record_21.a + 1))
    with sp.match_record(self.data.x2, "modify_record_23") as modify_record_23:
      sp.verify((abs(modify_record_23.b)) == (modify_record_23.a + 1))
      modify_record_23.d = 'xyz'
    with sp.match_record(self.data.x3, "modify_record_26") as modify_record_26:
      sp.verify((abs(modify_record_26.b)) == (modify_record_26.a + 1))
      modify_record_26.d = 'xyz'
    with sp.match_record(self.data.x4, "modify_record_29") as modify_record_29:
      sp.verify((abs(modify_record_29.b)) == (modify_record_29.a + 1))
      modify_record_29.d = 'xyz'

  @sp.entry_point
  def ep2(self, params):
    with sp.match_record(self.data.x1, "modify_record_35") as modify_record_35:
      sp.verify((abs(modify_record_35.b)) == (modify_record_35.a + 1))
      modify_record_35.d = 'xyz'

  @sp.entry_point
  def ep3(self, params):
    with sp.match_tuple(self.data.y, "a", "b", "c", "d") as a, b, c, d:
      sp.verify((abs(b.value)) == (a.value + 1))
      d.value = 'xyz'
      sp.result((a.value, b.value, c.value, d.value))

  @sp.entry_point
  def ep4(self, params):
    with sp.match_record(self.data.x1, "modify_record_49") as modify_record_49:
      pass

  @sp.entry_point
  def ep5(self, params):
    with sp.match_record(self.data.x1, "modify_record_61") as modify_record_61:
      sp.send(params, sp.tez(0))
      modify_record_61.d = 'xyz'
    self.data.x1.a += 5

  @sp.entry_point
  def ep6(self, params):
    with sp.match_record(self.data.z, "modify_record_68") as modify_record_68:
      with sp.match_record(modify_record_68.d, "modify_record_69") as modify_record_69:
        modify_record_68.b = 100
        modify_record_69.e = 2
        modify_record_69.f = 'y'

  @sp.entry_point
  def ep7(self, params):
    sp.verify(self.data.z.d.e == 2)
    with sp.match_record(self.data.z.d, "modify_record_77") as modify_record_77:
      sp.verify(modify_record_77.e == 2)
      modify_record_77.e = 3
      modify_record_77.f = 'z'
      sp.verify(modify_record_77.e == 3)
      modify_record_77.e = 4
      sp.verify(modify_record_77.e == 4)
    sp.verify(self.data.z.d.e == 4)