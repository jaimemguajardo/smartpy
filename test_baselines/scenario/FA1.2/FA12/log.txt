Comment...
 h1: FA1.2 template - Fungible assets
Table Of Contents

 FA1.2 template - Fungible assets
 Accounts
 Contract
 Entry points
# Admin mints a few coins
# Alice transfers to Bob
# Bob tries to transfer from Alice but he doesn't have her approval
# Alice approves Bob and Bob transfers
# Bob tries to over-transfer from Alice
# Admin burns Bob token
# Alice tries to burn Bob token
# Admin pauses the contract and Alice cannot transfer anymore
# Admin transfers while on pause
# Admin unpauses the contract and transferts are allowed
 Views
# Balance
# Administrator
# Total Supply
# Allowance
Comment...
 h1: Accounts
Computing sp.list([sp.test_account("Administrator"), sp.test_account("Alice"), sp.test_account("Robert")])...
 => sp.list([sp.record(seed = 'Administrator', address = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), public_key = sp.key('edpktzrjdb1tx6dQecQGZL6CwhujWg1D2CXfXWBriqtJSA6kvqMwA2'), public_key_hash = sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), secret_key = sp.secret_key('edskRqFp3Z9AqoKrMNFb9bnWNwEsRzbjqjBhzmFMLF9UqB6VBmw7F8ppTiXaAnHtysmi6xFxoHf6rMUz6Y1ipiDz2EgwZQv3pa')), sp.record(seed = 'Alice', address = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), public_key = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG'), public_key_hash = sp.key_hash('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), secret_key = sp.secret_key('edskRijgcXx8gzqkq7SCBbrb6aDZQMmP6dznCQWgU1Jr4qPfJT1yFq5A39ja9G4wahS8uWtBurZy14Hy7GZkQh7WnopJTKtCQG')), sp.record(seed = 'Robert', address = sp.address('tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP'), public_key = sp.key('edpkvThfdv8Efh1MuqSTUk5EnUFCTjqN6kXDCNXpQ8udN3cKRhNDr2'), public_key_hash = sp.key_hash('tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP'), secret_key = sp.secret_key('edskRiaffUWqB9zgaEhuX6EmejbLzk2xcpSEXLv3G4cDfcbY75c71ASyGnFHXuaTAVMPt2bJLGGye1gm24oBmAc2k5VDHHo5Ua'))])
Comment...
 h1: Contract
Comment...
 h1: Entry points
Creating contract
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {}) (Pair False 0))
 => test_baselines/scenario/FA1.2/FA12/step_006_cont_0_storage.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_006_cont_0_storage.json 4
 => test_baselines/scenario/FA1.2/FA12/step_006_cont_0_sizes.csv 2
 => test_baselines/scenario/FA1.2/FA12/step_006_cont_0_storage.py 1
 => test_baselines/scenario/FA1.2/FA12/step_006_cont_0_types.py 7
 => test_baselines/scenario/FA1.2/FA12/step_006_cont_0_contract.tz 787
 => test_baselines/scenario/FA1.2/FA12/step_006_cont_0_contract.json 834
 => test_baselines/scenario/FA1.2/FA12/step_006_cont_0_contract.py 79
Comment...
 h2: Admin mints a few coins
 => test_baselines/scenario/FA1.2/FA12/step_008_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_008_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_008_cont_0_params.json 1
Executing mint(sp.record(address = sp.reduce(sp.test_account("Alice").address), value = 12))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {} 12)}) (Pair False 12))
 => test_baselines/scenario/FA1.2/FA12/step_009_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_009_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_009_cont_0_params.json 1
Executing mint(sp.record(address = sp.reduce(sp.test_account("Alice").address), value = 3))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {} 15)}) (Pair False 15))
 => test_baselines/scenario/FA1.2/FA12/step_010_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_010_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_010_cont_0_params.json 1
Executing mint(sp.record(address = sp.reduce(sp.test_account("Alice").address), value = 3))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {} 18)}) (Pair False 18))
Comment...
 h2: Alice transfers to Bob
 => test_baselines/scenario/FA1.2/FA12/step_012_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_012_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_012_cont_0_params.json 4
Executing transfer(sp.record(from_ = sp.reduce(sp.test_account("Alice").address), to_ = sp.reduce(sp.test_account("Robert").address), value = 4))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" (Pair {} 4); Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {} 14)}) (Pair False 18))
Verifying sp.contract_data(0).balances[sp.reduce(sp.test_account("Alice").address)].balance == 14...
 OK
Comment...
 h2: Bob tries to transfer from Alice but he doesn't have her approval
 => test_baselines/scenario/FA1.2/FA12/step_015_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_015_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_015_cont_0_params.json 4
Executing transfer(sp.record(from_ = sp.reduce(sp.test_account("Alice").address), to_ = sp.reduce(sp.test_account("Robert").address), value = 4))...
 -> --- Expected failure in transaction --- Missing item in map: (sp.address('tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP')) is not in ({}), while evaluating self.data.balances[params.from_].approvals[sp.sender]
Comment...
 h2: Alice approves Bob and Bob transfers
 => test_baselines/scenario/FA1.2/FA12/step_017_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_017_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_017_cont_0_params.json 1
Executing approve(sp.record(spender = sp.reduce(sp.test_account("Robert").address), value = 5))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" (Pair {} 4); Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 5} 14)}) (Pair False 18))
 => test_baselines/scenario/FA1.2/FA12/step_018_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_018_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_018_cont_0_params.json 4
Executing transfer(sp.record(from_ = sp.reduce(sp.test_account("Alice").address), to_ = sp.reduce(sp.test_account("Robert").address), value = 4))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" (Pair {} 8); Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1} 10)}) (Pair False 18))
Comment...
 h2: Bob tries to over-transfer from Alice
 => test_baselines/scenario/FA1.2/FA12/step_020_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_020_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_020_cont_0_params.json 4
Executing transfer(sp.record(from_ = sp.reduce(sp.test_account("Alice").address), to_ = sp.reduce(sp.test_account("Robert").address), value = 4))...
 -> --- Expected failure in transaction --- WrongCondition in line 13: (sp.sender == self.data.administrator) | ((~ self.data.paused) & ((params.from_ == sp.sender) | (self.data.balances[params.from_].approvals[sp.sender] >= params.value)))
Comment...
 h2: Admin burns Bob token
 => test_baselines/scenario/FA1.2/FA12/step_022_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_022_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_022_cont_0_params.json 1
Executing burn(sp.record(address = sp.reduce(sp.test_account("Robert").address), value = 1))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" (Pair {} 7); Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1} 10)}) (Pair False 17))
Verifying sp.contract_data(0).balances[sp.reduce(sp.test_account("Alice").address)].balance == 10...
 OK
Comment...
 h2: Alice tries to burn Bob token
 => test_baselines/scenario/FA1.2/FA12/step_025_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_025_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_025_cont_0_params.json 1
Executing burn(sp.record(address = sp.reduce(sp.test_account("Robert").address), value = 1))...
 -> --- Expected failure in transaction --- WrongCondition in line 69: sp.sender == self.data.administrator
Comment...
 h2: Admin pauses the contract and Alice cannot transfer anymore
 => test_baselines/scenario/FA1.2/FA12/step_027_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_027_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_027_cont_0_params.json 1
Executing setPause(True)...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" (Pair {} 7); Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1} 10)}) (Pair True 17))
 => test_baselines/scenario/FA1.2/FA12/step_028_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_028_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_028_cont_0_params.json 4
Executing transfer(sp.record(from_ = sp.reduce(sp.test_account("Alice").address), to_ = sp.reduce(sp.test_account("Robert").address), value = 4))...
 -> --- Expected failure in transaction --- WrongCondition in line 13: (sp.sender == self.data.administrator) | ((~ self.data.paused) & ((params.from_ == sp.sender) | (self.data.balances[params.from_].approvals[sp.sender] >= params.value)))
Verifying sp.contract_data(0).balances[sp.reduce(sp.test_account("Alice").address)].balance == 10...
 OK
Comment...
 h2: Admin transfers while on pause
 => test_baselines/scenario/FA1.2/FA12/step_031_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_031_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_031_cont_0_params.json 4
Executing transfer(sp.record(from_ = sp.reduce(sp.test_account("Alice").address), to_ = sp.reduce(sp.test_account("Robert").address), value = 1))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" (Pair {} 8); Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1} 9)}) (Pair True 17))
Comment...
 h2: Admin unpauses the contract and transferts are allowed
 => test_baselines/scenario/FA1.2/FA12/step_033_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_033_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_033_cont_0_params.json 1
Executing setPause(False)...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" (Pair {} 8); Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1} 9)}) (Pair False 17))
Verifying sp.contract_data(0).balances[sp.reduce(sp.test_account("Alice").address)].balance == 9...
 OK
 => test_baselines/scenario/FA1.2/FA12/step_035_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_035_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_035_cont_0_params.json 4
Executing transfer(sp.record(from_ = sp.reduce(sp.test_account("Alice").address), to_ = sp.reduce(sp.test_account("Robert").address), value = 1))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" (Pair {} 9); Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1} 8)}) (Pair False 17))
Verifying sp.contract_data(0).totalSupply == 17...
 OK
Verifying sp.contract_data(0).balances[sp.reduce(sp.test_account("Alice").address)].balance == 8...
 OK
Verifying sp.contract_data(0).balances[sp.reduce(sp.test_account("Robert").address)].balance == 9...
 OK
Comment...
 h1: Views
Comment...
 h2: Balance
Creating contract
 -> None
 => test_baselines/scenario/FA1.2/FA12/step_041_cont_1_storage.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_041_cont_1_storage.json 1
 => test_baselines/scenario/FA1.2/FA12/step_041_cont_1_sizes.csv 2
 => test_baselines/scenario/FA1.2/FA12/step_041_cont_1_storage.py 1
 => test_baselines/scenario/FA1.2/FA12/step_041_cont_1_types.py 7
 => test_baselines/scenario/FA1.2/FA12/step_041_cont_1_contract.tz 11
 => test_baselines/scenario/FA1.2/FA12/step_041_cont_1_contract.json 5
 => test_baselines/scenario/FA1.2/FA12/step_041_cont_1_contract.py 9
 => test_baselines/scenario/FA1.2/FA12/step_042_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_042_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_042_cont_0_params.json 1
Executing getBalance((sp.reduce(sp.test_account("Alice").address), sp.contract(sp.TNat, sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF')).open_some()))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" (Pair {} 9); Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1} 8)}) (Pair False 17))
Executing default(8)...
 -> (Some 8)
Verifying sp.pack(sp.set_type_expr(sp.contract_data(1).last, sp.TOption(sp.TNat))) == sp.pack(sp.set_type_expr(sp.some(8), sp.TOption(sp.TNat)))...
 OK
Comment...
 h2: Administrator
Creating contract
 -> None
 => test_baselines/scenario/FA1.2/FA12/step_045_cont_2_storage.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_045_cont_2_storage.json 1
 => test_baselines/scenario/FA1.2/FA12/step_045_cont_2_sizes.csv 2
 => test_baselines/scenario/FA1.2/FA12/step_045_cont_2_storage.py 1
 => test_baselines/scenario/FA1.2/FA12/step_045_cont_2_types.py 7
 => test_baselines/scenario/FA1.2/FA12/step_045_cont_2_contract.tz 11
 => test_baselines/scenario/FA1.2/FA12/step_045_cont_2_contract.json 5
 => test_baselines/scenario/FA1.2/FA12/step_045_cont_2_contract.py 9
 => test_baselines/scenario/FA1.2/FA12/step_046_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_046_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_046_cont_0_params.json 1
Executing getAdministrator((sp.unit, sp.contract(sp.TAddress, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H')).open_some()))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" (Pair {} 9); Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1} 8)}) (Pair False 17))
Executing default(sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'))...
 -> (Some "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w")
Verifying sp.pack(sp.set_type_expr(sp.contract_data(2).last, sp.TOption(sp.TAddress))) == sp.pack(sp.set_type_expr(sp.some(sp.reduce(sp.test_account("Administrator").address)), sp.TOption(sp.TAddress)))...
 OK
Comment...
 h2: Total Supply
Creating contract
 -> None
 => test_baselines/scenario/FA1.2/FA12/step_049_cont_3_storage.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_049_cont_3_storage.json 1
 => test_baselines/scenario/FA1.2/FA12/step_049_cont_3_sizes.csv 2
 => test_baselines/scenario/FA1.2/FA12/step_049_cont_3_storage.py 1
 => test_baselines/scenario/FA1.2/FA12/step_049_cont_3_types.py 7
 => test_baselines/scenario/FA1.2/FA12/step_049_cont_3_contract.tz 11
 => test_baselines/scenario/FA1.2/FA12/step_049_cont_3_contract.json 5
 => test_baselines/scenario/FA1.2/FA12/step_049_cont_3_contract.py 9
 => test_baselines/scenario/FA1.2/FA12/step_050_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_050_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_050_cont_0_params.json 1
Executing getTotalSupply((sp.unit, sp.contract(sp.TNat, sp.address('KT1Tezooo3zzSmartPyzzSTATiCzzzseJjWC')).open_some()))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" (Pair {} 9); Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1} 8)}) (Pair False 17))
Executing default(17)...
 -> (Some 17)
Verifying sp.pack(sp.set_type_expr(sp.contract_data(3).last, sp.TOption(sp.TNat))) == sp.pack(sp.set_type_expr(sp.some(17), sp.TOption(sp.TNat)))...
 OK
Comment...
 h2: Allowance
Creating contract
 -> None
 => test_baselines/scenario/FA1.2/FA12/step_053_cont_4_storage.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_053_cont_4_storage.json 1
 => test_baselines/scenario/FA1.2/FA12/step_053_cont_4_sizes.csv 2
 => test_baselines/scenario/FA1.2/FA12/step_053_cont_4_storage.py 1
 => test_baselines/scenario/FA1.2/FA12/step_053_cont_4_types.py 7
 => test_baselines/scenario/FA1.2/FA12/step_053_cont_4_contract.tz 11
 => test_baselines/scenario/FA1.2/FA12/step_053_cont_4_contract.json 5
 => test_baselines/scenario/FA1.2/FA12/step_053_cont_4_contract.py 9
 => test_baselines/scenario/FA1.2/FA12/step_054_cont_0_params.py 1
 => test_baselines/scenario/FA1.2/FA12/step_054_cont_0_params.tz 1
 => test_baselines/scenario/FA1.2/FA12/step_054_cont_0_params.json 7
Executing getAllowance((sp.record(owner = sp.reduce(sp.test_account("Alice").address), spender = sp.reduce(sp.test_account("Robert").address)), sp.contract(sp.TNat, sp.address('KT1Tezooo4zzSmartPyzzSTATiCzzzyPVdv3')).open_some()))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" (Pair {} 9); Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair {Elt "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1} 8)}) (Pair False 17))
Executing default(1)...
 -> (Some 1)
Verifying sp.pack(sp.set_type_expr(sp.contract_data(4).last, sp.TOption(sp.TNat))) == sp.pack(sp.set_type_expr(sp.some(1), sp.TOption(sp.TNat)))...
 OK
