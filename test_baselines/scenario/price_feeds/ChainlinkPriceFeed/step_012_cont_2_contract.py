import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(active = True, admin = sp.address('tz1axGtTkg1hJvGenTrhqpFbW1S8GcQpPdve'), aggregator = sp.some(sp.address('KT1CfuSjCcunNQ5qCCro2Kc74uivnor9d8ba%latestRoundData')))

  @sp.entry_point
  def administrate(self, params):
    sp.verify(sp.sender == self.data.admin, message = 'Proxy_NotAdmin')
    sp.set_type(params, sp.TList(sp.TVariant(changeActive = sp.TBool, changeAdmin = sp.TAddress, changeAggregator = sp.TAddress).layout(("changeActive", ("changeAdmin", "changeAggregator")))))
    sp.for action in params:
      with action.match_cases() as arg:
        with arg.match('changeActive') as arg:
          self.data.active = arg
        with arg.match('changeAdmin') as arg:
          self.data.admin = arg
        with arg.match('changeAggregator') as arg:
          self.data.aggregator = sp.some(arg)


  @sp.entry_point
  def decimals(self, params):
    sp.transfer(params, sp.tez(0), sp.contract(sp.TPair(sp.TUnit, sp.TAddress), self.data.aggregator.open_some(message = 'Proxy_AggregatorNotConfigured'), entry_point='decimals').open_some(message = 'Proxy_InvalidParametersInDecimalsView'))

  @sp.entry_point
  def description(self, params):
    sp.transfer(params, sp.tez(0), sp.contract(sp.TPair(sp.TUnit, sp.TAddress), self.data.aggregator.open_some(message = 'Proxy_AggregatorNotConfigured'), entry_point='description').open_some(message = 'Proxy_InvalidParametersInDescriptionView'))

  @sp.entry_point
  def latestRoundData(self, params):
    sp.transfer(params, sp.tez(0), sp.contract(sp.TPair(sp.TUnit, sp.TAddress), self.data.aggregator.open_some(message = 'Proxy_AggregatorNotConfigured'), entry_point='latestRoundData').open_some(message = 'Proxy_InvalidParametersInLatestRoundDataView'))

  @sp.entry_point
  def version(self, params):
    sp.transfer(params, sp.tez(0), sp.contract(sp.TPair(sp.TUnit, sp.TAddress), self.data.aggregator.open_some(message = 'Proxy_AggregatorNotConfigured'), entry_point='version').open_some(message = 'Proxy_InvalidParametersInVersionView'))