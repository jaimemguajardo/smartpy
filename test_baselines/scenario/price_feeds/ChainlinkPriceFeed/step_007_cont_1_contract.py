import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(active = True, admin = sp.address('tz1axGtTkg1hJvGenTrhqpFbW1S8GcQpPdve'), decimals = 8, latestRoundId = 0, linkTokenAddress = sp.address("KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1"), maxSubmissions = 6, metadata = {'' : sp.bytes('0x3c55524c3e')}, minSubmissions = 3, oraclePayment = 1, oracles = sp.set([sp.address('KT1LLTzYhdhxTqKu7ByJz8KaShF6qPTdx5os'), sp.address('KT1LhTzYhdhxTqKu7ByJz8KaShF6qPTdx5os'), sp.address('KT1P7oeoKWHx5SXt73qpEanzkr8yeEKABqko'), sp.address('KT1SCkxmTqTkmc7zoAP5uMYT9rp9iqVVRgdt')]), oraclesDetails = {sp.address('KT1LLTzYhdhxTqKu7ByJz8KaShF6qPTdx5os') : sp.record(adminAddress = sp.address('tz1axGtTkg1hJvGenTrhqpFbW1S8GcQpPdve'), endingRound = 4294967295, lastStartedRound = 0, startingRound = 0, withdrawable = 0), sp.address('KT1LhTzYhdhxTqKu7ByJz8KaShF6qPTdx5os') : sp.record(adminAddress = sp.address('tz1axGtTkg1hJvGenTrhqpFbW1S8GcQpPdve'), endingRound = 4294967295, lastStartedRound = 0, startingRound = 0, withdrawable = 0), sp.address('KT1P7oeoKWHx5SXt73qpEanzkr8yeEKABqko') : sp.record(adminAddress = sp.address('tz1axGtTkg1hJvGenTrhqpFbW1S8GcQpPdve'), endingRound = 4294967295, lastStartedRound = 0, startingRound = 0, withdrawable = 0), sp.address('KT1SCkxmTqTkmc7zoAP5uMYT9rp9iqVVRgdt') : sp.record(adminAddress = sp.address('tz1axGtTkg1hJvGenTrhqpFbW1S8GcQpPdve'), endingRound = 4294967295, lastStartedRound = 0, startingRound = 0, withdrawable = 0)}, recordedFunds = sp.record(allocated = 0, available = 0), reportingRoundId = 0, restartDelay = 2, rounds = {}, roundsDetails = {}, timeout = 10)

  @sp.entry_point
  def administrate(self, params):
    sp.verify(sp.sender == self.data.admin, message = 'Aggregator_NotAdmin')
    sp.set_type(params, sp.TList(sp.TVariant(changeActive = sp.TBool, changeAdmin = sp.TAddress, changeOracles = sp.TRecord(added = sp.TList(sp.TPair(sp.TAddress, sp.TRecord(adminAddress = sp.TAddress, endingRound = sp.TOption(sp.TNat), lastStartedRound = sp.TNat, startingRound = sp.TNat, withdrawable = sp.TNat).layout((("adminAddress", "endingRound"), ("lastStartedRound", ("startingRound", "withdrawable")))))), removed = sp.TList(sp.TAddress)).layout(("added", "removed")), updateFutureRounds = sp.TRecord(maxSubmissions = sp.TNat, minSubmissions = sp.TNat, oraclePayment = sp.TNat, restartDelay = sp.TNat, timeout = sp.TNat).layout((("maxSubmissions", "minSubmissions"), ("oraclePayment", ("restartDelay", "timeout"))))).layout((("changeActive", "changeAdmin"), ("changeOracles", "updateFutureRounds")))))
    sp.for action in params:
      with action.match_cases() as arg:
        with arg.match('changeActive') as arg:
          self.data.active = arg
        with arg.match('changeAdmin') as arg:
          self.data.admin = arg
        with arg.match('updateFutureRounds') as arg:
          sp.verify(sp.sender == self.data.admin, message = 'Aggregator_NotAdmin')
          compute_629 = sp.local("compute_629", sp.len(self.data.roundsDetails[self.data.reportingRoundId].activeOracles))
          sp.verify(arg.maxSubmissions >= arg.minSubmissions, message = 'Aggregator_MaxInferiorToMin')
          sp.verify(compute_629.value >= arg.maxSubmissions, message = 'Aggregator_MaxExceedActive')
          sp.verify((compute_629.value == 0) | (compute_629.value > arg.restartDelay), message = 'Aggregator_DelayExceedTotal')
          sp.verify((compute_629.value == 0) | (arg.minSubmissions > 0), message = 'Aggregator_MinSubmissionsTooLow')
          y4 = sp.local("y4", self.requiredReserve(sp.record(in_param = sp.record(oraclePayment = arg.oraclePayment, oraclesCount = compute_629.value), in_storage = self.data)))
          self.data = y4.value.storage
          sp.for op in y4.value.operations:
            sp.operations().push(op)
          sp.verify(self.data.recordedFunds.available >= y4.value.result, message = 'Aggregator_InsufficientFundsForPayment')
          self.data.restartDelay = arg.restartDelay
          self.data.minSubmissions = arg.minSubmissions
          self.data.maxSubmissions = arg.maxSubmissions
          self.data.timeout = arg.timeout
          self.data.oraclePayment = arg.oraclePayment
        with arg.match('changeOracles') as arg:
          sp.verify(sp.sender == self.data.admin, message = 'Aggregator_NotAdmin')
          sp.for oracleAddress in arg.removed:
            sp.if self.data.oracles.contains(oracleAddress):
              self.data.oracles.remove(oracleAddress)
              del self.data.oraclesDetails[oracleAddress]
          sp.for oracle in arg.added:
            match_pair_654_fst, match_pair_654_snd = sp.match_tuple(oracle, "match_pair_654_fst", "match_pair_654_snd")
            sp.set_type(match_pair_654_snd.endingRound, sp.TOption(sp.TNat))
            endingRound = sp.local("endingRound", 4294967295)
            sp.if match_pair_654_snd.endingRound.is_some():
              endingRound.value = match_pair_654_snd.endingRound.open_some()
            self.data.oracles.add(match_pair_654_fst)
            self.data.oraclesDetails[match_pair_654_fst] = sp.record(adminAddress = match_pair_654_snd.adminAddress, endingRound = endingRound.value, lastStartedRound = 0, startingRound = match_pair_654_snd.startingRound, withdrawable = 0)
            sp.if match_pair_654_snd.startingRound <= self.data.reportingRoundId:
              self.data.roundsDetails[self.data.reportingRoundId].activeOracles.add(match_pair_654_fst)


  @sp.entry_point
  def decimals(self, params):
    sp.transfer(self.data.decimals, sp.tez(0), params)

  @sp.entry_point
  def forceBalanceUpdate(self, params):
    sp.transfer(sp.record(requests = sp.list([sp.record(owner = sp.self_address, token_id = 0)]), callback = sp.self_entry_point('updateAvailableFunds')), sp.tez(0), sp.contract(sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")), self.data.linkTokenAddress, entry_point='balance_of').open_some(message = 'Aggregator_InvalidTokenkInterface'))

  @sp.entry_point
  def latestRoundData(self, params):
    sp.transfer(self.data.rounds[self.data.latestRoundId], sp.tez(0), params)

  @sp.entry_point
  def submit(self, params):
    match_pair_579_fst, match_pair_579_snd = sp.match_tuple(params, "match_pair_579_fst", "match_pair_579_snd")
    sp.verify(self.data.active)
    sp.verify(self.data.oracles.contains(sp.sender), message = 'Aggregator_NotOracle')
    sp.verify(self.data.oraclesDetails[sp.sender].startingRound <= match_pair_579_fst, message = 'Aggregator_NotYetEnabledOracle')
    sp.verify(self.data.oraclesDetails[sp.sender].endingRound > match_pair_579_fst, message = 'Aggregator_NotLongerAllowedOracle')
    sp.verify((((match_pair_579_fst + 1) == self.data.reportingRoundId) | (match_pair_579_fst == self.data.reportingRoundId)) | (match_pair_579_fst == (self.data.reportingRoundId + 1)), message = 'Aggregator_InvalidRound')
    sp.if (match_pair_579_fst + 1) == self.data.reportingRoundId:
      sp.verify(~ (self.data.roundsDetails[sp.as_nat(self.data.reportingRoundId - 1)].submissions.contains(sp.sender)), message = 'Aggregator_SubmittedInCurrent')
      sp.verify(sp.len(self.data.roundsDetails[sp.as_nat(self.data.reportingRoundId - 1)].submissions) < self.data.roundsDetails[sp.as_nat(self.data.reportingRoundId - 1)].maxSubmissions, message = 'Aggregator_RoundMaxSubmissionExceed')
      self.data.roundsDetails[sp.as_nat(self.data.reportingRoundId - 1)].submissions[sp.sender] = match_pair_579_snd
      sp.if sp.len(self.data.roundsDetails[sp.as_nat(self.data.reportingRoundId - 1)].submissions) >= self.data.roundsDetails[sp.as_nat(self.data.reportingRoundId - 1)].minSubmissions:
        y5 = sp.local("y5", self.median(sp.record(in_param = self.data.roundsDetails[sp.as_nat(self.data.reportingRoundId - 1)].submissions.values(), in_storage = self.data)))
        self.data = y5.value.storage
        sp.for op in y5.value.operations:
          sp.operations().push(op)
        self.data.rounds[sp.as_nat(self.data.reportingRoundId - 1)].answer = y5.value.result
        self.data.rounds[sp.as_nat(self.data.reportingRoundId - 1)].updatedAt = sp.now
        self.data.rounds[sp.as_nat(self.data.reportingRoundId - 1)].answeredInRound = self.data.reportingRoundId
    sp.else:
      sp.if match_pair_579_fst == self.data.reportingRoundId:
        sp.verify(~ (self.data.roundsDetails[self.data.reportingRoundId].submissions.contains(sp.sender)), message = 'Aggregator_AlreadySubmittedForThisRound')
        sp.verify(sp.len(self.data.roundsDetails[self.data.reportingRoundId].submissions) < self.data.roundsDetails[self.data.reportingRoundId].maxSubmissions, message = 'Aggregator_RoundMaxSubmissionExceed')
        self.data.roundsDetails[self.data.reportingRoundId].submissions[sp.sender] = match_pair_579_snd
        sp.if sp.len(self.data.roundsDetails[self.data.reportingRoundId].submissions) >= self.data.roundsDetails[self.data.reportingRoundId].minSubmissions:
          y6 = sp.local("y6", self.median(sp.record(in_param = self.data.roundsDetails[self.data.reportingRoundId].submissions.values(), in_storage = self.data)))
          self.data = y6.value.storage
          sp.for op in y6.value.operations:
            sp.operations().push(op)
          self.data.rounds[self.data.reportingRoundId].answer = y6.value.result
          self.data.rounds[self.data.reportingRoundId].updatedAt = sp.now
          self.data.rounds[self.data.reportingRoundId].answeredInRound = self.data.reportingRoundId
          self.data.latestRoundId = self.data.reportingRoundId
      sp.else:
        sp.if self.data.reportingRoundId > 0:
          sp.verify((self.data.oraclesDetails[sp.sender].lastStartedRound == 0) | ((self.data.reportingRoundId + 1) > (self.data.oraclesDetails[sp.sender].lastStartedRound + self.data.restartDelay)), message = 'Aggregator_WaitBeforeInit')
          sp.verify((sp.now > sp.add_seconds(self.data.rounds[self.data.reportingRoundId].startedAt, (sp.to_int(self.data.timeout)) * 60)) | (self.data.rounds[self.data.reportingRoundId].answeredInRound == self.data.reportingRoundId), message = 'Aggregator_PreviousRoundNotOver')
        self.data.rounds[self.data.reportingRoundId + 1] = sp.record(answer = 0, answeredInRound = 0, roundId = self.data.reportingRoundId + 1, startedAt = sp.now, updatedAt = sp.now)
        y7 = sp.local("y7", self.getActiveOracles(sp.record(in_param = self.data.reportingRoundId + 1, in_storage = self.data)))
        self.data = y7.value.storage
        sp.for op in y7.value.operations:
          sp.operations().push(op)
        self.data.roundsDetails[self.data.reportingRoundId + 1] = sp.record(activeOracles = y7.value.result, maxSubmissions = self.data.maxSubmissions, minSubmissions = self.data.minSubmissions, submissions = {sp.sender : match_pair_579_snd}, timeout = self.data.timeout)
        self.data.oraclesDetails[sp.sender].lastStartedRound = self.data.reportingRoundId + 1
        self.data.reportingRoundId += 1
    self.data.recordedFunds.available = sp.as_nat(self.data.recordedFunds.available - self.data.oraclePayment, message = 'Aggregator_OraclePaymentUnderflow')
    self.data.recordedFunds.allocated += self.data.oraclePayment
    self.data.recordedFunds = self.data.recordedFunds
    self.data.oraclesDetails[sp.sender].withdrawable += self.data.oraclePayment

  @sp.entry_point
  def updateAvailableFunds(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))))
    sp.verify(sp.sender == self.data.linkTokenAddress, message = 'Aggregator_NotLinkToken')
    balance = sp.local("balance", 0)
    sp.for resp in params:
      balance.value += resp.balance
    sp.if balance.value != self.data.recordedFunds.available:
      self.data.recordedFunds.available = balance.value

  @sp.entry_point
  def withdrawPayment(self, params):
    sp.verify(self.data.oraclesDetails[params.oracleAddress].adminAddress == sp.sender, message = 'Aggregator_NotOracleAdmin')
    sp.verify(self.data.oraclesDetails[params.oracleAddress].withdrawable >= params.amount, message = 'Aggregator_InsufficientWithdrawableFunds')
    self.data.oraclesDetails[params.oracleAddress].withdrawable = sp.as_nat(self.data.oraclesDetails[params.oracleAddress].withdrawable - params.amount)
    self.data.recordedFunds.allocated = sp.as_nat(self.data.recordedFunds.allocated - params.amount)
    sp.transfer(sp.list([sp.record(from_ = params.oracleAddresse, txs = sp.list([sp.record(to_ = params.recipientAddress, token_id = 0, amount = params.amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.linkTokenAddress, entry_point='transfer').open_some(message = 'Aggregator_InvalidTokenkInterface'))
    sp.transfer(sp.record(requests = sp.list([sp.record(owner = sp.self_address, token_id = 0)]), callback = sp.self_entry_point('updateAvailableFunds')), sp.tez(0), sp.contract(sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")), self.data.linkTokenAddress, entry_point='balance_of').open_some(message = 'Aggregator_InvalidTokenkInterface'))