import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(admin = sp.address('tz1axGtTkg1hJvGenTrhqpFbW1S8GcQpPdve'), latestRoundData = sp.none, proxy = sp.address('KT1PG6uK91ymZYVtjnRXv2mEdFYSH6P6uJhC'))

  @sp.entry_point
  def getLatestRoundData(self, params):
    sp.transfer(sp.self_entry_point_address('setLatestRoundData'), sp.tez(0), sp.contract(sp.TAddress, self.data.proxy, entry_point='latestRoundData').open_some(message = 'Wrong Interface: Could not resolve proxy latestRoundData entry-point.'))

  @sp.entry_point
  def setLatestRoundData(self, params):
    sp.set_type(params, sp.TRecord(answer = sp.TNat, answeredInRound = sp.TNat, roundId = sp.TNat, startedAt = sp.TTimestamp, updatedAt = sp.TTimestamp).layout((("answer", "answeredInRound"), ("roundId", ("startedAt", "updatedAt")))))
    sp.verify(sp.sender == self.data.proxy)
    self.data.latestRoundData = sp.some(params)

  @sp.entry_point
  def setup(self, params):
    sp.verify(sp.sender == self.data.admin)
    self.data.admin = params.admin
    self.data.proxy = params.proxy