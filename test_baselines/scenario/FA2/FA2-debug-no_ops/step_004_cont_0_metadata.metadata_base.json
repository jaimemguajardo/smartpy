{
  "version": "FA2-debug-no_ops",
  "description": "This is a didactic reference implementation of FA2, a.k.a. TZIP-012, using SmartPy.\n\nThis particular contract uses the configuration named: FA2-debug-no_ops.",
  "interfaces": [ "TZIP-012-2020-12-24" ],
  "authors": [ "Seb Mondet <https://seb.mondet.org>" ],
  "homepage": "https://gitlab.com/smondet/fa2-smartpy",
  "views": [
    {
      "name": "get_balance",
      "pure": true,
      "description": "This is the `get_balance` view defined in TZIP-12.",
      "implementations": [
        {
          "michelsonStorageView": {
            "parameter": { "prim": "pair", "args": [ { "prim": "address", "annots": [ "%owner" ] }, { "prim": "nat", "annots": [ "%token_id" ] } ] },
            "returnType": { "prim": "nat" },
            "code": [
              { "prim": "UNPAIR" },
              { "prim": "SWAP" },
              { "prim": "DUP" },
              { "prim": "DUG", "args": [ { "int": "2" } ] },
              { "prim": "GET", "args": [ { "int": "6" } ] },
              { "prim": "SWAP" },
              { "prim": "DUP" },
              { "prim": "DUG", "args": [ { "int": "2" } ] },
              { "prim": "CDR" },
              { "prim": "MEM" },
              { "prim": "IF", "args": [ [], [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "FA2_TOKEN_UNDEFINED" } ] }, { "prim": "FAILWITH" } ] ] },
              { "prim": "SWAP" },
              { "prim": "CAR" },
              { "prim": "CDR" },
              { "prim": "CDR" },
              { "prim": "SWAP" },
              { "prim": "GET" },
              { "prim": "IF_NONE", "args": [ [ { "prim": "PUSH", "args": [ { "prim": "int" }, { "int": "467" } ] }, { "prim": "FAILWITH" } ], [] ] }
            ]
          }
        }
      ]
    },
    {
      "name": "token_metadata",
      "pure": true,
      "description":
        "\n        Return the token-metadata URI for the given token.\n\n        For a reference implementation, dynamic-views seem to be the\n        most flexible choice.\n        ",
      "implementations": [
        {
          "michelsonStorageView": {
            "parameter": { "prim": "nat" },
            "returnType": { "prim": "pair", "args": [ { "prim": "nat" }, { "prim": "map", "args": [ { "prim": "string" }, { "prim": "bytes" } ] } ] },
            "code": [
              { "prim": "UNPAIR" },
              { "prim": "SWAP" },
              { "prim": "GET", "args": [ { "int": "6" } ] },
              { "prim": "SWAP" },
              { "prim": "DUP" },
              { "prim": "DUG", "args": [ { "int": "2" } ] },
              { "prim": "GET" },
              { "prim": "IF_NONE", "args": [ [ { "prim": "PUSH", "args": [ { "prim": "int" }, { "int": "565" } ] }, { "prim": "FAILWITH" } ], [] ] },
              { "prim": "CAR" },
              { "prim": "PUSH", "args": [ { "prim": "nat" }, { "int": "0" } ] },
              { "prim": "DIG", "args": [ { "int": "2" } ] },
              { "prim": "ADD" },
              { "prim": "PAIR" }
            ]
          }
        }
      ]
    },
    {
      "name": "does_token_exist",
      "pure": true,
      "description": "Ask whether a token ID is exists.",
      "implementations": [
        {
          "michelsonStorageView": {
            "parameter": { "prim": "nat" },
            "returnType": { "prim": "bool" },
            "code": [ { "prim": "UNPAIR" }, { "prim": "SWAP" }, { "prim": "GET", "args": [ { "int": "6" } ] }, { "prim": "SWAP" }, { "prim": "MEM" } ]
          }
        }
      ]
    },
    {
      "name": "count_tokens",
      "pure": true,
      "description": "Get how many tokens are in this FA2 contract.\n        ",
      "implementations": [ { "michelsonStorageView": { "returnType": { "prim": "nat" }, "code": [ { "prim": "CAR" }, { "prim": "CDR" }, { "prim": "CAR" } ] } } ]
    },
    {
      "name": "all_tokens",
      "pure": true,
      "description":
        "\n            This view is specified (but optional) in the standard.\n\n            This contract is built with assume_consecutive_token_ids =\n            True, so we return a list constructed from the number of tokens.\n            ",
      "implementations": [
        {
          "michelsonStorageView": {
            "returnType": { "prim": "list", "args": [ { "prim": "nat" } ] },
            "code": [
              { "prim": "DUP" },
              { "prim": "CAR" },
              { "prim": "CDR" },
              { "prim": "CAR" },
              { "prim": "NIL", "args": [ { "prim": "nat" } ] },
              { "prim": "SWAP" },
              { "prim": "PUSH", "args": [ { "prim": "nat" }, { "int": "0" } ] },
              { "prim": "DUP" },
              { "prim": "DUP", "args": [ { "int": "3" } ] },
              { "prim": "COMPARE" },
              { "prim": "GT" },
              {
                "prim": "LOOP",
                "args": [
                  [
                    { "prim": "DUP" },
                    { "prim": "DIG", "args": [ { "int": "3" } ] },
                    { "prim": "SWAP" },
                    { "prim": "CONS" },
                    { "prim": "DUG", "args": [ { "int": "2" } ] },
                    { "prim": "PUSH", "args": [ { "prim": "nat" }, { "int": "1" } ] },
                    { "prim": "ADD" },
                    { "prim": "DUP" },
                    { "prim": "DUP", "args": [ { "int": "3" } ] },
                    { "prim": "COMPARE" },
                    { "prim": "GT" }
                  ]
                ]
              },
              { "prim": "DROP", "args": [ { "int": "2" } ] },
              { "prim": "SWAP" },
              { "prim": "DROP" },
              { "prim": "NIL", "args": [ { "prim": "nat" } ] },
              { "prim": "SWAP" },
              { "prim": "ITER", "args": [ [ { "prim": "CONS" } ] ] }
            ]
          }
        }
      ]
    },
    {
      "name": "is_operator",
      "pure": true,
      "implementations": [
        {
          "michelsonStorageView": {
            "parameter": {
              "prim": "pair",
              "args": [
                { "prim": "address", "annots": [ "%owner" ] },
                { "prim": "pair", "args": [ { "prim": "address", "annots": [ "%operator" ] }, { "prim": "nat", "annots": [ "%token_id" ] } ] }
              ]
            },
            "returnType": { "prim": "bool" },
            "code": [
              { "prim": "UNPAIR" },
              { "prim": "SWAP" },
              { "prim": "CDR" },
              { "prim": "CAR" },
              { "prim": "CDR" },
              { "prim": "SWAP" },
              { "prim": "DUP" },
              { "prim": "GET", "args": [ { "int": "4" } ] },
              { "prim": "SWAP" },
              { "prim": "DUP" },
              { "prim": "DUG", "args": [ { "int": "3" } ] },
              { "prim": "GET", "args": [ { "int": "3" } ] },
              { "prim": "PAIR", "annots": [ "%operator", "%token_id" ] },
              { "prim": "DIG", "args": [ { "int": "2" } ] },
              { "prim": "CAR" },
              { "prim": "PAIR", "annots": [ "%owner" ] },
              { "prim": "MEM" }
            ]
          }
        }
      ]
    },
    {
      "name": "total_supply",
      "pure": true,
      "implementations": [
        {
          "michelsonStorageView": {
            "parameter": { "prim": "nat" },
            "returnType": { "prim": "nat" },
            "code": [
              { "prim": "UNPAIR" },
              { "prim": "SWAP" },
              { "prim": "GET", "args": [ { "int": "6" } ] },
              { "prim": "SWAP" },
              { "prim": "GET" },
              { "prim": "IF_NONE", "args": [ [ { "prim": "PUSH", "args": [ { "prim": "int" }, { "int": "602" } ] }, { "prim": "FAILWITH" } ], [] ] },
              { "prim": "CDR" }
            ]
          }
        }
      ]
    }
  ],
  "source": { "tools": [ "SmartPy" ], "location": "https://gitlab.com/smondet/fa2-smartpy.git" },
  "permissions": { "operator": "owner-transfer", "receiver": "owner-no-hook", "sender": "owner-no-hook" },
  "fa2-smartpy": {
    "configuration": {
      "add_mutez_transfer": false,
      "allow_self_transfer": false,
      "assume_consecutive_token_ids": true,
      "force_layouts": true,
      "lazy_entry_points": false,
      "lazy_entry_points_multiple": false,
      "name": "FA2-debug-no_ops",
      "non_fungible": false,
      "readable": true,
      "single_asset": false,
      "store_total_supply": true,
      "support_operator": false
    }
  }
}