import smartpy as sp

tstorage = sp.TRecord(l = sp.TLambda(sp.TNat, sp.TInt), s = sp.TString, value = sp.TNat).layout(("l", ("s", "value")))
tparameter = sp.TVariant(add = sp.TUnit, concat1 = sp.TUnit, concat2 = sp.TUnit, seq = sp.TUnit).layout((("add", "concat1"), ("concat2", "seq")))
tglobals = { }
tviews = { }
