import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(x = 11)

  @sp.entry_point
  def entry_point_1(self, params):
    self.data.x += 12
    self.data.x += 15

  @sp.entry_point
  def f(self, params):
    self.data.x += params

  @sp.entry_point
  def g(self, params):
    self.data.x += params