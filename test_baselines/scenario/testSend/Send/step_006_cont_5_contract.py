import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init()

  @sp.entry_point
  def ep(self, params):
    sp.transfer(-42, sp.tez(0), sp.contract(sp.TInt, sp.address('tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr'), entry_point='myEntryPoint').open_some())