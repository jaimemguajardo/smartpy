import smartpy as sp

tstorage = sp.TRecord(ledger = sp.TSaplingState(8)).layout("ledger")
tparameter = sp.TVariant(handle = sp.TList(sp.TRecord(key = sp.TOption(sp.TKeyHash), transaction = sp.TSaplingTransaction(8)).layout(("key", "transaction")))).layout("handle")
tglobals = { }
tviews = { }
