import smartpy as sp

tstorage = sp.TRecord(x = sp.TOption(sp.TIntOrNat)).layout("x")
tparameter = sp.TVariant(check = sp.TRecord(params = sp.TIntOrNat, result = sp.TIntOrNat).layout(("params", "result")), test = sp.TIntOrNat).layout(("check", "test"))
tglobals = { }
tviews = { }
