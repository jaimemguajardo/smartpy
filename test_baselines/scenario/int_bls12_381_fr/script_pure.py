import smartpy as sp

class Int__Bls12_381_fr(sp.Contract):
    def __init__(self):
        self.init(fr = sp.none)

    """
    INT: Convert a field element to type int. The returned value is always between 0 (inclusive) and the order of Fr (exclusive).

    :: bls12_381_fr : 'S -> int : 'S
    """
    @sp.entry_point
    def toInt(self, fr):
        self.data.fr = sp.some(sp.to_int(fr))

@sp.add_test(name = "Int__BLS12_381_fr")
def test():
    c1 = Int__Bls12_381_fr();

    scenario = sp.test_scenario()
    scenario += c1

    scenario += c1.toInt(sp.bls12_381_fr("0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221"));
