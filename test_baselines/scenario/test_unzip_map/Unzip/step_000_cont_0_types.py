import smartpy as sp

tstorage = sp.TRecord(m = sp.TMap(sp.TString, sp.TRecord(a = sp.TIntOrNat, b = sp.TIntOrNat).layout(("a", "b"))), out = sp.TString).layout(("m", "out"))
tparameter = sp.TVariant(ep = sp.TUnit).layout("ep")
tglobals = { }
tviews = { }
