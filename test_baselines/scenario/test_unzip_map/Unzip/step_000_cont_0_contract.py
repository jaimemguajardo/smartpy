import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(m = {'abc' : sp.record(a = 10, b = 20)}, out = 'z')

  @sp.entry_point
  def ep(self, params):
    k = sp.local("k", 'abc')
    with sp.match_record(self.data.m[k.value], "data") as data:
      k.value = 'xyz' + k.value
    self.data.out = k.value