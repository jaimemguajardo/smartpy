import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(x = 0, y = 0)

  @sp.entry_point
  def change(self, params):
    self.data.x = params.x
    self.data.y = params.y

  @sp.entry_point
  def go_x(self, params):
    self.data.x += 1
    self.data.y -= 1

  @sp.entry_point
  def go_y(self, params):
    self.data.x -= 1
    self.data.y += 1