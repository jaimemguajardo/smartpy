Comment...
 h1: Target Administration tests
Table Of Contents

 Target Administration tests
# Init contracts
## Administrated
## multisignAdmin
# Set multisignAdmin as administrated's admin
# Use multisignadmin to administrate target
## Signer 1 new proposal: setActive = True on target
Comment...
 h2: Init contracts
Comment...
 h3: Administrated
Creating contract
 -> (Pair False (Pair "tz0Fakeadmin" None))
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_004_cont_0_storage.tz 1
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_004_cont_0_storage.json 1
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_004_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_004_cont_0_storage.py 1
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_004_cont_0_types.py 7
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_004_cont_0_contract.tz 67
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_004_cont_0_contract.json 87
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_004_cont_0_contract.py 21
Comment...
 h3: multisignAdmin
Creating contract
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1}) (Pair "1970-01-01T00:00:00Z" (Pair 1 2))) (Pair (Pair {} 1) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 0 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 0 "edpkFakesigner2"))}))))
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_006_cont_1_storage.tz 1
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_006_cont_1_storage.json 52
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_006_cont_1_sizes.csv 2
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_006_cont_1_storage.py 1
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_006_cont_1_types.py 7
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_006_cont_1_contract.tz 1718
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_006_cont_1_contract.json 2500
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_006_cont_1_contract.py 61
Comment...
 h2: Set multisignAdmin as administrated's admin
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_008_cont_0_params.py 1
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_008_cont_0_params.tz 1
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_008_cont_0_params.json 1
Executing administrate(sp.list([variant('setAdmin', sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF'))]))...
 -> (Pair False (Pair "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF" None))
Comment...
 h2: Use multisignadmin to administrate target
Comment...
 h3: Signer 1 new proposal: setActive = True on target
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_011_cont_1_params.py 1
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_011_cont_1_params.tz 1
 => test_baselines/scenario_michel/multisign_admin/Target_Administration_tests/step_011_cont_1_params.json 1
Executing newProposal(sp.list([variant('targetAdmin', sp.list([variant('setActive', True)]))]))...
 -> (Pair (Pair (Pair {Elt "tz0Fakesigner1" 0; Elt "tz0Fakesigner2" 1} {Elt "edpkFakesigner1" 0; Elt "edpkFakesigner2" 1}) (Pair "1970-01-01T00:00:00Z" (Pair 1 2))) (Pair (Pair {Elt 0 (Pair (Pair {Right {Left True}} (Pair False 1)) (Pair {} (Pair "1970-01-01T00:00:00Z" {0})))} 1) (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate" (Pair 5 {Elt 0 (Pair "tz0Fakesigner1" (Pair 1 "edpkFakesigner1")); Elt 1 (Pair "tz0Fakesigner2" (Pair 0 "edpkFakesigner2"))}))))
Executing administrate(sp.list([variant('setActive', True)]))...
 -> (Pair True (Pair "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF" None))
Verifying sp.contract_data(0).active...
 OK
