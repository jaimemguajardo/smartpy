import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(a = 0, b = 1, c = True, d = 'abc')

  @sp.entry_point
  def ep1(self, params):
    with sp.match_record(self.data, "modify_record_93") as modify_record_93:
      sp.verify((abs(modify_record_93.b)) == (modify_record_93.a + 1))
      modify_record_93.d = 'xyz'

  @sp.entry_point
  def ep2(self, params):
    with sp.match_record(self.data, "modify_record_99") as modify_record_99:
      modify_record_99.d = 'abc'