import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(x = 0)

  @sp.entry_point
  def entry_point_1(self, params):
    x = sp.bind_block("x")
    with x:
      with params.match_cases() as arg:
        with arg.match('A') as arg:
          sp.result(arg + 12)
        with arg.match('B') as arg:
          sp.never(arg)

    self.data.x = x.value