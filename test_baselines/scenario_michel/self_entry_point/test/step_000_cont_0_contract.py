import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init()

  @sp.entry_point
  def ep1(self, params):
    sp.set_type(params, sp.TUnit)

  @sp.entry_point
  def ep2(self, params):
    sp.set_type(params, sp.TContract(sp.TTimestamp))