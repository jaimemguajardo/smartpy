import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(administrator = sp.address('tz0FakeAdministrator'), all_tokens = 0, ledger = {}, locked = {}, metadata = {'' : sp.bytes('0x')}, operators = {}, paused = False, tokens = {})

  @sp.entry_point
  def balance_of(self, params):
    sp.verify(~ self.data.paused)
    sp.set_type(params, sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")))
    def f0(lparams_0):
      sp.verify(self.data.tokens.contains(lparams_0.token_id), message = 'FA2_TOKEN_UNDEFINED')
      sp.if self.data.ledger.contains(sp.set_type_expr(lparams_0.owner, sp.TAddress)):
        sp.result(sp.record(request = sp.record(owner = sp.set_type_expr(lparams_0.owner, sp.TAddress), token_id = sp.set_type_expr(lparams_0.token_id, sp.TNat)), balance = self.data.ledger[sp.set_type_expr(lparams_0.owner, sp.TAddress)].balance))
      sp.else:
        sp.result(sp.record(request = sp.record(owner = sp.set_type_expr(lparams_0.owner, sp.TAddress), token_id = sp.set_type_expr(lparams_0.token_id, sp.TNat)), balance = 0))
    responses = sp.local("responses", params.requests.map(sp.build_lambda(f0)))
    sp.transfer(responses.value, sp.tez(0), sp.set_type_expr(params.callback, sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))))))

  @sp.entry_point
  def cancel_request(self, params):
    compute_304 = sp.local("compute_304", sp.record(client = sp.sender, client_request_id = params.client_request_id))
    sp.verify(self.data.locked.contains(compute_304.value), message = 'EscrowRequestIdUnknownForClient')
    sp.verify(sp.now >= self.data.locked[compute_304.value].cancel_timeout, message = 'EscrowCantCancelBeforeTimeout')
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = sp.sender, token_id = 0, amount = self.data.locked[compute_304.value].amount)]))]), sp.tez(0), sp.self_entry_point('transfer'))
    del self.data.locked[compute_304.value]
    sp.if ~ params.force:
      sp.transfer(compute_304.value, sp.tez(0), sp.contract(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat).layout(("client", "client_request_id")), params.oracle, entry_point='cancel_request').open_some(message = 'EscrowOracleNotFound'))

  @sp.entry_point
  def force_fulfill_request(self, params):
    sp.set_type(params, sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(((("cancel_timeout", "client_request_id"), ("fulfill_timeout", "job_id")), (("oracle", "parameters"), ("tag", "target"))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout((("client", "client_request_id"), ("result", "tag"))))).layout(("request", "result")))
    request_324, result_324 = sp.match_record(params, "request", "result")
    ticket_325_data, ticket_325_copy = sp.match_tuple(sp.read_ticket_raw(request_324), "ticket_325_data", "ticket_325_copy")
    ticket_325_ticketer, ticket_325_content, ticket_325_amount = sp.match_tuple(ticket_325_data, "ticket_325_ticketer", "ticket_325_content", "ticket_325_amount")
    ticket_326_data, ticket_326_copy = sp.match_tuple(sp.read_ticket_raw(result_324), "ticket_326_data", "ticket_326_copy")
    ticket_326_ticketer, ticket_326_content, ticket_326_amount = sp.match_tuple(ticket_326_data, "ticket_326_ticketer", "ticket_326_content", "ticket_326_amount")
    compute_330 = sp.local("compute_330", sp.record(client = ticket_325_ticketer, client_request_id = ticket_325_content.client_request_id))
    sp.verify(self.data.locked.contains(compute_330.value), message = 'EscrowRequestUnknown')
    sp.verify(ticket_325_content.fulfill_timeout >= sp.now, message = 'EscrowCantFulfillAfterTimeout')
    sp.verify(ticket_325_content.tag == 'OracleRequest', message = 'TicketExpectedTag:OracleRequest')
    sp.verify(ticket_326_content.tag == 'OracleResult', message = 'TicketExpectedTag:OracleResult')
    sp.verify(ticket_325_ticketer == ticket_326_content.client, message = 'TicketClientNotMatch')
    sp.verify(ticket_326_ticketer == ticket_325_content.oracle, message = 'TicketOracleNotMatch')
    sp.verify(ticket_325_content.client_request_id == ticket_326_content.client_request_id, message = 'TicketClientRequestIdNotMatch')
    sp.verify(sp.sender == ticket_326_ticketer, message = 'EscrowSenderAndTicketerNotMatch')
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = ticket_325_content.oracle, token_id = 0, amount = self.data.locked[compute_330.value].amount)]))]), sp.tez(0), sp.self_entry_point('transfer'))
    del self.data.locked[compute_330.value]

  @sp.entry_point
  def fulfill_request(self, params):
    sp.set_type(params, sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(((("cancel_timeout", "client_request_id"), ("fulfill_timeout", "job_id")), (("oracle", "parameters"), ("tag", "target"))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout((("client", "client_request_id"), ("result", "tag"))))).layout(("request", "result")))
    request_324, result_324 = sp.match_record(params, "request", "result")
    ticket_325_data, ticket_325_copy = sp.match_tuple(sp.read_ticket_raw(request_324), "ticket_325_data", "ticket_325_copy")
    ticket_325_ticketer, ticket_325_content, ticket_325_amount = sp.match_tuple(ticket_325_data, "ticket_325_ticketer", "ticket_325_content", "ticket_325_amount")
    ticket_326_data, ticket_326_copy = sp.match_tuple(sp.read_ticket_raw(result_324), "ticket_326_data", "ticket_326_copy")
    ticket_326_ticketer, ticket_326_content, ticket_326_amount = sp.match_tuple(ticket_326_data, "ticket_326_ticketer", "ticket_326_content", "ticket_326_amount")
    compute_330 = sp.local("compute_330", sp.record(client = ticket_325_ticketer, client_request_id = ticket_325_content.client_request_id))
    sp.verify(self.data.locked.contains(compute_330.value), message = 'EscrowRequestUnknown')
    sp.verify(ticket_325_content.fulfill_timeout >= sp.now, message = 'EscrowCantFulfillAfterTimeout')
    sp.verify(ticket_325_content.tag == 'OracleRequest', message = 'TicketExpectedTag:OracleRequest')
    sp.verify(ticket_326_content.tag == 'OracleResult', message = 'TicketExpectedTag:OracleResult')
    sp.verify(ticket_325_ticketer == ticket_326_content.client, message = 'TicketClientNotMatch')
    sp.verify(ticket_326_ticketer == ticket_325_content.oracle, message = 'TicketOracleNotMatch')
    sp.verify(ticket_325_content.client_request_id == ticket_326_content.client_request_id, message = 'TicketClientRequestIdNotMatch')
    sp.verify(sp.sender == ticket_326_ticketer, message = 'EscrowSenderAndTicketerNotMatch')
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = ticket_325_content.oracle, token_id = 0, amount = self.data.locked[compute_330.value].amount)]))]), sp.tez(0), sp.self_entry_point('transfer'))
    del self.data.locked[compute_330.value]
    sp.transfer(sp.record(request = ticket_325_copy, result = ticket_326_copy), sp.tez(0), sp.contract(sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(((("cancel_timeout", "client_request_id"), ("fulfill_timeout", "job_id")), (("oracle", "parameters"), ("tag", "target"))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout((("client", "client_request_id"), ("result", "tag"))))).layout(("request", "result")), ticket_325_content.target).open_some(message = 'EscrowTargetNotFound'))

  @sp.entry_point
  def mint(self, params):
    sp.verify(sp.sender == self.data.administrator)
    sp.verify(params.token_id == 0, message = 'single-asset: token-id <> 0')
    sp.verify(self.data.all_tokens == params.token_id, message = 'Token-IDs should be consecutive')
    self.data.all_tokens = sp.max(self.data.all_tokens, params.token_id + 1)
    sp.if self.data.ledger.contains(sp.set_type_expr(params.address, sp.TAddress)):
      self.data.ledger[sp.set_type_expr(params.address, sp.TAddress)].balance += params.amount
    sp.else:
      self.data.ledger[sp.set_type_expr(params.address, sp.TAddress)] = sp.record(balance = params.amount)
    sp.if self.data.tokens.contains(params.token_id):
      pass
    sp.else:
      self.data.tokens[params.token_id] = sp.record(metadata_map = params.metadata, total_supply = params.amount)

  @sp.entry_point
  def send_request(self, params):
    sp.set_type(params, sp.TRecord(amount = sp.TNat, request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(((("cancel_timeout", "client_request_id"), ("fulfill_timeout", "job_id")), (("oracle", "parameters"), ("tag", "target")))))).layout(("amount", "request")))
    amount_273, request_273 = sp.match_record(params, "amount", "request")
    ticket_276_data, ticket_276_copy = sp.match_tuple(sp.read_ticket_raw(request_273), "ticket_276_data", "ticket_276_copy")
    ticket_276_ticketer, ticket_276_content, ticket_276_amount = sp.match_tuple(ticket_276_data, "ticket_276_ticketer", "ticket_276_content", "ticket_276_amount")
    sp.verify(sp.sender == ticket_276_ticketer, message = 'EscrowSenderAndTicketerNotMatch')
    sp.transfer(sp.list([sp.record(from_ = sp.sender, txs = sp.list([sp.record(to_ = sp.self_address, token_id = 0, amount = amount_273)]))]), sp.tez(0), sp.self_entry_point('transfer'))
    sp.verify(ticket_276_content.tag == 'OracleRequest', message = 'TicketExpectedTag:OracleRequest')
    compute_292 = sp.local("compute_292", sp.record(client = ticket_276_ticketer, client_request_id = ticket_276_content.client_request_id))
    sp.verify(~ (self.data.locked.contains(compute_292.value)), message = 'EscrowRequestIdAlreadyKnownForClient')
    self.data.locked[compute_292.value] = sp.record(amount = amount_273, cancel_timeout = ticket_276_content.cancel_timeout)
    sp.transfer(sp.record(amount = amount_273, request = ticket_276_copy), sp.tez(0), sp.contract(sp.TRecord(amount = sp.TNat, request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(((("cancel_timeout", "client_request_id"), ("fulfill_timeout", "job_id")), (("oracle", "parameters"), ("tag", "target")))))).layout(("amount", "request")), ticket_276_content.oracle, entry_point='create_request').open_some(message = 'EscrowOracleNotFound'))

  @sp.entry_point
  def set_administrator(self, params):
    sp.verify(sp.sender == self.data.administrator)
    self.data.administrator = params

  @sp.entry_point
  def set_metdata(self, params):
    sp.verify(sp.sender == self.data.administrator)
    self.data.metadata[params.k] = params.v

  @sp.entry_point
  def set_pause(self, params):
    sp.verify(sp.sender == self.data.administrator)
    self.data.paused = params

  @sp.entry_point
  def transfer(self, params):
    sp.verify(~ self.data.paused)
    sp.set_type(params, sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))))
    sp.for transfer in params:
      sp.for tx in transfer.txs:
        sp.verify(tx.token_id == 0, message = 'single-asset: token-id <> 0')
        sp.verify((((sp.sender == self.data.administrator) | (transfer.from_ == sp.sender)) | (self.data.operators.contains(sp.set_type_expr(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id), sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))))))) | (sp.sender == sp.self_address), message = 'FA2_NOT_OPERATOR')
        sp.verify(self.data.tokens.contains(tx.token_id), message = 'FA2_TOKEN_UNDEFINED')
        sp.if tx.amount > 0:
          sp.verify(self.data.ledger[sp.set_type_expr(transfer.from_, sp.TAddress)].balance >= tx.amount, message = 'FA2_INSUFFICIENT_BALANCE')
          self.data.ledger[sp.set_type_expr(transfer.from_, sp.TAddress)].balance = sp.as_nat(self.data.ledger[sp.set_type_expr(transfer.from_, sp.TAddress)].balance - tx.amount)
          sp.if self.data.ledger.contains(sp.set_type_expr(tx.to_, sp.TAddress)):
            self.data.ledger[sp.set_type_expr(tx.to_, sp.TAddress)].balance += tx.amount
          sp.else:
            self.data.ledger[sp.set_type_expr(tx.to_, sp.TAddress)] = sp.record(balance = tx.amount)

  @sp.entry_point
  def update_operators(self, params):
    sp.set_type(params, sp.TList(sp.TVariant(add_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), remove_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))).layout(("add_operator", "remove_operator"))))
    sp.for update in params:
      with update.match_cases() as arg:
        with arg.match('add_operator') as arg:
          sp.verify((arg.owner == sp.sender) | (sp.sender == self.data.administrator))
          self.data.operators[sp.set_type_expr(sp.record(owner = arg.owner, operator = arg.operator, token_id = arg.token_id), sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))))] = sp.unit
        with arg.match('remove_operator') as arg:
          sp.verify((arg.owner == sp.sender) | (sp.sender == self.data.administrator))
          del self.data.operators[sp.set_type_expr(sp.record(owner = arg.owner, operator = arg.operator, token_id = arg.token_id), sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))))]
