import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(locked = {}, token = sp.address("KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1"), token_id = 0)

  @sp.entry_point
  def cancel_request(self, params):
    compute_304 = sp.local("compute_304", sp.record(client = sp.sender, client_request_id = params.client_request_id))
    sp.verify(self.data.locked.contains(compute_304.value), message = 'EscrowRequestIdUnknownForClient')
    sp.verify(sp.now >= self.data.locked[compute_304.value].cancel_timeout, message = 'EscrowCantCancelBeforeTimeout')
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = sp.sender, token_id = self.data.token_id, amount = self.data.locked[compute_304.value].amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.token, entry_point='transfer').open_some())
    del self.data.locked[compute_304.value]
    sp.if ~ params.force:
      sp.transfer(compute_304.value, sp.tez(0), sp.contract(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat).layout(("client", "client_request_id")), params.oracle, entry_point='cancel_request').open_some(message = 'EscrowOracleNotFound'))

  @sp.entry_point
  def force_fulfill_request(self, params):
    sp.set_type(params, sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(((("cancel_timeout", "client_request_id"), ("fulfill_timeout", "job_id")), (("oracle", "parameters"), ("tag", "target"))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout((("client", "client_request_id"), ("result", "tag"))))).layout(("request", "result")))
    request_324, result_324 = sp.match_record(params, "request", "result")
    ticket_325_data, ticket_325_copy = sp.match_tuple(sp.read_ticket_raw(request_324), "ticket_325_data", "ticket_325_copy")
    ticket_325_ticketer, ticket_325_content, ticket_325_amount = sp.match_tuple(ticket_325_data, "ticket_325_ticketer", "ticket_325_content", "ticket_325_amount")
    ticket_326_data, ticket_326_copy = sp.match_tuple(sp.read_ticket_raw(result_324), "ticket_326_data", "ticket_326_copy")
    ticket_326_ticketer, ticket_326_content, ticket_326_amount = sp.match_tuple(ticket_326_data, "ticket_326_ticketer", "ticket_326_content", "ticket_326_amount")
    compute_330 = sp.local("compute_330", sp.record(client = ticket_325_ticketer, client_request_id = ticket_325_content.client_request_id))
    sp.verify(self.data.locked.contains(compute_330.value), message = 'EscrowRequestUnknown')
    sp.verify(ticket_325_content.fulfill_timeout >= sp.now, message = 'EscrowCantFulfillAfterTimeout')
    sp.verify(ticket_325_content.tag == 'OracleRequest', message = 'TicketExpectedTag:OracleRequest')
    sp.verify(ticket_326_content.tag == 'OracleResult', message = 'TicketExpectedTag:OracleResult')
    sp.verify(ticket_325_ticketer == ticket_326_content.client, message = 'TicketClientNotMatch')
    sp.verify(ticket_326_ticketer == ticket_325_content.oracle, message = 'TicketOracleNotMatch')
    sp.verify(ticket_325_content.client_request_id == ticket_326_content.client_request_id, message = 'TicketClientRequestIdNotMatch')
    sp.verify(sp.sender == ticket_326_ticketer, message = 'EscrowSenderAndTicketerNotMatch')
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = ticket_325_content.oracle, token_id = self.data.token_id, amount = self.data.locked[compute_330.value].amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.token, entry_point='transfer').open_some())
    del self.data.locked[compute_330.value]

  @sp.entry_point
  def fulfill_request(self, params):
    sp.set_type(params, sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(((("cancel_timeout", "client_request_id"), ("fulfill_timeout", "job_id")), (("oracle", "parameters"), ("tag", "target"))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout((("client", "client_request_id"), ("result", "tag"))))).layout(("request", "result")))
    request_324, result_324 = sp.match_record(params, "request", "result")
    ticket_325_data, ticket_325_copy = sp.match_tuple(sp.read_ticket_raw(request_324), "ticket_325_data", "ticket_325_copy")
    ticket_325_ticketer, ticket_325_content, ticket_325_amount = sp.match_tuple(ticket_325_data, "ticket_325_ticketer", "ticket_325_content", "ticket_325_amount")
    ticket_326_data, ticket_326_copy = sp.match_tuple(sp.read_ticket_raw(result_324), "ticket_326_data", "ticket_326_copy")
    ticket_326_ticketer, ticket_326_content, ticket_326_amount = sp.match_tuple(ticket_326_data, "ticket_326_ticketer", "ticket_326_content", "ticket_326_amount")
    compute_330 = sp.local("compute_330", sp.record(client = ticket_325_ticketer, client_request_id = ticket_325_content.client_request_id))
    sp.verify(self.data.locked.contains(compute_330.value), message = 'EscrowRequestUnknown')
    sp.verify(ticket_325_content.fulfill_timeout >= sp.now, message = 'EscrowCantFulfillAfterTimeout')
    sp.verify(ticket_325_content.tag == 'OracleRequest', message = 'TicketExpectedTag:OracleRequest')
    sp.verify(ticket_326_content.tag == 'OracleResult', message = 'TicketExpectedTag:OracleResult')
    sp.verify(ticket_325_ticketer == ticket_326_content.client, message = 'TicketClientNotMatch')
    sp.verify(ticket_326_ticketer == ticket_325_content.oracle, message = 'TicketOracleNotMatch')
    sp.verify(ticket_325_content.client_request_id == ticket_326_content.client_request_id, message = 'TicketClientRequestIdNotMatch')
    sp.verify(sp.sender == ticket_326_ticketer, message = 'EscrowSenderAndTicketerNotMatch')
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = ticket_325_content.oracle, token_id = self.data.token_id, amount = self.data.locked[compute_330.value].amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.token, entry_point='transfer').open_some())
    del self.data.locked[compute_330.value]
    sp.transfer(sp.record(request = ticket_325_copy, result = ticket_326_copy), sp.tez(0), sp.contract(sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(((("cancel_timeout", "client_request_id"), ("fulfill_timeout", "job_id")), (("oracle", "parameters"), ("tag", "target"))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout((("client", "client_request_id"), ("result", "tag"))))).layout(("request", "result")), ticket_325_content.target).open_some(message = 'EscrowTargetNotFound'))

  @sp.entry_point
  def send_request(self, params):
    sp.set_type(params, sp.TRecord(amount = sp.TNat, request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(((("cancel_timeout", "client_request_id"), ("fulfill_timeout", "job_id")), (("oracle", "parameters"), ("tag", "target")))))).layout(("amount", "request")))
    amount_273, request_273 = sp.match_record(params, "amount", "request")
    ticket_276_data, ticket_276_copy = sp.match_tuple(sp.read_ticket_raw(request_273), "ticket_276_data", "ticket_276_copy")
    ticket_276_ticketer, ticket_276_content, ticket_276_amount = sp.match_tuple(ticket_276_data, "ticket_276_ticketer", "ticket_276_content", "ticket_276_amount")
    sp.verify(sp.sender == ticket_276_ticketer, message = 'EscrowSenderAndTicketerNotMatch')
    sp.transfer(sp.list([sp.record(from_ = sp.sender, txs = sp.list([sp.record(to_ = sp.self_address, token_id = self.data.token_id, amount = amount_273)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.token, entry_point='transfer').open_some())
    sp.verify(ticket_276_content.tag == 'OracleRequest', message = 'TicketExpectedTag:OracleRequest')
    compute_292 = sp.local("compute_292", sp.record(client = ticket_276_ticketer, client_request_id = ticket_276_content.client_request_id))
    sp.verify(~ (self.data.locked.contains(compute_292.value)), message = 'EscrowRequestIdAlreadyKnownForClient')
    self.data.locked[compute_292.value] = sp.record(amount = amount_273, cancel_timeout = ticket_276_content.cancel_timeout)
    sp.transfer(sp.record(amount = amount_273, request = ticket_276_copy), sp.tez(0), sp.contract(sp.TRecord(amount = sp.TNat, request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(((("cancel_timeout", "client_request_id"), ("fulfill_timeout", "job_id")), (("oracle", "parameters"), ("tag", "target")))))).layout(("amount", "request")), ticket_276_content.oracle, entry_point='create_request').open_some(message = 'EscrowOracleNotFound'))