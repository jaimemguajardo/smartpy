import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(admin = sp.address('tz0FakeReceiver1 admin'), escrow = sp.address("KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H"), last_request_id = 0, requester = sp.address("KT1Tezooo4zzSmartPyzzSTATiCzzzyPVdv3"), value = sp.none)

  @sp.entry_point
  def set_value(self, params):
    sp.verify(sp.sender == self.data.escrow, message = 'ReceiverNotEscrow')
    sp.set_type(params, sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(((("cancel_timeout", "client_request_id"), ("fulfill_timeout", "job_id")), (("oracle", "parameters"), ("tag", "target"))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout((("client", "client_request_id"), ("result", "tag"))))).layout(("request", "result")))
    request_495, result_495 = sp.match_record(params, "request", "result")
    ticket_497_data, ticket_497_copy = sp.match_tuple(sp.read_ticket_raw(request_495), "ticket_497_data", "ticket_497_copy")
    ticket_497_ticketer, ticket_497_content, ticket_497_amount = sp.match_tuple(ticket_497_data, "ticket_497_ticketer", "ticket_497_content", "ticket_497_amount")
    ticket_498_data, ticket_498_copy = sp.match_tuple(sp.read_ticket_raw(result_495), "ticket_498_data", "ticket_498_copy")
    ticket_498_ticketer, ticket_498_content, ticket_498_amount = sp.match_tuple(ticket_498_data, "ticket_498_ticketer", "ticket_498_content", "ticket_498_amount")
    sp.verify(ticket_497_ticketer == self.data.requester, message = 'ReceiverBadRequester')
    sp.verify(ticket_497_content.client_request_id > self.data.last_request_id, message = 'ReceiverBadRequestId')
    self.data.last_request_id = ticket_497_content.client_request_id
    self.data.value = sp.some(ticket_498_content.result.open_variant('int'))

  @sp.entry_point
  def setup(self, params):
    sp.verify(sp.sender == self.data.admin, message = 'ReceiverNotAdmin')
    self.data.requester = params.requester
    self.data.escrow = params.escrow