import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(l = lambda(sp.TLambda(sp.TNat, sp.TInt)), s = '', value = 0)

  @sp.entry_point
  def add(self, params):
    self.data.value = abs(sp.michelson("ADD")(15, 16))

  @sp.entry_point
  def concat1(self, params):
    self.data.s = sp.michelson("CONCAT")(sp.list(['a', 'b', 'c']))

  @sp.entry_point
  def concat2(self, params):
    self.data.s = sp.michelson("CONCAT")('a', 'b')

  @sp.entry_point
  def seq(self, params):
    self.data.value = abs(sp.michelson("DIP {SWAP}; ADD; MUL; DUP; MUL;")(15, 16, 17))