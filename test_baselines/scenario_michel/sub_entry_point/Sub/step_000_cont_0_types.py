import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat, y = sp.TString, z = sp.TIntOrNat).layout(("x", ("y", "z")))
tparameter = sp.TVariant(f = sp.TUnit, g = sp.TUnit).layout(("f", "g"))
tglobals = { "a": sp.TLambda(sp.TRecord(in_param = sp.TIntOrNat, in_storage = sp.TRecord(x = sp.TIntOrNat, y = sp.TString, z = sp.TIntOrNat).layout(("x", ("y", "z")))).layout(("in_param", "in_storage")), sp.TRecord(operations = sp.TList(sp.TOperation), result = sp.TIntOrNat, storage = sp.TRecord(x = sp.TIntOrNat, y = sp.TString, z = sp.TIntOrNat).layout(("x", ("y", "z")))).layout(("operations", ("result", "storage")))) }
tviews = { }
