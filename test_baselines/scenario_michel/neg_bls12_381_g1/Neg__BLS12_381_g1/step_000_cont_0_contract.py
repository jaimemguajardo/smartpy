import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(g1 = sp.none)

  @sp.entry_point
  def negate(self, params):
    sp.set_type(params.g1, sp.TBls12_381_g1)
    self.data.g1 = sp.some(- params.g1)