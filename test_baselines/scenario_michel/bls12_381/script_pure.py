import smartpy as sp


class Bls12_381(sp.Contract):
    def __init__(self, **params):
        self.init(**params)

    """
    ADD: Add two curve points or field elements.

    :: bls12_381_g1 : bls12_381_g1 : 'S -> bls12_381_g1 : 'S
    :: bls12_381_g2 : bls12_381_g2 : 'S -> bls12_381_g2 : 'S
    :: bls12_381_fr : bls12_381_fr : 'S -> bls12_381_fr : 'S
    """
    @sp.entry_point
    def add(self, g1, g2, fr):
        self.data.g1 += g1;
        self.data.g2 += g2;
        self.data.fr += fr;

    """
    NEG: Negate a curve point or field element.

    :: bls12_381_g1 : 'S -> bls12_381_g1 : 'S
    :: bls12_381_g2 : 'S -> bls12_381_g2 : 'S
    :: bls12_381_fr : 'S -> bls12_381_fr : 'S
    """
    @sp.entry_point
    def negate(self):
        self.data.g1 = - self.data.g1

    """
    INT: Convert a field element to type int. The returned value is always between 0 (inclusive) and the order of Fr (exclusive).

    :: bls12_381_fr : 'S -> int : 'S
    """
    @sp.entry_point
    def toInt(self):
        sp.verify(sp.to_int(self.data.fr) ==  30425446867866594294858714277721586621961856045737725883479188213565831504476, message = "Failed to cast field element Fr to Int");

    """
    MUL: Multiply a curve point or field element by a scalar field element. Fr
    elements can be built from naturals by multiplying by the unit of Fr using PUSH bls12_381_fr 1; MUL. Note
    that the multiplication will be computed using the natural modulo the order
    of Fr.

    :: bls12_381_g1 : bls12_381_fr : 'S -> bls12_381_g1 : 'S
    :: bls12_381_g2 : bls12_381_fr : 'S -> bls12_381_g2 : 'S
    :: bls12_381_fr : bls12_381_fr : 'S -> bls12_381_fr : 'S
    :: nat : bls12_381_fr : 'S -> bls12_381_fr : 'S
    :: int : bls12_381_fr : 'S -> bls12_381_fr : 'S
    :: bls12_381_fr : nat : 'S -> bls12_381_fr : 'S
    :: bls12_381_fr : int : 'S -> bls12_381_fr : 'S
    """
    @sp.entry_point
    def mul(self, pair):
        self.data.mulResult = sp.some(sp.fst(pair) * sp.snd(pair));

    """
    PAIRING_CHECK:
    Verify that the product of pairings of the given list of points is equal to 1 in Fq12. Returns true if the list is empty.
    Can be used to verify if two pairings P1 and P2 are equal by verifying P1 * P2^(-1) = 1.

    :: list (pair bls12_381_g1 bls12_381_g2) : 'S -> bool : 'S
    """
    @sp.entry_point
    def pairing_check(self, listOfPairs):
        self.data.checkResult = sp.some(sp.pairing_check(listOfPairs));

@sp.add_test(name = "BLS12-381")
def test():
    c1 = Bls12_381(
        g1 = sp.bls12_381_g1("0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1"),
        g2 = sp.bls12_381_g2("0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801"),
        fr = sp.bls12_381_fr("0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221"),
        mulResult = sp.none,
        checkResult = sp.none
    );

    scenario = sp.test_scenario()
    scenario += c1

    scenario += c1.add(
        g1 = sp.bls12_381_g1("0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1"),
        g2 = sp.bls12_381_g2("0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801"),
        fr = sp.bls12_381_fr("0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221")
    );

    scenario += c1.negate();
    scenario += c1.toInt();

    scenario += c1.mul(
        sp.pair(
            sp.bls12_381_fr("0xd30edc8fce6c34442d371da0e24fc3fb83ea957cfea9766c62a531dda98e4b52"),
            sp.bls12_381_fr("0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221")
        )
    );

    scenario += c1.pairing_check(
        sp.list([
           sp.pair(
                sp.bls12_381_g1("0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1"),
                sp.bls12_381_g2("0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801")
            ),
            sp.pair(
                sp.bls12_381_g1("0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1"),
                sp.bls12_381_g2("0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801")
            )
        ])
    );
