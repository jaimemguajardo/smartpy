#!/usr/bin/env sh
#
# Usage: ./with_sandbox.sh <command>
#
# Runs <command> with SANDBOX_PORT set to the port of a sandbox
# running for the duration of its exeuction.
#
# If SANDBOX_PORT is already set, silently executes <command> without
# doing anything else.
#
# The exit status of <command> is preserved. The sandbox is killed
# even if <command> fails.
#
# If the sandbox fails to initialize or the process is killed, logs
# are left in place.

set -e -o pipefail

if [ ! -z "${SANDBOX_PORT+x}" ]; then
    exec "$@"
fi

# The default lsof on Alpine Linux is unfit for our purposes:
lsof -v 2>&1 | grep version > /dev/null || { echo "lsof is broken."; exit 1; }

function kill_sandbox() {
    pid=$(cat $sandbox_dir/pid)
    if [ ! -z "$pid" ] && kill -s 0 $pid &> /dev/null; then
        echo "[port $port] Killing sandbox..."
        kill $pid
    fi
}

function term() {
    echo "TERM signal received"
    kill_sandbox
    exit 1
}

trap term SIGTERM

for p in $(seq 20000 2 21000)
do
    &> /dev/null lsof -i :$p && continue
    sandbox_dir=_sandbox.$p
    mkdir $sandbox_dir &> /dev/null || continue
    port=$p
    break
done
[ -z $port ] && { echo "Failed to find a free port."; exit 1; }

echo "[port $port] Log directory: $sandbox_dir"
echo "[port $port] Launching sandbox..."
export PATH=_build/tezos-bin/:$PATH # needed by 'tezos-sandbox'
&> $sandbox_dir/tezos-sandbox.log.txt \
tezos-sandbox mini \
   --root $sandbox_dir/root \
   --size 1 \
   --base-port $port \
   --number-of-boot 2 \
   --protocol-hash PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA \
   --no-baking \
   --until-level 2_000_000 \
   --timestamp-delay=-3600 \
&
pid=$!
echo $pid > $sandbox_dir/pid

echo "[port $port] Waiting for sandbox to initialize..."
for i in {1..120}
do
    sleep 1
    if ! kill -s 0 $pid &> /dev/null; then
        echo "[port $port] Sandbox process has died."
        exit 1
    fi
    >> $sandbox_dir/check.txt 2>&1 date
    >> $sandbox_dir/check.txt 2>&1 lsof -i :$port || continue
    >> $sandbox_dir/check.txt 2>&1 tezos-client -P $port bootstrapped || continue
    >> $sandbox_dir/check.txt 2>&1 tezos-client -P $port rpc get /chains/main/blocks/1/protocols || continue
    launched=1
    break
done

if [ -z ${launched+x} ]; then
    echo "[port $port] Sandbox failed to initialize."
    kill_sandbox
    exit 1
fi

echo "[port $port] Sandbox initialized."

set +e
SANDBOX_PORT=$port "$@"
exit_status=$?
set -e

kill_sandbox

sleep 1 # Wait before deleting the directory, otherwise it might be recreated by the sandbox.

rm -rf $sandbox_dir

echo "[port $port] Done."

exit $exit_status
